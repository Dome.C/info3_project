/*
 * Code for class WEL_BIF_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F266_5155(EIF_REFERENCE);
extern EIF_TYPED_VALUE F266_5156(EIF_REFERENCE);
extern EIF_TYPED_VALUE F266_5157(EIF_REFERENCE);
extern EIF_TYPED_VALUE F266_5158(EIF_REFERENCE);
extern EIF_TYPED_VALUE F266_5159(EIF_REFERENCE);
extern EIF_TYPED_VALUE F266_5160(EIF_REFERENCE);
extern EIF_TYPED_VALUE F266_5161(EIF_REFERENCE);
extern EIF_TYPED_VALUE F266_5162(EIF_REFERENCE);
extern EIF_TYPED_VALUE F266_5163(EIF_REFERENCE);
extern EIF_TYPED_VALUE F266_5164(EIF_REFERENCE);
extern EIF_TYPED_VALUE F266_5165(EIF_REFERENCE);
extern EIF_TYPED_VALUE F266_5166(EIF_REFERENCE);
extern void EIF_Minit266(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_BIF_CONSTANTS}.bif_browseforcomputer */
EIF_TYPED_VALUE F266_5155 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4096L);
	return r;
}

/* {WEL_BIF_CONSTANTS}.bif_browseforprinter */
EIF_TYPED_VALUE F266_5156 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8192L);
	return r;
}

/* {WEL_BIF_CONSTANTS}.bif_browseincludefiles */
EIF_TYPED_VALUE F266_5157 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16384L);
	return r;
}

/* {WEL_BIF_CONSTANTS}.bif_dontgobelowdomain */
EIF_TYPED_VALUE F266_5158 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_BIF_CONSTANTS}.bif_editbox */
EIF_TYPED_VALUE F266_5159 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_BIF_CONSTANTS}.bif_returnfsancestors */
EIF_TYPED_VALUE F266_5160 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_BIF_CONSTANTS}.bif_returnonlyfsdirs */
EIF_TYPED_VALUE F266_5161 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_BIF_CONSTANTS}.bif_statustext */
EIF_TYPED_VALUE F266_5162 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_BIF_CONSTANTS}.bif_usenewui */
EIF_TYPED_VALUE F266_5163 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 80L);
	return r;
}

/* {WEL_BIF_CONSTANTS}.bif_validate */
EIF_TYPED_VALUE F266_5164 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_BIF_CONSTANTS}.bif_newdialogstyle */
EIF_TYPED_VALUE F266_5165 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_BIF_CONSTANTS}.bif_nonewfolderbutton */
EIF_TYPED_VALUE F266_5166 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

void EIF_Minit266 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
