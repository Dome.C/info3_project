/*
 * Code for class WEL_DDL_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F27_227(EIF_REFERENCE);
extern EIF_TYPED_VALUE F27_228(EIF_REFERENCE);
extern EIF_TYPED_VALUE F27_229(EIF_REFERENCE);
extern EIF_TYPED_VALUE F27_230(EIF_REFERENCE);
extern EIF_TYPED_VALUE F27_231(EIF_REFERENCE);
extern EIF_TYPED_VALUE F27_232(EIF_REFERENCE);
extern EIF_TYPED_VALUE F27_233(EIF_REFERENCE);
extern EIF_TYPED_VALUE F27_234(EIF_REFERENCE);
extern EIF_TYPED_VALUE F27_235(EIF_REFERENCE);
extern void EIF_Minit27(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_DDL_CONSTANTS}.ddl_readwrite */
EIF_TYPED_VALUE F27_227 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_DDL_CONSTANTS}.ddl_readonly */
EIF_TYPED_VALUE F27_228 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_DDL_CONSTANTS}.ddl_hidden */
EIF_TYPED_VALUE F27_229 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_DDL_CONSTANTS}.ddl_system */
EIF_TYPED_VALUE F27_230 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_DDL_CONSTANTS}.ddl_directory */
EIF_TYPED_VALUE F27_231 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_DDL_CONSTANTS}.ddl_archive */
EIF_TYPED_VALUE F27_232 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_DDL_CONSTANTS}.ddl_postmsgs */
EIF_TYPED_VALUE F27_233 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8192L);
	return r;
}

/* {WEL_DDL_CONSTANTS}.ddl_drives */
EIF_TYPED_VALUE F27_234 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16384L);
	return r;
}

/* {WEL_DDL_CONSTANTS}.ddl_exclusive */
EIF_TYPED_VALUE F27_235 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32768L);
	return r;
}

void EIF_Minit27 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
