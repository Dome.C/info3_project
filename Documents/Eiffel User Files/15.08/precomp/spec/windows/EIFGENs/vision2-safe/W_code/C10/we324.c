/*
 * Code for class WEL_TB_STYLE_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F324_6264(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6265(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6266(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6267(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6268(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6269(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6270(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6271(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6272(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6273(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6274(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6275(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6276(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6277(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6278(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6279(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6280(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6281(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6282(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6283(EIF_REFERENCE);
extern EIF_TYPED_VALUE F324_6284(EIF_REFERENCE);
extern void EIF_Minit324(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_altdrag */
EIF_TYPED_VALUE F324_6264 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1024L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_tooltips */
EIF_TYPED_VALUE F324_6265 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_wrapable */
EIF_TYPED_VALUE F324_6266 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_flat */
EIF_TYPED_VALUE F324_6267 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2048L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_list */
EIF_TYPED_VALUE F324_6268 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4096L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_customerase */
EIF_TYPED_VALUE F324_6269 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8192L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_transparent */
EIF_TYPED_VALUE F324_6270 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32768L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_button */
EIF_TYPED_VALUE F324_6271 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_check */
EIF_TYPED_VALUE F324_6272 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_autosize */
EIF_TYPED_VALUE F324_6273 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_checkgroup */
EIF_TYPED_VALUE F324_6274 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 6L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_group */
EIF_TYPED_VALUE F324_6275 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_sep */
EIF_TYPED_VALUE F324_6276 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_dropdown */
EIF_TYPED_VALUE F324_6277 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.btns_showtext */
EIF_TYPED_VALUE F324_6278 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.btns_dropdown */
EIF_TYPED_VALUE F324_6279 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.btns_autosize */
EIF_TYPED_VALUE F324_6280 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_ex_drawddarrows */
EIF_TYPED_VALUE F324_6281 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_ex_hideclippedbuttons */
EIF_TYPED_VALUE F324_6282 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_ex_doublebuffer */
EIF_TYPED_VALUE F324_6283 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_TB_STYLE_CONSTANTS}.tbstyle_ex_mixedbuttons */
EIF_TYPED_VALUE F324_6284 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

void EIF_Minit324 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
