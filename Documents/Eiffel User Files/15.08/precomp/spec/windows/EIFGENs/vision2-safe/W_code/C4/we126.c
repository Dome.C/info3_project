/*
 * Code for class WEL_CBEN_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F126_3479(EIF_REFERENCE);
extern EIF_TYPED_VALUE F126_3480(EIF_REFERENCE);
extern EIF_TYPED_VALUE F126_3481(EIF_REFERENCE);
extern EIF_TYPED_VALUE F126_3482(EIF_REFERENCE);
extern EIF_TYPED_VALUE F126_3483(EIF_REFERENCE);
extern EIF_TYPED_VALUE F126_3484(EIF_REFERENCE);
extern EIF_TYPED_VALUE F126_3485(EIF_REFERENCE);
extern EIF_TYPED_VALUE F126_3486(EIF_REFERENCE);
extern EIF_TYPED_VALUE F126_3487(EIF_REFERENCE);
extern void EIF_Minit126(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_CBEN_CONSTANTS}.cben_getdispinfo */
EIF_TYPED_VALUE F126_3479 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -807L);
	return r;
}

/* {WEL_CBEN_CONSTANTS}.cben_insertitem */
EIF_TYPED_VALUE F126_3480 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -801L);
	return r;
}

/* {WEL_CBEN_CONSTANTS}.cben_deleteitem */
EIF_TYPED_VALUE F126_3481 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -802L);
	return r;
}

/* {WEL_CBEN_CONSTANTS}.cben_beginedit */
EIF_TYPED_VALUE F126_3482 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -804L);
	return r;
}

/* {WEL_CBEN_CONSTANTS}.cben_endedit */
EIF_TYPED_VALUE F126_3483 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -806L);
	return r;
}

/* {WEL_CBEN_CONSTANTS}.cbenf_dropdown */
EIF_TYPED_VALUE F126_3484 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_CBEN_CONSTANTS}.cbenf_escape */
EIF_TYPED_VALUE F126_3485 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_CBEN_CONSTANTS}.cbenf_killfocus */
EIF_TYPED_VALUE F126_3486 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_CBEN_CONSTANTS}.cbenf_return */
EIF_TYPED_VALUE F126_3487 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

void EIF_Minit126 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
