/*
 * Code for class WEL_CBS_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F46_674(EIF_REFERENCE);
extern EIF_TYPED_VALUE F46_675(EIF_REFERENCE);
extern EIF_TYPED_VALUE F46_676(EIF_REFERENCE);
extern EIF_TYPED_VALUE F46_677(EIF_REFERENCE);
extern EIF_TYPED_VALUE F46_678(EIF_REFERENCE);
extern EIF_TYPED_VALUE F46_679(EIF_REFERENCE);
extern EIF_TYPED_VALUE F46_680(EIF_REFERENCE);
extern EIF_TYPED_VALUE F46_681(EIF_REFERENCE);
extern EIF_TYPED_VALUE F46_682(EIF_REFERENCE);
extern EIF_TYPED_VALUE F46_683(EIF_REFERENCE);
extern EIF_TYPED_VALUE F46_684(EIF_REFERENCE);
extern void EIF_Minit46(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_CBS_CONSTANTS}.cbs_simple */
EIF_TYPED_VALUE F46_674 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_CBS_CONSTANTS}.cbs_dropdown */
EIF_TYPED_VALUE F46_675 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_CBS_CONSTANTS}.cbs_dropdownlist */
EIF_TYPED_VALUE F46_676 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_CBS_CONSTANTS}.cbs_ownerdrawfixed */
EIF_TYPED_VALUE F46_677 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_CBS_CONSTANTS}.cbs_ownerdrawvariable */
EIF_TYPED_VALUE F46_678 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_CBS_CONSTANTS}.cbs_autohscroll */
EIF_TYPED_VALUE F46_679 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_CBS_CONSTANTS}.cbs_oemconvert */
EIF_TYPED_VALUE F46_680 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_CBS_CONSTANTS}.cbs_sort */
EIF_TYPED_VALUE F46_681 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_CBS_CONSTANTS}.cbs_hasstrings */
EIF_TYPED_VALUE F46_682 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

/* {WEL_CBS_CONSTANTS}.cbs_nointegralheight */
EIF_TYPED_VALUE F46_683 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1024L);
	return r;
}

/* {WEL_CBS_CONSTANTS}.cbs_disablenoscroll */
EIF_TYPED_VALUE F46_684 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2048L);
	return r;
}

void EIF_Minit46 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
