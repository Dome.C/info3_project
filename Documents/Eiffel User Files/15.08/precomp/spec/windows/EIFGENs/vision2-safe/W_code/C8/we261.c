/*
 * Code for class WEL_STATUS_WINDOW_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F261_5123(EIF_REFERENCE);
extern EIF_TYPED_VALUE F261_5124(EIF_REFERENCE);
extern EIF_TYPED_VALUE F261_5125(EIF_REFERENCE);
extern EIF_TYPED_VALUE F261_5126(EIF_REFERENCE);
extern EIF_TYPED_VALUE F261_5127(EIF_REFERENCE);
extern EIF_TYPED_VALUE F261_5128(EIF_REFERENCE);
extern EIF_TYPED_VALUE F261_5129(EIF_REFERENCE);
extern EIF_TYPED_VALUE F261_5130(EIF_REFERENCE);
extern EIF_TYPED_VALUE F261_5131(EIF_REFERENCE);
extern void EIF_Minit261(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_STATUS_WINDOW_CONSTANTS}.sb_getrect */
EIF_TYPED_VALUE F261_5123 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "sb_getrect";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 260, Current, 0, 0, 5224);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(260, Current, 5224);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) SB_GETRECT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STATUS_WINDOW_CONSTANTS}.sb_setminheight */
EIF_TYPED_VALUE F261_5124 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "sb_setminheight";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 260, Current, 0, 0, 5225);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(260, Current, 5225);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) SB_SETMINHEIGHT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STATUS_WINDOW_CONSTANTS}.sb_getborders */
EIF_TYPED_VALUE F261_5125 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "sb_getborders";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 260, Current, 0, 0, 5226);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(260, Current, 5226);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) SB_GETBORDERS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STATUS_WINDOW_CONSTANTS}.sb_gettext */
EIF_TYPED_VALUE F261_5126 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "sb_gettext";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 260, Current, 0, 0, 5227);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(260, Current, 5227);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) SB_GETTEXT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STATUS_WINDOW_CONSTANTS}.sb_gettextlength */
EIF_TYPED_VALUE F261_5127 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "sb_gettextlength";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 260, Current, 0, 0, 5228);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(260, Current, 5228);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) SB_GETTEXTLENGTH;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STATUS_WINDOW_CONSTANTS}.sb_settext */
EIF_TYPED_VALUE F261_5128 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "sb_settext";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 260, Current, 0, 0, 5229);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(260, Current, 5229);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) SB_SETTEXT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STATUS_WINDOW_CONSTANTS}.sb_getparts */
EIF_TYPED_VALUE F261_5129 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "sb_getparts";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 260, Current, 0, 0, 5230);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(260, Current, 5230);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) SB_GETPARTS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STATUS_WINDOW_CONSTANTS}.sb_setparts */
EIF_TYPED_VALUE F261_5130 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "sb_setparts";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 260, Current, 0, 0, 5231);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(260, Current, 5231);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) SB_SETPARTS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STATUS_WINDOW_CONSTANTS}.sb_simple */
EIF_TYPED_VALUE F261_5131 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "sb_simple";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 260, Current, 0, 0, 5232);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(260, Current, 5232);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) SB_SIMPLE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit261 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
