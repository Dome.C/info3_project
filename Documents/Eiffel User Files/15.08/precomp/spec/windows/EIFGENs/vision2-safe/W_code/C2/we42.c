/*
 * Code for class WEL_NIF_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F42_594(EIF_REFERENCE);
extern EIF_TYPED_VALUE F42_595(EIF_REFERENCE);
extern EIF_TYPED_VALUE F42_596(EIF_REFERENCE);
extern EIF_TYPED_VALUE F42_597(EIF_REFERENCE);
extern EIF_TYPED_VALUE F42_598(EIF_REFERENCE);
extern EIF_TYPED_VALUE F42_599(EIF_REFERENCE);
extern void EIF_Minit42(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_NIF_CONSTANTS}.nif_icon */
EIF_TYPED_VALUE F42_594 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_NIF_CONSTANTS}.nif_message */
EIF_TYPED_VALUE F42_595 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_NIF_CONSTANTS}.nif_tip */
EIF_TYPED_VALUE F42_596 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_NIF_CONSTANTS}.nif_state */
EIF_TYPED_VALUE F42_597 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_NIF_CONSTANTS}.nif_info */
EIF_TYPED_VALUE F42_598 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_NIF_CONSTANTS}.nif_guid */
EIF_TYPED_VALUE F42_599 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

void EIF_Minit42 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
