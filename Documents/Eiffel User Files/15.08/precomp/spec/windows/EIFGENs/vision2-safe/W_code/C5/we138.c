/*
 * Code for class WEL_DISK_SPACE
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern void F138_3704(EIF_REFERENCE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F138_3705(EIF_REFERENCE);
extern EIF_TYPED_VALUE F138_3706(EIF_REFERENCE);
static EIF_TYPED_VALUE F138_3707_body(EIF_REFERENCE);
extern EIF_TYPED_VALUE F138_3707(EIF_REFERENCE);
static EIF_TYPED_VALUE F138_3708_body(EIF_REFERENCE);
extern EIF_TYPED_VALUE F138_3708(EIF_REFERENCE);
extern EIF_TYPED_VALUE F138_3709(EIF_REFERENCE);
extern EIF_TYPED_VALUE F138_3710(EIF_REFERENCE);
extern EIF_TYPED_VALUE F138_3711(EIF_REFERENCE);
extern EIF_TYPED_VALUE F138_3712(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F138_3713(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F138_3714(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern void EIF_Minit138(void);

#ifdef __cplusplus
}
#endif

#include "wel_disk_space.h"
#include "eif_misc.h"
#include "eif_out.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef INLINE_F138_3714
static int inline_F138_3714 (EIF_CHARACTER_8 arg1, EIF_NATURAL_64* arg2, EIF_NATURAL_64* arg3, EIF_NATURAL_64* arg4)
{
	{
				TCHAR szRootPath[4]; /* Path to root directory of requested drive. */
				ULARGE_INTEGER u, t, f;
				EIF_BOOLEAN Result;

				szRootPath[0] = (TCHAR) arg1;
				szRootPath[1] = ':';
				szRootPath[2] = '\\';
				szRootPath[3] = (char) 0;

				Result = EIF_TEST(GetDiskFreeSpaceEx (szRootPath, &u, &t, &f));
				*(EIF_NATURAL_64 *) arg2 = u.QuadPart;
				*(EIF_NATURAL_64 *) arg3 = t.QuadPart;
				*(EIF_NATURAL_64 *) arg4 = f.QuadPart;

				return Result;
			}
	;
}
#define INLINE_F138_3714
#endif

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_DISK_SPACE}.query_local_drive */
void F138_3704 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x)
{
	GTCX
	char *l_feature_name = "query_local_drive";
	RTEX;
	EIF_NATURAL_64 loc1 = (EIF_NATURAL_64) 0;
	EIF_NATURAL_64 loc2 = (EIF_NATURAL_64) 0;
	EIF_NATURAL_64 loc3 = (EIF_NATURAL_64) 0;
#define arg1 arg1x.it_c1
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE up2x = {{0}, SK_POINTER};
#define up2 up2x.it_p
	EIF_TYPED_VALUE up3x = {{0}, SK_POINTER};
#define up3 up3x.it_p
	EIF_TYPED_VALUE uc1x = {{0}, SK_CHAR8};
#define uc1 uc1x.it_c1
	EIF_BOOLEAN tb1;
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_c1 = * (EIF_CHARACTER_8 *) arg1x.it_r;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_VOID, NULL);
	RTLU(SK_CHAR8,&arg1);
	RTLU (SK_REF, &Current);
	RTLU(SK_UINT64, &loc1);
	RTLU(SK_UINT64, &loc2);
	RTLU(SK_UINT64, &loc3);
	
	RTEAA(l_feature_name, 137, Current, 3, 1, 3740);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(137, Current, 3740);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAA(Current, dtype, 3709, 0x04000000, 1); /* last_query_success */
	uc1 = arg1;
	up1 = (EIF_NATURAL_64 *) &(loc1);
	up2 = (EIF_NATURAL_64 *) &(loc2);
	up3 = (EIF_NATURAL_64 *) &(loc3);
	tb1 = EIF_TEST ((((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(3712, dtype))(Current, uc1x, up1x, up2x, up3x))).it_b);
	*(EIF_BOOLEAN *)(Current + RTWA(3709, dtype)) = (EIF_BOOLEAN) tb1;
	RTHOOK(2);
	RTDBGAA(Current, dtype, 3705, 0x3C000000, 1); /* last_free_space_in_bytes */
	*(EIF_NATURAL_64 *)(Current + RTWA(3705, dtype)) = (EIF_NATURAL_64) loc3;
	RTHOOK(3);
	RTDBGAA(Current, dtype, 3706, 0x3C000000, 1); /* last_total_space_in_bytes */
	*(EIF_NATURAL_64 *)(Current + RTWA(3706, dtype)) = (EIF_NATURAL_64) loc2;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(4);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(6);
	RTEE;
#undef up1
#undef up2
#undef up3
#undef uc1
#undef arg1
}

/* {WEL_DISK_SPACE}.last_free_space */
EIF_TYPED_VALUE F138_3705 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "last_free_space";
	RTEX;
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_NATURAL_64 tu8_1;
	EIF_NATURAL_64 tu8_2;
	EIF_BOOLEAN tb1;
	EIF_NATURAL_64 Result = ((EIF_NATURAL_64) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_UINT64, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 137, Current, 0, 0, 3741);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(137, Current, 3741);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("last_query_successful", EX_PRE);
		tb1 = *(EIF_BOOLEAN *)(Current + RTWA(3709, dtype));
		RTTE(tb1, label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTHOOK(2);
	RTDBGAL(Current, 0, 0x3C000000, 1,0); /* Result */
	tu8_1 = *(EIF_NATURAL_64 *)(Current + RTWA(3705, dtype));
	ui4_1 = ((EIF_INTEGER_32) 20L);
	tu8_2 = eif_bit_shift_right(tu8_1,ui4_1);
	Result = (EIF_NATURAL_64) tu8_2;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(3);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_UINT64; r.it_n8 = Result; return r; }
#undef ui4_1
}

/* {WEL_DISK_SPACE}.last_total_space */
EIF_TYPED_VALUE F138_3706 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "last_total_space";
	RTEX;
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_NATURAL_64 tu8_1;
	EIF_NATURAL_64 tu8_2;
	EIF_BOOLEAN tb1;
	EIF_NATURAL_64 Result = ((EIF_NATURAL_64) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_UINT64, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 137, Current, 0, 0, 3742);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(137, Current, 3742);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("last_query_successful", EX_PRE);
		tb1 = *(EIF_BOOLEAN *)(Current + RTWA(3709, dtype));
		RTTE(tb1, label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTHOOK(2);
	RTDBGAL(Current, 0, 0x3C000000, 1,0); /* Result */
	tu8_1 = *(EIF_NATURAL_64 *)(Current + RTWA(3706, dtype));
	ui4_1 = ((EIF_INTEGER_32) 20L);
	tu8_2 = eif_bit_shift_right(tu8_1,ui4_1);
	Result = (EIF_NATURAL_64) tu8_2;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(3);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_UINT64; r.it_n8 = Result; return r; }
#undef ui4_1
}

/* {WEL_DISK_SPACE}.last_free_space_in_bytes */
static EIF_TYPED_VALUE F138_3707_body (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "last_free_space_in_bytes";
	RTEX;
	EIF_BOOLEAN tb1;
	EIF_NATURAL_64 Result = ((EIF_NATURAL_64) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_UINT64, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 137, Current, 0, 0, 3743);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(137, Current, 3743);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("last_query_successful", EX_PRE);
		tb1 = *(EIF_BOOLEAN *)(Current + RTWA(3709, dtype));
		RTTE(tb1, label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_UINT64; r.it_n8 = Result; return r; }
}

EIF_TYPED_VALUE F138_3707 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_UINT64;
	r.it_n8 = *(EIF_NATURAL_64 *)(Current + RTWA(3705,Dtype(Current)));
	return r;
}


/* {WEL_DISK_SPACE}.last_total_space_in_bytes */
static EIF_TYPED_VALUE F138_3708_body (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "last_total_space_in_bytes";
	RTEX;
	EIF_BOOLEAN tb1;
	EIF_NATURAL_64 Result = ((EIF_NATURAL_64) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_UINT64, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 137, Current, 0, 0, 3744);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(137, Current, 3744);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("last_query_successful", EX_PRE);
		tb1 = *(EIF_BOOLEAN *)(Current + RTWA(3709, dtype));
		RTTE(tb1, label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_UINT64; r.it_n8 = Result; return r; }
}

EIF_TYPED_VALUE F138_3708 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_UINT64;
	r.it_n8 = *(EIF_NATURAL_64 *)(Current + RTWA(3706,Dtype(Current)));
	return r;
}


/* {WEL_DISK_SPACE}.last_free_space_in_string */
EIF_TYPED_VALUE F138_3709 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "last_free_space_in_string";
	RTEX;
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE uu8_1x = {{0}, SK_UINT64};
#define uu8_1 uu8_1x.it_n8
	EIF_TYPED_VALUE uu8_2x = {{0}, SK_UINT64};
#define uu8_2 uu8_2x.it_n8
	EIF_NATURAL_64 tu8_1;
	EIF_NATURAL_64 tu8_2;
	EIF_BOOLEAN tb1;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(2);
	RTLR(0,Current);
	RTLR(1,Result);
	RTLIU(2);
	RTLU (SK_REF, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 137, Current, 0, 0, 3745);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(137, Current, 3745);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("last_query_successful", EX_PRE);
		tb1 = *(EIF_BOOLEAN *)(Current + RTWA(3709, dtype));
		RTTE((EIF_BOOLEAN)(tb1 == (EIF_BOOLEAN) 1), label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTHOOK(2);
	RTDBGAL(Current, 0, 0xF8000392, 0,0); /* Result */
	tu8_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(3703, dtype))(Current)).it_n8);
	uu8_1 = tu8_1;
	tu8_2 = *(EIF_NATURAL_64 *)(Current + RTWA(3705, dtype));
	uu8_2 = tu8_2;
	Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(3710, dtype))(Current, uu8_1x, uu8_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(3);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef uu8_1
#undef uu8_2
}

/* {WEL_DISK_SPACE}.last_total_space_in_string */
EIF_TYPED_VALUE F138_3710 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "last_total_space_in_string";
	RTEX;
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE uu8_1x = {{0}, SK_UINT64};
#define uu8_1 uu8_1x.it_n8
	EIF_TYPED_VALUE uu8_2x = {{0}, SK_UINT64};
#define uu8_2 uu8_2x.it_n8
	EIF_NATURAL_64 tu8_1;
	EIF_NATURAL_64 tu8_2;
	EIF_BOOLEAN tb1;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(2);
	RTLR(0,Current);
	RTLR(1,Result);
	RTLIU(2);
	RTLU (SK_REF, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 137, Current, 0, 0, 3746);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(137, Current, 3746);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("last_query_successful", EX_PRE);
		tb1 = *(EIF_BOOLEAN *)(Current + RTWA(3709, dtype));
		RTTE((EIF_BOOLEAN)(tb1 == (EIF_BOOLEAN) 1), label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTHOOK(2);
	RTDBGAL(Current, 0, 0xF8000392, 0,0); /* Result */
	tu8_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(3704, dtype))(Current)).it_n8);
	uu8_1 = tu8_1;
	tu8_2 = *(EIF_NATURAL_64 *)(Current + RTWA(3706, dtype));
	uu8_2 = tu8_2;
	Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(3710, dtype))(Current, uu8_1x, uu8_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(3);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef uu8_1
#undef uu8_2
}

/* {WEL_DISK_SPACE}.last_query_success */
EIF_TYPED_VALUE F138_3711 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_BOOL;
	r.it_b = *(EIF_BOOLEAN *)(Current + RTWA(3709,Dtype(Current)));
	return r;
}


/* {WEL_DISK_SPACE}.convert_space_into_string */
EIF_TYPED_VALUE F138_3712 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x)
{
	GTCX
	char *l_feature_name = "convert_space_into_string";
	RTEX;
#define arg1 arg1x.it_n8
#define arg2 arg2x.it_n8
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ur1x = {{0}, SK_REF};
#define ur1 ur1x.it_r
	EIF_TYPED_VALUE uu8_1x = {{0}, SK_UINT64};
#define uu8_1 uu8_1x.it_n8
	EIF_TYPED_VALUE uu8_2x = {{0}, SK_UINT64};
#define uu8_2 uu8_2x.it_n8
	EIF_REFERENCE tr1 = NULL;
	EIF_NATURAL_64 tu8_1;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_n8 = * (EIF_NATURAL_64 *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_n8 = * (EIF_NATURAL_64 *) arg1x.it_r;
	
	RTLI(4);
	RTLR(0,tr1);
	RTLR(1,ur1);
	RTLR(2,Current);
	RTLR(3,Result);
	RTLIU(4);
	RTLU (SK_REF, &Result);
	RTLU(SK_UINT64,&arg1);
	RTLU(SK_UINT64,&arg2);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 137, Current, 0, 2, 3748);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(137, Current, 3748);
	RTIV(Current, RTAL);
	RTHOOK(1);
	tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 1048576L);
	if ((EIF_BOOLEAN) (arg1 > tu8_1)) {
		RTHOOK(2);
		RTDBGAL(Current, 0, 0xF8000392, 0,0); /* Result */
		uu8_1 = arg1;
		tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 1048576L);
		uu8_2 = tu8_1;
		tr1 = RTMS_EX_H("TB",2,21570);
		ur1 = tr1;
		Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(3711, dtype))(Current, uu8_1x, uu8_2x, ur1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	} else {
		RTHOOK(3);
		tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 1024L);
		if ((EIF_BOOLEAN) (arg1 > tu8_1)) {
			RTHOOK(4);
			RTDBGAL(Current, 0, 0xF8000392, 0,0); /* Result */
			uu8_1 = arg1;
			tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 1024L);
			uu8_2 = tu8_1;
			tr1 = RTMS_EX_H("GB",2,18242);
			ur1 = tr1;
			Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(3711, dtype))(Current, uu8_1x, uu8_2x, ur1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		} else {
			RTHOOK(5);
			tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 1048576L);
			if ((EIF_BOOLEAN) (arg2 > tu8_1)) {
				RTHOOK(6);
				RTDBGAL(Current, 0, 0xF8000392, 0,0); /* Result */
				uu8_1 = arg2;
				tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 1048576L);
				uu8_2 = tu8_1;
				tr1 = RTMS_EX_H("MB",2,19778);
				ur1 = tr1;
				Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(3711, dtype))(Current, uu8_1x, uu8_2x, ur1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			} else {
				RTHOOK(7);
				tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 1024L);
				if ((EIF_BOOLEAN) (arg2 > tu8_1)) {
					RTHOOK(8);
					RTDBGAL(Current, 0, 0xF8000392, 0,0); /* Result */
					uu8_1 = arg2;
					tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 1024L);
					uu8_2 = tu8_1;
					tr1 = RTMS_EX_H("KB",2,19266);
					ur1 = tr1;
					Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(3711, dtype))(Current, uu8_1x, uu8_2x, ur1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				} else {
					RTHOOK(9);
					RTDBGAL(Current, 0, 0xF8000392, 0,0); /* Result */
					uu8_1 = arg2;
					tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 1L);
					uu8_2 = tu8_1;
					tr1 = RTMS_EX_H("B",1,66);
					ur1 = tr1;
					Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(3711, dtype))(Current, uu8_1x, uu8_2x, ur1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				}
			}
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(10);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(4);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ur1
#undef uu8_1
#undef uu8_2
#undef arg2
#undef arg1
}

/* {WEL_DISK_SPACE}.format_space_string */
EIF_TYPED_VALUE F138_3713 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x, EIF_TYPED_VALUE arg3x)
{
	GTCX
	char *l_feature_name = "format_space_string";
	RTEX;
	EIF_NATURAL_64 loc1 = (EIF_NATURAL_64) 0;
	EIF_NATURAL_64 loc2 = (EIF_NATURAL_64) 0;
	EIF_NATURAL_64 loc3 = (EIF_NATURAL_64) 0;
#define arg1 arg1x.it_n8
#define arg2 arg2x.it_n8
#define arg3 arg3x.it_r
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ur1x = {{0}, SK_REF};
#define ur1 ur1x.it_r
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE uc1x = {{0}, SK_CHAR8};
#define uc1 uc1x.it_c1
	EIF_REFERENCE tr1 = NULL;
	EIF_NATURAL_64 tu8_1;
	EIF_NATURAL_64 tu8_2;
	EIF_BOOLEAN tb1;
	EIF_BOOLEAN tb2;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_n8 = * (EIF_NATURAL_64 *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_n8 = * (EIF_NATURAL_64 *) arg1x.it_r;
	
	RTLI(5);
	RTLR(0,arg3);
	RTLR(1,tr1);
	RTLR(2,Result);
	RTLR(3,ur1);
	RTLR(4,Current);
	RTLIU(5);
	RTLU (SK_REF, &Result);
	RTLU(SK_UINT64,&arg1);
	RTLU(SK_UINT64,&arg2);
	RTLU(SK_REF,&arg3);
	RTLU (SK_REF, &Current);
	RTLU(SK_UINT64, &loc1);
	RTLU(SK_UINT64, &loc2);
	RTLU(SK_UINT64, &loc3);
	
	RTEAA(l_feature_name, 137, Current, 3, 3, 3749);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(137, Current, 3749);
	RTCC(arg3, 137, l_feature_name, 3, eif_new_type(914, 0x01), 0x01);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("valid_divisor", EX_PRE);
		tb1 = '\01';
		tb2 = '\01';
		tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 1L);
		if (!((EIF_BOOLEAN)(arg2 == tu8_1))) {
			tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 1024L);
			tb2 = (EIF_BOOLEAN)(arg2 == tu8_1);
		}
		if (!tb2) {
			tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 1048576L);
			tb1 = (EIF_BOOLEAN)(arg2 == tu8_1);
		}
		RTTE(tb1, label_1);
		RTCK;
		RTHOOK(2);
		RTCT("valid_extension", EX_PRE);
		tb1 = '\0';
		if ((EIF_BOOLEAN)(arg3 != NULL)) {
			tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(15044, "is_empty", arg3))(arg3)).it_b);
			tb1 = (EIF_BOOLEAN) !tb2;
		}
		RTTE(tb1, label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTHOOK(3);
	RTDBGAL(Current, 0, 0xF8000392, 0,0); /* Result */
	tr1 = RTLN(eif_new_type(914, 0x01).id);
	ui4_1 = ((EIF_INTEGER_32) 8L);
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWC(15027, Dtype(tr1)))(tr1, ui4_1x);
	RTNHOOK(3,1);
	Result = (EIF_REFERENCE) RTCCL(tr1);
	RTHOOK(4);
	RTDBGAL(Current, 1, 0x3C000000, 1, 0); /* loc1 */
	loc1 = (EIF_NATURAL_64) (EIF_NATURAL_64) (arg1 / arg2);
	RTHOOK(5);
	tr1 = c_outu64(loc1);
	ur1 = RTCCL(tr1);
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(15190, "append", Result))(Result, ur1x);
	RTHOOK(6);
	tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 100L);
	if ((EIF_BOOLEAN) (loc1 < tu8_1)) {
		RTHOOK(7);
		tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 10L);
		if ((EIF_BOOLEAN) (loc1 < tu8_1)) {
			RTHOOK(8);
			RTDBGAL(Current, 2, 0x3C000000, 1, 0); /* loc2 */
			tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 100L);
			tu8_2 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 10L);
			loc2 = (EIF_NATURAL_64) (EIF_NATURAL_64) (((EIF_NATURAL_64) (((EIF_NATURAL_64) ((EIF_NATURAL_64) (arg1 % arg2) * tu8_1)) / arg2)) / tu8_2);
			RTHOOK(9);
			RTDBGAL(Current, 3, 0x3C000000, 1, 0); /* loc3 */
			tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 100L);
			tu8_2 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 10L);
			loc3 = (EIF_NATURAL_64) (EIF_NATURAL_64) (((EIF_NATURAL_64) (((EIF_NATURAL_64) ((EIF_NATURAL_64) (arg1 % arg2) * tu8_1)) / arg2)) % tu8_2);
			RTHOOK(10);
			tb1 = '\01';
			tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 0L);
			if (!(EIF_BOOLEAN)(loc2 != tu8_1)) {
				tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 0L);
				tb1 = (EIF_BOOLEAN)(loc3 != tu8_1);
			}
			if (tb1) {
				RTHOOK(11);
				uc1 = (EIF_CHARACTER_8) '.';
				(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(15203, "append_character", Result))(Result, uc1x);
				RTHOOK(12);
				tr1 = c_outu64(loc2);
				ur1 = RTCCL(tr1);
				(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(15190, "append", Result))(Result, ur1x);
				RTHOOK(13);
				tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 0L);
				if ((EIF_BOOLEAN)(loc3 != tu8_1)) {
					RTHOOK(14);
					tr1 = c_outu64(loc3);
					ur1 = RTCCL(tr1);
					(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(15190, "append", Result))(Result, ur1x);
				}
			}
		} else {
			RTHOOK(15);
			RTDBGAL(Current, 2, 0x3C000000, 1, 0); /* loc2 */
			tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 100L);
			tu8_2 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 10L);
			loc2 = (EIF_NATURAL_64) (EIF_NATURAL_64) (((EIF_NATURAL_64) (((EIF_NATURAL_64) ((EIF_NATURAL_64) (arg1 % arg2) * tu8_1)) / arg2)) / tu8_2);
			RTHOOK(16);
			tu8_1 = (EIF_NATURAL_64) ((EIF_INTEGER_32) 0L);
			if ((EIF_BOOLEAN)(loc2 != tu8_1)) {
				RTHOOK(17);
				uc1 = (EIF_CHARACTER_8) '.';
				(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(15203, "append_character", Result))(Result, uc1x);
				RTHOOK(18);
				tr1 = c_outu64(loc2);
				ur1 = RTCCL(tr1);
				(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(15190, "append", Result))(Result, ur1x);
			}
		}
	}
	RTHOOK(19);
	uc1 = (EIF_CHARACTER_8) ' ';
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(15203, "append_character", Result))(Result, uc1x);
	RTHOOK(20);
	ur1 = RTCCL(arg3);
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(15190, "append", Result))(Result, ur1x);
	if (RTAL & CK_ENSURE) {
		RTHOOK(21);
		RTCT("result_not_void", EX_POST);
		if ((EIF_BOOLEAN)(Result != NULL)) {
			RTCK;
		} else {
			RTCF;
		}
		RTHOOK(22);
		RTCT("result_not_empty", EX_POST);
		tb1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(15044, "is_empty", Result))(Result)).it_b);
		if ((EIF_BOOLEAN) !tb1) {
			RTCK;
		} else {
			RTCF;
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(23);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(8);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ur1
#undef ui4_1
#undef uc1
#undef arg3
#undef arg2
#undef arg1
}

/* {WEL_DISK_SPACE}.cwin_query_disk_space */
EIF_TYPED_VALUE F138_3714 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x, EIF_TYPED_VALUE arg3x, EIF_TYPED_VALUE arg4x)
{
	GTCX
	char *l_feature_name = "cwin_query_disk_space";
	RTEX;
#define arg1 arg1x.it_c1
#define arg2 arg2x.it_p
#define arg3 arg3x.it_p
#define arg4 arg4x.it_p
	EIF_BOOLEAN Result = ((EIF_BOOLEAN) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg4x.type & SK_HEAD) == SK_REF) arg4x.it_p = * (EIF_NATURAL_64* *) arg4x.it_r;
	if ((arg3x.type & SK_HEAD) == SK_REF) arg3x.it_p = * (EIF_NATURAL_64* *) arg3x.it_r;
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_p = * (EIF_NATURAL_64* *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_c1 = * (EIF_CHARACTER_8 *) arg1x.it_r;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_BOOL, &Result);
	RTLU(SK_CHAR8,&arg1);
	RTLU(SK_POINTER,&arg2);
	RTLU(SK_POINTER,&arg3);
	RTLU(SK_POINTER,&arg4);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 137, Current, 0, 4, 3750);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(137, Current, 3750);
	RTIV(Current, RTAL);
	Result = EIF_TEST(inline_F138_3714 ((EIF_CHARACTER_8) arg1, (EIF_NATURAL_64*) arg2, (EIF_NATURAL_64*) arg3, (EIF_NATURAL_64*) arg4));
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(6);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_BOOL; r.it_b = Result; return r; }
#undef arg4
#undef arg3
#undef arg2
#undef arg1
}

void EIF_Minit138 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
