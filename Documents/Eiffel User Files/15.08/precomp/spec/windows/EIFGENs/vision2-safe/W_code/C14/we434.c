/*
 * Code for class WEL_RBBIM_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F434_7521(EIF_REFERENCE);
extern EIF_TYPED_VALUE F434_7522(EIF_REFERENCE);
extern EIF_TYPED_VALUE F434_7523(EIF_REFERENCE);
extern EIF_TYPED_VALUE F434_7524(EIF_REFERENCE);
extern EIF_TYPED_VALUE F434_7525(EIF_REFERENCE);
extern EIF_TYPED_VALUE F434_7526(EIF_REFERENCE);
extern EIF_TYPED_VALUE F434_7527(EIF_REFERENCE);
extern EIF_TYPED_VALUE F434_7528(EIF_REFERENCE);
extern EIF_TYPED_VALUE F434_7529(EIF_REFERENCE);
extern void EIF_Minit434(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_RBBIM_CONSTANTS}.rbbim_style */
EIF_TYPED_VALUE F434_7521 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbim_style";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 433, Current, 0, 0, 7394);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(433, Current, 7394);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBIM_STYLE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBBIM_CONSTANTS}.rbbim_colors */
EIF_TYPED_VALUE F434_7522 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbim_colors";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 433, Current, 0, 0, 7395);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(433, Current, 7395);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBIM_COLORS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBBIM_CONSTANTS}.rbbim_text */
EIF_TYPED_VALUE F434_7523 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbim_text";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 433, Current, 0, 0, 7396);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(433, Current, 7396);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBIM_TEXT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBBIM_CONSTANTS}.rbbim_image */
EIF_TYPED_VALUE F434_7524 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbim_image";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 433, Current, 0, 0, 7397);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(433, Current, 7397);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBIM_IMAGE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBBIM_CONSTANTS}.rbbim_child */
EIF_TYPED_VALUE F434_7525 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbim_child";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 433, Current, 0, 0, 7398);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(433, Current, 7398);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBIM_CHILD;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBBIM_CONSTANTS}.rbbim_childsize */
EIF_TYPED_VALUE F434_7526 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbim_childsize";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 433, Current, 0, 0, 7399);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(433, Current, 7399);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBIM_CHILDSIZE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBBIM_CONSTANTS}.rbbim_size */
EIF_TYPED_VALUE F434_7527 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbim_size";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 433, Current, 0, 0, 7400);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(433, Current, 7400);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBIM_SIZE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBBIM_CONSTANTS}.rbbim_background */
EIF_TYPED_VALUE F434_7528 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbim_background";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 433, Current, 0, 0, 7401);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(433, Current, 7401);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBIM_BACKGROUND;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBBIM_CONSTANTS}.rbbim_id */
EIF_TYPED_VALUE F434_7529 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbim_id";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 433, Current, 0, 0, 7402);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(433, Current, 7402);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBIM_ID;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit434 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
