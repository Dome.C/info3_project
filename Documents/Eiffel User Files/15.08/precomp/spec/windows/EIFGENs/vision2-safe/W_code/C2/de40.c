/*
 * Code for class DECLARATOR
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F40_577(EIF_REFERENCE);
extern EIF_TYPED_VALUE F40_578(EIF_REFERENCE);
extern EIF_TYPED_VALUE F40_579(EIF_REFERENCE);
extern EIF_TYPED_VALUE F40_580(EIF_REFERENCE);
extern EIF_TYPED_VALUE F40_581(EIF_REFERENCE);
extern EIF_TYPED_VALUE F40_582(EIF_REFERENCE);
extern EIF_TYPED_VALUE F40_583(EIF_REFERENCE);
extern EIF_TYPED_VALUE F40_584(EIF_REFERENCE);
extern EIF_TYPED_VALUE F40_585(EIF_REFERENCE);
extern EIF_TYPED_VALUE F40_586(EIF_REFERENCE);
extern EIF_TYPED_VALUE F40_587(EIF_REFERENCE);
extern EIF_TYPED_VALUE F40_588(EIF_REFERENCE);
extern EIF_TYPED_VALUE F40_589(EIF_REFERENCE);
extern EIF_TYPED_VALUE F40_590(EIF_REFERENCE);
extern void EIF_Minit40(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {DECLARATOR}.s1 */
EIF_TYPED_VALUE F40_577 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(579,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s1_2 */
EIF_TYPED_VALUE F40_578 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(580,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s1_3 */
EIF_TYPED_VALUE F40_579 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(581,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s1_4 */
EIF_TYPED_VALUE F40_580 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(582,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s2 */
EIF_TYPED_VALUE F40_581 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(583,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s3 */
EIF_TYPED_VALUE F40_582 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(584,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s4 */
EIF_TYPED_VALUE F40_583 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(585,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s5 */
EIF_TYPED_VALUE F40_584 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(586,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s6 */
EIF_TYPED_VALUE F40_585 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(587,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s7 */
EIF_TYPED_VALUE F40_586 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(588,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s8 */
EIF_TYPED_VALUE F40_587 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(589,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s8_2 */
EIF_TYPED_VALUE F40_588 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(590,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s8_3 */
EIF_TYPED_VALUE F40_589 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(591,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s8_4 */
EIF_TYPED_VALUE F40_590 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(592,Dtype(Current)));
	return r;
}


void EIF_Minit40 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
