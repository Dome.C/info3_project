/*
 * Code for class WEL_REGISTRY_KEY_VALUE_TYPE
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F276_5223(EIF_REFERENCE);
extern EIF_TYPED_VALUE F276_5224(EIF_REFERENCE);
extern EIF_TYPED_VALUE F276_5225(EIF_REFERENCE);
extern EIF_TYPED_VALUE F276_5226(EIF_REFERENCE);
extern EIF_TYPED_VALUE F276_5227(EIF_REFERENCE);
extern EIF_TYPED_VALUE F276_5228(EIF_REFERENCE);
extern EIF_TYPED_VALUE F276_5229(EIF_REFERENCE);
extern EIF_TYPED_VALUE F276_5230(EIF_REFERENCE);
extern EIF_TYPED_VALUE F276_5231(EIF_REFERENCE);
extern EIF_TYPED_VALUE F276_5232(EIF_REFERENCE);
extern void EIF_Minit276(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_REGISTRY_KEY_VALUE_TYPE}.reg_binary */
EIF_TYPED_VALUE F276_5223 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_REGISTRY_KEY_VALUE_TYPE}.reg_dword */
EIF_TYPED_VALUE F276_5224 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_REGISTRY_KEY_VALUE_TYPE}.reg_dword_little_endian */
EIF_TYPED_VALUE F276_5225 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_REGISTRY_KEY_VALUE_TYPE}.reg_dword_big_endian */
EIF_TYPED_VALUE F276_5226 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

/* {WEL_REGISTRY_KEY_VALUE_TYPE}.reg_expand_sz */
EIF_TYPED_VALUE F276_5227 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_REGISTRY_KEY_VALUE_TYPE}.reg_sz */
EIF_TYPED_VALUE F276_5228 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_REGISTRY_KEY_VALUE_TYPE}.reg_link */
EIF_TYPED_VALUE F276_5229 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 6L);
	return r;
}

/* {WEL_REGISTRY_KEY_VALUE_TYPE}.reg_multi_sz */
EIF_TYPED_VALUE F276_5230 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 7L);
	return r;
}

/* {WEL_REGISTRY_KEY_VALUE_TYPE}.reg_none */
EIF_TYPED_VALUE F276_5231 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_REGISTRY_KEY_VALUE_TYPE}.reg_resource_list */
EIF_TYPED_VALUE F276_5232 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

void EIF_Minit276 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
