/*
 * Code for class WEL_LBS_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F343_6427(EIF_REFERENCE);
extern EIF_TYPED_VALUE F343_6428(EIF_REFERENCE);
extern EIF_TYPED_VALUE F343_6429(EIF_REFERENCE);
extern EIF_TYPED_VALUE F343_6430(EIF_REFERENCE);
extern EIF_TYPED_VALUE F343_6431(EIF_REFERENCE);
extern EIF_TYPED_VALUE F343_6432(EIF_REFERENCE);
extern EIF_TYPED_VALUE F343_6433(EIF_REFERENCE);
extern EIF_TYPED_VALUE F343_6434(EIF_REFERENCE);
extern EIF_TYPED_VALUE F343_6435(EIF_REFERENCE);
extern EIF_TYPED_VALUE F343_6436(EIF_REFERENCE);
extern EIF_TYPED_VALUE F343_6437(EIF_REFERENCE);
extern EIF_TYPED_VALUE F343_6438(EIF_REFERENCE);
extern EIF_TYPED_VALUE F343_6439(EIF_REFERENCE);
extern EIF_TYPED_VALUE F343_6440(EIF_REFERENCE);
extern void EIF_Minit343(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_LBS_CONSTANTS}.lbs_notify */
EIF_TYPED_VALUE F343_6427 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_LBS_CONSTANTS}.lbs_sort */
EIF_TYPED_VALUE F343_6428 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_LBS_CONSTANTS}.lbs_noredraw */
EIF_TYPED_VALUE F343_6429 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_LBS_CONSTANTS}.lbs_multiplesel */
EIF_TYPED_VALUE F343_6430 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_LBS_CONSTANTS}.lbs_ownerdrawfixed */
EIF_TYPED_VALUE F343_6431 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_LBS_CONSTANTS}.lbs_ownerdrawvariable */
EIF_TYPED_VALUE F343_6432 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_LBS_CONSTANTS}.lbs_hasstrings */
EIF_TYPED_VALUE F343_6433 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_LBS_CONSTANTS}.lbs_usetabstops */
EIF_TYPED_VALUE F343_6434 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_LBS_CONSTANTS}.lbs_nointegralheight */
EIF_TYPED_VALUE F343_6435 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_LBS_CONSTANTS}.lbs_multicolumn */
EIF_TYPED_VALUE F343_6436 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

/* {WEL_LBS_CONSTANTS}.lbs_wantkeyboardinput */
EIF_TYPED_VALUE F343_6437 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1024L);
	return r;
}

/* {WEL_LBS_CONSTANTS}.lbs_extendedsel */
EIF_TYPED_VALUE F343_6438 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2048L);
	return r;
}

/* {WEL_LBS_CONSTANTS}.lbs_disablenoscroll */
EIF_TYPED_VALUE F343_6439 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4096L);
	return r;
}

/* {WEL_LBS_CONSTANTS}.lbs_standard */
EIF_TYPED_VALUE F343_6440 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 10485763L);
	return r;
}

void EIF_Minit343 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
