/*
 * Code for class WEL_FILE_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F71_885(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_886(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_887(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_888(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_889(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_890(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_891(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_892(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_893(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_894(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_895(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_896(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_897(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_898(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_899(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_900(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_901(EIF_REFERENCE);
extern EIF_TYPED_VALUE F71_902(EIF_REFERENCE);
extern void EIF_Minit71(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_FILE_CONSTANTS}.generic_read */
EIF_TYPED_VALUE F71_885 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0x80000000L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.generic_write */
EIF_TYPED_VALUE F71_886 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1073741824L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.generic_execute */
EIF_TYPED_VALUE F71_887 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 536870912L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.generic_all */
EIF_TYPED_VALUE F71_888 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 268435456L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.create_new */
EIF_TYPED_VALUE F71_889 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.create_always */
EIF_TYPED_VALUE F71_890 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.open_existing */
EIF_TYPED_VALUE F71_891 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.open_always */
EIF_TYPED_VALUE F71_892 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.truncate_existing */
EIF_TYPED_VALUE F71_893 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.file_attribute_normal */
EIF_TYPED_VALUE F71_894 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.file_attribute_temporary */
EIF_TYPED_VALUE F71_895 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.file_share_read */
EIF_TYPED_VALUE F71_896 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.file_share_write */
EIF_TYPED_VALUE F71_897 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.file_flag_write_through */
EIF_TYPED_VALUE F71_898 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0x80000000L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.file_flag_no_buffering */
EIF_TYPED_VALUE F71_899 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 536870912L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.file_begin */
EIF_TYPED_VALUE F71_900 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.file_current */
EIF_TYPED_VALUE F71_901 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_FILE_CONSTANTS}.file_end */
EIF_TYPED_VALUE F71_902 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

void EIF_Minit71 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
