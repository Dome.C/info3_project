/*
 * Code for class WEL_TVAF_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F369_6707(EIF_REFERENCE);
extern EIF_TYPED_VALUE F369_6708(EIF_REFERENCE);
extern EIF_TYPED_VALUE F369_6709(EIF_REFERENCE);
extern EIF_TYPED_VALUE F369_6710(EIF_REFERENCE);
extern EIF_TYPED_VALUE F369_6711(EIF_REFERENCE);
extern EIF_TYPED_VALUE F369_6712(EIF_REFERENCE);
extern EIF_TYPED_VALUE F369_6713(EIF_REFERENCE);
extern void EIF_Minit369(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_TVAF_CONSTANTS}.tvc_bykeyboard */
EIF_TYPED_VALUE F369_6707 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_TVAF_CONSTANTS}.tvc_bymouse */
EIF_TYPED_VALUE F369_6708 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_TVAF_CONSTANTS}.tvc_unknown */
EIF_TYPED_VALUE F369_6709 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_TVAF_CONSTANTS}.tve_collapse */
EIF_TYPED_VALUE F369_6710 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_TVAF_CONSTANTS}.tve_collapsereset */
EIF_TYPED_VALUE F369_6711 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32768L);
	return r;
}

/* {WEL_TVAF_CONSTANTS}.tve_expand */
EIF_TYPED_VALUE F369_6712 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_TVAF_CONSTANTS}.tve_toggle */
EIF_TYPED_VALUE F369_6713 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

void EIF_Minit369 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
