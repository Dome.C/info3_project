/*
 * Code for class WEL_SW_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F478_8250(EIF_REFERENCE);
extern EIF_TYPED_VALUE F478_8251(EIF_REFERENCE);
extern EIF_TYPED_VALUE F478_8252(EIF_REFERENCE);
extern EIF_TYPED_VALUE F478_8253(EIF_REFERENCE);
extern EIF_TYPED_VALUE F478_8254(EIF_REFERENCE);
extern EIF_TYPED_VALUE F478_8255(EIF_REFERENCE);
extern EIF_TYPED_VALUE F478_8256(EIF_REFERENCE);
extern EIF_TYPED_VALUE F478_8257(EIF_REFERENCE);
extern EIF_TYPED_VALUE F478_8258(EIF_REFERENCE);
extern EIF_TYPED_VALUE F478_8259(EIF_REFERENCE);
extern EIF_TYPED_VALUE F478_8260(EIF_REFERENCE);
extern EIF_TYPED_VALUE F478_8261(EIF_REFERENCE);
extern EIF_TYPED_VALUE F478_8262(EIF_REFERENCE);
extern EIF_TYPED_VALUE F478_8263(EIF_REFERENCE);
extern void EIF_Minit478(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_SW_CONSTANTS}.sw_hide */
EIF_TYPED_VALUE F478_8250 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_SW_CONSTANTS}.sw_minimize */
EIF_TYPED_VALUE F478_8251 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 6L);
	return r;
}

/* {WEL_SW_CONSTANTS}.sw_restore */
EIF_TYPED_VALUE F478_8252 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 9L);
	return r;
}

/* {WEL_SW_CONSTANTS}.sw_shownormal */
EIF_TYPED_VALUE F478_8253 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_SW_CONSTANTS}.sw_show */
EIF_TYPED_VALUE F478_8254 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

/* {WEL_SW_CONSTANTS}.sw_showmaximized */
EIF_TYPED_VALUE F478_8255 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_SW_CONSTANTS}.sw_showminimized */
EIF_TYPED_VALUE F478_8256 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_SW_CONSTANTS}.sw_showminnoactive */
EIF_TYPED_VALUE F478_8257 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 7L);
	return r;
}

/* {WEL_SW_CONSTANTS}.sw_showna */
EIF_TYPED_VALUE F478_8258 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_SW_CONSTANTS}.sw_shownoactivate */
EIF_TYPED_VALUE F478_8259 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_SW_CONSTANTS}.sw_parentclosing */
EIF_TYPED_VALUE F478_8260 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_SW_CONSTANTS}.sw_parentopening */
EIF_TYPED_VALUE F478_8261 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_SW_CONSTANTS}.sw_otherzoom */
EIF_TYPED_VALUE F478_8262 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_SW_CONSTANTS}.sw_otherunzoom */
EIF_TYPED_VALUE F478_8263 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

void EIF_Minit478 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
