/*
 * Code for class WEL_FW_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F158_3955(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3956(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3957(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3958(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3959(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3960(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3961(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3962(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3963(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3964(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3965(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3966(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3967(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3968(EIF_REFERENCE);
extern EIF_TYPED_VALUE F158_3969(EIF_REFERENCE);
extern void EIF_Minit158(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_FW_CONSTANTS}.fw_dontcare */
EIF_TYPED_VALUE F158_3955 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_dontcare";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4064);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4064);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_DONTCARE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_thin */
EIF_TYPED_VALUE F158_3956 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_thin";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4065);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4065);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_THIN;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_extralight */
EIF_TYPED_VALUE F158_3957 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_extralight";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4066);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4066);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_EXTRALIGHT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_light */
EIF_TYPED_VALUE F158_3958 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_light";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4067);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4067);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_LIGHT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_normal */
EIF_TYPED_VALUE F158_3959 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_normal";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4068);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4068);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_NORMAL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_medium */
EIF_TYPED_VALUE F158_3960 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_medium";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4069);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4069);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_MEDIUM;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_semibold */
EIF_TYPED_VALUE F158_3961 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_semibold";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4070);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4070);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_SEMIBOLD;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_bold */
EIF_TYPED_VALUE F158_3962 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_bold";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4071);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4071);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_BOLD;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_extrabold */
EIF_TYPED_VALUE F158_3963 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_extrabold";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4072);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4072);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_EXTRABOLD;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_heavy */
EIF_TYPED_VALUE F158_3964 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_heavy";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4073);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4073);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_HEAVY;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_ultralight */
EIF_TYPED_VALUE F158_3965 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_ultralight";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4074);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4074);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_ULTRALIGHT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_regular */
EIF_TYPED_VALUE F158_3966 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_regular";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4075);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4075);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_REGULAR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_demibold */
EIF_TYPED_VALUE F158_3967 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_demibold";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4076);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4076);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_DEMIBOLD;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_ultrabold */
EIF_TYPED_VALUE F158_3968 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_ultrabold";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4062);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4062);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_ULTRABOLD;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FW_CONSTANTS}.fw_black */
EIF_TYPED_VALUE F158_3969 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "fw_black";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 157, Current, 0, 0, 4063);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(157, Current, 4063);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FW_BLACK;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit158 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
