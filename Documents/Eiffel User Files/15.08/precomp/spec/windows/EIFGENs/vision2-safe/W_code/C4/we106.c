/*
 * Code for class WEL_CURVES_CAPABILITIES_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F106_1421(EIF_REFERENCE);
extern EIF_TYPED_VALUE F106_1422(EIF_REFERENCE);
extern EIF_TYPED_VALUE F106_1423(EIF_REFERENCE);
extern EIF_TYPED_VALUE F106_1424(EIF_REFERENCE);
extern EIF_TYPED_VALUE F106_1425(EIF_REFERENCE);
extern EIF_TYPED_VALUE F106_1426(EIF_REFERENCE);
extern EIF_TYPED_VALUE F106_1427(EIF_REFERENCE);
extern EIF_TYPED_VALUE F106_1428(EIF_REFERENCE);
extern EIF_TYPED_VALUE F106_1429(EIF_REFERENCE);
extern EIF_TYPED_VALUE F106_1430(EIF_REFERENCE);
extern void EIF_Minit106(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_CURVES_CAPABILITIES_CONSTANTS}.cc_none */
EIF_TYPED_VALUE F106_1421 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_none";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 105, Current, 0, 0, 1462);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(105, Current, 1462);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_NONE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CURVES_CAPABILITIES_CONSTANTS}.cc_circles */
EIF_TYPED_VALUE F106_1422 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_circles";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 105, Current, 0, 0, 1463);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(105, Current, 1463);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_CIRCLES;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CURVES_CAPABILITIES_CONSTANTS}.cc_pie */
EIF_TYPED_VALUE F106_1423 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_pie";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 105, Current, 0, 0, 1464);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(105, Current, 1464);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_PIE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CURVES_CAPABILITIES_CONSTANTS}.cc_chord */
EIF_TYPED_VALUE F106_1424 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_chord";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 105, Current, 0, 0, 1465);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(105, Current, 1465);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_CHORD;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CURVES_CAPABILITIES_CONSTANTS}.cc_ellipses */
EIF_TYPED_VALUE F106_1425 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_ellipses";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 105, Current, 0, 0, 1466);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(105, Current, 1466);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_ELLIPSES;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CURVES_CAPABILITIES_CONSTANTS}.cc_wide */
EIF_TYPED_VALUE F106_1426 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_wide";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 105, Current, 0, 0, 1457);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(105, Current, 1457);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_WIDE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CURVES_CAPABILITIES_CONSTANTS}.cc_styled */
EIF_TYPED_VALUE F106_1427 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_styled";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 105, Current, 0, 0, 1458);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(105, Current, 1458);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_STYLED;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CURVES_CAPABILITIES_CONSTANTS}.cc_wide_styled */
EIF_TYPED_VALUE F106_1428 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_wide_styled";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 105, Current, 0, 0, 1459);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(105, Current, 1459);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_WIDESTYLED;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CURVES_CAPABILITIES_CONSTANTS}.cc_interiors */
EIF_TYPED_VALUE F106_1429 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_interiors";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 105, Current, 0, 0, 1460);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(105, Current, 1460);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_INTERIORS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CURVES_CAPABILITIES_CONSTANTS}.cc_round_rect */
EIF_TYPED_VALUE F106_1430 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_round_rect";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 105, Current, 0, 0, 1461);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(105, Current, 1461);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_ROUNDRECT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit106 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
