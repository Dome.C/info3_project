/*
 * Code for class WEL_FONT_FAMILY_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F477_8244(EIF_REFERENCE);
extern EIF_TYPED_VALUE F477_8245(EIF_REFERENCE);
extern EIF_TYPED_VALUE F477_8246(EIF_REFERENCE);
extern EIF_TYPED_VALUE F477_8247(EIF_REFERENCE);
extern EIF_TYPED_VALUE F477_8248(EIF_REFERENCE);
extern EIF_TYPED_VALUE F477_8249(EIF_REFERENCE);
extern void EIF_Minit477(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_FONT_FAMILY_CONSTANTS}.ff_dontcare */
EIF_TYPED_VALUE F477_8244 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ff_dontcare";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 476, Current, 0, 0, 8072);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(476, Current, 8072);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FF_DONTCARE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FONT_FAMILY_CONSTANTS}.ff_roman */
EIF_TYPED_VALUE F477_8245 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ff_roman";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 476, Current, 0, 0, 8073);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(476, Current, 8073);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FF_ROMAN;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FONT_FAMILY_CONSTANTS}.ff_swiss */
EIF_TYPED_VALUE F477_8246 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ff_swiss";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 476, Current, 0, 0, 8068);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(476, Current, 8068);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FF_SWISS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FONT_FAMILY_CONSTANTS}.ff_modern */
EIF_TYPED_VALUE F477_8247 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ff_modern";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 476, Current, 0, 0, 8069);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(476, Current, 8069);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FF_MODERN;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FONT_FAMILY_CONSTANTS}.ff_script */
EIF_TYPED_VALUE F477_8248 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ff_script";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 476, Current, 0, 0, 8070);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(476, Current, 8070);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FF_SCRIPT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_FONT_FAMILY_CONSTANTS}.ff_decorative */
EIF_TYPED_VALUE F477_8249 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ff_decorative";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 476, Current, 0, 0, 8071);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(476, Current, 8071);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) FF_DECORATIVE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit477 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
