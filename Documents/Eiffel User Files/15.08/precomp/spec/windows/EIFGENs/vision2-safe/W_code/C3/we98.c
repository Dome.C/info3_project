/*
 * Code for class WEL_TTS_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F98_1252(EIF_REFERENCE);
extern EIF_TYPED_VALUE F98_1253(EIF_REFERENCE);
extern void EIF_Minit98(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_TTS_CONSTANTS}.tts_alwaystip */
EIF_TYPED_VALUE F98_1252 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "tts_alwaystip";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 97, Current, 0, 0, 1273);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(97, Current, 1273);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTS_ALWAYSTIP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTS_CONSTANTS}.tts_noprefix */
EIF_TYPED_VALUE F98_1253 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "tts_noprefix";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 97, Current, 0, 0, 1274);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(97, Current, 1274);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTS_NOPREFIX;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit98 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
