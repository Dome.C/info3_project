/*
 * Code for class WEL_TBN_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F172_4281(EIF_REFERENCE);
extern EIF_TYPED_VALUE F172_4282(EIF_REFERENCE);
extern EIF_TYPED_VALUE F172_4283(EIF_REFERENCE);
extern EIF_TYPED_VALUE F172_4284(EIF_REFERENCE);
extern EIF_TYPED_VALUE F172_4285(EIF_REFERENCE);
extern EIF_TYPED_VALUE F172_4286(EIF_REFERENCE);
extern EIF_TYPED_VALUE F172_4287(EIF_REFERENCE);
extern EIF_TYPED_VALUE F172_4288(EIF_REFERENCE);
extern EIF_TYPED_VALUE F172_4289(EIF_REFERENCE);
extern EIF_TYPED_VALUE F172_4290(EIF_REFERENCE);
extern EIF_TYPED_VALUE F172_4291(EIF_REFERENCE);
extern void EIF_Minit172(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_TBN_CONSTANTS}.tbn_getbuttoninfo */
EIF_TYPED_VALUE F172_4281 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -680L);
	return r;
}

/* {WEL_TBN_CONSTANTS}.tbn_begindrag */
EIF_TYPED_VALUE F172_4282 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -701L);
	return r;
}

/* {WEL_TBN_CONSTANTS}.tbn_enddrag */
EIF_TYPED_VALUE F172_4283 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -702L);
	return r;
}

/* {WEL_TBN_CONSTANTS}.tbn_beginadjust */
EIF_TYPED_VALUE F172_4284 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -703L);
	return r;
}

/* {WEL_TBN_CONSTANTS}.tbn_endadjust */
EIF_TYPED_VALUE F172_4285 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -704L);
	return r;
}

/* {WEL_TBN_CONSTANTS}.tbn_reset */
EIF_TYPED_VALUE F172_4286 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -705L);
	return r;
}

/* {WEL_TBN_CONSTANTS}.tbn_queryinsert */
EIF_TYPED_VALUE F172_4287 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -706L);
	return r;
}

/* {WEL_TBN_CONSTANTS}.tbn_querydelete */
EIF_TYPED_VALUE F172_4288 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -707L);
	return r;
}

/* {WEL_TBN_CONSTANTS}.tbn_toolbarchange */
EIF_TYPED_VALUE F172_4289 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -708L);
	return r;
}

/* {WEL_TBN_CONSTANTS}.tbn_custhelp */
EIF_TYPED_VALUE F172_4290 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -709L);
	return r;
}

/* {WEL_TBN_CONSTANTS}.tbn_dropdown */
EIF_TYPED_VALUE F172_4291 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -710L);
	return r;
}

void EIF_Minit172 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
