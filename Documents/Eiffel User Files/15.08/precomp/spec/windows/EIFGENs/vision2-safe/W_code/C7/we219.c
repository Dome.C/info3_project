/*
 * Code for class WEL_STOCK_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F219_4683(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4684(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4685(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4686(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4687(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4688(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4689(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4690(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4691(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4692(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4693(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4694(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4695(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4696(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4697(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4698(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4699(EIF_REFERENCE);
extern EIF_TYPED_VALUE F219_4700(EIF_REFERENCE);
extern void EIF_Minit219(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_STOCK_CONSTANTS}.white_brush */
EIF_TYPED_VALUE F219_4683 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.ltgray_brush */
EIF_TYPED_VALUE F219_4684 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.gray_brush */
EIF_TYPED_VALUE F219_4685 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.dkgray_brush */
EIF_TYPED_VALUE F219_4686 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.black_brush */
EIF_TYPED_VALUE F219_4687 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.null_brush */
EIF_TYPED_VALUE F219_4688 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.hollow_brush */
EIF_TYPED_VALUE F219_4689 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.white_pen */
EIF_TYPED_VALUE F219_4690 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 6L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.black_pen */
EIF_TYPED_VALUE F219_4691 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 7L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.null_pen */
EIF_TYPED_VALUE F219_4692 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.oem_fixed_font */
EIF_TYPED_VALUE F219_4693 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 10L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.ansi_fixed_font */
EIF_TYPED_VALUE F219_4694 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 11L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.ansi_var_font */
EIF_TYPED_VALUE F219_4695 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 12L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.system_font */
EIF_TYPED_VALUE F219_4696 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 13L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.device_default_font */
EIF_TYPED_VALUE F219_4697 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 14L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.system_fixed_font */
EIF_TYPED_VALUE F219_4698 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.default_gui_font */
EIF_TYPED_VALUE F219_4699 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 17L);
	return r;
}

/* {WEL_STOCK_CONSTANTS}.default_palette */
EIF_TYPED_VALUE F219_4700 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 15L);
	return r;
}

void EIF_Minit219 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
