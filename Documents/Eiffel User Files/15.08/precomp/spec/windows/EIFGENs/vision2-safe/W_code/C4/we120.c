/*
 * Code for class WEL_ODT_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F120_1629(EIF_REFERENCE);
extern EIF_TYPED_VALUE F120_1630(EIF_REFERENCE);
extern EIF_TYPED_VALUE F120_1631(EIF_REFERENCE);
extern EIF_TYPED_VALUE F120_1632(EIF_REFERENCE);
extern EIF_TYPED_VALUE F120_1633(EIF_REFERENCE);
extern EIF_TYPED_VALUE F120_1634(EIF_REFERENCE);
extern void EIF_Minit120(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_ODT_CONSTANTS}.odt_menu */
EIF_TYPED_VALUE F120_1629 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "odt_menu";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 119, Current, 0, 0, 1666);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(119, Current, 1666);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ODT_MENU;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ODT_CONSTANTS}.odt_listbox */
EIF_TYPED_VALUE F120_1630 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "odt_listbox";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 119, Current, 0, 0, 1667);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(119, Current, 1667);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ODT_LISTBOX;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ODT_CONSTANTS}.odt_combobox */
EIF_TYPED_VALUE F120_1631 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "odt_combobox";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 119, Current, 0, 0, 1668);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(119, Current, 1668);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ODT_COMBOBOX;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ODT_CONSTANTS}.odt_button */
EIF_TYPED_VALUE F120_1632 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "odt_button";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 119, Current, 0, 0, 1669);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(119, Current, 1669);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ODT_BUTTON;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ODT_CONSTANTS}.odt_static */
EIF_TYPED_VALUE F120_1633 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "odt_static";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 119, Current, 0, 0, 1670);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(119, Current, 1670);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ODT_STATIC;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ODT_CONSTANTS}.odt_tab */
EIF_TYPED_VALUE F120_1634 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "odt_tab";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 119, Current, 0, 0, 1671);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(119, Current, 1671);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ODT_TAB;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit120 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
