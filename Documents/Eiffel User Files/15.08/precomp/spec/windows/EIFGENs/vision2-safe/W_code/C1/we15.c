/*
 * Code for class WEL_CFU_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F15_165(EIF_REFERENCE);
extern EIF_TYPED_VALUE F15_166(EIF_REFERENCE);
extern EIF_TYPED_VALUE F15_167(EIF_REFERENCE);
extern EIF_TYPED_VALUE F15_168(EIF_REFERENCE);
extern EIF_TYPED_VALUE F15_169(EIF_REFERENCE);
extern EIF_TYPED_VALUE F15_170(EIF_REFERENCE);
extern void EIF_Minit15(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_CFU_CONSTANTS}.cfu_underline_none */
EIF_TYPED_VALUE F15_165 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_CFU_CONSTANTS}.cfu_underline */
EIF_TYPED_VALUE F15_166 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_CFU_CONSTANTS}.cfu_underline_word */
EIF_TYPED_VALUE F15_167 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_CFU_CONSTANTS}.cfu_underline_double */
EIF_TYPED_VALUE F15_168 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_CFU_CONSTANTS}.cfu_underline_dotted */
EIF_TYPED_VALUE F15_169 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_CFU_CONSTANTS}.cfu_cf1_underline */
EIF_TYPED_VALUE F15_170 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 255L);
	return r;
}

void EIF_Minit15 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
