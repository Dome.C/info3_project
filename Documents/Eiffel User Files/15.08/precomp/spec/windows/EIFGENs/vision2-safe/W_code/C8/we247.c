/*
 * Code for class WEL_SBT_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F247_4983(EIF_REFERENCE);
extern EIF_TYPED_VALUE F247_4984(EIF_REFERENCE);
extern EIF_TYPED_VALUE F247_4985(EIF_REFERENCE);
extern EIF_TYPED_VALUE F247_4986(EIF_REFERENCE);
extern void EIF_Minit247(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_SBT_CONSTANTS}.sbt_borders */
EIF_TYPED_VALUE F247_4983 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_SBT_CONSTANTS}.sbt_noborders */
EIF_TYPED_VALUE F247_4984 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "sbt_noborders";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 246, Current, 0, 0, 5087);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(246, Current, 5087);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) SBT_NOBORDERS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_SBT_CONSTANTS}.sbt_ownerdraw */
EIF_TYPED_VALUE F247_4985 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "sbt_ownerdraw";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 246, Current, 0, 0, 5088);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(246, Current, 5088);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) SBT_OWNERDRAW;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_SBT_CONSTANTS}.sbt_popout */
EIF_TYPED_VALUE F247_4986 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "sbt_popout";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 246, Current, 0, 0, 5089);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(246, Current, 5089);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) SBT_POPOUT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit247 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
