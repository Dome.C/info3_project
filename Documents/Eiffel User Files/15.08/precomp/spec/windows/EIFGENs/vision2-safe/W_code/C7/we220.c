/*
 * Code for class WEL_GW_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F220_4701(EIF_REFERENCE);
extern EIF_TYPED_VALUE F220_4702(EIF_REFERENCE);
extern EIF_TYPED_VALUE F220_4703(EIF_REFERENCE);
extern EIF_TYPED_VALUE F220_4704(EIF_REFERENCE);
extern EIF_TYPED_VALUE F220_4705(EIF_REFERENCE);
extern EIF_TYPED_VALUE F220_4706(EIF_REFERENCE);
extern void EIF_Minit220(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_GW_CONSTANTS}.gw_hwndfirst */
EIF_TYPED_VALUE F220_4701 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "gw_hwndfirst";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 219, Current, 0, 0, 4804);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(219, Current, 4804);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) GW_HWNDFIRST;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_GW_CONSTANTS}.gw_hwndlast */
EIF_TYPED_VALUE F220_4702 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "gw_hwndlast";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 219, Current, 0, 0, 4805);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(219, Current, 4805);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) GW_HWNDLAST;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_GW_CONSTANTS}.gw_hwndnext */
EIF_TYPED_VALUE F220_4703 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "gw_hwndnext";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 219, Current, 0, 0, 4806);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(219, Current, 4806);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) GW_HWNDNEXT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_GW_CONSTANTS}.gw_hwndprev */
EIF_TYPED_VALUE F220_4704 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "gw_hwndprev";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 219, Current, 0, 0, 4807);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(219, Current, 4807);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) GW_HWNDPREV;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_GW_CONSTANTS}.gw_owner */
EIF_TYPED_VALUE F220_4705 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "gw_owner";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 219, Current, 0, 0, 4808);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(219, Current, 4808);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) GW_OWNER;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_GW_CONSTANTS}.gw_child */
EIF_TYPED_VALUE F220_4706 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "gw_child";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 219, Current, 0, 0, 4809);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(219, Current, 4809);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) GW_CHILD;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit220 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
