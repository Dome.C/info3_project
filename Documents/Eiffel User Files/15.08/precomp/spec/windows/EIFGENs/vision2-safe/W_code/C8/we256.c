/*
 * Code for class WEL_MDI_TILE_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F256_5080(EIF_REFERENCE);
extern EIF_TYPED_VALUE F256_5081(EIF_REFERENCE);
extern EIF_TYPED_VALUE F256_5082(EIF_REFERENCE);
extern void EIF_Minit256(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_MDI_TILE_CONSTANTS}.mditile_vertical */
EIF_TYPED_VALUE F256_5080 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "mditile_vertical";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 255, Current, 0, 0, 5183);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(255, Current, 5183);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) MDITILE_VERTICAL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_MDI_TILE_CONSTANTS}.mditile_horizontal */
EIF_TYPED_VALUE F256_5081 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "mditile_horizontal";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 255, Current, 0, 0, 5184);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(255, Current, 5184);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) MDITILE_HORIZONTAL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_MDI_TILE_CONSTANTS}.mditile_skipdisabled */
EIF_TYPED_VALUE F256_5082 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "mditile_skipdisabled";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 255, Current, 0, 0, 5185);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(255, Current, 5185);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) MDITILE_SKIPDISABLED;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit256 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
