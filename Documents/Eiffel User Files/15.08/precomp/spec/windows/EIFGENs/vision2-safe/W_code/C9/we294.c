/*
 * Code for class WEL_PD_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F294_5539(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5540(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5541(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5542(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5543(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5544(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5545(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5546(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5547(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5548(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5549(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5550(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5551(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5552(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5553(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5554(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5555(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5556(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5557(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5558(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5559(EIF_REFERENCE);
extern EIF_TYPED_VALUE F294_5560(EIF_REFERENCE);
extern void EIF_Minit294(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_PD_CONSTANTS}.pd_allpages */
EIF_TYPED_VALUE F294_5539 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_selection */
EIF_TYPED_VALUE F294_5540 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_pagenums */
EIF_TYPED_VALUE F294_5541 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_noselection */
EIF_TYPED_VALUE F294_5542 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_nopagenums */
EIF_TYPED_VALUE F294_5543 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_collate */
EIF_TYPED_VALUE F294_5544 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_printtofile */
EIF_TYPED_VALUE F294_5545 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_printsetup */
EIF_TYPED_VALUE F294_5546 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_nowarning */
EIF_TYPED_VALUE F294_5547 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_returndc */
EIF_TYPED_VALUE F294_5548 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_returnic */
EIF_TYPED_VALUE F294_5549 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_returndefault */
EIF_TYPED_VALUE F294_5550 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1024L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_showhelp */
EIF_TYPED_VALUE F294_5551 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2048L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_enableprinthook */
EIF_TYPED_VALUE F294_5552 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4096L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_enablesetuphook */
EIF_TYPED_VALUE F294_5553 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8192L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_enableprinttemplate */
EIF_TYPED_VALUE F294_5554 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16384L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_enablesetuptemplate */
EIF_TYPED_VALUE F294_5555 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32768L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_enableprinttemplatehandle */
EIF_TYPED_VALUE F294_5556 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 65536L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_enablesetuptemplatehandle */
EIF_TYPED_VALUE F294_5557 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 131072L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_usedevmodecopies */
EIF_TYPED_VALUE F294_5558 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 262144L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_disableprinttofile */
EIF_TYPED_VALUE F294_5559 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 524288L);
	return r;
}

/* {WEL_PD_CONSTANTS}.pd_hideprinttofile */
EIF_TYPED_VALUE F294_5560 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1048576L);
	return r;
}

void EIF_Minit294 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
