/*
 * Class BAG [BOOLEAN]
 */

#include "eif_macros.h"


#ifdef __cplusplus
extern "C" {
#endif

static const EIF_TYPE_INDEX egt_0_2024 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_1_2024 [] = {0xFF01,1551,2023,866,0xFFFF};
static const EIF_TYPE_INDEX egt_2_2024 [] = {0xFF01,2023,866,0xFFFF};
static const EIF_TYPE_INDEX egt_3_2024 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_4_2024 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_5_2024 [] = {0xFF01,2023,866,0xFFFF};
static const EIF_TYPE_INDEX egt_6_2024 [] = {0xFF01,2023,866,0xFFFF};
static const EIF_TYPE_INDEX egt_7_2024 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_8_2024 [] = {0xFF01,126,0xFFFF};
static const EIF_TYPE_INDEX egt_9_2024 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_10_2024 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_11_2024 [] = {0xFF01,29,0xFFFF};
static const EIF_TYPE_INDEX egt_12_2024 [] = {2023,866,0xFFFF};
static const EIF_TYPE_INDEX egt_13_2024 [] = {0xFF01,2023,866,0xFFFF};
static const EIF_TYPE_INDEX egt_14_2024 [] = {0xFF01,2018,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_15_2024 [] = {0xFFF8,1,0xFFFF};


static const struct desc_info desc_2024[] = {
	{EIF_GENERIC(NULL), 0xFFFFFFFF, 0xFFFFFFFF},
	{EIF_GENERIC(egt_0_2024), 0, 0xFFFFFFFF},
	{EIF_GENERIC(egt_1_2024), 1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 2, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 3, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 4, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 5, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 6, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 7, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 8, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 9, 0xFFFFFFFF},
	{EIF_GENERIC(egt_2_2024), 10, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 11, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 12, 0xFFFFFFFF},
	{EIF_GENERIC(egt_3_2024), 13, 0xFFFFFFFF},
	{EIF_GENERIC(egt_4_2024), 14, 0xFFFFFFFF},
	{EIF_GENERIC(egt_5_2024), 15, 0xFFFFFFFF},
	{EIF_GENERIC(egt_6_2024), 16, 0xFFFFFFFF},
	{EIF_GENERIC(egt_7_2024), 17, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 18, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 19, 0xFFFFFFFF},
	{EIF_GENERIC(egt_8_2024), 20, 0xFFFFFFFF},
	{EIF_GENERIC(egt_9_2024), 21, 0xFFFFFFFF},
	{EIF_GENERIC(egt_10_2024), 22, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 23, 0xFFFFFFFF},
	{EIF_GENERIC(egt_11_2024), 24, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 25, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 26, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 27, 0xFFFFFFFF},
	{EIF_GENERIC(egt_12_2024), 28, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06E3 /*881*/), 29, 0xFFFFFFFF},
	{EIF_GENERIC(egt_13_2024), 30, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 8658, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 8659, 0},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 8660, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 8661, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 8662, 0xFFFFFFFF},
	{EIF_GENERIC(egt_14_2024), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_15_2024), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 9045, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 9046, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 9047, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), -1, 0xFFFFFFFF},
};
void Init2024(void)
{
	IDSC(desc_2024, 0, 2023);
	IDSC(desc_2024 + 1, 1, 2023);
	IDSC(desc_2024 + 32, 1134, 2023);
	IDSC(desc_2024 + 41, 1135, 2023);
	IDSC(desc_2024 + 50, 778, 2023);
}


#ifdef __cplusplus
}
#endif
