/*
 * Code for class WEL_HDI_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F464_8158(EIF_REFERENCE);
extern EIF_TYPED_VALUE F464_8159(EIF_REFERENCE);
extern EIF_TYPED_VALUE F464_8160(EIF_REFERENCE);
extern EIF_TYPED_VALUE F464_8161(EIF_REFERENCE);
extern EIF_TYPED_VALUE F464_8162(EIF_REFERENCE);
extern EIF_TYPED_VALUE F464_8163(EIF_REFERENCE);
extern EIF_TYPED_VALUE F464_8164(EIF_REFERENCE);
extern void EIF_Minit464(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_HDI_CONSTANTS}.hdi_bitmap */
EIF_TYPED_VALUE F464_8158 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdi_bitmap";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 463, Current, 0, 0, 7987);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(463, Current, 7987);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDI_BITMAP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDI_CONSTANTS}.hdi_format */
EIF_TYPED_VALUE F464_8159 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdi_format";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 463, Current, 0, 0, 7988);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(463, Current, 7988);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDI_FORMAT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDI_CONSTANTS}.hdi_height */
EIF_TYPED_VALUE F464_8160 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdi_height";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 463, Current, 0, 0, 7989);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(463, Current, 7989);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDI_HEIGHT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDI_CONSTANTS}.hdi_lparam */
EIF_TYPED_VALUE F464_8161 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdi_lparam";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 463, Current, 0, 0, 7990);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(463, Current, 7990);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDI_LPARAM;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDI_CONSTANTS}.hdi_text */
EIF_TYPED_VALUE F464_8162 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdi_text";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 463, Current, 0, 0, 7991);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(463, Current, 7991);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDI_TEXT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDI_CONSTANTS}.hdi_width */
EIF_TYPED_VALUE F464_8163 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdi_width";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 463, Current, 0, 0, 7992);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(463, Current, 7992);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDI_WIDTH;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDI_CONSTANTS}.hdi_image */
EIF_TYPED_VALUE F464_8164 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdi_image";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 463, Current, 0, 0, 7993);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(463, Current, 7993);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDI_IMAGE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit464 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
