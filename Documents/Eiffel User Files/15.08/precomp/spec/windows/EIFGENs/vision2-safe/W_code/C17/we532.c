/*
 * Code for class WEL_TOOLTIP_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F532_8592(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8593(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8594(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8595(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8596(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8597(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8598(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8599(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8600(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8601(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8602(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8603(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8604(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8605(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8606(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8607(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8608(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8609(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8610(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8611(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8612(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8613(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8614(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8615(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8616(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8617(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8618(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8619(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8620(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8621(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8622(EIF_REFERENCE);
extern EIF_TYPED_VALUE F532_8623(EIF_REFERENCE);
extern void EIF_Minit532(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_TOOLTIP_CONSTANTS}.ttdt_automatic */
EIF_TYPED_VALUE F532_8592 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttdt_automatic";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8417);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8417);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTDT_AUTOMATIC;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttdt_autopop */
EIF_TYPED_VALUE F532_8593 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttdt_autopop";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8418);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8418);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTDT_AUTOPOP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttdt_initial */
EIF_TYPED_VALUE F532_8594 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttdt_initial";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8419);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8419);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTDT_INITIAL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttdt_reshow */
EIF_TYPED_VALUE F532_8595 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttdt_reshow";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8420);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8420);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTDT_RESHOW;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_activate */
EIF_TYPED_VALUE F532_8596 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_activate";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8421);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8421);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_ACTIVATE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_addtool */
EIF_TYPED_VALUE F532_8597 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_addtool";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8422);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8422);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_ADDTOOL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_deltool */
EIF_TYPED_VALUE F532_8598 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_deltool";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8423);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8423);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_DELTOOL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_enumtools */
EIF_TYPED_VALUE F532_8599 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_enumtools";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8424);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8424);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_ENUMTOOLS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_getcurrenttool */
EIF_TYPED_VALUE F532_8600 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_getcurrenttool";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8425);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8425);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETCURRENTTOOL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_getdelaytime */
EIF_TYPED_VALUE F532_8601 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_getdelaytime";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8426);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8426);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETDELAYTIME;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_gettext */
EIF_TYPED_VALUE F532_8602 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_gettext";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8427);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8427);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETTEXT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_gettipbkcolor */
EIF_TYPED_VALUE F532_8603 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_gettipbkcolor";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8428);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8428);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETTIPBKCOLOR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_gettiptextcolor */
EIF_TYPED_VALUE F532_8604 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_gettiptextcolor";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8429);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8429);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETTIPTEXTCOLOR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_setmaxtipwidth */
EIF_TYPED_VALUE F532_8605 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_setmaxtipwidth";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8430);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8430);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_SETMAXTIPWIDTH;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_getmaxtipwidth */
EIF_TYPED_VALUE F532_8606 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_getmaxtipwidth";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8431);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8431);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETMAXTIPWIDTH;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_gettoolcount */
EIF_TYPED_VALUE F532_8607 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_gettoolcount";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8432);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8432);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETTOOLCOUNT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_gettoolinfo */
EIF_TYPED_VALUE F532_8608 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_gettoolinfo";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8433);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8433);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETTOOLINFO;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_hittest */
EIF_TYPED_VALUE F532_8609 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_hittest";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8434);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8434);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_HITTEST;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_newtoolrect */
EIF_TYPED_VALUE F532_8610 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_newtoolrect";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8435);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8435);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_NEWTOOLRECT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_relayevent */
EIF_TYPED_VALUE F532_8611 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_relayevent";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8436);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8436);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_RELAYEVENT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_setdelaytime */
EIF_TYPED_VALUE F532_8612 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_setdelaytime";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8437);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8437);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_SETDELAYTIME;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_settipbkcolor */
EIF_TYPED_VALUE F532_8613 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_settipbkcolor";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8438);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8438);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_SETTIPBKCOLOR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_settiptextcolor */
EIF_TYPED_VALUE F532_8614 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_settiptextcolor";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8439);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8439);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_SETTIPTEXTCOLOR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_settoolinfo */
EIF_TYPED_VALUE F532_8615 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_settoolinfo";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8440);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8440);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_SETTOOLINFO;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_updatetiptext */
EIF_TYPED_VALUE F532_8616 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_updatetiptext";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8441);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8441);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_UPDATETIPTEXT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttm_windowfrompoint */
EIF_TYPED_VALUE F532_8617 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_windowfrompoint";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8442);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8442);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_WINDOWFROMPOINT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.tts_alwaystip */
EIF_TYPED_VALUE F532_8618 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "tts_alwaystip";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8443);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8443);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTS_ALWAYSTIP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.tts_noprefix */
EIF_TYPED_VALUE F532_8619 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "tts_noprefix";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 531, Current, 0, 0, 8444);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(531, Current, 8444);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTS_NOPREFIX;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TOOLTIP_CONSTANTS}.ttf_idishwnd */
EIF_TYPED_VALUE F532_8620 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_TOOLTIP_CONSTANTS}.ttf_centertip */
EIF_TYPED_VALUE F532_8621 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_TOOLTIP_CONSTANTS}.ttf_rtlreading */
EIF_TYPED_VALUE F532_8622 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_TOOLTIP_CONSTANTS}.ttf_subclass */
EIF_TYPED_VALUE F532_8623 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

void EIF_Minit532 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
