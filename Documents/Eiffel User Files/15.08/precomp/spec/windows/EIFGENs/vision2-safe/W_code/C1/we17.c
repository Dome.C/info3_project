/*
 * Code for class WEL_ACCELERATOR_FLAG_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F17_175(EIF_REFERENCE);
extern EIF_TYPED_VALUE F17_176(EIF_REFERENCE);
extern EIF_TYPED_VALUE F17_177(EIF_REFERENCE);
extern EIF_TYPED_VALUE F17_178(EIF_REFERENCE);
extern EIF_TYPED_VALUE F17_179(EIF_REFERENCE);
extern void EIF_Minit17(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_ACCELERATOR_FLAG_CONSTANTS}.fcontrol */
EIF_TYPED_VALUE F17_175 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_ACCELERATOR_FLAG_CONSTANTS}.fnoinvert */
EIF_TYPED_VALUE F17_176 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_ACCELERATOR_FLAG_CONSTANTS}.fshift */
EIF_TYPED_VALUE F17_177 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_ACCELERATOR_FLAG_CONSTANTS}.fvirtkey */
EIF_TYPED_VALUE F17_178 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_ACCELERATOR_FLAG_CONSTANTS}.falt */
EIF_TYPED_VALUE F17_179 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

void EIF_Minit17 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
