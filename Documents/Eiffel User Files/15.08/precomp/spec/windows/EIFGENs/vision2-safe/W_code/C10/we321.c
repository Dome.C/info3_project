/*
 * Code for class WEL_LBN_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F321_6252(EIF_REFERENCE);
extern EIF_TYPED_VALUE F321_6253(EIF_REFERENCE);
extern EIF_TYPED_VALUE F321_6254(EIF_REFERENCE);
extern EIF_TYPED_VALUE F321_6255(EIF_REFERENCE);
extern EIF_TYPED_VALUE F321_6256(EIF_REFERENCE);
extern EIF_TYPED_VALUE F321_6257(EIF_REFERENCE);
extern void EIF_Minit321(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_LBN_CONSTANTS}.lbn_errspace */
EIF_TYPED_VALUE F321_6252 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -2L);
	return r;
}

/* {WEL_LBN_CONSTANTS}.lbn_selchange */
EIF_TYPED_VALUE F321_6253 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_LBN_CONSTANTS}.lbn_dblclk */
EIF_TYPED_VALUE F321_6254 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_LBN_CONSTANTS}.lbn_selcancel */
EIF_TYPED_VALUE F321_6255 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_LBN_CONSTANTS}.lbn_setfocus */
EIF_TYPED_VALUE F321_6256 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_LBN_CONSTANTS}.lbn_killfocus */
EIF_TYPED_VALUE F321_6257 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

void EIF_Minit321 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
