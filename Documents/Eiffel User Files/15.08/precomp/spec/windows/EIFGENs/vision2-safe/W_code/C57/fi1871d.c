/*
 * Class FINITE [INTEGER_8]
 */

#include "eif_macros.h"


#ifdef __cplusplus
extern "C" {
#endif

static const EIF_TYPE_INDEX egt_0_1871 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_1_1871 [] = {0xFF01,1551,1870,845,0xFFFF};
static const EIF_TYPE_INDEX egt_2_1871 [] = {0xFF01,1870,845,0xFFFF};
static const EIF_TYPE_INDEX egt_3_1871 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_4_1871 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_5_1871 [] = {0xFF01,1870,845,0xFFFF};
static const EIF_TYPE_INDEX egt_6_1871 [] = {0xFF01,1870,845,0xFFFF};
static const EIF_TYPE_INDEX egt_7_1871 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_8_1871 [] = {0xFF01,126,0xFFFF};
static const EIF_TYPE_INDEX egt_9_1871 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_10_1871 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_11_1871 [] = {0xFF01,29,0xFFFF};
static const EIF_TYPE_INDEX egt_12_1871 [] = {1870,845,0xFFFF};
static const EIF_TYPE_INDEX egt_13_1871 [] = {0xFF01,1870,845,0xFFFF};
static const EIF_TYPE_INDEX egt_14_1871 [] = {0xFF01,1859,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_15_1871 [] = {0xFFF8,1,0xFFFF};


static const struct desc_info desc_1871[] = {
	{EIF_GENERIC(NULL), 9218, 0xFFFFFFFF},
	{EIF_GENERIC(egt_0_1871), 0, 0xFFFFFFFF},
	{EIF_GENERIC(egt_1_1871), 1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 2, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 3, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 4, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 5, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 6, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 7, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 8, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 9, 0xFFFFFFFF},
	{EIF_GENERIC(egt_2_1871), 10, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 11, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 12, 0xFFFFFFFF},
	{EIF_GENERIC(egt_3_1871), 13, 0xFFFFFFFF},
	{EIF_GENERIC(egt_4_1871), 14, 0xFFFFFFFF},
	{EIF_GENERIC(egt_5_1871), 15, 0xFFFFFFFF},
	{EIF_GENERIC(egt_6_1871), 16, 0xFFFFFFFF},
	{EIF_GENERIC(egt_7_1871), 17, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 18, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 19, 0xFFFFFFFF},
	{EIF_GENERIC(egt_8_1871), 20, 0xFFFFFFFF},
	{EIF_GENERIC(egt_9_1871), 21, 0xFFFFFFFF},
	{EIF_GENERIC(egt_10_1871), 22, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 23, 0xFFFFFFFF},
	{EIF_GENERIC(egt_11_1871), 24, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 25, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 26, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 27, 0xFFFFFFFF},
	{EIF_GENERIC(egt_12_1871), 28, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06E3 /*881*/), 29, 0xFFFFFFFF},
	{EIF_GENERIC(egt_13_1871), 30, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 9217, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 8638, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 8639, 0},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 8640, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 8641, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 8642, 0xFFFFFFFF},
	{EIF_GENERIC(egt_14_1871), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_15_1871), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), -1, 0xFFFFFFFF},
};
void Init1871(void)
{
	IDSC(desc_1871, 0, 1870);
	IDSC(desc_1871 + 1, 1, 1870);
	IDSC(desc_1871 + 32, 1134, 1870);
	IDSC(desc_1871 + 41, 554, 1870);
	IDSC(desc_1871 + 42, 878, 1870);
}


#ifdef __cplusplus
}
#endif
