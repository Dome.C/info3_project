/*
 * Code for class WEL_RB_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F179_4372(EIF_REFERENCE);
extern EIF_TYPED_VALUE F179_4373(EIF_REFERENCE);
extern EIF_TYPED_VALUE F179_4374(EIF_REFERENCE);
extern EIF_TYPED_VALUE F179_4375(EIF_REFERENCE);
extern EIF_TYPED_VALUE F179_4376(EIF_REFERENCE);
extern EIF_TYPED_VALUE F179_4377(EIF_REFERENCE);
extern EIF_TYPED_VALUE F179_4378(EIF_REFERENCE);
extern EIF_TYPED_VALUE F179_4379(EIF_REFERENCE);
extern EIF_TYPED_VALUE F179_4380(EIF_REFERENCE);
extern EIF_TYPED_VALUE F179_4381(EIF_REFERENCE);
extern void EIF_Minit179(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_RB_CONSTANTS}.rb_deleteband */
EIF_TYPED_VALUE F179_4372 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rb_deleteband";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 178, Current, 0, 0, 4478);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(178, Current, 4478);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RB_DELETEBAND;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RB_CONSTANTS}.rb_getbandinfo */
EIF_TYPED_VALUE F179_4373 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rb_getbandinfo";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 178, Current, 0, 0, 4479);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(178, Current, 4479);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RB_GETBANDINFO;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RB_CONSTANTS}.rb_getbarinfo */
EIF_TYPED_VALUE F179_4374 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rb_getbarinfo";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 178, Current, 0, 0, 4480);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(178, Current, 4480);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RB_GETBARINFO;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RB_CONSTANTS}.rb_getbandcount */
EIF_TYPED_VALUE F179_4375 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rb_getbandcount";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 178, Current, 0, 0, 4481);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(178, Current, 4481);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RB_GETBANDCOUNT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RB_CONSTANTS}.rb_getrowcount */
EIF_TYPED_VALUE F179_4376 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rb_getrowcount";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 178, Current, 0, 0, 4482);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(178, Current, 4482);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RB_GETROWCOUNT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RB_CONSTANTS}.rb_getrowheight */
EIF_TYPED_VALUE F179_4377 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rb_getrowheight";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 178, Current, 0, 0, 4483);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(178, Current, 4483);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RB_GETROWHEIGHT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RB_CONSTANTS}.rb_insertband */
EIF_TYPED_VALUE F179_4378 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rb_insertband";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 178, Current, 0, 0, 4484);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(178, Current, 4484);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RB_INSERTBAND;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RB_CONSTANTS}.rb_setbandinfo */
EIF_TYPED_VALUE F179_4379 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rb_setbandinfo";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 178, Current, 0, 0, 4485);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(178, Current, 4485);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RB_SETBANDINFO;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RB_CONSTANTS}.rb_setbarinfo */
EIF_TYPED_VALUE F179_4380 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rb_setbarinfo";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 178, Current, 0, 0, 4486);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(178, Current, 4486);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RB_SETBARINFO;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RB_CONSTANTS}.rb_setparent */
EIF_TYPED_VALUE F179_4381 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rb_setparent";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 178, Current, 0, 0, 4487);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(178, Current, 4487);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RB_SETPARENT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit179 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
