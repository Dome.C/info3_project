/*
 * Code for class WEL_GDIP_STATUS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F131_3615(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3616(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3617(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3618(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3619(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3620(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3621(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3622(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3623(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3624(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3625(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3626(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3627(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3628(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3629(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3630(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3631(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3632(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3633(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3634(EIF_REFERENCE);
extern EIF_TYPED_VALUE F131_3635(EIF_REFERENCE);
extern void EIF_Minit131(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_GDIP_STATUS}.ok */
EIF_TYPED_VALUE F131_3615 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_GDIP_STATUS}.genericerror */
EIF_TYPED_VALUE F131_3616 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_GDIP_STATUS}.invalidparameter */
EIF_TYPED_VALUE F131_3617 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_GDIP_STATUS}.outofmemory */
EIF_TYPED_VALUE F131_3618 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_GDIP_STATUS}.objectbusy */
EIF_TYPED_VALUE F131_3619 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_GDIP_STATUS}.insufficientbuffer */
EIF_TYPED_VALUE F131_3620 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

/* {WEL_GDIP_STATUS}.notimplemented */
EIF_TYPED_VALUE F131_3621 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 6L);
	return r;
}

/* {WEL_GDIP_STATUS}.win32error */
EIF_TYPED_VALUE F131_3622 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 7L);
	return r;
}

/* {WEL_GDIP_STATUS}.wrongstate */
EIF_TYPED_VALUE F131_3623 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_GDIP_STATUS}.aborted */
EIF_TYPED_VALUE F131_3624 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 9L);
	return r;
}

/* {WEL_GDIP_STATUS}.filenotfound */
EIF_TYPED_VALUE F131_3625 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 10L);
	return r;
}

/* {WEL_GDIP_STATUS}.valueoverflow */
EIF_TYPED_VALUE F131_3626 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 11L);
	return r;
}

/* {WEL_GDIP_STATUS}.accessdenied */
EIF_TYPED_VALUE F131_3627 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 12L);
	return r;
}

/* {WEL_GDIP_STATUS}.unknownimageformat */
EIF_TYPED_VALUE F131_3628 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 13L);
	return r;
}

/* {WEL_GDIP_STATUS}.fontfamilynotfound */
EIF_TYPED_VALUE F131_3629 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 14L);
	return r;
}

/* {WEL_GDIP_STATUS}.fontstylenotfound */
EIF_TYPED_VALUE F131_3630 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 15L);
	return r;
}

/* {WEL_GDIP_STATUS}.nottruetypefont */
EIF_TYPED_VALUE F131_3631 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_GDIP_STATUS}.unsupportedgdiplusversion */
EIF_TYPED_VALUE F131_3632 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 17L);
	return r;
}

/* {WEL_GDIP_STATUS}.gdiplusnotinitialized */
EIF_TYPED_VALUE F131_3633 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 18L);
	return r;
}

/* {WEL_GDIP_STATUS}.propertynotfound */
EIF_TYPED_VALUE F131_3634 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 19L);
	return r;
}

/* {WEL_GDIP_STATUS}.propertynotsupported */
EIF_TYPED_VALUE F131_3635 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 20L);
	return r;
}

void EIF_Minit131 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
