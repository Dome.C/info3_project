/*
 * Code for class WEL_RBBS_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F164_4044(EIF_REFERENCE);
extern EIF_TYPED_VALUE F164_4045(EIF_REFERENCE);
extern EIF_TYPED_VALUE F164_4046(EIF_REFERENCE);
extern EIF_TYPED_VALUE F164_4047(EIF_REFERENCE);
extern EIF_TYPED_VALUE F164_4048(EIF_REFERENCE);
extern EIF_TYPED_VALUE F164_4049(EIF_REFERENCE);
extern void EIF_Minit164(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_RBBS_CONSTANTS}.rbbs_break */
EIF_TYPED_VALUE F164_4044 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbs_break";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 163, Current, 0, 0, 4151);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(163, Current, 4151);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBS_BREAK;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBBS_CONSTANTS}.rbbs_fixedsize */
EIF_TYPED_VALUE F164_4045 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbs_fixedsize";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 163, Current, 0, 0, 4152);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(163, Current, 4152);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBS_FIXEDSIZE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBBS_CONSTANTS}.rbbs_childedge */
EIF_TYPED_VALUE F164_4046 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbs_childedge";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 163, Current, 0, 0, 4153);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(163, Current, 4153);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBS_CHILDEDGE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBBS_CONSTANTS}.rbbs_hidden */
EIF_TYPED_VALUE F164_4047 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbs_hidden";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 163, Current, 0, 0, 4154);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(163, Current, 4154);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBS_HIDDEN;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBBS_CONSTANTS}.rbbs_novert */
EIF_TYPED_VALUE F164_4048 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbs_novert";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 163, Current, 0, 0, 4155);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(163, Current, 4155);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBS_NOVERT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBBS_CONSTANTS}.rbbs_fixedbmp */
EIF_TYPED_VALUE F164_4049 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbbs_fixedbmp";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 163, Current, 0, 0, 4156);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(163, Current, 4156);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBBS_FIXEDBMP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit164 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
