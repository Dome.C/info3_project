/*
 * Code for class WEL_HDM_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F275_5211(EIF_REFERENCE);
extern EIF_TYPED_VALUE F275_5212(EIF_REFERENCE);
extern EIF_TYPED_VALUE F275_5213(EIF_REFERENCE);
extern EIF_TYPED_VALUE F275_5214(EIF_REFERENCE);
extern EIF_TYPED_VALUE F275_5215(EIF_REFERENCE);
extern EIF_TYPED_VALUE F275_5216(EIF_REFERENCE);
extern EIF_TYPED_VALUE F275_5217(EIF_REFERENCE);
extern EIF_TYPED_VALUE F275_5218(EIF_REFERENCE);
extern EIF_TYPED_VALUE F275_5219(EIF_REFERENCE);
extern EIF_TYPED_VALUE F275_5220(EIF_REFERENCE);
extern EIF_TYPED_VALUE F275_5221(EIF_REFERENCE);
extern EIF_TYPED_VALUE F275_5222(EIF_REFERENCE);
extern void EIF_Minit275(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_HDM_CONSTANTS}.hdm_first */
EIF_TYPED_VALUE F275_5211 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4608L);
	return r;
}

/* {WEL_HDM_CONSTANTS}.hdm_delete_item */
EIF_TYPED_VALUE F275_5212 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4610L);
	return r;
}

/* {WEL_HDM_CONSTANTS}.hdm_get_item */
EIF_TYPED_VALUE F275_5213 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4619L);
	return r;
}

/* {WEL_HDM_CONSTANTS}.hdm_get_item_count */
EIF_TYPED_VALUE F275_5214 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4608L);
	return r;
}

/* {WEL_HDM_CONSTANTS}.hdm_hit_test */
EIF_TYPED_VALUE F275_5215 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4614L);
	return r;
}

/* {WEL_HDM_CONSTANTS}.hdm_insert_item */
EIF_TYPED_VALUE F275_5216 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4618L);
	return r;
}

/* {WEL_HDM_CONSTANTS}.hdm_layout */
EIF_TYPED_VALUE F275_5217 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4613L);
	return r;
}

/* {WEL_HDM_CONSTANTS}.hdm_set_item */
EIF_TYPED_VALUE F275_5218 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4620L);
	return r;
}

/* {WEL_HDM_CONSTANTS}.hdm_set_image_list */
EIF_TYPED_VALUE F275_5219 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4616L);
	return r;
}

/* {WEL_HDM_CONSTANTS}.hdm_get_image_list */
EIF_TYPED_VALUE F275_5220 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4617L);
	return r;
}

/* {WEL_HDM_CONSTANTS}.hdm_get_item_rect */
EIF_TYPED_VALUE F275_5221 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4615L);
	return r;
}

/* {WEL_HDM_CONSTANTS}.hdm_get_bitmap_margin */
EIF_TYPED_VALUE F275_5222 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4629L);
	return r;
}

void EIF_Minit275 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
