/*
 * Code for class EV_FIGURE_ARC
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern void F692_13289(EIF_REFERENCE);
extern EIF_TYPED_VALUE F692_13290(EIF_REFERENCE);
extern EIF_TYPED_VALUE F692_13291(EIF_REFERENCE);
extern void F692_13292(EIF_REFERENCE, EIF_TYPED_VALUE);
extern void F692_13293(EIF_REFERENCE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F692_13294(EIF_REFERENCE);
extern EIF_TYPED_VALUE F692_13295(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F692_13296(EIF_REFERENCE);
extern void F692_29574(EIF_REFERENCE, int);
extern void EIF_Minit692(void);

#ifdef __cplusplus
}
#endif

#include "eif_helpers.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {EV_FIGURE_ARC}.default_create */
void F692_13289 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "default_create";
	RTEX;
	EIF_REAL_64 tr8_1;
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_VOID, NULL);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 691, Current, 0, 0, 17109);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(691, Current, 17109);
	RTIV(Current, RTAL);
	RTHOOK(1);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTWF(32, 686))(Current);
	RTHOOK(2);
	RTDBGAA(Current, dtype, 11757, 0x20000000, 1); /* start_angle */
	*(EIF_REAL_64 *)(Current + RTWA(11757, dtype)) = (EIF_REAL_64) (EIF_REAL_64) 0.2;
	RTHOOK(3);
	RTDBGAA(Current, dtype, 11758, 0x20000000, 1); /* aperture */
	tr8_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6803, dtype))(Current)).it_r8);
	*(EIF_REAL_64 *)(Current + RTWA(11758, dtype)) = (EIF_REAL_64) tr8_1;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(4);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
}

/* {EV_FIGURE_ARC}.start_angle */
EIF_TYPED_VALUE F692_13290 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REAL64;
	r.it_r8 = *(EIF_REAL_64 *)(Current + RTWA(11757,Dtype(Current)));
	return r;
}


/* {EV_FIGURE_ARC}.aperture */
EIF_TYPED_VALUE F692_13291 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REAL64;
	r.it_r8 = *(EIF_REAL_64 *)(Current + RTWA(11758,Dtype(Current)));
	return r;
}


/* {EV_FIGURE_ARC}.set_start_angle */
void F692_13292 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x)
{
	GTCX
	char *l_feature_name = "set_start_angle";
	RTEX;
#define arg1 arg1x.it_r8
	EIF_REAL_64 tr8_1;
	EIF_REAL_64 tr8_2;
	EIF_BOOLEAN tb1;
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_r8 = * (EIF_REAL_64 *) arg1x.it_r;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_VOID, NULL);
	RTLU(SK_REAL64,&arg1);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 691, Current, 0, 1, 17112);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(691, Current, 17112);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("a_start_angle_within_bounds", EX_PRE);
		tb1 = '\0';
		tr8_1 = (EIF_REAL_64) (((EIF_INTEGER_32) 0L));
		if ((EIF_BOOLEAN) (arg1 >= tr8_1)) {
			tr8_1 = (EIF_REAL_64) (((EIF_INTEGER_32) 2L));
			tr8_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6803, dtype))(Current)).it_r8);
			tb1 = (EIF_BOOLEAN) (arg1 <= (EIF_REAL_64) (tr8_1 * tr8_2));
		}
		RTTE(tb1, label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTHOOK(2);
	RTDBGAA(Current, dtype, 11757, 0x20000000, 1); /* start_angle */
	*(EIF_REAL_64 *)(Current + RTWA(11757, dtype)) = (EIF_REAL_64) arg1;
	if (RTAL & CK_ENSURE) {
		RTHOOK(3);
		RTCT("start_angle_assigned", EX_POST);
		tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
		if ((EIF_BOOLEAN)(tr8_1 == arg1)) {
			RTCK;
		} else {
			RTCF;
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(4);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(3);
	RTEE;
#undef arg1
}

/* {EV_FIGURE_ARC}.set_aperture */
void F692_13293 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x)
{
	GTCX
	char *l_feature_name = "set_aperture";
	RTEX;
#define arg1 arg1x.it_r8
	EIF_REAL_64 tr8_1;
	EIF_REAL_64 tr8_2;
	EIF_BOOLEAN tb1;
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_r8 = * (EIF_REAL_64 *) arg1x.it_r;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_VOID, NULL);
	RTLU(SK_REAL64,&arg1);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 691, Current, 0, 1, 17113);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(691, Current, 17113);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("an_aperture_within_bounds", EX_PRE);
		tb1 = '\0';
		tr8_1 = (EIF_REAL_64) (((EIF_INTEGER_32) 0L));
		if ((EIF_BOOLEAN) (arg1 >= tr8_1)) {
			tr8_1 = (EIF_REAL_64) (((EIF_INTEGER_32) 2L));
			tr8_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6803, dtype))(Current)).it_r8);
			tb1 = (EIF_BOOLEAN) (arg1 <= (EIF_REAL_64) (tr8_1 * tr8_2));
		}
		RTTE(tb1, label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTHOOK(2);
	RTDBGAA(Current, dtype, 11758, 0x20000000, 1); /* aperture */
	*(EIF_REAL_64 *)(Current + RTWA(11758, dtype)) = (EIF_REAL_64) arg1;
	if (RTAL & CK_ENSURE) {
		RTHOOK(3);
		RTCT("aperture_assigned", EX_POST);
		tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11758, dtype));
		if ((EIF_BOOLEAN)(tr8_1 == arg1)) {
			RTCK;
		} else {
			RTCF;
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(4);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(3);
	RTEE;
#undef arg1
}

/* {EV_FIGURE_ARC}.bounding_box */
EIF_TYPED_VALUE F692_13294 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "bounding_box";
	RTEX;
	EIF_REAL_64 loc1 = (EIF_REAL_64) 0;
	EIF_REAL_64 loc2 = (EIF_REAL_64) 0;
	EIF_INTEGER_32 loc3 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc4 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc5 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc6 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc7 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc8 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc9 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc10 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc11 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc12 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc13 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc14 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc15 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc16 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc17 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc18 = (EIF_INTEGER_32) 0;
	EIF_REAL_64 loc19 = (EIF_REAL_64) 0;
	EIF_REAL_64 loc20 = (EIF_REAL_64) 0;
	EIF_REAL_64 loc21 = (EIF_REAL_64) 0;
	EIF_REAL_64 loc22 = (EIF_REAL_64) 0;
	EIF_REAL_64 loc23 = (EIF_REAL_64) 0;
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ur8_1x = {{0}, SK_REAL64};
#define ur8_1 ur8_1x.it_r8
	EIF_TYPED_VALUE ur8_2x = {{0}, SK_REAL64};
#define ur8_2 ur8_2x.it_r8
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_TYPED_VALUE ui4_3x = {{0}, SK_INT32};
#define ui4_3 ui4_3x.it_i4
	EIF_TYPED_VALUE ui4_4x = {{0}, SK_INT32};
#define ui4_4 ui4_4x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_REAL_64 tr8_1;
	EIF_REAL_64 tr8_2;
	EIF_REAL_64 tr8_3;
	EIF_REAL_64 tr8_4;
	EIF_REAL_64 tr8_5;
	EIF_REAL_64 tr8_6;
	EIF_REAL_64 tr8_7;
	EIF_REAL_64 tr8_8;
	EIF_REAL_64 tr8_9;
	EIF_REAL_64 tr8_10;
	EIF_REAL_64 tr8_11;
	EIF_INTEGER_32 ti4_1;
	EIF_BOOLEAN tb1;
	EIF_BOOLEAN tb2;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(3);
	RTLR(0,Current);
	RTLR(1,tr1);
	RTLR(2,Result);
	RTLIU(3);
	RTLU (SK_REF, &Result);
	RTLU (SK_REF, &Current);
	RTLU(SK_REAL64, &loc1);
	RTLU(SK_REAL64, &loc2);
	RTLU(SK_INT32, &loc3);
	RTLU(SK_INT32, &loc4);
	RTLU(SK_INT32, &loc5);
	RTLU(SK_INT32, &loc6);
	RTLU(SK_INT32, &loc7);
	RTLU(SK_INT32, &loc8);
	RTLU(SK_INT32, &loc9);
	RTLU(SK_INT32, &loc10);
	RTLU(SK_INT32, &loc11);
	RTLU(SK_INT32, &loc12);
	RTLU(SK_INT32, &loc13);
	RTLU(SK_INT32, &loc14);
	RTLU(SK_INT32, &loc15);
	RTLU(SK_INT32, &loc16);
	RTLU(SK_INT32, &loc17);
	RTLU(SK_INT32, &loc18);
	RTLU(SK_REAL64, &loc19);
	RTLU(SK_REAL64, &loc20);
	RTLU(SK_REAL64, &loc21);
	RTLU(SK_REAL64, &loc22);
	RTLU(SK_REAL64, &loc23);
	
	RTEAA(l_feature_name, 691, Current, 23, 0, 17114);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(691, Current, 17114);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 15, 0x10000000, 1, 0); /* loc15 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(9330, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,1);
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(11536, "x_abs", tr1))(tr1)).it_i4);
	loc15 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(2);
	RTDBGAL(Current, 16, 0x10000000, 1, 0); /* loc16 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(9330, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(2,1);
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(11537, "y_abs", tr1))(tr1)).it_i4);
	loc16 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(3);
	RTDBGAL(Current, 17, 0x10000000, 1, 0); /* loc17 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(9334, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(3,1);
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(11536, "x_abs", tr1))(tr1)).it_i4);
	loc17 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(4);
	RTDBGAL(Current, 18, 0x10000000, 1, 0); /* loc18 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(9334, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(4,1);
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(11537, "y_abs", tr1))(tr1)).it_i4);
	loc18 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(5);
	RTDBGAL(Current, 19, 0x20000000, 1, 0); /* loc19 */
	loc19 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11758, dtype));
	loc19 = (EIF_REAL_64) (EIF_REAL_64) (loc19 + tr8_1);
	RTHOOK(6);
	RTDBGAL(Current, 11, 0x10000000, 1, 0); /* loc11 */
	ui4_1 = loc18;
	ti4_1 = eif_min_int32 (loc16,ui4_1);
	loc11 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(7);
	RTDBGAL(Current, 12, 0x10000000, 1, 0); /* loc12 */
	ui4_1 = loc17;
	ti4_1 = eif_min_int32 (loc15,ui4_1);
	loc12 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(8);
	RTDBGAL(Current, 13, 0x10000000, 1, 0); /* loc13 */
	ui4_1 = loc16;
	ti4_1 = eif_max_int32 (loc18,ui4_1);
	loc13 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(9);
	RTDBGAL(Current, 14, 0x10000000, 1, 0); /* loc14 */
	ui4_1 = loc15;
	ti4_1 = eif_max_int32 (loc17,ui4_1);
	loc14 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(10);
	RTDBGAL(Current, 9, 0x10000000, 1, 0); /* loc9 */
	loc9 = (EIF_INTEGER_32) (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc12 + loc14) / ((EIF_INTEGER_32) 2L));
	RTHOOK(11);
	RTDBGAL(Current, 10, 0x10000000, 1, 0); /* loc10 */
	loc10 = (EIF_INTEGER_32) (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc11 + loc13) / ((EIF_INTEGER_32) 2L));
	RTHOOK(12);
	RTDBGAL(Current, 7, 0x10000000, 1, 0); /* loc7 */
	loc7 = (EIF_INTEGER_32) (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc14 - loc12) / ((EIF_INTEGER_32) 2L));
	RTHOOK(13);
	RTDBGAL(Current, 8, 0x10000000, 1, 0); /* loc8 */
	loc8 = (EIF_INTEGER_32) (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc13 - loc11) / ((EIF_INTEGER_32) 2L));
	RTHOOK(14);
	RTDBGAL(Current, 1, 0x20000000, 1, 0); /* loc1 */
	tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	ur8_1 = tr8_1;
	tr8_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(6827, dtype))(Current, ur8_1x)).it_r8);
	ur8_1 = (EIF_REAL_64) ((EIF_REAL_64) ((EIF_REAL_64) (loc7) /  (EIF_REAL_64) (loc8)) * tr8_1);
	loc1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(6828, dtype))(Current, ur8_1x)).it_r8);
	RTHOOK(15);
	tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6905, dtype))(Current)).it_r8);
	tr8_3 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_4 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6909, dtype))(Current)).it_r8);
	if ((EIF_BOOLEAN) ((EIF_BOOLEAN) (tr8_1 > tr8_2) && (EIF_BOOLEAN) (tr8_3 < tr8_4))) {
		RTHOOK(16);
		RTDBGAL(Current, 1, 0x20000000, 1, 0); /* loc1 */
		tr8_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6803, dtype))(Current)).it_r8);
		loc1 += tr8_1;
	}
	RTHOOK(17);
	RTDBGAL(Current, 2, 0x20000000, 1, 0); /* loc2 */
	ur8_1 = loc19;
	tr8_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(6827, dtype))(Current, ur8_1x)).it_r8);
	ur8_1 = (EIF_REAL_64) ((EIF_REAL_64) ((EIF_REAL_64) (loc7) /  (EIF_REAL_64) (loc8)) * tr8_1);
	loc2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(6828, dtype))(Current, ur8_1x)).it_r8);
	RTHOOK(18);
	tb1 = '\0';
	ur8_1 = loc19;
	tr8_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6907, dtype))(Current)).it_r8);
	ur8_2 = tr8_1;
	tr8_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(6903, dtype))(Current, ur8_1x, ur8_2x)).it_r8);
	tr8_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6905, dtype))(Current)).it_r8);
	if ((EIF_BOOLEAN) (tr8_1 > tr8_2)) {
		ur8_1 = loc19;
		tr8_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6907, dtype))(Current)).it_r8);
		ur8_2 = tr8_1;
		tr8_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(6903, dtype))(Current, ur8_1x, ur8_2x)).it_r8);
		tr8_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6909, dtype))(Current)).it_r8);
		tb1 = (EIF_BOOLEAN) (tr8_1 < tr8_2);
	}
	if (tb1) {
		RTHOOK(19);
		RTDBGAL(Current, 2, 0x20000000, 1, 0); /* loc2 */
		tr8_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6803, dtype))(Current)).it_r8);
		loc2 += tr8_1;
	}
	RTHOOK(20);
	RTDBGAL(Current, 20, 0x20000000, 1, 0); /* loc20 */
	ur8_1 = loc1;
	loc20 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(6823, dtype))(Current, ur8_1x)).it_r8);
	RTHOOK(21);
	RTDBGAL(Current, 21, 0x20000000, 1, 0); /* loc21 */
	ur8_1 = loc2;
	loc21 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(6823, dtype))(Current, ur8_1x)).it_r8);
	RTHOOK(22);
	RTDBGAL(Current, 22, 0x20000000, 1, 0); /* loc22 */
	ur8_1 = loc1;
	loc22 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(6825, dtype))(Current, ur8_1x)).it_r8);
	RTHOOK(23);
	RTDBGAL(Current, 23, 0x20000000, 1, 0); /* loc23 */
	ur8_1 = loc2;
	loc23 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(6825, dtype))(Current, ur8_1x)).it_r8);
	RTHOOK(24);
	RTDBGAL(Current, 3, 0x10000000, 1, 0); /* loc3 */
	tr8_1 = (EIF_REAL_64) (loc9);
	tr8_2 = (EIF_REAL_64) (loc7);
	ur8_1 = loc21;
	tr8_3 = eif_min_real64 (loc20,ur8_1);
	tr1 = RTLN(eif_new_type(863, 0x00).id);
	*(EIF_REAL_64 *)tr1 = (EIF_REAL_64) (tr8_1 + (EIF_REAL_64) (tr8_2 * tr8_3));
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(14431, "floor", tr1))(tr1)).it_i4);
	loc3 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(25);
	RTDBGAL(Current, 4, 0x10000000, 1, 0); /* loc4 */
	tr8_1 = (EIF_REAL_64) (loc9);
	tr8_2 = (EIF_REAL_64) (loc7);
	ur8_1 = loc21;
	tr8_3 = eif_max_real64 (loc20,ur8_1);
	tr1 = RTLN(eif_new_type(863, 0x00).id);
	*(EIF_REAL_64 *)tr1 = (EIF_REAL_64) (tr8_1 + (EIF_REAL_64) (tr8_2 * tr8_3));
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(14430, "ceiling", tr1))(tr1)).it_i4);
	loc4 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(26);
	RTDBGAL(Current, 5, 0x10000000, 1, 0); /* loc5 */
	tr8_1 = (EIF_REAL_64) (loc10);
	tr8_2 = (EIF_REAL_64) (loc8);
	ur8_1 = loc23;
	tr8_3 = eif_max_real64 (loc22,ur8_1);
	tr1 = RTLN(eif_new_type(863, 0x00).id);
	*(EIF_REAL_64 *)tr1 = (EIF_REAL_64) (tr8_1 - (EIF_REAL_64) (tr8_2 * tr8_3));
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(14431, "floor", tr1))(tr1)).it_i4);
	loc5 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(27);
	RTDBGAL(Current, 6, 0x10000000, 1, 0); /* loc6 */
	tr8_1 = (EIF_REAL_64) (loc10);
	tr8_2 = (EIF_REAL_64) (loc8);
	ur8_1 = loc23;
	tr8_3 = eif_min_real64 (loc22,ur8_1);
	tr1 = RTLN(eif_new_type(863, 0x00).id);
	*(EIF_REAL_64 *)tr1 = (EIF_REAL_64) (tr8_1 - (EIF_REAL_64) (tr8_2 * tr8_3));
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(14430, "ceiling", tr1))(tr1)).it_i4);
	loc6 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(28);
	tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6905, dtype))(Current)).it_r8);
	tr8_3 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_4 = *(EIF_REAL_64 *)(Current + RTWA(11758, dtype));
	tr8_5 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6905, dtype))(Current)).it_r8);
	tr8_6 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_7 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6905, dtype))(Current)).it_r8);
	tr8_8 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_9 = *(EIF_REAL_64 *)(Current + RTWA(11758, dtype));
	tr8_10 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6909, dtype))(Current)).it_r8);
	tr8_11 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6803, dtype))(Current)).it_r8);
	if ((EIF_BOOLEAN) ((EIF_BOOLEAN) ((EIF_BOOLEAN) (tr8_1 < tr8_2) && (EIF_BOOLEAN) ((EIF_REAL_64) (tr8_3 + tr8_4) > tr8_5)) || (EIF_BOOLEAN) ((EIF_BOOLEAN) (tr8_6 > tr8_7) && (EIF_BOOLEAN) ((EIF_REAL_64) (tr8_8 + tr8_9) > (EIF_REAL_64) (tr8_10 + tr8_11))))) {
		RTHOOK(29);
		RTDBGAL(Current, 5, 0x10000000, 1, 0); /* loc5 */
		loc5 = (EIF_INTEGER_32) loc11;
	}
	RTHOOK(30);
	tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6803, dtype))(Current)).it_r8);
	tr8_3 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_4 = *(EIF_REAL_64 *)(Current + RTWA(11758, dtype));
	tr8_5 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6803, dtype))(Current)).it_r8);
	tr8_6 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_7 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6803, dtype))(Current)).it_r8);
	tr8_8 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_9 = *(EIF_REAL_64 *)(Current + RTWA(11758, dtype));
	tr8_10 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6908, dtype))(Current)).it_r8);
	if ((EIF_BOOLEAN) ((EIF_BOOLEAN) ((EIF_BOOLEAN) (tr8_1 < tr8_2) && (EIF_BOOLEAN) ((EIF_REAL_64) (tr8_3 + tr8_4) > tr8_5)) || (EIF_BOOLEAN) ((EIF_BOOLEAN) (tr8_6 > tr8_7) && (EIF_BOOLEAN) ((EIF_REAL_64) (tr8_8 + tr8_9) > tr8_10)))) {
		RTHOOK(31);
		RTDBGAL(Current, 3, 0x10000000, 1, 0); /* loc3 */
		loc3 = (EIF_INTEGER_32) loc12;
	}
	RTHOOK(32);
	tb1 = '\01';
	tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6909, dtype))(Current)).it_r8);
	tr8_3 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_4 = *(EIF_REAL_64 *)(Current + RTWA(11758, dtype));
	tr8_5 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6909, dtype))(Current)).it_r8);
	if (!((EIF_BOOLEAN) ((EIF_BOOLEAN) (tr8_1 < tr8_2) && (EIF_BOOLEAN) ((EIF_REAL_64) (tr8_3 + tr8_4) > tr8_5)))) {
		tb2 = '\0';
		tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
		tr8_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6909, dtype))(Current)).it_r8);
		if ((EIF_BOOLEAN) (tr8_1 > tr8_2)) {
			tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
			tr8_2 = *(EIF_REAL_64 *)(Current + RTWA(11758, dtype));
			tr8_3 = (EIF_REAL_64) (((EIF_INTEGER_32) 7L));
			tr8_4 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6905, dtype))(Current)).it_r8);
			tb2 = (EIF_BOOLEAN) ((EIF_REAL_64) (tr8_1 + tr8_2) > (EIF_REAL_64) (tr8_3 * tr8_4));
		}
		tb1 = tb2;
	}
	if (tb1) {
		RTHOOK(33);
		RTDBGAL(Current, 6, 0x10000000, 1, 0); /* loc6 */
		loc6 = (EIF_INTEGER_32) loc13;
	}
	RTHOOK(34);
	tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_2 = *(EIF_REAL_64 *)(Current + RTWA(11758, dtype));
	tr8_3 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6907, dtype))(Current)).it_r8);
	if ((EIF_BOOLEAN) ((EIF_REAL_64) (tr8_1 + tr8_2) > tr8_3)) {
		RTHOOK(35);
		RTDBGAL(Current, 4, 0x10000000, 1, 0); /* loc4 */
		loc4 = (EIF_INTEGER_32) loc14;
	}
	RTHOOK(36);
	RTDBGAL(Current, 0, 0xF8000270, 0,0); /* Result */
	tr1 = RTLN(eif_new_type(624, 0x01).id);
	ui4_1 = loc3;
	ui4_2 = loc5;
	ui4_3 = (EIF_INTEGER_32) (loc4 - loc3);
	ui4_4 = (EIF_INTEGER_32) (loc6 - loc5);
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWC(10662, Dtype(tr1)))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
	RTNHOOK(36,1);
	Result = (EIF_REFERENCE) RTCCL(tr1);
	if (RTAL & CK_ENSURE) {
		RTHOOK(37);
		RTCT("not_void", EX_POST);
		if ((EIF_BOOLEAN)(Result != NULL)) {
			RTCK;
		} else {
			RTCF;
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(38);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(25);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ur8_1
#undef ur8_2
#undef ui4_1
#undef ui4_2
#undef ui4_3
#undef ui4_4
}

/* {EV_FIGURE_ARC}.position_on_figure */
EIF_TYPED_VALUE F692_13295 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x)
{
	GTCX
	char *l_feature_name = "position_on_figure";
	RTEX;
	EIF_INTEGER_32 loc1 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc2 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc3 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc4 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc5 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc6 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc7 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc8 = (EIF_INTEGER_32) 0;
	EIF_REAL_64 loc9 = (EIF_REAL_64) 0;
	EIF_REAL_64 loc10 = (EIF_REAL_64) 0;
	EIF_BOOLEAN loc11 = (EIF_BOOLEAN) 0;
#define arg1 arg1x.it_i4
#define arg2 arg2x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ur8_1x = {{0}, SK_REAL64};
#define ur8_1 ur8_1x.it_r8
	EIF_TYPED_VALUE ur8_2x = {{0}, SK_REAL64};
#define ur8_2 ur8_2x.it_r8
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_TYPED_VALUE ui4_3x = {{0}, SK_INT32};
#define ui4_3 ui4_3x.it_i4
	EIF_TYPED_VALUE ui4_4x = {{0}, SK_INT32};
#define ui4_4 ui4_4x.it_i4
	EIF_TYPED_VALUE ui4_5x = {{0}, SK_INT32};
#define ui4_5 ui4_5x.it_i4
	EIF_TYPED_VALUE ui4_6x = {{0}, SK_INT32};
#define ui4_6 ui4_6x.it_i4
	EIF_TYPED_VALUE ui4_7x = {{0}, SK_INT32};
#define ui4_7 ui4_7x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_REAL_64 tr8_1;
	EIF_REAL_64 tr8_2;
	EIF_REAL_64 tr8_3;
	EIF_INTEGER_32 ti4_1;
	EIF_BOOLEAN tb1;
	EIF_BOOLEAN tb2;
	EIF_BOOLEAN Result = ((EIF_BOOLEAN) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_i4 = * (EIF_INTEGER_32 *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(2);
	RTLR(0,Current);
	RTLR(1,tr1);
	RTLIU(2);
	RTLU (SK_BOOL, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU(SK_INT32,&arg2);
	RTLU (SK_REF, &Current);
	RTLU(SK_INT32, &loc1);
	RTLU(SK_INT32, &loc2);
	RTLU(SK_INT32, &loc3);
	RTLU(SK_INT32, &loc4);
	RTLU(SK_INT32, &loc5);
	RTLU(SK_INT32, &loc6);
	RTLU(SK_INT32, &loc7);
	RTLU(SK_INT32, &loc8);
	RTLU(SK_REAL64, &loc9);
	RTLU(SK_REAL64, &loc10);
	RTLU(SK_BOOL, &loc11);
	
	RTEAA(l_feature_name, 691, Current, 11, 2, 17115);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(691, Current, 17115);
	RTIV(Current, RTAL);
	RTHOOK(1);
	tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11758, dtype));
	if ((EIF_BOOLEAN)(tr8_1 != (EIF_REAL_64) 0.0)) {
		RTHOOK(2);
		RTDBGAL(Current, 2, 0x10000000, 1, 0); /* loc2 */
		tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(9330, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		RTNHOOK(2,1);
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(11536, "x_abs", tr1))(tr1)).it_i4);
		loc2 = (EIF_INTEGER_32) ti4_1;
		RTHOOK(3);
		RTDBGAL(Current, 1, 0x10000000, 1, 0); /* loc1 */
		tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(9330, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		RTNHOOK(3,1);
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(11537, "y_abs", tr1))(tr1)).it_i4);
		loc1 = (EIF_INTEGER_32) ti4_1;
		RTHOOK(4);
		RTDBGAL(Current, 3, 0x10000000, 1, 0); /* loc3 */
		tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(9334, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		RTNHOOK(4,1);
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(11536, "x_abs", tr1))(tr1)).it_i4);
		loc3 = (EIF_INTEGER_32) ti4_1;
		RTHOOK(5);
		RTDBGAL(Current, 4, 0x10000000, 1, 0); /* loc4 */
		tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(9334, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		RTNHOOK(5,1);
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(11537, "y_abs", tr1))(tr1)).it_i4);
		loc4 = (EIF_INTEGER_32) ti4_1;
		RTHOOK(6);
		RTDBGAL(Current, 8, 0x10000000, 1, 0); /* loc8 */
		ui4_1 = loc3;
		ti4_1 = eif_min_int32 (loc2,ui4_1);
		loc8 = (EIF_INTEGER_32) ti4_1;
		RTHOOK(7);
		RTDBGAL(Current, 7, 0x10000000, 1, 0); /* loc7 */
		ui4_1 = loc4;
		ti4_1 = eif_min_int32 (loc1,ui4_1);
		loc7 = (EIF_INTEGER_32) ti4_1;
		RTHOOK(8);
		RTDBGAL(Current, 5, 0x10000000, 1, 0); /* loc5 */
		ti4_1 = eif_abs_int32 ((EIF_INTEGER_32) (loc3 - loc2));
		loc5 = (EIF_INTEGER_32) (EIF_INTEGER_32) (loc8 + (EIF_INTEGER_32) (ti4_1 / ((EIF_INTEGER_32) 2L)));
		RTHOOK(9);
		RTDBGAL(Current, 6, 0x10000000, 1, 0); /* loc6 */
		ti4_1 = eif_abs_int32 ((EIF_INTEGER_32) (loc4 - loc1));
		loc6 = (EIF_INTEGER_32) (EIF_INTEGER_32) (loc7 + (EIF_INTEGER_32) (ti4_1 / ((EIF_INTEGER_32) 2L)));
		RTHOOK(10);
		RTDBGAL(Current, 9, 0x20000000, 1, 0); /* loc9 */
		ui4_1 = arg1;
		ui4_2 = arg2;
		ui4_3 = loc5;
		ui4_4 = loc6;
		loc9 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(6894, dtype))(Current, ui4_1x, ui4_2x, ui4_3x, ui4_4x)).it_r8);
		RTHOOK(11);
		RTDBGAL(Current, 10, 0x20000000, 1, 0); /* loc10 */
		tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
		tr8_2 = *(EIF_REAL_64 *)(Current + RTWA(11758, dtype));
		ur8_1 = (EIF_REAL_64) (tr8_1 + tr8_2);
		tr8_3 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6907, dtype))(Current)).it_r8);
		ur8_2 = tr8_3;
		loc10 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(6903, dtype))(Current, ur8_1x, ur8_2x)).it_r8);
		RTHOOK(12);
		tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
		if ((EIF_BOOLEAN) (tr8_1 < loc10)) {
			RTHOOK(13);
			RTDBGAL(Current, 11, 0x04000000, 1, 0); /* loc11 */
			tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
			loc11 = (EIF_BOOLEAN) (EIF_BOOLEAN) ((EIF_BOOLEAN) (loc9 >= tr8_1) && (EIF_BOOLEAN) (loc9 <= loc10));
		} else {
			RTHOOK(14);
			RTDBGAL(Current, 11, 0x04000000, 1, 0); /* loc11 */
			tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
			loc11 = (EIF_BOOLEAN) (EIF_BOOLEAN) ((EIF_BOOLEAN) (loc9 >= tr8_1) || (EIF_BOOLEAN) (loc9 <= loc10));
		}
		RTHOOK(15);
		RTDBGAL(Current, 0, 0x04000000, 1,0); /* Result */
		tb1 = '\0';
		ui4_1 = arg1;
		ui4_2 = arg2;
		ui4_3 = loc5;
		ui4_4 = loc6;
		ui4_5 = (EIF_INTEGER_32) (loc5 - loc8);
		ui4_6 = (EIF_INTEGER_32) (loc6 - loc7);
		ti4_1 = *(EIF_INTEGER_32 *)(Current + RTWA(11728, dtype));
		ui4_7 = ti4_1;
		tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(6900, dtype))(Current, ui4_1x, ui4_2x, ui4_3x, ui4_4x, ui4_5x, ui4_6x, ui4_7x)).it_b);
		if (tb2) {
			tb1 = loc11;
		}
		Result = (EIF_BOOLEAN) tb1;
	} else {
		RTHOOK(16);
		RTDBGAL(Current, 0, 0x04000000, 1,0); /* Result */
		Result = (EIF_BOOLEAN) (EIF_BOOLEAN) 0;
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(17);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(15);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_BOOL; r.it_b = Result; return r; }
#undef up1
#undef ur8_1
#undef ur8_2
#undef ui4_1
#undef ui4_2
#undef ui4_3
#undef ui4_4
#undef ui4_5
#undef ui4_6
#undef ui4_7
#undef arg2
#undef arg1
}

/* {EV_FIGURE_ARC}.metrics */
EIF_TYPED_VALUE F692_13296 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "metrics";
	RTEX;
	EIF_INTEGER_32 loc1 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc2 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc3 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc4 = (EIF_INTEGER_32) 0;
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(3);
	RTLR(0,Current);
	RTLR(1,tr1);
	RTLR(2,Result);
	RTLIU(3);
	RTLU (SK_REF, &Result);
	RTLU (SK_REF, &Current);
	RTLU(SK_INT32, &loc1);
	RTLU(SK_INT32, &loc2);
	RTLU(SK_INT32, &loc3);
	RTLU(SK_INT32, &loc4);
	
	RTEAA(l_feature_name, 691, Current, 4, 0, 17116);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(691, Current, 17116);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 2, 0x10000000, 1, 0); /* loc2 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(9330, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,1);
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(11536, "x_abs", tr1))(tr1)).it_i4);
	loc2 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(2);
	RTDBGAL(Current, 1, 0x10000000, 1, 0); /* loc1 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(9330, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(2,1);
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(11537, "y_abs", tr1))(tr1)).it_i4);
	loc1 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(3);
	RTDBGAL(Current, 3, 0x10000000, 1, 0); /* loc3 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(9334, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(3,1);
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(11536, "x_abs", tr1))(tr1)).it_i4);
	loc3 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(4);
	RTDBGAL(Current, 4, 0x10000000, 1, 0); /* loc4 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(9334, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(4,1);
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(11537, "y_abs", tr1))(tr1)).it_i4);
	loc4 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(5);
	RTDBGAL(Current, 0, 0xF8000348, 0,0); /* Result */
	{
		static EIF_TYPE_INDEX typarr0[] = {0xFF01,0xFFF9,4,840,869,869,869,869,0xFFFF};
		EIF_TYPE typres0;
		static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
		
		typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(Dftype(Current), typarr0)));
		tr1 = RTLNTS(typres0.id, 5, 1);
	}
	ui4_1 = loc3;
	ti4_1 = eif_min_int32 (loc2,ui4_1);
	((EIF_TYPED_VALUE *)tr1+1)->it_i4 = ti4_1;
	ui4_1 = loc4;
	ti4_1 = eif_min_int32 (loc1,ui4_1);
	((EIF_TYPED_VALUE *)tr1+2)->it_i4 = ti4_1;
	ti4_1 = eif_abs_int32 ((EIF_INTEGER_32) (loc3 - loc2));
	((EIF_TYPED_VALUE *)tr1+3)->it_i4 = ti4_1;
	ti4_1 = eif_abs_int32 ((EIF_INTEGER_32) (loc4 - loc1));
	((EIF_TYPED_VALUE *)tr1+4)->it_i4 = ti4_1;
	Result = (EIF_REFERENCE) RTCCL(tr1);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(6);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(6);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ui4_1
}

/* {EV_FIGURE_ARC}._invariant */
void F692_29574 (EIF_REFERENCE Current, int where)
{
	GTCX
	char *l_feature_name = "_invariant";
	RTEX;
	EIF_REAL_64 tr8_1;
	EIF_REAL_64 tr8_2;
	EIF_REAL_64 tr8_3;
	EIF_BOOLEAN tb1;
	RTCDT;
	RTLD;
	RTDA;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_VOID, NULL);
	RTLU (SK_REF, &Current);
	RTEAINV(l_feature_name, 691, Current, 0, 29573);
	RTSA(dtype);
	RTME(dtype, 0);
	RTIT("start_angle_within_bounds", Current);
	tb1 = '\0';
	tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
	tr8_2 = (EIF_REAL_64) (((EIF_INTEGER_32) 0L));
	if ((EIF_BOOLEAN) (tr8_1 >= tr8_2)) {
		tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11757, dtype));
		tr8_2 = (EIF_REAL_64) (((EIF_INTEGER_32) 2L));
		tr8_3 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6803, dtype))(Current)).it_r8);
		tb1 = (EIF_BOOLEAN) (tr8_1 <= (EIF_REAL_64) (tr8_2 * tr8_3));
	}
	if (tb1) {
		RTCK;
	} else {
		RTCF;
	}
	RTIT("aperture_within_bounds", Current);
	tb1 = '\0';
	tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11758, dtype));
	tr8_2 = (EIF_REAL_64) (((EIF_INTEGER_32) 0L));
	if ((EIF_BOOLEAN) (tr8_1 >= tr8_2)) {
		tr8_1 = *(EIF_REAL_64 *)(Current + RTWA(11758, dtype));
		tr8_2 = (EIF_REAL_64) (((EIF_INTEGER_32) 2L));
		tr8_3 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6803, dtype))(Current)).it_r8);
		tb1 = (EIF_BOOLEAN) (tr8_1 <= (EIF_REAL_64) (tr8_2 * tr8_3));
	}
	if (tb1) {
		RTCK;
	} else {
		RTCF;
	}
	RTLO(2);
	RTMD(0);
	RTLE;
	RTEE;
}

void EIF_Minit692 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
