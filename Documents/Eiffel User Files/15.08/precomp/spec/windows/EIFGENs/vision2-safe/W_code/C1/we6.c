/*
 * Code for class WEL_CB_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F6_49(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_50(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_51(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_52(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_53(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_54(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_55(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_56(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_57(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_58(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_59(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_60(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_61(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_62(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_63(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_64(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_65(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_66(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_67(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_68(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_69(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_70(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_71(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_72(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_73(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_74(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_75(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_76(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_77(EIF_REFERENCE);
extern EIF_TYPED_VALUE F6_78(EIF_REFERENCE);
extern void EIF_Minit6(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_CB_CONSTANTS}.cb_geteditsel */
EIF_TYPED_VALUE F6_49 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 320L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_limittext */
EIF_TYPED_VALUE F6_50 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 321L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_seteditsel */
EIF_TYPED_VALUE F6_51 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 322L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_addstring */
EIF_TYPED_VALUE F6_52 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 323L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_deletestring */
EIF_TYPED_VALUE F6_53 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 324L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_dir */
EIF_TYPED_VALUE F6_54 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 325L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_getcount */
EIF_TYPED_VALUE F6_55 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 326L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_getcursel */
EIF_TYPED_VALUE F6_56 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 327L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_getlbtext */
EIF_TYPED_VALUE F6_57 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 328L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_getlbtextlen */
EIF_TYPED_VALUE F6_58 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 329L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_insertstring */
EIF_TYPED_VALUE F6_59 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 330L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_resetcontent */
EIF_TYPED_VALUE F6_60 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 331L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_findstring */
EIF_TYPED_VALUE F6_61 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 332L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_selectstring */
EIF_TYPED_VALUE F6_62 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 333L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_setcursel */
EIF_TYPED_VALUE F6_63 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 334L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_showdropdown */
EIF_TYPED_VALUE F6_64 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 335L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_getitemdata */
EIF_TYPED_VALUE F6_65 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 336L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_setitemdata */
EIF_TYPED_VALUE F6_66 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 337L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_getdroppedcontrolrect */
EIF_TYPED_VALUE F6_67 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 338L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_setitemheight */
EIF_TYPED_VALUE F6_68 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 339L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_getitemheight */
EIF_TYPED_VALUE F6_69 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 340L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_setextendedui */
EIF_TYPED_VALUE F6_70 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 341L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_getextendedui */
EIF_TYPED_VALUE F6_71 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 342L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_getdroppedstate */
EIF_TYPED_VALUE F6_72 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 343L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_findstringexact */
EIF_TYPED_VALUE F6_73 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 344L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_okay */
EIF_TYPED_VALUE F6_74 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_err */
EIF_TYPED_VALUE F6_75 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -1L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_errspace */
EIF_TYPED_VALUE F6_76 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -2L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_gettopindex */
EIF_TYPED_VALUE F6_77 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 347L);
	return r;
}

/* {WEL_CB_CONSTANTS}.cb_settopindex */
EIF_TYPED_VALUE F6_78 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 348L);
	return r;
}

void EIF_Minit6 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
