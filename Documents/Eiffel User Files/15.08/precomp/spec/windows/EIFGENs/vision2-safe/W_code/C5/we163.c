/*
 * Code for class WEL_OFN_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F163_4021(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4022(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4023(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4024(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4025(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4026(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4027(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4028(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4029(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4030(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4031(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4032(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4033(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4034(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4035(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4036(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4037(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4038(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4039(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4040(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4041(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4042(EIF_REFERENCE);
extern EIF_TYPED_VALUE F163_4043(EIF_REFERENCE);
extern void EIF_Minit163(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_OFN_CONSTANTS}.ofn_readonly */
EIF_TYPED_VALUE F163_4021 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_overwriteprompt */
EIF_TYPED_VALUE F163_4022 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_hidereadonly */
EIF_TYPED_VALUE F163_4023 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_nochangedir */
EIF_TYPED_VALUE F163_4024 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_showhelp */
EIF_TYPED_VALUE F163_4025 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_enablehook */
EIF_TYPED_VALUE F163_4026 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_enabletemplate */
EIF_TYPED_VALUE F163_4027 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_enabletemplatehandle */
EIF_TYPED_VALUE F163_4028 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_novalidate */
EIF_TYPED_VALUE F163_4029 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_allowmultiselect */
EIF_TYPED_VALUE F163_4030 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_extensiondifferent */
EIF_TYPED_VALUE F163_4031 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1024L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_pathmustexist */
EIF_TYPED_VALUE F163_4032 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2048L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_filemustexist */
EIF_TYPED_VALUE F163_4033 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4096L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_createprompt */
EIF_TYPED_VALUE F163_4034 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8192L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_shareaware */
EIF_TYPED_VALUE F163_4035 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16384L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_noreadonlyreturn */
EIF_TYPED_VALUE F163_4036 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32768L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_notestfilecreate */
EIF_TYPED_VALUE F163_4037 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 65536L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_nonetworkbutton */
EIF_TYPED_VALUE F163_4038 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 131072L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_nolongnames */
EIF_TYPED_VALUE F163_4039 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 262144L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_sharefallthrough */
EIF_TYPED_VALUE F163_4040 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_sharenowarn */
EIF_TYPED_VALUE F163_4041 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_sharewarn */
EIF_TYPED_VALUE F163_4042 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_OFN_CONSTANTS}.ofn_explorer */
EIF_TYPED_VALUE F163_4043 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 524288L);
	return r;
}

void EIF_Minit163 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
