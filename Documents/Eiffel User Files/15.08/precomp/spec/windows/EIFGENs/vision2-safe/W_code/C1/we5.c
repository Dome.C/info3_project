/*
 * Code for class WEL_CBEIF_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F5_42(EIF_REFERENCE);
extern EIF_TYPED_VALUE F5_43(EIF_REFERENCE);
extern EIF_TYPED_VALUE F5_44(EIF_REFERENCE);
extern EIF_TYPED_VALUE F5_45(EIF_REFERENCE);
extern EIF_TYPED_VALUE F5_46(EIF_REFERENCE);
extern EIF_TYPED_VALUE F5_47(EIF_REFERENCE);
extern EIF_TYPED_VALUE F5_48(EIF_REFERENCE);
extern void EIF_Minit5(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_CBEIF_CONSTANTS}.cbeif_text */
EIF_TYPED_VALUE F5_42 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_CBEIF_CONSTANTS}.cbeif_image */
EIF_TYPED_VALUE F5_43 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_CBEIF_CONSTANTS}.cbeif_selectedimage */
EIF_TYPED_VALUE F5_44 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_CBEIF_CONSTANTS}.cbeif_overlay */
EIF_TYPED_VALUE F5_45 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_CBEIF_CONSTANTS}.cbeif_indent */
EIF_TYPED_VALUE F5_46 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_CBEIF_CONSTANTS}.cbeif_di_setitem */
EIF_TYPED_VALUE F5_47 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 268435456L);
	return r;
}

/* {WEL_CBEIF_CONSTANTS}.cbeif_lparam */
EIF_TYPED_VALUE F5_48 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

void EIF_Minit5 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
