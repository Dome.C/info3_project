/*
 * Code for class WEL_CBN_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F14_154(EIF_REFERENCE);
extern EIF_TYPED_VALUE F14_155(EIF_REFERENCE);
extern EIF_TYPED_VALUE F14_156(EIF_REFERENCE);
extern EIF_TYPED_VALUE F14_157(EIF_REFERENCE);
extern EIF_TYPED_VALUE F14_158(EIF_REFERENCE);
extern EIF_TYPED_VALUE F14_159(EIF_REFERENCE);
extern EIF_TYPED_VALUE F14_160(EIF_REFERENCE);
extern EIF_TYPED_VALUE F14_161(EIF_REFERENCE);
extern EIF_TYPED_VALUE F14_162(EIF_REFERENCE);
extern EIF_TYPED_VALUE F14_163(EIF_REFERENCE);
extern EIF_TYPED_VALUE F14_164(EIF_REFERENCE);
extern void EIF_Minit14(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_CBN_CONSTANTS}.cbn_errspace */
EIF_TYPED_VALUE F14_154 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -1L);
	return r;
}

/* {WEL_CBN_CONSTANTS}.cbn_selchange */
EIF_TYPED_VALUE F14_155 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_CBN_CONSTANTS}.cbn_dblclk */
EIF_TYPED_VALUE F14_156 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_CBN_CONSTANTS}.cbn_setfocus */
EIF_TYPED_VALUE F14_157 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_CBN_CONSTANTS}.cbn_killfocus */
EIF_TYPED_VALUE F14_158 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_CBN_CONSTANTS}.cbn_editchange */
EIF_TYPED_VALUE F14_159 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

/* {WEL_CBN_CONSTANTS}.cbn_editupdate */
EIF_TYPED_VALUE F14_160 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 6L);
	return r;
}

/* {WEL_CBN_CONSTANTS}.cbn_dropdown */
EIF_TYPED_VALUE F14_161 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 7L);
	return r;
}

/* {WEL_CBN_CONSTANTS}.cbn_closeup */
EIF_TYPED_VALUE F14_162 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_CBN_CONSTANTS}.cbn_selendok */
EIF_TYPED_VALUE F14_163 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 9L);
	return r;
}

/* {WEL_CBN_CONSTANTS}.cbn_selendcancel */
EIF_TYPED_VALUE F14_164 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 10L);
	return r;
}

void EIF_Minit14 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
