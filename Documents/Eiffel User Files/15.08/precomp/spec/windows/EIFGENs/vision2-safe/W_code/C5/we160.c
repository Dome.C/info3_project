/*
 * Code for class WEL_LVS_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F160_3973(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3974(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3975(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3976(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3977(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3978(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3979(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3980(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3981(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3982(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3983(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3984(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3985(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3986(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3987(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3988(EIF_REFERENCE);
extern EIF_TYPED_VALUE F160_3989(EIF_REFERENCE);
extern void EIF_Minit160(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_LVS_CONSTANTS}.lvs_alignleft */
EIF_TYPED_VALUE F160_3973 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2048L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_aligntop */
EIF_TYPED_VALUE F160_3974 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_autoarrange */
EIF_TYPED_VALUE F160_3975 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_editlabels */
EIF_TYPED_VALUE F160_3976 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_icon */
EIF_TYPED_VALUE F160_3977 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_list */
EIF_TYPED_VALUE F160_3978 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_nocolumnheader */
EIF_TYPED_VALUE F160_3979 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16384L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_nolabelwrap */
EIF_TYPED_VALUE F160_3980 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_noscroll */
EIF_TYPED_VALUE F160_3981 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8192L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_ownerdrawfixed */
EIF_TYPED_VALUE F160_3982 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1024L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_report */
EIF_TYPED_VALUE F160_3983 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_shareimagelists */
EIF_TYPED_VALUE F160_3984 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_showselalways */
EIF_TYPED_VALUE F160_3985 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_singlesel */
EIF_TYPED_VALUE F160_3986 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_smallicon */
EIF_TYPED_VALUE F160_3987 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_sortascending */
EIF_TYPED_VALUE F160_3988 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_LVS_CONSTANTS}.lvs_sortdescending */
EIF_TYPED_VALUE F160_3989 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

void EIF_Minit160 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
