/*
 * Code for class WEL_RASTER_CAPABILITIES_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F157_3939(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3940(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3941(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3942(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3943(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3944(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3945(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3946(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3947(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3948(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3949(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3950(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3951(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3952(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3953(EIF_REFERENCE);
extern EIF_TYPED_VALUE F157_3954(EIF_REFERENCE);
extern void EIF_Minit157(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_bitblt */
EIF_TYPED_VALUE F157_3939 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_bitblt";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4046);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4046);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_BITBLT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_banding */
EIF_TYPED_VALUE F157_3940 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_banding";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4047);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4047);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_BANDING;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_scaling */
EIF_TYPED_VALUE F157_3941 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_scaling";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4048);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4048);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_SCALING;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_bitmap64 */
EIF_TYPED_VALUE F157_3942 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_bitmap64";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4049);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4049);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_BITMAP64;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_gdi20_output */
EIF_TYPED_VALUE F157_3943 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_gdi20_output";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4050);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4050);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_GDI20_OUTPUT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_gdi20_state */
EIF_TYPED_VALUE F157_3944 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_gdi20_state";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4051);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4051);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_GDI20_STATE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_savebitmap */
EIF_TYPED_VALUE F157_3945 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_savebitmap";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4052);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4052);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_SAVEBITMAP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_di_bitmap */
EIF_TYPED_VALUE F157_3946 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_di_bitmap";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4053);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4053);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_DI_BITMAP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_palette */
EIF_TYPED_VALUE F157_3947 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_palette";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4054);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4054);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_PALETTE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_dibtodev */
EIF_TYPED_VALUE F157_3948 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_dibtodev";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4055);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4055);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_DIBTODEV;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_bigfont */
EIF_TYPED_VALUE F157_3949 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_bigfont";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4056);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4056);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_BIGFONT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_stretchblt */
EIF_TYPED_VALUE F157_3950 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_stretchblt";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4057);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4057);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_STRETCHBLT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_floodfill */
EIF_TYPED_VALUE F157_3951 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_floodfill";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4058);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4058);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_FLOODFILL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_stretchdib */
EIF_TYPED_VALUE F157_3952 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_stretchdib";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4059);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4059);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_STRETCHDIB;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_op_dx_output */
EIF_TYPED_VALUE F157_3953 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_op_dx_output";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4060);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4060);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_OP_DX_OUTPUT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RASTER_CAPABILITIES_CONSTANTS}.rc_devbits */
EIF_TYPED_VALUE F157_3954 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rc_devbits";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 156, Current, 0, 0, 4061);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(156, Current, 4061);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RC_DEVBITS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit157 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
