/*
 * Code for class WEL_TCS_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F177_4343(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4344(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4345(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4346(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4347(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4348(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4349(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4350(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4351(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4352(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4353(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4354(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4355(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4356(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4357(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4358(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4359(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4360(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4361(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4362(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4363(EIF_REFERENCE);
extern EIF_TYPED_VALUE F177_4364(EIF_REFERENCE);
extern void EIF_Minit177(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_TCS_CONSTANTS}.tcs_bottom */
EIF_TYPED_VALUE F177_4343 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_buttons */
EIF_TYPED_VALUE F177_4344 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_fixedwidth */
EIF_TYPED_VALUE F177_4345 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1024L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_flatbuttons */
EIF_TYPED_VALUE F177_4346 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_focusnever */
EIF_TYPED_VALUE F177_4347 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32768L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_focusonbuttondown */
EIF_TYPED_VALUE F177_4348 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4096L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_forceiconleft */
EIF_TYPED_VALUE F177_4349 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_forcelabelleft */
EIF_TYPED_VALUE F177_4350 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_hottrack */
EIF_TYPED_VALUE F177_4351 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_multiline */
EIF_TYPED_VALUE F177_4352 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_multiselect */
EIF_TYPED_VALUE F177_4353 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_ownerdrawfixed */
EIF_TYPED_VALUE F177_4354 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8192L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_raggedright */
EIF_TYPED_VALUE F177_4355 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2048L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_right */
EIF_TYPED_VALUE F177_4356 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_rightjustify */
EIF_TYPED_VALUE F177_4357 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_scrollopposite */
EIF_TYPED_VALUE F177_4358 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_singleline */
EIF_TYPED_VALUE F177_4359 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_tabs */
EIF_TYPED_VALUE F177_4360 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_tooltips */
EIF_TYPED_VALUE F177_4361 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16384L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_vertical */
EIF_TYPED_VALUE F177_4362 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_ex_flatseparators */
EIF_TYPED_VALUE F177_4363 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_TCS_CONSTANTS}.tcs_ex_registerdrop */
EIF_TYPED_VALUE F177_4364 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

void EIF_Minit177 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
