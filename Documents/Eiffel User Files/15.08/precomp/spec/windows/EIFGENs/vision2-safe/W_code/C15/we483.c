/*
 * Code for class WEL_THEME_PART_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F483_8314(EIF_REFERENCE);
extern EIF_TYPED_VALUE F483_8315(EIF_REFERENCE);
extern EIF_TYPED_VALUE F483_8316(EIF_REFERENCE);
extern EIF_TYPED_VALUE F483_8317(EIF_REFERENCE);
extern EIF_TYPED_VALUE F483_8318(EIF_REFERENCE);
extern EIF_TYPED_VALUE F483_8319(EIF_REFERENCE);
extern EIF_TYPED_VALUE F483_8320(EIF_REFERENCE);
extern EIF_TYPED_VALUE F483_8321(EIF_REFERENCE);
extern EIF_TYPED_VALUE F483_8322(EIF_REFERENCE);
extern EIF_TYPED_VALUE F483_8323(EIF_REFERENCE);
extern EIF_TYPED_VALUE F483_8324(EIF_REFERENCE);
extern EIF_TYPED_VALUE F483_8325(EIF_REFERENCE);
extern EIF_TYPED_VALUE F483_8326(EIF_REFERENCE);
extern EIF_TYPED_VALUE F483_8327(EIF_REFERENCE);
extern void EIF_Minit483(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_THEME_PART_CONSTANTS}.bp_pushbutton */
EIF_TYPED_VALUE F483_8314 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_THEME_PART_CONSTANTS}.bp_groupbox */
EIF_TYPED_VALUE F483_8315 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_THEME_PART_CONSTANTS}.tabp_body */
EIF_TYPED_VALUE F483_8316 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 10L);
	return r;
}

/* {WEL_THEME_PART_CONSTANTS}.tabp_item */
EIF_TYPED_VALUE F483_8317 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_THEME_PART_CONSTANTS}.tp_button */
EIF_TYPED_VALUE F483_8318 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_THEME_PART_CONSTANTS}.tp_dropdownbutton */
EIF_TYPED_VALUE F483_8319 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_THEME_PART_CONSTANTS}.tp_splitbutton */
EIF_TYPED_VALUE F483_8320 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_THEME_PART_CONSTANTS}.tp_splitbuttondropdown */
EIF_TYPED_VALUE F483_8321 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_THEME_PART_CONSTANTS}.tp_separator */
EIF_TYPED_VALUE F483_8322 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

/* {WEL_THEME_PART_CONSTANTS}.tp_separatorvert */
EIF_TYPED_VALUE F483_8323 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 6L);
	return r;
}

/* {WEL_THEME_PART_CONSTANTS}.tabp_tabitem */
EIF_TYPED_VALUE F483_8324 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_THEME_PART_CONSTANTS}.tabp_tabitemleftedge */
EIF_TYPED_VALUE F483_8325 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_THEME_PART_CONSTANTS}.tabp_tabitemrightedge */
EIF_TYPED_VALUE F483_8326 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_THEME_PART_CONSTANTS}.tabp_tabitembothedge */
EIF_TYPED_VALUE F483_8327 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

void EIF_Minit483 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
