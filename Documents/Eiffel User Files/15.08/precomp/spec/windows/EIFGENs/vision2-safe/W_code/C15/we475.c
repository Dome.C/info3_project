/*
 * Code for class WEL_HDF_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F475_8228(EIF_REFERENCE);
extern EIF_TYPED_VALUE F475_8229(EIF_REFERENCE);
extern EIF_TYPED_VALUE F475_8230(EIF_REFERENCE);
extern EIF_TYPED_VALUE F475_8231(EIF_REFERENCE);
extern EIF_TYPED_VALUE F475_8232(EIF_REFERENCE);
extern EIF_TYPED_VALUE F475_8233(EIF_REFERENCE);
extern EIF_TYPED_VALUE F475_8234(EIF_REFERENCE);
extern EIF_TYPED_VALUE F475_8235(EIF_REFERENCE);
extern EIF_TYPED_VALUE F475_8236(EIF_REFERENCE);
extern void EIF_Minit475(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_HDF_CONSTANTS}.hdf_center */
EIF_TYPED_VALUE F475_8228 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdf_center";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 474, Current, 0, 0, 8052);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(474, Current, 8052);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDF_CENTER;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDF_CONSTANTS}.hdf_left */
EIF_TYPED_VALUE F475_8229 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdf_left";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 474, Current, 0, 0, 8053);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(474, Current, 8053);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDF_LEFT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDF_CONSTANTS}.hdf_right */
EIF_TYPED_VALUE F475_8230 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdf_right";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 474, Current, 0, 0, 8054);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(474, Current, 8054);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDF_RIGHT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDF_CONSTANTS}.hdf_justify_mask */
EIF_TYPED_VALUE F475_8231 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdf_justify_mask";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 474, Current, 0, 0, 8055);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(474, Current, 8055);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDF_JUSTIFYMASK;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDF_CONSTANTS}.hdf_owner_draw */
EIF_TYPED_VALUE F475_8232 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdf_owner_draw";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 474, Current, 0, 0, 8056);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(474, Current, 8056);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDF_OWNERDRAW;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDF_CONSTANTS}.hdf_bitmap */
EIF_TYPED_VALUE F475_8233 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdf_bitmap";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 474, Current, 0, 0, 8057);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(474, Current, 8057);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDF_BITMAP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDF_CONSTANTS}.hdf_string */
EIF_TYPED_VALUE F475_8234 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdf_string";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 474, Current, 0, 0, 8058);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(474, Current, 8058);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDF_STRING;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDF_CONSTANTS}.hdf_image */
EIF_TYPED_VALUE F475_8235 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdf_image";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 474, Current, 0, 0, 8059);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(474, Current, 8059);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDF_IMAGE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDF_CONSTANTS}.hdf_rtl_reading */
EIF_TYPED_VALUE F475_8236 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdf_rtl_reading";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 474, Current, 0, 0, 8060);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(474, Current, 8060);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDF_RTLREADING;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit475 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
