/*
 * Code for class WEL_DEVICE_TECHNOLOGY_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F96_1241(EIF_REFERENCE);
extern EIF_TYPED_VALUE F96_1242(EIF_REFERENCE);
extern EIF_TYPED_VALUE F96_1243(EIF_REFERENCE);
extern EIF_TYPED_VALUE F96_1244(EIF_REFERENCE);
extern EIF_TYPED_VALUE F96_1245(EIF_REFERENCE);
extern EIF_TYPED_VALUE F96_1246(EIF_REFERENCE);
extern EIF_TYPED_VALUE F96_1247(EIF_REFERENCE);
extern void EIF_Minit96(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_DEVICE_TECHNOLOGY_CONSTANTS}.dt_plotter */
EIF_TYPED_VALUE F96_1241 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dt_plotter";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 95, Current, 0, 0, 1262);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(95, Current, 1262);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DT_PLOTTER;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DEVICE_TECHNOLOGY_CONSTANTS}.dt_rasdisplay */
EIF_TYPED_VALUE F96_1242 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dt_rasdisplay";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 95, Current, 0, 0, 1263);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(95, Current, 1263);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DT_RASDISPLAY;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DEVICE_TECHNOLOGY_CONSTANTS}.dt_rasprinter */
EIF_TYPED_VALUE F96_1243 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dt_rasprinter";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 95, Current, 0, 0, 1264);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(95, Current, 1264);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DT_RASPRINTER;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DEVICE_TECHNOLOGY_CONSTANTS}.dt_rascamera */
EIF_TYPED_VALUE F96_1244 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dt_rascamera";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 95, Current, 0, 0, 1265);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(95, Current, 1265);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DT_RASCAMERA;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DEVICE_TECHNOLOGY_CONSTANTS}.dt_charstream */
EIF_TYPED_VALUE F96_1245 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dt_charstream";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 95, Current, 0, 0, 1266);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(95, Current, 1266);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DT_CHARSTREAM;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DEVICE_TECHNOLOGY_CONSTANTS}.dt_metafile */
EIF_TYPED_VALUE F96_1246 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dt_metafile";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 95, Current, 0, 0, 1267);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(95, Current, 1267);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DT_METAFILE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DEVICE_TECHNOLOGY_CONSTANTS}.dt_dispfile */
EIF_TYPED_VALUE F96_1247 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dt_dispfile";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 95, Current, 0, 0, 1268);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(95, Current, 1268);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DT_DISPFILE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit96 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
