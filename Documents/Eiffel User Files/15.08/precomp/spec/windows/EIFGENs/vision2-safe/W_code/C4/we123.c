/*
 * Code for class WEL_QS_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F123_3143(EIF_REFERENCE);
extern EIF_TYPED_VALUE F123_3144(EIF_REFERENCE);
extern EIF_TYPED_VALUE F123_3145(EIF_REFERENCE);
extern EIF_TYPED_VALUE F123_3146(EIF_REFERENCE);
extern EIF_TYPED_VALUE F123_3147(EIF_REFERENCE);
extern EIF_TYPED_VALUE F123_3148(EIF_REFERENCE);
extern EIF_TYPED_VALUE F123_3149(EIF_REFERENCE);
extern EIF_TYPED_VALUE F123_3150(EIF_REFERENCE);
extern EIF_TYPED_VALUE F123_3151(EIF_REFERENCE);
extern EIF_TYPED_VALUE F123_3152(EIF_REFERENCE);
extern EIF_TYPED_VALUE F123_3153(EIF_REFERENCE);
extern EIF_TYPED_VALUE F123_3154(EIF_REFERENCE);
extern EIF_TYPED_VALUE F123_3155(EIF_REFERENCE);
extern EIF_TYPED_VALUE F123_3156(EIF_REFERENCE);
extern void EIF_Minit123(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_QS_CONSTANTS}.qs_allevents */
EIF_TYPED_VALUE F123_3143 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1215L);
	return r;
}

/* {WEL_QS_CONSTANTS}.qs_allinput */
EIF_TYPED_VALUE F123_3144 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1279L);
	return r;
}

/* {WEL_QS_CONSTANTS}.qs_allpostmessage */
EIF_TYPED_VALUE F123_3145 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_QS_CONSTANTS}.qs_hotkey */
EIF_TYPED_VALUE F123_3146 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_QS_CONSTANTS}.qs_input */
EIF_TYPED_VALUE F123_3147 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1031L);
	return r;
}

/* {WEL_QS_CONSTANTS}.qs_key */
EIF_TYPED_VALUE F123_3148 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_QS_CONSTANTS}.qs_mouse */
EIF_TYPED_VALUE F123_3149 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 6L);
	return r;
}

/* {WEL_QS_CONSTANTS}.qs_mousebutton */
EIF_TYPED_VALUE F123_3150 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_QS_CONSTANTS}.qs_mousemove */
EIF_TYPED_VALUE F123_3151 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_QS_CONSTANTS}.qs_paint */
EIF_TYPED_VALUE F123_3152 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_QS_CONSTANTS}.qs_postmessage */
EIF_TYPED_VALUE F123_3153 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_QS_CONSTANTS}.qs_rawinput */
EIF_TYPED_VALUE F123_3154 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1024L);
	return r;
}

/* {WEL_QS_CONSTANTS}.qs_sendmessage */
EIF_TYPED_VALUE F123_3155 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_QS_CONSTANTS}.qs_timer */
EIF_TYPED_VALUE F123_3156 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

void EIF_Minit123 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
