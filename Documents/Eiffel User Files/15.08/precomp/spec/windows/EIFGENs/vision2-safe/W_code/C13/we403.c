/*
 * Code for class WEL_BN_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F403_7060(EIF_REFERENCE);
extern EIF_TYPED_VALUE F403_7061(EIF_REFERENCE);
extern EIF_TYPED_VALUE F403_7062(EIF_REFERENCE);
extern EIF_TYPED_VALUE F403_7063(EIF_REFERENCE);
extern EIF_TYPED_VALUE F403_7064(EIF_REFERENCE);
extern EIF_TYPED_VALUE F403_7065(EIF_REFERENCE);
extern EIF_TYPED_VALUE F403_7066(EIF_REFERENCE);
extern EIF_TYPED_VALUE F403_7067(EIF_REFERENCE);
extern void EIF_Minit403(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_BN_CONSTANTS}.bn_clicked */
EIF_TYPED_VALUE F403_7060 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_BN_CONSTANTS}.bn_paint */
EIF_TYPED_VALUE F403_7061 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_BN_CONSTANTS}.bn_hilite */
EIF_TYPED_VALUE F403_7062 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_BN_CONSTANTS}.bn_unhilite */
EIF_TYPED_VALUE F403_7063 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_BN_CONSTANTS}.bn_disable */
EIF_TYPED_VALUE F403_7064 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_BN_CONSTANTS}.bn_doubleclicked */
EIF_TYPED_VALUE F403_7065 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

/* {WEL_BN_CONSTANTS}.bn_killfocus */
EIF_TYPED_VALUE F403_7066 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 7L);
	return r;
}

/* {WEL_BN_CONSTANTS}.bn_setfocus */
EIF_TYPED_VALUE F403_7067 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 6L);
	return r;
}

void EIF_Minit403 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
