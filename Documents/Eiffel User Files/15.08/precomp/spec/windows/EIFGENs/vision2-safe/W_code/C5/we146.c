/*
 * Code for class WEL_LVNI_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F146_3770(EIF_REFERENCE);
extern EIF_TYPED_VALUE F146_3771(EIF_REFERENCE);
extern EIF_TYPED_VALUE F146_3772(EIF_REFERENCE);
extern EIF_TYPED_VALUE F146_3773(EIF_REFERENCE);
extern EIF_TYPED_VALUE F146_3774(EIF_REFERENCE);
extern EIF_TYPED_VALUE F146_3775(EIF_REFERENCE);
extern EIF_TYPED_VALUE F146_3776(EIF_REFERENCE);
extern EIF_TYPED_VALUE F146_3777(EIF_REFERENCE);
extern EIF_TYPED_VALUE F146_3778(EIF_REFERENCE);
extern void EIF_Minit146(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_LVNI_CONSTANTS}.lvni_above */
EIF_TYPED_VALUE F146_3770 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_LVNI_CONSTANTS}.lvni_all */
EIF_TYPED_VALUE F146_3771 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_LVNI_CONSTANTS}.lvni_below */
EIF_TYPED_VALUE F146_3772 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

/* {WEL_LVNI_CONSTANTS}.lvni_toleft */
EIF_TYPED_VALUE F146_3773 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1024L);
	return r;
}

/* {WEL_LVNI_CONSTANTS}.lvni_toright */
EIF_TYPED_VALUE F146_3774 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2048L);
	return r;
}

/* {WEL_LVNI_CONSTANTS}.lvni_cut */
EIF_TYPED_VALUE F146_3775 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_LVNI_CONSTANTS}.lvni_drophilited */
EIF_TYPED_VALUE F146_3776 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_LVNI_CONSTANTS}.lvni_focused */
EIF_TYPED_VALUE F146_3777 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_LVNI_CONSTANTS}.lvni_selected */
EIF_TYPED_VALUE F146_3778 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

void EIF_Minit146 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
