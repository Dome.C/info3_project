/*
 * Code for class WEL_LVN_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F130_3602(EIF_REFERENCE);
extern EIF_TYPED_VALUE F130_3603(EIF_REFERENCE);
extern EIF_TYPED_VALUE F130_3604(EIF_REFERENCE);
extern EIF_TYPED_VALUE F130_3605(EIF_REFERENCE);
extern EIF_TYPED_VALUE F130_3606(EIF_REFERENCE);
extern EIF_TYPED_VALUE F130_3607(EIF_REFERENCE);
extern EIF_TYPED_VALUE F130_3608(EIF_REFERENCE);
extern EIF_TYPED_VALUE F130_3609(EIF_REFERENCE);
extern EIF_TYPED_VALUE F130_3610(EIF_REFERENCE);
extern EIF_TYPED_VALUE F130_3611(EIF_REFERENCE);
extern EIF_TYPED_VALUE F130_3612(EIF_REFERENCE);
extern EIF_TYPED_VALUE F130_3613(EIF_REFERENCE);
extern EIF_TYPED_VALUE F130_3614(EIF_REFERENCE);
extern void EIF_Minit130(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_LVN_CONSTANTS}.lvn_begindrag */
EIF_TYPED_VALUE F130_3602 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -109L);
	return r;
}

/* {WEL_LVN_CONSTANTS}.lvn_beginlabeledit */
EIF_TYPED_VALUE F130_3603 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -175L);
	return r;
}

/* {WEL_LVN_CONSTANTS}.lvn_beginrdrag */
EIF_TYPED_VALUE F130_3604 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -111L);
	return r;
}

/* {WEL_LVN_CONSTANTS}.lvn_columnclick */
EIF_TYPED_VALUE F130_3605 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -108L);
	return r;
}

/* {WEL_LVN_CONSTANTS}.lvn_deleteallitems */
EIF_TYPED_VALUE F130_3606 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -104L);
	return r;
}

/* {WEL_LVN_CONSTANTS}.lvn_deleteitem */
EIF_TYPED_VALUE F130_3607 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -103L);
	return r;
}

/* {WEL_LVN_CONSTANTS}.lvn_endlabeledit */
EIF_TYPED_VALUE F130_3608 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -176L);
	return r;
}

/* {WEL_LVN_CONSTANTS}.lvn_getdispinfo */
EIF_TYPED_VALUE F130_3609 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -177L);
	return r;
}

/* {WEL_LVN_CONSTANTS}.lvn_insertitem */
EIF_TYPED_VALUE F130_3610 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -102L);
	return r;
}

/* {WEL_LVN_CONSTANTS}.lvn_itemchanged */
EIF_TYPED_VALUE F130_3611 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -101L);
	return r;
}

/* {WEL_LVN_CONSTANTS}.lvn_itemchanging */
EIF_TYPED_VALUE F130_3612 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -100L);
	return r;
}

/* {WEL_LVN_CONSTANTS}.lvn_keydown */
EIF_TYPED_VALUE F130_3613 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -155L);
	return r;
}

/* {WEL_LVN_CONSTANTS}.lvn_setdispinfo */
EIF_TYPED_VALUE F130_3614 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -178L);
	return r;
}

void EIF_Minit130 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
