/*
 * Code for class WEL_HHT_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F296_5563(EIF_REFERENCE);
extern EIF_TYPED_VALUE F296_5564(EIF_REFERENCE);
extern EIF_TYPED_VALUE F296_5565(EIF_REFERENCE);
extern EIF_TYPED_VALUE F296_5566(EIF_REFERENCE);
extern EIF_TYPED_VALUE F296_5567(EIF_REFERENCE);
extern EIF_TYPED_VALUE F296_5568(EIF_REFERENCE);
extern void EIF_Minit296(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_HHT_CONSTANTS}.hht_nowhere */
EIF_TYPED_VALUE F296_5563 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hht_nowhere";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 295, Current, 0, 0, 5660);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(295, Current, 5660);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HHT_NOWHERE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HHT_CONSTANTS}.hht_on_divider */
EIF_TYPED_VALUE F296_5564 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hht_on_divider";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 295, Current, 0, 0, 5661);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(295, Current, 5661);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HHT_ONDIVIDER;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HHT_CONSTANTS}.hht_on_div_open */
EIF_TYPED_VALUE F296_5565 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hht_on_div_open";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 295, Current, 0, 0, 5662);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(295, Current, 5662);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HHT_ONDIVOPEN;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HHT_CONSTANTS}.hht_on_header */
EIF_TYPED_VALUE F296_5566 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hht_on_header";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 295, Current, 0, 0, 5663);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(295, Current, 5663);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HHT_ONHEADER;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HHT_CONSTANTS}.hht_to_left */
EIF_TYPED_VALUE F296_5567 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hht_to_left";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 295, Current, 0, 0, 5664);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(295, Current, 5664);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HHT_TOLEFT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HHT_CONSTANTS}.hht_to_right */
EIF_TYPED_VALUE F296_5568 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hht_to_right";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 295, Current, 0, 0, 5665);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(295, Current, 5665);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HHT_TORIGHT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit296 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
