/*
 * Code for class WEL_HDS_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F322_6258(EIF_REFERENCE);
extern EIF_TYPED_VALUE F322_6259(EIF_REFERENCE);
extern EIF_TYPED_VALUE F322_6260(EIF_REFERENCE);
extern void EIF_Minit322(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_HDS_CONSTANTS}.hds_buttons */
EIF_TYPED_VALUE F322_6258 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hds_buttons";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 321, Current, 0, 0, 6203);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(321, Current, 6203);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDS_BUTTONS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDS_CONSTANTS}.hds_hidden */
EIF_TYPED_VALUE F322_6259 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hds_hidden";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 321, Current, 0, 0, 6204);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(321, Current, 6204);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDS_HIDDEN;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDS_CONSTANTS}.hds_horz */
EIF_TYPED_VALUE F322_6260 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hds_horz";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 321, Current, 0, 0, 6205);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(321, Current, 6205);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDS_HORZ;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit322 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
