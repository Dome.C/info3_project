/*
 * Class WEL_BRUSH_STYLE_CONSTANTS
 */

#include "eif_macros.h"


#ifdef __cplusplus
extern "C" {
#endif

static const EIF_TYPE_INDEX egt_0_287 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_1_287 [] = {0xFF01,1551,286,0xFFFF};
static const EIF_TYPE_INDEX egt_2_287 [] = {0xFF01,286,0xFFFF};
static const EIF_TYPE_INDEX egt_3_287 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_4_287 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_5_287 [] = {0xFF01,286,0xFFFF};
static const EIF_TYPE_INDEX egt_6_287 [] = {0xFF01,286,0xFFFF};
static const EIF_TYPE_INDEX egt_7_287 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_8_287 [] = {0xFF01,126,0xFFFF};
static const EIF_TYPE_INDEX egt_9_287 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_10_287 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_11_287 [] = {0xFF01,29,0xFFFF};
static const EIF_TYPE_INDEX egt_12_287 [] = {0xFF01,286,0xFFFF};


static const struct desc_info desc_287[] = {
	{EIF_GENERIC(NULL), 0xFFFFFFFF, 0xFFFFFFFF},
	{EIF_GENERIC(egt_0_287), 0, 0xFFFFFFFF},
	{EIF_GENERIC(egt_1_287), 1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 2, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 3, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 4, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 5, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 6, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 7, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 8, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 9, 0xFFFFFFFF},
	{EIF_GENERIC(egt_2_287), 10, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 11, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 12, 0xFFFFFFFF},
	{EIF_GENERIC(egt_3_287), 13, 0xFFFFFFFF},
	{EIF_GENERIC(egt_4_287), 14, 0xFFFFFFFF},
	{EIF_GENERIC(egt_5_287), 15, 0xFFFFFFFF},
	{EIF_GENERIC(egt_6_287), 16, 0xFFFFFFFF},
	{EIF_GENERIC(egt_7_287), 17, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 18, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 19, 0xFFFFFFFF},
	{EIF_GENERIC(egt_8_287), 20, 0xFFFFFFFF},
	{EIF_GENERIC(egt_9_287), 21, 0xFFFFFFFF},
	{EIF_GENERIC(egt_10_287), 22, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 23, 0xFFFFFFFF},
	{EIF_GENERIC(egt_11_287), 24, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 25, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 26, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 27, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x023D /*286*/), 28, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06E3 /*881*/), 29, 0xFFFFFFFF},
	{EIF_GENERIC(egt_12_287), 30, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 5558, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 5559, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 5560, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 5561, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 5562, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 5563, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 5564, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 5565, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 5566, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 5567, 0xFFFFFFFF},
};
void Init287(void)
{
	IDSC(desc_287, 0, 286);
	IDSC(desc_287 + 1, 1, 286);
	IDSC(desc_287 + 32, 341, 286);
}


#ifdef __cplusplus
}
#endif
