/*
 * Code for class WEL_LVS_EX_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F152_3828(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3829(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3830(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3831(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3832(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3833(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3834(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3835(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3836(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3837(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3838(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3839(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3840(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3841(EIF_REFERENCE);
extern EIF_TYPED_VALUE F152_3842(EIF_REFERENCE);
extern void EIF_Minit152(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_checkboxes */
EIF_TYPED_VALUE F152_3828 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_flatsb */
EIF_TYPED_VALUE F152_3829 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_fullrowselect */
EIF_TYPED_VALUE F152_3830 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_gridlines */
EIF_TYPED_VALUE F152_3831 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_headerdragdrop */
EIF_TYPED_VALUE F152_3832 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_infotip */
EIF_TYPED_VALUE F152_3833 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1024L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_labeltip */
EIF_TYPED_VALUE F152_3834 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16384L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_multiworkareas */
EIF_TYPED_VALUE F152_3835 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8192L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_oneclickactivate */
EIF_TYPED_VALUE F152_3836 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_regional */
EIF_TYPED_VALUE F152_3837 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_subitemimages */
EIF_TYPED_VALUE F152_3838 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_trackselect */
EIF_TYPED_VALUE F152_3839 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_twoclickactivate */
EIF_TYPED_VALUE F152_3840 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_underlinecold */
EIF_TYPED_VALUE F152_3841 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4096L);
	return r;
}

/* {WEL_LVS_EX_CONSTANTS}.lvs_ex_underlinehot */
EIF_TYPED_VALUE F152_3842 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2048L);
	return r;
}

void EIF_Minit152 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
