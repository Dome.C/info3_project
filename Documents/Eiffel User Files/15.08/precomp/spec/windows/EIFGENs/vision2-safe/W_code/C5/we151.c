/*
 * Code for class WEL_TTM_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F151_3808(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3809(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3810(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3811(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3812(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3813(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3814(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3815(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3816(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3817(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3818(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3819(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3820(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3821(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3822(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3823(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3824(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3825(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3826(EIF_REFERENCE);
extern EIF_TYPED_VALUE F151_3827(EIF_REFERENCE);
extern void EIF_Minit151(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_TTM_CONSTANTS}.ttm_activate */
EIF_TYPED_VALUE F151_3808 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_activate";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3860);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3860);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_ACTIVATE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_addtool */
EIF_TYPED_VALUE F151_3809 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_addtool";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3861);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3861);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_ADDTOOL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_deltool */
EIF_TYPED_VALUE F151_3810 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_deltool";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3862);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3862);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_DELTOOL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_enumtools */
EIF_TYPED_VALUE F151_3811 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_enumtools";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3863);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3863);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_ENUMTOOLS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_getcurrenttool */
EIF_TYPED_VALUE F151_3812 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_getcurrenttool";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3864);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3864);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETCURRENTTOOL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_getdelaytime */
EIF_TYPED_VALUE F151_3813 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_getdelaytime";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3845);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3845);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETDELAYTIME;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_gettext */
EIF_TYPED_VALUE F151_3814 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_gettext";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3846);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3846);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETTEXT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_gettipbkcolor */
EIF_TYPED_VALUE F151_3815 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_gettipbkcolor";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3847);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3847);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETTIPBKCOLOR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_gettiptextcolor */
EIF_TYPED_VALUE F151_3816 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_gettiptextcolor";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3848);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3848);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETTIPTEXTCOLOR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_gettoolcount */
EIF_TYPED_VALUE F151_3817 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_gettoolcount";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3849);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3849);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETTOOLCOUNT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_gettoolinfo */
EIF_TYPED_VALUE F151_3818 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_gettoolinfo";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3850);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3850);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_GETTOOLINFO;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_hittest */
EIF_TYPED_VALUE F151_3819 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_hittest";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3851);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3851);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_HITTEST;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_newtoolrect */
EIF_TYPED_VALUE F151_3820 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_newtoolrect";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3852);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3852);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_NEWTOOLRECT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_relayevent */
EIF_TYPED_VALUE F151_3821 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_relayevent";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3853);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3853);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_RELAYEVENT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_setdelaytime */
EIF_TYPED_VALUE F151_3822 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_setdelaytime";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3854);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3854);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_SETDELAYTIME;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_settipbkcolor */
EIF_TYPED_VALUE F151_3823 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_settipbkcolor";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3855);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3855);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_SETTIPBKCOLOR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_settiptextcolor */
EIF_TYPED_VALUE F151_3824 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_settiptextcolor";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3856);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3856);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_SETTIPTEXTCOLOR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_settoolinfo */
EIF_TYPED_VALUE F151_3825 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_settoolinfo";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3857);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3857);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_SETTOOLINFO;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_updatetiptext */
EIF_TYPED_VALUE F151_3826 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_updatetiptext";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3858);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3858);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_UPDATETIPTEXT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTM_CONSTANTS}.ttm_windowfrompoint */
EIF_TYPED_VALUE F151_3827 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttm_windowfrompoint";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 150, Current, 0, 0, 3859);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(150, Current, 3859);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTM_WINDOWFROMPOINT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit151 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
