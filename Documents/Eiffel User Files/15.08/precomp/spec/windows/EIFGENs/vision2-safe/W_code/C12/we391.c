/*
 * Code for class WEL_IDC_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F391_6922(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6923(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6924(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6925(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6926(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6927(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6928(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6929(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6930(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6931(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6932(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6933(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6934(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6935(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6936(EIF_REFERENCE);
extern EIF_TYPED_VALUE F391_6937(EIF_REFERENCE);
extern void EIF_Minit391(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_IDC_CONSTANTS}.idc_appstarting */
EIF_TYPED_VALUE F391_6922 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_appstarting";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6831);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6831);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDC_APPSTARTING;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_arrow */
EIF_TYPED_VALUE F391_6923 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_arrow";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6832);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6832);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDC_ARROW;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_help */
EIF_TYPED_VALUE F391_6924 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_help";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6833);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6833);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDC_HELP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_hand */
EIF_TYPED_VALUE F391_6925 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_hand";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6834);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6834);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) MAKEINTRESOURCE(32649);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_ibeam */
EIF_TYPED_VALUE F391_6926 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_ibeam";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6835);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6835);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDC_IBEAM;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_no */
EIF_TYPED_VALUE F391_6927 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_no";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6836);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6836);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDC_NO;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_wait */
EIF_TYPED_VALUE F391_6928 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_wait";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6837);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6837);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDC_WAIT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_cross */
EIF_TYPED_VALUE F391_6929 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_cross";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6838);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6838);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDC_CROSS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_uparrow */
EIF_TYPED_VALUE F391_6930 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_uparrow";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6839);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6839);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDC_UPARROW;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_sizeall */
EIF_TYPED_VALUE F391_6931 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_sizeall";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6840);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6840);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDC_SIZEALL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_sizenwse */
EIF_TYPED_VALUE F391_6932 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_sizenwse";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6841);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6841);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDC_SIZENWSE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_sizenesw */
EIF_TYPED_VALUE F391_6933 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_sizenesw";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6842);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6842);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDC_SIZENESW;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_sizewe */
EIF_TYPED_VALUE F391_6934 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_sizewe";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6843);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6843);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDC_SIZEWE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_sizens */
EIF_TYPED_VALUE F391_6935 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_sizens";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6844);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(390, Current, 6844);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDC_SIZENS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_size */
EIF_TYPED_VALUE F391_6936 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_size";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6845);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 0);
	RTGC;
	RTDBGEAA(390, Current, 6845);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 0, 0x40000000, 1,0); /* Result */
	Result = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6787, Dtype(Current)))(Current)).it_p);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDC_CONSTANTS}.idc_icon */
EIF_TYPED_VALUE F391_6937 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idc_icon";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 390, Current, 0, 0, 6846);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 0);
	RTGC;
	RTDBGEAA(390, Current, 6846);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 0, 0x40000000, 1,0); /* Result */
	Result = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(6779, Dtype(Current)))(Current)).it_p);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

void EIF_Minit391 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
