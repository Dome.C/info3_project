/*
 * Code for class WEL_NIM_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F150_3803(EIF_REFERENCE);
extern EIF_TYPED_VALUE F150_3804(EIF_REFERENCE);
extern EIF_TYPED_VALUE F150_3805(EIF_REFERENCE);
extern EIF_TYPED_VALUE F150_3806(EIF_REFERENCE);
extern EIF_TYPED_VALUE F150_3807(EIF_REFERENCE);
extern void EIF_Minit150(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_NIM_CONSTANTS}.nim_add */
EIF_TYPED_VALUE F150_3803 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_NIM_CONSTANTS}.nim_modify */
EIF_TYPED_VALUE F150_3804 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_NIM_CONSTANTS}.nim_delete */
EIF_TYPED_VALUE F150_3805 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_NIM_CONSTANTS}.nim_setfocus */
EIF_TYPED_VALUE F150_3806 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_NIM_CONSTANTS}.nim_setversion */
EIF_TYPED_VALUE F150_3807 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

void EIF_Minit150 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
