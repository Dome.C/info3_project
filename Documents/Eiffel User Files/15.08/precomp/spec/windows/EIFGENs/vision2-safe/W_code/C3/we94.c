/*
 * Code for class WEL_CBEM_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F94_1228(EIF_REFERENCE);
extern EIF_TYPED_VALUE F94_1229(EIF_REFERENCE);
extern EIF_TYPED_VALUE F94_1230(EIF_REFERENCE);
extern EIF_TYPED_VALUE F94_1231(EIF_REFERENCE);
extern EIF_TYPED_VALUE F94_1232(EIF_REFERENCE);
extern EIF_TYPED_VALUE F94_1233(EIF_REFERENCE);
extern EIF_TYPED_VALUE F94_1234(EIF_REFERENCE);
extern EIF_TYPED_VALUE F94_1235(EIF_REFERENCE);
extern EIF_TYPED_VALUE F94_1236(EIF_REFERENCE);
extern EIF_TYPED_VALUE F94_1237(EIF_REFERENCE);
extern EIF_TYPED_VALUE F94_1238(EIF_REFERENCE);
extern void EIF_Minit94(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_CBEM_CONSTANTS}.cbem_insertitem */
EIF_TYPED_VALUE F94_1228 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1035L);
	return r;
}

/* {WEL_CBEM_CONSTANTS}.cbem_setimagelist */
EIF_TYPED_VALUE F94_1229 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1026L);
	return r;
}

/* {WEL_CBEM_CONSTANTS}.cbem_getimagelist */
EIF_TYPED_VALUE F94_1230 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1027L);
	return r;
}

/* {WEL_CBEM_CONSTANTS}.cbem_getitem */
EIF_TYPED_VALUE F94_1231 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1037L);
	return r;
}

/* {WEL_CBEM_CONSTANTS}.cbem_setitem */
EIF_TYPED_VALUE F94_1232 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1036L);
	return r;
}

/* {WEL_CBEM_CONSTANTS}.cbem_deleteitem */
EIF_TYPED_VALUE F94_1233 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 324L);
	return r;
}

/* {WEL_CBEM_CONSTANTS}.cbem_getcombocontrol */
EIF_TYPED_VALUE F94_1234 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1030L);
	return r;
}

/* {WEL_CBEM_CONSTANTS}.cbem_geteditcontrol */
EIF_TYPED_VALUE F94_1235 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1031L);
	return r;
}

/* {WEL_CBEM_CONSTANTS}.cbem_setexstyle */
EIF_TYPED_VALUE F94_1236 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1032L);
	return r;
}

/* {WEL_CBEM_CONSTANTS}.cbem_getexstyle */
EIF_TYPED_VALUE F94_1237 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1033L);
	return r;
}

/* {WEL_CBEM_CONSTANTS}.cbem_haseditchanged */
EIF_TYPED_VALUE F94_1238 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1034L);
	return r;
}

void EIF_Minit94 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
