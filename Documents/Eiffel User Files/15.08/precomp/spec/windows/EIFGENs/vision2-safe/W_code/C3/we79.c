/*
 * Code for class WEL_LVHT_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F79_1059(EIF_REFERENCE);
extern EIF_TYPED_VALUE F79_1060(EIF_REFERENCE);
extern EIF_TYPED_VALUE F79_1061(EIF_REFERENCE);
extern EIF_TYPED_VALUE F79_1062(EIF_REFERENCE);
extern EIF_TYPED_VALUE F79_1063(EIF_REFERENCE);
extern EIF_TYPED_VALUE F79_1064(EIF_REFERENCE);
extern EIF_TYPED_VALUE F79_1065(EIF_REFERENCE);
extern EIF_TYPED_VALUE F79_1066(EIF_REFERENCE);
extern void EIF_Minit79(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_LVHT_CONSTANTS}.lvht_above */
EIF_TYPED_VALUE F79_1059 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_LVHT_CONSTANTS}.lvht_below */
EIF_TYPED_VALUE F79_1060 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_LVHT_CONSTANTS}.lvht_nowhere */
EIF_TYPED_VALUE F79_1061 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_LVHT_CONSTANTS}.lvht_onitemicon */
EIF_TYPED_VALUE F79_1062 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_LVHT_CONSTANTS}.lvht_onitemlabel */
EIF_TYPED_VALUE F79_1063 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_LVHT_CONSTANTS}.lvht_onitemstateicon */
EIF_TYPED_VALUE F79_1064 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_LVHT_CONSTANTS}.lvht_toleft */
EIF_TYPED_VALUE F79_1065 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_LVHT_CONSTANTS}.lvht_toright */
EIF_TYPED_VALUE F79_1066 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

void EIF_Minit79 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
