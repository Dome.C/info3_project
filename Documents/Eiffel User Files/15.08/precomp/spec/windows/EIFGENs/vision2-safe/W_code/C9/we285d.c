/*
 * Class WEL_POLYGON_FILL_MODE_CONSTANTS
 */

#include "eif_macros.h"


#ifdef __cplusplus
extern "C" {
#endif

static const EIF_TYPE_INDEX egt_0_285 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_1_285 [] = {0xFF01,1551,284,0xFFFF};
static const EIF_TYPE_INDEX egt_2_285 [] = {0xFF01,284,0xFFFF};
static const EIF_TYPE_INDEX egt_3_285 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_4_285 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_5_285 [] = {0xFF01,284,0xFFFF};
static const EIF_TYPE_INDEX egt_6_285 [] = {0xFF01,284,0xFFFF};
static const EIF_TYPE_INDEX egt_7_285 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_8_285 [] = {0xFF01,126,0xFFFF};
static const EIF_TYPE_INDEX egt_9_285 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_10_285 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_11_285 [] = {0xFF01,29,0xFFFF};
static const EIF_TYPE_INDEX egt_12_285 [] = {0xFF01,284,0xFFFF};


static const struct desc_info desc_285[] = {
	{EIF_GENERIC(NULL), 0xFFFFFFFF, 0xFFFFFFFF},
	{EIF_GENERIC(egt_0_285), 0, 0xFFFFFFFF},
	{EIF_GENERIC(egt_1_285), 1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 2, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 3, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 4, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 5, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 6, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 7, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 8, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 9, 0xFFFFFFFF},
	{EIF_GENERIC(egt_2_285), 10, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 11, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 12, 0xFFFFFFFF},
	{EIF_GENERIC(egt_3_285), 13, 0xFFFFFFFF},
	{EIF_GENERIC(egt_4_285), 14, 0xFFFFFFFF},
	{EIF_GENERIC(egt_5_285), 15, 0xFFFFFFFF},
	{EIF_GENERIC(egt_6_285), 16, 0xFFFFFFFF},
	{EIF_GENERIC(egt_7_285), 17, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 18, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 19, 0xFFFFFFFF},
	{EIF_GENERIC(egt_8_285), 20, 0xFFFFFFFF},
	{EIF_GENERIC(egt_9_285), 21, 0xFFFFFFFF},
	{EIF_GENERIC(egt_10_285), 22, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 23, 0xFFFFFFFF},
	{EIF_GENERIC(egt_11_285), 24, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 25, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 26, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 27, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0239 /*284*/), 28, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06E3 /*881*/), 29, 0xFFFFFFFF},
	{EIF_GENERIC(egt_12_285), 30, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 5426, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 5427, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 5428, 0xFFFFFFFF},
};
void Init285(void)
{
	IDSC(desc_285, 0, 284);
	IDSC(desc_285 + 1, 1, 284);
	IDSC(desc_285 + 32, 375, 284);
}


#ifdef __cplusplus
}
#endif
