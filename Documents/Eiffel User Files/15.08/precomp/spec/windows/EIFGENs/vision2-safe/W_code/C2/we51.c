/*
 * Code for class WEL_CP_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F51_693(EIF_REFERENCE);
extern EIF_TYPED_VALUE F51_694(EIF_REFERENCE);
extern EIF_TYPED_VALUE F51_695(EIF_REFERENCE);
extern EIF_TYPED_VALUE F51_696(EIF_REFERENCE);
extern EIF_TYPED_VALUE F51_697(EIF_REFERENCE);
extern EIF_TYPED_VALUE F51_698(EIF_REFERENCE);
extern EIF_TYPED_VALUE F51_699(EIF_REFERENCE);
extern void EIF_Minit51(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_CP_CONSTANTS}.cp_acp */
EIF_TYPED_VALUE F51_693 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_CP_CONSTANTS}.cp_oemcp */
EIF_TYPED_VALUE F51_694 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_CP_CONSTANTS}.cp_maccp */
EIF_TYPED_VALUE F51_695 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_CP_CONSTANTS}.cp_thread_acp */
EIF_TYPED_VALUE F51_696 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_CP_CONSTANTS}.cp_symbol */
EIF_TYPED_VALUE F51_697 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 42L);
	return r;
}

/* {WEL_CP_CONSTANTS}.cp_utf7 */
EIF_TYPED_VALUE F51_698 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 65000L);
	return r;
}

/* {WEL_CP_CONSTANTS}.cp_utf8 */
EIF_TYPED_VALUE F51_699 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 65001L);
	return r;
}

void EIF_Minit51 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
