/*
 * Code for class WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F62_816(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_817(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_818(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_819(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_820(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_821(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_822(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_823(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_824(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_825(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_826(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_827(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_828(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_829(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_830(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_831(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_832(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_833(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_834(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_835(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_836(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_837(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_838(EIF_REFERENCE);
extern EIF_TYPED_VALUE F62_839(EIF_REFERENCE, EIF_TYPED_VALUE);
extern void EIF_Minit62(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_copy */
EIF_TYPED_VALUE F62_816 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_copy";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 838);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 838);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_COPY;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_paste */
EIF_TYPED_VALUE F62_817 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_paste";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 839);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 839);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_PASTE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_cut */
EIF_TYPED_VALUE F62_818 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_cut";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 840);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 840);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_CUT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_print */
EIF_TYPED_VALUE F62_819 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_print";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 841);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 841);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_PRINT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_delete */
EIF_TYPED_VALUE F62_820 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_delete";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 842);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 842);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_DELETE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_printpre */
EIF_TYPED_VALUE F62_821 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_printpre";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 843);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 843);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_PRINTPRE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_filenew */
EIF_TYPED_VALUE F62_822 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_filenew";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 844);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 844);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_FILENEW;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_properties */
EIF_TYPED_VALUE F62_823 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_properties";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 845);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 845);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_PROPERTIES;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_fileopen */
EIF_TYPED_VALUE F62_824 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_fileopen";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 846);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 846);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_FILEOPEN;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_redow */
EIF_TYPED_VALUE F62_825 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_redow";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 847);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 847);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_REDOW;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_filesave */
EIF_TYPED_VALUE F62_826 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_filesave";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 848);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 848);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_FILESAVE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_replace */
EIF_TYPED_VALUE F62_827 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_replace";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 849);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 849);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_REPLACE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_find */
EIF_TYPED_VALUE F62_828 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_find";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 850);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 850);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_FIND;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_undo */
EIF_TYPED_VALUE F62_829 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_undo";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 851);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 851);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_UNDO;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.std_help */
EIF_TYPED_VALUE F62_830 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "std_help";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 852);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 852);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) STD_HELP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.view_largeicons */
EIF_TYPED_VALUE F62_831 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "view_largeicons";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 853);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 853);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) VIEW_LARGEICONS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.view_sortname */
EIF_TYPED_VALUE F62_832 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "view_sortname";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 854);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 854);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) VIEW_SORTNAME;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.view_smallicons */
EIF_TYPED_VALUE F62_833 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "view_smallicons";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 855);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 855);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) VIEW_SMALLICONS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.view_sortsize */
EIF_TYPED_VALUE F62_834 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "view_sortsize";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 856);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 856);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) VIEW_SORTSIZE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.view_list */
EIF_TYPED_VALUE F62_835 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "view_list";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 857);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 857);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) VIEW_LIST;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.view_sortdate */
EIF_TYPED_VALUE F62_836 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "view_sortdate";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 858);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 858);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) VIEW_SORTDATE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.view_details */
EIF_TYPED_VALUE F62_837 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "view_details";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 835);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 835);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) VIEW_DETAILS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.view_sorttype */
EIF_TYPED_VALUE F62_838 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "view_sorttype";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 0, 836);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(61, Current, 836);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) VIEW_SORTTYPE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_STANDARD_TOOL_BAR_BITMAP_CONSTANTS}.valid_standard_tool_bar_bitmap_constant */
EIF_TYPED_VALUE F62_839 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x)
{
	GTCX
	char *l_feature_name = "valid_standard_tool_bar_bitmap_constant";
	RTEX;
#define arg1 arg1x.it_i4
	EIF_INTEGER_32 ti4_1;
	EIF_BOOLEAN tb1;
	EIF_BOOLEAN tb2;
	EIF_BOOLEAN tb3;
	EIF_BOOLEAN tb4;
	EIF_BOOLEAN tb5;
	EIF_BOOLEAN tb6;
	EIF_BOOLEAN tb7;
	EIF_BOOLEAN tb8;
	EIF_BOOLEAN tb9;
	EIF_BOOLEAN tb10;
	EIF_BOOLEAN tb11;
	EIF_BOOLEAN tb12;
	EIF_BOOLEAN tb13;
	EIF_BOOLEAN tb14;
	EIF_BOOLEAN tb15;
	EIF_BOOLEAN tb16;
	EIF_BOOLEAN tb17;
	EIF_BOOLEAN tb18;
	EIF_BOOLEAN tb19;
	EIF_BOOLEAN tb20;
	EIF_BOOLEAN tb21;
	EIF_BOOLEAN tb22;
	EIF_BOOLEAN Result = ((EIF_BOOLEAN) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_BOOL, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 61, Current, 0, 1, 837);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(61, Current, 837);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 0, 0x04000000, 1,0); /* Result */
	tb1 = '\01';
	tb2 = '\01';
	tb3 = '\01';
	tb4 = '\01';
	tb5 = '\01';
	tb6 = '\01';
	tb7 = '\01';
	tb8 = '\01';
	tb9 = '\01';
	tb10 = '\01';
	tb11 = '\01';
	tb12 = '\01';
	tb13 = '\01';
	tb14 = '\01';
	tb15 = '\01';
	tb16 = '\01';
	tb17 = '\01';
	tb18 = '\01';
	tb19 = '\01';
	tb20 = '\01';
	tb21 = '\01';
	tb22 = '\01';
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(815, dtype))(Current)).it_i4);
	if (!(EIF_BOOLEAN)(arg1 == ti4_1)) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(816, dtype))(Current)).it_i4);
		tb22 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb22) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(817, dtype))(Current)).it_i4);
		tb21 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb21) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(818, dtype))(Current)).it_i4);
		tb20 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb20) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(819, dtype))(Current)).it_i4);
		tb19 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb19) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(820, dtype))(Current)).it_i4);
		tb18 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb18) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(821, dtype))(Current)).it_i4);
		tb17 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb17) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(822, dtype))(Current)).it_i4);
		tb16 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb16) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(823, dtype))(Current)).it_i4);
		tb15 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb15) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(824, dtype))(Current)).it_i4);
		tb14 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb14) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(825, dtype))(Current)).it_i4);
		tb13 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb13) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(826, dtype))(Current)).it_i4);
		tb12 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb12) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(827, dtype))(Current)).it_i4);
		tb11 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb11) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(828, dtype))(Current)).it_i4);
		tb10 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb10) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(829, dtype))(Current)).it_i4);
		tb9 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb9) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(830, dtype))(Current)).it_i4);
		tb8 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb8) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(831, dtype))(Current)).it_i4);
		tb7 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb7) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(832, dtype))(Current)).it_i4);
		tb6 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb6) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(833, dtype))(Current)).it_i4);
		tb5 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb5) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(834, dtype))(Current)).it_i4);
		tb4 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb4) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(835, dtype))(Current)).it_i4);
		tb3 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb3) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(836, dtype))(Current)).it_i4);
		tb2 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	if (!tb2) {
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(837, dtype))(Current)).it_i4);
		tb1 = (EIF_BOOLEAN)(arg1 == ti4_1);
	}
	Result = (EIF_BOOLEAN) tb1;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(3);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_BOOL; r.it_b = Result; return r; }
#undef arg1
}

void EIF_Minit62 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
