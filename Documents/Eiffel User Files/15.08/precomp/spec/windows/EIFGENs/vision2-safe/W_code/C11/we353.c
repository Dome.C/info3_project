/*
 * Code for class WEL_GWL_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F353_6593(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6594(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6595(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6596(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6597(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6598(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6599(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6600(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6601(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6602(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6603(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6604(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6605(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6606(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6607(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6608(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6609(EIF_REFERENCE);
extern EIF_TYPED_VALUE F353_6610(EIF_REFERENCE);
extern void EIF_Minit353(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef INLINE_F353_6605
static EIF_INTEGER_32 inline_F353_6605 (void)
{
	return (EIF_INTEGER_32) (DWLP_MSGRESULT)
	;
}
#define INLINE_F353_6605
#endif
#ifndef INLINE_F353_6606
static EIF_INTEGER_32 inline_F353_6606 (void)
{
	return (EIF_INTEGER_32) (DWLP_DLGPROC)
	;
}
#define INLINE_F353_6606
#endif
#ifndef INLINE_F353_6607
static EIF_INTEGER_32 inline_F353_6607 (void)
{
	return (EIF_INTEGER_32) (DWLP_USER)
	;
}
#define INLINE_F353_6607
#endif

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_GWL_CONSTANTS}.gwl_exstyle */
EIF_TYPED_VALUE F353_6593 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -20L);
	return r;
}

/* {WEL_GWL_CONSTANTS}.gwl_style */
EIF_TYPED_VALUE F353_6594 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -16L);
	return r;
}

/* {WEL_GWL_CONSTANTS}.gwl_wndproc */
EIF_TYPED_VALUE F353_6595 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -4L);
	return r;
}

/* {WEL_GWL_CONSTANTS}.gwlp_wndproc */
EIF_TYPED_VALUE F353_6596 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -4L);
	return r;
}

/* {WEL_GWL_CONSTANTS}.gwl_hinstance */
EIF_TYPED_VALUE F353_6597 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -6L);
	return r;
}

/* {WEL_GWL_CONSTANTS}.gwlp_hinstance */
EIF_TYPED_VALUE F353_6598 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -6L);
	return r;
}

/* {WEL_GWL_CONSTANTS}.gwl_hwndparent */
EIF_TYPED_VALUE F353_6599 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -8L);
	return r;
}

/* {WEL_GWL_CONSTANTS}.gwlp_hwndparent */
EIF_TYPED_VALUE F353_6600 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -8L);
	return r;
}

/* {WEL_GWL_CONSTANTS}.gwl_id */
EIF_TYPED_VALUE F353_6601 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -12L);
	return r;
}

/* {WEL_GWL_CONSTANTS}.gwlp_id */
EIF_TYPED_VALUE F353_6602 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -12L);
	return r;
}

/* {WEL_GWL_CONSTANTS}.gwl_userdata */
EIF_TYPED_VALUE F353_6603 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -21L);
	return r;
}

/* {WEL_GWL_CONSTANTS}.gwlp_userdata */
EIF_TYPED_VALUE F353_6604 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -21L);
	return r;
}

/* {WEL_GWL_CONSTANTS}.dwlp_msgresult */
EIF_TYPED_VALUE F353_6605 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dwlp_msgresult";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 352, Current, 0, 0, 6519);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(352, Current, 6519);
	RTIV(Current, RTAL);
	Result = inline_F353_6605 ();
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_GWL_CONSTANTS}.dwlp_dlgproc */
EIF_TYPED_VALUE F353_6606 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dwlp_dlgproc";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 352, Current, 0, 0, 6520);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(352, Current, 6520);
	RTIV(Current, RTAL);
	Result = inline_F353_6606 ();
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_GWL_CONSTANTS}.dwlp_user */
EIF_TYPED_VALUE F353_6607 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dwlp_user";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 352, Current, 0, 0, 6521);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(352, Current, 6521);
	RTIV(Current, RTAL);
	Result = inline_F353_6607 ();
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_GWL_CONSTANTS}.dwl_msgresult */
EIF_TYPED_VALUE F353_6608 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dwl_msgresult";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 352, Current, 0, 0, 6522);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 0);
	RTGC;
	RTDBGEAA(352, Current, 6522);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 0, 0x10000000, 1,0); /* Result */
	Result = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_GWL_CONSTANTS}.dwl_dlgproc */
EIF_TYPED_VALUE F353_6609 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dwl_dlgproc";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 352, Current, 0, 0, 6523);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 0);
	RTGC;
	RTDBGEAA(352, Current, 6523);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 0, 0x10000000, 1,0); /* Result */
	Result = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_GWL_CONSTANTS}.dwl_user */
EIF_TYPED_VALUE F353_6610 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dwl_user";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 352, Current, 0, 0, 6524);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 0);
	RTGC;
	RTDBGEAA(352, Current, 6524);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 0, 0x10000000, 1,0); /* Result */
	Result = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit353 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
