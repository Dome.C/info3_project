/*
 * Code for class WEL_REGISTRY_ACCESS_MODE
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F273_5197(EIF_REFERENCE);
extern EIF_TYPED_VALUE F273_5198(EIF_REFERENCE);
extern EIF_TYPED_VALUE F273_5199(EIF_REFERENCE);
extern EIF_TYPED_VALUE F273_5200(EIF_REFERENCE);
extern EIF_TYPED_VALUE F273_5201(EIF_REFERENCE);
extern EIF_TYPED_VALUE F273_5202(EIF_REFERENCE);
extern EIF_TYPED_VALUE F273_5203(EIF_REFERENCE);
extern EIF_TYPED_VALUE F273_5204(EIF_REFERENCE);
extern EIF_TYPED_VALUE F273_5205(EIF_REFERENCE);
extern EIF_TYPED_VALUE F273_5206(EIF_REFERENCE);
extern void EIF_Minit273(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_REGISTRY_ACCESS_MODE}.key_create_link */
EIF_TYPED_VALUE F273_5197 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "key_create_link";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 272, Current, 0, 0, 5298);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(272, Current, 5298);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) KEY_CREATE_LINK;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_REGISTRY_ACCESS_MODE}.key_create_sub_key */
EIF_TYPED_VALUE F273_5198 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "key_create_sub_key";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 272, Current, 0, 0, 5299);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(272, Current, 5299);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) KEY_CREATE_SUB_KEY;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_REGISTRY_ACCESS_MODE}.key_enumerate_sub_keys */
EIF_TYPED_VALUE F273_5199 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "key_enumerate_sub_keys";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 272, Current, 0, 0, 5300);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(272, Current, 5300);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) KEY_ENUMERATE_SUB_KEYS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_REGISTRY_ACCESS_MODE}.key_execute */
EIF_TYPED_VALUE F273_5200 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "key_execute";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 272, Current, 0, 0, 5301);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(272, Current, 5301);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) KEY_EXECUTE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_REGISTRY_ACCESS_MODE}.key_notify */
EIF_TYPED_VALUE F273_5201 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "key_notify";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 272, Current, 0, 0, 5302);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(272, Current, 5302);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) KEY_NOTIFY;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_REGISTRY_ACCESS_MODE}.key_query_value */
EIF_TYPED_VALUE F273_5202 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "key_query_value";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 272, Current, 0, 0, 5303);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(272, Current, 5303);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) KEY_QUERY_VALUE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_REGISTRY_ACCESS_MODE}.key_set_value */
EIF_TYPED_VALUE F273_5203 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "key_set_value";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 272, Current, 0, 0, 5304);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(272, Current, 5304);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) KEY_SET_VALUE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_REGISTRY_ACCESS_MODE}.key_all_access */
EIF_TYPED_VALUE F273_5204 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "key_all_access";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 272, Current, 0, 0, 5305);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(272, Current, 5305);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) KEY_ALL_ACCESS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_REGISTRY_ACCESS_MODE}.key_read */
EIF_TYPED_VALUE F273_5205 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "key_read";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 272, Current, 0, 0, 5306);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(272, Current, 5306);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) KEY_READ;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_REGISTRY_ACCESS_MODE}.key_write */
EIF_TYPED_VALUE F273_5206 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "key_write";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 272, Current, 0, 0, 5307);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(272, Current, 5307);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) KEY_WRITE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit273 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
