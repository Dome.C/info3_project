/*
 * Code for class WEL_TCM_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F182_4401(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4402(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4403(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4404(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4405(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4406(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4407(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4408(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4409(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4410(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4411(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4412(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4413(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4414(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4415(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4416(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4417(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4418(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4419(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4420(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4421(EIF_REFERENCE);
extern EIF_TYPED_VALUE F182_4422(EIF_REFERENCE);
extern void EIF_Minit182(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_TCM_CONSTANTS}.tcm_adjustrect */
EIF_TYPED_VALUE F182_4401 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4904L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_deleteallitems */
EIF_TYPED_VALUE F182_4402 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4873L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_deleteitem */
EIF_TYPED_VALUE F182_4403 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4872L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_getcurfocus */
EIF_TYPED_VALUE F182_4404 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4911L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_getcursel */
EIF_TYPED_VALUE F182_4405 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4875L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_getimagelist */
EIF_TYPED_VALUE F182_4406 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4866L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_getitem */
EIF_TYPED_VALUE F182_4407 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4924L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_getitemcount */
EIF_TYPED_VALUE F182_4408 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4868L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_getitemrect */
EIF_TYPED_VALUE F182_4409 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4874L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_getrowcount */
EIF_TYPED_VALUE F182_4410 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4908L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_gettooltips */
EIF_TYPED_VALUE F182_4411 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4909L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_hittest */
EIF_TYPED_VALUE F182_4412 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4877L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_insertitem */
EIF_TYPED_VALUE F182_4413 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4926L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_removeimage */
EIF_TYPED_VALUE F182_4414 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4906L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_setcursel */
EIF_TYPED_VALUE F182_4415 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4876L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_setimagelist */
EIF_TYPED_VALUE F182_4416 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4867L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_setitem */
EIF_TYPED_VALUE F182_4417 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4925L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_setitemextra */
EIF_TYPED_VALUE F182_4418 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4878L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_setitemsize */
EIF_TYPED_VALUE F182_4419 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4905L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_setpadding */
EIF_TYPED_VALUE F182_4420 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4907L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_settooltips */
EIF_TYPED_VALUE F182_4421 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4910L);
	return r;
}

/* {WEL_TCM_CONSTANTS}.tcm_highlightitem */
EIF_TYPED_VALUE F182_4422 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4915L);
	return r;
}

void EIF_Minit182 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
