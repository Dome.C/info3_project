/*
 * Code for class WEL_INPUT_EVENT_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F128_3563(EIF_REFERENCE);
extern EIF_TYPED_VALUE F128_3564(EIF_REFERENCE);
extern EIF_TYPED_VALUE F128_3565(EIF_REFERENCE);
extern EIF_TYPED_VALUE F128_3566(EIF_REFERENCE);
extern EIF_TYPED_VALUE F128_3567(EIF_REFERENCE);
extern EIF_TYPED_VALUE F128_3568(EIF_REFERENCE);
extern EIF_TYPED_VALUE F128_3569(EIF_REFERENCE);
extern EIF_TYPED_VALUE F128_3570(EIF_REFERENCE);
extern EIF_TYPED_VALUE F128_3571(EIF_REFERENCE);
extern EIF_TYPED_VALUE F128_3572(EIF_REFERENCE);
extern EIF_TYPED_VALUE F128_3573(EIF_REFERENCE);
extern void EIF_Minit128(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_INPUT_EVENT_CONSTANTS}.keyeventf_extendedkey */
EIF_TYPED_VALUE F128_3563 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_INPUT_EVENT_CONSTANTS}.keyeventf_keyup */
EIF_TYPED_VALUE F128_3564 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_INPUT_EVENT_CONSTANTS}.mouseeventf_absolute */
EIF_TYPED_VALUE F128_3565 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32768L);
	return r;
}

/* {WEL_INPUT_EVENT_CONSTANTS}.mouseeventf_move */
EIF_TYPED_VALUE F128_3566 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_INPUT_EVENT_CONSTANTS}.mouseeventf_leftdown */
EIF_TYPED_VALUE F128_3567 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_INPUT_EVENT_CONSTANTS}.mouseeventf_leftup */
EIF_TYPED_VALUE F128_3568 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_INPUT_EVENT_CONSTANTS}.mouseeventf_rightdown */
EIF_TYPED_VALUE F128_3569 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_INPUT_EVENT_CONSTANTS}.mouseeventf_rightup */
EIF_TYPED_VALUE F128_3570 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_INPUT_EVENT_CONSTANTS}.mouseeventf_middledown */
EIF_TYPED_VALUE F128_3571 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_INPUT_EVENT_CONSTANTS}.mouseeventf_middleup */
EIF_TYPED_VALUE F128_3572 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_INPUT_EVENT_CONSTANTS}.mouseeventf_wheel */
EIF_TYPED_VALUE F128_3573 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2048L);
	return r;
}

void EIF_Minit128 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
