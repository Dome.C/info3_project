/*
 * Code for class WEL_LINE_CAPABILITIES_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F88_1177(EIF_REFERENCE);
extern EIF_TYPED_VALUE F88_1178(EIF_REFERENCE);
extern EIF_TYPED_VALUE F88_1179(EIF_REFERENCE);
extern EIF_TYPED_VALUE F88_1180(EIF_REFERENCE);
extern EIF_TYPED_VALUE F88_1181(EIF_REFERENCE);
extern EIF_TYPED_VALUE F88_1182(EIF_REFERENCE);
extern EIF_TYPED_VALUE F88_1183(EIF_REFERENCE);
extern EIF_TYPED_VALUE F88_1184(EIF_REFERENCE);
extern void EIF_Minit88(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_LINE_CAPABILITIES_CONSTANTS}.lc_none */
EIF_TYPED_VALUE F88_1177 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "lc_none";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 87, Current, 0, 0, 1203);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(87, Current, 1203);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) LC_NONE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_LINE_CAPABILITIES_CONSTANTS}.lc_polyline */
EIF_TYPED_VALUE F88_1178 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "lc_polyline";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 87, Current, 0, 0, 1196);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(87, Current, 1196);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) LC_POLYLINE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_LINE_CAPABILITIES_CONSTANTS}.lc_marker */
EIF_TYPED_VALUE F88_1179 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "lc_marker";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 87, Current, 0, 0, 1197);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(87, Current, 1197);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) LC_MARKER;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_LINE_CAPABILITIES_CONSTANTS}.lc_polymarker */
EIF_TYPED_VALUE F88_1180 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "lc_polymarker";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 87, Current, 0, 0, 1198);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(87, Current, 1198);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) LC_POLYMARKER;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_LINE_CAPABILITIES_CONSTANTS}.lc_wide */
EIF_TYPED_VALUE F88_1181 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "lc_wide";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 87, Current, 0, 0, 1199);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(87, Current, 1199);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) LC_WIDE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_LINE_CAPABILITIES_CONSTANTS}.lc_styled */
EIF_TYPED_VALUE F88_1182 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "lc_styled";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 87, Current, 0, 0, 1200);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(87, Current, 1200);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) LC_STYLED;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_LINE_CAPABILITIES_CONSTANTS}.lc_wide_styled */
EIF_TYPED_VALUE F88_1183 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "lc_wide_styled";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 87, Current, 0, 0, 1201);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(87, Current, 1201);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) LC_WIDESTYLED;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_LINE_CAPABILITIES_CONSTANTS}.lc_interiors */
EIF_TYPED_VALUE F88_1184 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "lc_interiors";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 87, Current, 0, 0, 1202);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(87, Current, 1202);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) LC_INTERIORS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit88 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
