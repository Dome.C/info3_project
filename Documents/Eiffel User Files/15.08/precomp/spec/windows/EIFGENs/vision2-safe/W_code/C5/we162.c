/*
 * Code for class WEL_THEME_TS_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F162_4015(EIF_REFERENCE);
extern EIF_TYPED_VALUE F162_4016(EIF_REFERENCE);
extern EIF_TYPED_VALUE F162_4017(EIF_REFERENCE);
extern EIF_TYPED_VALUE F162_4018(EIF_REFERENCE);
extern EIF_TYPED_VALUE F162_4019(EIF_REFERENCE);
extern EIF_TYPED_VALUE F162_4020(EIF_REFERENCE);
extern void EIF_Minit162(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_THEME_TS_CONSTANTS}.ts_normal */
EIF_TYPED_VALUE F162_4015 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_THEME_TS_CONSTANTS}.ts_hot */
EIF_TYPED_VALUE F162_4016 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_THEME_TS_CONSTANTS}.ts_pressed */
EIF_TYPED_VALUE F162_4017 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_THEME_TS_CONSTANTS}.ts_disabled */
EIF_TYPED_VALUE F162_4018 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_THEME_TS_CONSTANTS}.ts_checked */
EIF_TYPED_VALUE F162_4019 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

/* {WEL_THEME_TS_CONSTANTS}.ts_hotchecked */
EIF_TYPED_VALUE F162_4020 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 6L);
	return r;
}

void EIF_Minit162 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
