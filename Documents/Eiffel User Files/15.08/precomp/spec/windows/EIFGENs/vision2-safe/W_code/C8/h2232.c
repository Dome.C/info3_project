/*
 * Code for class H2E_IDS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F232_4818(EIF_REFERENCE);
extern EIF_TYPED_VALUE F232_4819(EIF_REFERENCE);
extern EIF_TYPED_VALUE F232_4820(EIF_REFERENCE);
extern EIF_TYPED_VALUE F232_4821(EIF_REFERENCE);
extern EIF_TYPED_VALUE F232_4822(EIF_REFERENCE);
extern EIF_TYPED_VALUE F232_4823(EIF_REFERENCE);
extern EIF_TYPED_VALUE F232_4824(EIF_REFERENCE);
extern EIF_TYPED_VALUE F232_4825(EIF_REFERENCE);
extern EIF_TYPED_VALUE F232_4826(EIF_REFERENCE);
extern EIF_TYPED_VALUE F232_4827(EIF_REFERENCE);
extern EIF_TYPED_VALUE F232_4828(EIF_REFERENCE);
extern EIF_TYPED_VALUE F232_4829(EIF_REFERENCE);
extern void EIF_Minit232(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {H2E_IDS}.id_main_dialog */
EIF_TYPED_VALUE F232_4818 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {H2E_IDS}.id_about_dialog */
EIF_TYPED_VALUE F232_4819 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {H2E_IDS}.id_menu_application */
EIF_TYPED_VALUE F232_4820 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {H2E_IDS}.id_ico_application */
EIF_TYPED_VALUE F232_4821 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {H2E_IDS}.id_edit_h */
EIF_TYPED_VALUE F232_4822 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 101L);
	return r;
}

/* {H2E_IDS}.id_edit_eiffel */
EIF_TYPED_VALUE F232_4823 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 102L);
	return r;
}

/* {H2E_IDS}.id_edit_class_name */
EIF_TYPED_VALUE F232_4824 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 103L);
	return r;
}

/* {H2E_IDS}.cmd_file_browse_h */
EIF_TYPED_VALUE F232_4825 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 104L);
	return r;
}

/* {H2E_IDS}.cmd_file_browse_eiffel */
EIF_TYPED_VALUE F232_4826 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 105L);
	return r;
}

/* {H2E_IDS}.cmd_file_translate */
EIF_TYPED_VALUE F232_4827 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 106L);
	return r;
}

/* {H2E_IDS}.cmd_file_exit */
EIF_TYPED_VALUE F232_4828 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 107L);
	return r;
}

/* {H2E_IDS}.cmd_help_about */
EIF_TYPED_VALUE F232_4829 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 108L);
	return r;
}

void EIF_Minit232 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
