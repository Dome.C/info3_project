/*
 * Code for class EV_GRID_DRAWER_I
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern void F658_12098(EIF_REFERENCE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12099(EIF_REFERENCE);
extern EIF_TYPED_VALUE F658_12100(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12101(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12102(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12103(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12104(EIF_REFERENCE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12105(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12106(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12107(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12108(EIF_REFERENCE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12109(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12110(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12111(EIF_REFERENCE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12112(EIF_REFERENCE, EIF_TYPED_VALUE);
extern void F658_12113(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern void F658_12114(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern void F658_12115(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12116(EIF_REFERENCE);
extern EIF_TYPED_VALUE F658_12117(EIF_REFERENCE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12118(EIF_REFERENCE, EIF_TYPED_VALUE);
extern void F658_12119(EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12120(EIF_REFERENCE);
extern EIF_TYPED_VALUE F658_12121(EIF_REFERENCE);
extern EIF_TYPED_VALUE F658_12122(EIF_REFERENCE);
extern EIF_TYPED_VALUE F658_12123(EIF_REFERENCE);
extern EIF_TYPED_VALUE F658_12124(EIF_REFERENCE);
extern EIF_TYPED_VALUE F658_12125(EIF_REFERENCE);
extern EIF_TYPED_VALUE F658_12126(EIF_REFERENCE);
extern EIF_TYPED_VALUE F658_12127(EIF_REFERENCE, EIF_TYPED_VALUE);
extern EIF_TYPED_VALUE F658_12128(EIF_REFERENCE, EIF_TYPED_VALUE);
extern void F658_29561(EIF_REFERENCE, int);
extern void EIF_Minit658(void);

#ifdef __cplusplus
}
#endif

#include "eif_helpers.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {EV_GRID_DRAWER_I}.make_with_grid */
void F658_12098 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x)
{
	GTCX
	char *l_feature_name = "make_with_grid";
	RTEX;
#define arg1 arg1x.it_r
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_REFERENCE tr1 = NULL;
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	
	RTLI(3);
	RTLR(0,arg1);
	RTLR(1,Current);
	RTLR(2,tr1);
	RTLIU(3);
	RTLU (SK_VOID, NULL);
	RTLU(SK_REF,&arg1);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 1, 16009);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16009);
	RTCC(arg1, 657, l_feature_name, 1, eif_new_type(1203, 0x01), 0x01);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("a_grid_not_void", EX_PRE);
		RTTE((EIF_BOOLEAN)(arg1 != NULL), label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTHOOK(2);
	RTDBGAA(Current, dtype, 10835, 0xF80004B3, 0); /* grid */
	RTAR(Current, arg1);
	*(EIF_REFERENCE *)(Current + RTWA(10835, dtype)) = (EIF_REFERENCE) RTCCL(arg1);
	RTHOOK(3);
	RTDBGAA(Current, dtype, 10862, 0xF8000270, 0); /* temp_rectangle */
	tr1 = RTLNSMART(RTWCT(10862, dtype, Dftype(Current)).id);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTWC(32, Dtype(tr1)))(tr1);
	RTNHOOK(3,1);
	RTAR(Current, tr1);
	*(EIF_REFERENCE *)(Current + RTWA(10862, dtype)) = (EIF_REFERENCE) RTCCL(tr1);
	if (RTAL & CK_ENSURE) {
		RTHOOK(4);
		RTCT("grid_set", EX_POST);
		tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		if (RTCEQ(tr1, arg1)) {
			RTCK;
		} else {
			RTCF;
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(5);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(3);
	RTEE;
#undef up1
#undef arg1
}

/* {EV_GRID_DRAWER_I}.grid */
EIF_TYPED_VALUE F658_12099 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(10835,Dtype(Current)));
	return r;
}


/* {EV_GRID_DRAWER_I}.items_spanning_horizontal_span */
EIF_TYPED_VALUE F658_12100 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x)
{
	GTCX
	char *l_feature_name = "items_spanning_horizontal_span";
	RTEX;
	EIF_INTEGER_32 loc1 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc2 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc3 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc4 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc5 = (EIF_INTEGER_32) 0;
	EIF_BOOLEAN loc6 = (EIF_BOOLEAN) 0;
	EIF_BOOLEAN loc7 = (EIF_BOOLEAN) 0;
	EIF_REFERENCE loc8 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc9 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc10 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc11 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc12 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc13 = (EIF_REFERENCE) 0;
#define arg1 arg1x.it_i4
#define arg2 arg2x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_BOOLEAN tb1;
	EIF_BOOLEAN tb2;
	EIF_BOOLEAN tb3;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_i4 = * (EIF_INTEGER_32 *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(6);
	RTLR(0,loc13);
	RTLR(1,Current);
	RTLR(2,tr1);
	RTLR(3,Result);
	RTLR(4,loc3);
	RTLR(5,loc8);
	RTLIU(6);
	RTLU (SK_REF, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU(SK_INT32,&arg2);
	RTLU (SK_REF, &Current);
	RTLU(SK_INT32, &loc1);
	RTLU(SK_INT32, &loc2);
	RTLU(SK_REF, &loc3);
	RTLU(SK_INT32, &loc4);
	RTLU(SK_INT32, &loc5);
	RTLU(SK_BOOL, &loc6);
	RTLU(SK_BOOL, &loc7);
	RTLU(SK_REF, &loc8);
	RTLU(SK_INT32, &loc9);
	RTLU(SK_INT32, &loc10);
	RTLU(SK_INT32, &loc11);
	RTLU(SK_INT32, &loc12);
	RTLU(SK_REF, &loc13);
	
	RTEAA(l_feature_name, 657, Current, 13, 2, 16011);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16011);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("a_width_non_negative", EX_PRE);
		RTTE((EIF_BOOLEAN) (arg2 >= ((EIF_INTEGER_32) 0L)), label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTHOOK(2);
	RTDBGAL(Current, 13, 0xF80004B3, 0, 0); /* loc13 */
	loc13 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(3);
	RTDBGAL(Current, 0, 0xF8000686, 0,0); /* Result */
	{
		static EIF_TYPE_INDEX typarr0[] = {0xFF01,1670,869,0xFFFF};
		EIF_TYPE typres0;
		static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
		
		typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(Dftype(Current), typarr0)));
		tr1 = RTLN(typres0.id);
	}
	ui4_1 = ((EIF_INTEGER_32) 20L);
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWC(10419, Dtype(tr1)))(tr1, ui4_1x);
	RTNHOOK(3,1);
	Result = (EIF_REFERENCE) RTCCL(tr1);
	RTHOOK(4);
	RTDBGAL(Current, 3, 0xF8000684, 0, 0); /* loc3 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19321, "physical_column_indexes", loc13))(loc13)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	loc3 = (EIF_REFERENCE) tr1;
	RTHOOK(5);
	RTDBGAL(Current, 1, 0x10000000, 1, 0); /* loc1 */
	ti4_1 = *(EIF_INTEGER_32 *)(loc13 + RTVA(19360, "internal_client_x", loc13));
	loc1 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(6);
	RTDBGAL(Current, 12, 0x10000000, 1, 0); /* loc12 */
	ti4_1 = *(EIF_INTEGER_32 *)(loc13 + RTVA(19167, "viewable_width", loc13));
	loc12 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(7);
	RTDBGAL(Current, 2, 0x10000000, 1, 0); /* loc2 */
	ti4_1 = *(EIF_INTEGER_32 *)(loc13 + RTVA(19362, "viewport_x_offset", loc13));
	loc2 = (EIF_INTEGER_32) ti4_1;
	RTHOOK(8);
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19317, "column_count", loc13))(loc13)).it_i4);
	if ((EIF_BOOLEAN) (ti4_1 > ((EIF_INTEGER_32) 0L))) {
		RTHOOK(9);
		RTDBGAL(Current, 9, 0x10000000, 1, 0); /* loc9 */
		loc9 = (EIF_INTEGER_32) arg1;
		RTHOOK(10);
		RTDBGAL(Current, 10, 0x10000000, 1, 0); /* loc10 */
		loc10 = (EIF_INTEGER_32) (EIF_INTEGER_32) (arg1 + arg2);
		RTHOOK(11);
		tb1 = '\0';
		if ((EIF_BOOLEAN) (loc10 >= ((EIF_INTEGER_32) 0L))) {
			ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19165, "virtual_width", loc13))(loc13)).it_i4);
			tb1 = (EIF_BOOLEAN) (loc9 <= ti4_1);
		}
		if (tb1) {
			RTHOOK(12);
			RTDBGAL(Current, 9, 0x10000000, 1, 0); /* loc9 */
			ui4_1 = ((EIF_INTEGER_32) 0L);
			ti4_1 = eif_max_int32 (loc9,ui4_1);
			loc9 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(13);
			RTDBGAL(Current, 10, 0x10000000, 1, 0); /* loc10 */
			ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19165, "virtual_width", loc13))(loc13)).it_i4);
			ui4_1 = ti4_1;
			ti4_1 = eif_min_int32 (loc10,ui4_1);
			loc10 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(14);
			RTDBGAL(Current, 8, 0xF8000686, 0, 0); /* loc8 */
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19355, "column_offsets", loc13))(loc13)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			loc8 = (EIF_REFERENCE) RTCCL(tr1);
			RTHOOK(15);
			(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(8481, "start", loc8))(loc8);
			for (;;) {
				RTHOOK(16);
				tb1 = '\01';
				if (!loc7) {
					ti4_1 = *(EIF_INTEGER_32 *)(loc8 + RTVA(10433, "index", loc8));
					ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19317, "column_count", loc13))(loc13)).it_i4);
					tb1 = (EIF_BOOLEAN) (ti4_1 > (EIF_INTEGER_32) (ti4_2 + ((EIF_INTEGER_32) 1L)));
				}
				if (tb1) break;
				RTHOOK(17);
				RTDBGAL(Current, 11, 0x10000000, 1, 0); /* loc11 */
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc8))(loc8)).it_i4);
				loc11 = (EIF_INTEGER_32) ti4_1;
				RTHOOK(18);
				if ((EIF_BOOLEAN) ((EIF_BOOLEAN) !loc6 && (EIF_BOOLEAN) (loc11 > loc9))) {
					RTHOOK(19);
					RTDBGAL(Current, 4, 0x10000000, 1, 0); /* loc4 */
					ti4_1 = *(EIF_INTEGER_32 *)(loc8 + RTVA(10433, "index", loc8));
					loc4 = (EIF_INTEGER_32) (EIF_INTEGER_32) (ti4_1 - ((EIF_INTEGER_32) 1L));
					RTHOOK(20);
					RTDBGAL(Current, 6, 0x04000000, 1, 0); /* loc6 */
					loc6 = (EIF_BOOLEAN) (EIF_BOOLEAN) 1;
				}
				RTHOOK(21);
				tb2 = '\0';
				if (loc6) {
					ti4_1 = *(EIF_INTEGER_32 *)(loc8 + RTVA(10433, "index", loc8));
					ui4_1 = (EIF_INTEGER_32) (ti4_1 - ((EIF_INTEGER_32) 1L));
					tb3 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(19276, "column_displayed", loc13))(loc13, ui4_1x)).it_b);
					tb2 = tb3;
				}
				if (tb2) {
					RTHOOK(22);
					ti4_1 = *(EIF_INTEGER_32 *)(loc8 + RTVA(10433, "index", loc8));
					ui4_1 = (EIF_INTEGER_32) (ti4_1 - ((EIF_INTEGER_32) 1L));
					(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8520, "extend", Result))(Result, ui4_1x);
				}
				RTHOOK(23);
				if ((EIF_BOOLEAN) ((EIF_BOOLEAN) !loc7 && (EIF_BOOLEAN) (loc10 < loc11))) {
					RTHOOK(24);
					RTDBGAL(Current, 5, 0x10000000, 1, 0); /* loc5 */
					ti4_1 = *(EIF_INTEGER_32 *)(loc8 + RTVA(10433, "index", loc8));
					loc5 = (EIF_INTEGER_32) (EIF_INTEGER_32) (ti4_1 - ((EIF_INTEGER_32) 1L));
					RTHOOK(25);
					RTDBGAL(Current, 7, 0x04000000, 1, 0); /* loc7 */
					loc7 = (EIF_BOOLEAN) (EIF_BOOLEAN) 1;
				}
				RTHOOK(26);
				(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(8497, "forth", loc8))(loc8);
			}
			RTHOOK(27);
			if ((EIF_BOOLEAN)(loc5 == ((EIF_INTEGER_32) 0L))) {
				RTHOOK(28);
				RTDBGAL(Current, 5, 0x10000000, 1, 0); /* loc5 */
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19317, "column_count", loc13))(loc13)).it_i4);
				loc5 = (EIF_INTEGER_32) ti4_1;
			}
			RTHOOK(29);
			if ((EIF_BOOLEAN)(loc4 == ((EIF_INTEGER_32) 0L))) {
				RTHOOK(30);
				RTDBGAL(Current, 4, 0x10000000, 1, 0); /* loc4 */
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19317, "column_count", loc13))(loc13)).it_i4);
				loc4 = (EIF_INTEGER_32) ti4_1;
			}
		}
	}
	if (RTAL & CK_ENSURE) {
		RTHOOK(31);
		RTCT("result_not_void", EX_POST);
		if ((EIF_BOOLEAN)(Result != NULL)) {
			RTCK;
		} else {
			RTCF;
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(32);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(17);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ui4_1
#undef arg2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.items_spanning_vertical_span */
EIF_TYPED_VALUE F658_12101 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x)
{
	GTCX
	char *l_feature_name = "items_spanning_vertical_span";
	RTEX;
	EIF_INTEGER_32 loc1 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc2 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc3 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc4 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc5 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc6 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc7 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc8 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc9 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc10 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc11 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc12 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc13 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc14 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc15 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc16 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc17 = (EIF_REFERENCE) 0;
	EIF_BOOLEAN loc18 = (EIF_BOOLEAN) 0;
	EIF_INTEGER_32 loc19 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc20 = (EIF_REFERENCE) 0;
#define arg1 arg1x.it_i4
#define arg2 arg2x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_BOOLEAN tb1;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_i4 = * (EIF_INTEGER_32 *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(7);
	RTLR(0,loc20);
	RTLR(1,Current);
	RTLR(2,tr1);
	RTLR(3,Result);
	RTLR(4,loc16);
	RTLR(5,loc3);
	RTLR(6,loc17);
	RTLIU(7);
	RTLU (SK_REF, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU(SK_INT32,&arg2);
	RTLU (SK_REF, &Current);
	RTLU(SK_INT32, &loc1);
	RTLU(SK_INT32, &loc2);
	RTLU(SK_REF, &loc3);
	RTLU(SK_INT32, &loc4);
	RTLU(SK_INT32, &loc5);
	RTLU(SK_INT32, &loc6);
	RTLU(SK_INT32, &loc7);
	RTLU(SK_INT32, &loc8);
	RTLU(SK_INT32, &loc9);
	RTLU(SK_INT32, &loc10);
	RTLU(SK_INT32, &loc11);
	RTLU(SK_INT32, &loc12);
	RTLU(SK_INT32, &loc13);
	RTLU(SK_INT32, &loc14);
	RTLU(SK_INT32, &loc15);
	RTLU(SK_REF, &loc16);
	RTLU(SK_REF, &loc17);
	RTLU(SK_BOOL, &loc18);
	RTLU(SK_INT32, &loc19);
	RTLU(SK_REF, &loc20);
	
	RTEAA(l_feature_name, 657, Current, 20, 2, 16012);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16012);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("a_height_non_negative", EX_PRE);
		RTTE((EIF_BOOLEAN) (arg2 >= ((EIF_INTEGER_32) 0L)), label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTHOOK(2);
	RTDBGAL(Current, 20, 0xF80004B3, 0, 0); /* loc20 */
	loc20 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(3);
	RTDBGAL(Current, 0, 0xF8000686, 0,0); /* Result */
	{
		static EIF_TYPE_INDEX typarr0[] = {0xFF01,1670,869,0xFFFF};
		EIF_TYPE typres0;
		static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
		
		typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(Dftype(Current), typarr0)));
		tr1 = RTLN(typres0.id);
	}
	ui4_1 = ((EIF_INTEGER_32) 20L);
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWC(10419, Dtype(tr1)))(tr1, ui4_1x);
	RTNHOOK(3,1);
	Result = (EIF_REFERENCE) RTCCL(tr1);
	RTHOOK(4);
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19365, "header", loc20))(loc20)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(4,1);
	tb1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8471, "is_empty", tr1))(tr1)).it_b);
	if ((EIF_BOOLEAN) !tb1) {
		RTHOOK(5);
		RTDBGAL(Current, 4, 0x10000000, 1, 0); /* loc4 */
		loc4 = (EIF_INTEGER_32) arg1;
		RTHOOK(6);
		RTDBGAL(Current, 5, 0x10000000, 1, 0); /* loc5 */
		loc5 = (EIF_INTEGER_32) (EIF_INTEGER_32) (arg1 + arg2);
		RTHOOK(7);
		RTDBGAL(Current, 8, 0x10000000, 1, 0); /* loc8 */
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19318, "row_count", loc20))(loc20)).it_i4);
		loc8 = (EIF_INTEGER_32) ti4_1;
		RTHOOK(8);
		tb1 = '\0';
		if ((EIF_BOOLEAN) (loc5 >= ((EIF_INTEGER_32) 0L))) {
			ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19343, "total_row_height", loc20))(loc20)).it_i4);
			tb1 = (EIF_BOOLEAN) (loc4 < ti4_1);
		}
		if (tb1) {
			RTHOOK(9);
			RTDBGAL(Current, 4, 0x10000000, 1, 0); /* loc4 */
			ui4_1 = ((EIF_INTEGER_32) 0L);
			ti4_1 = eif_max_int32 (loc4,ui4_1);
			loc4 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(10);
			RTDBGAL(Current, 5, 0x10000000, 1, 0); /* loc5 */
			ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19166, "virtual_height", loc20))(loc20)).it_i4);
			ui4_1 = ti4_1;
			ti4_1 = eif_min_int32 (loc5,ui4_1);
			loc5 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(11);
			tb1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19502, "uses_row_offsets", loc20))(loc20)).it_b);
			if ((EIF_BOOLEAN) !tb1) {
				RTHOOK(12);
				RTDBGAL(Current, 1, 0x10000000, 1, 0); /* loc1 */
				ti4_1 = *(EIF_INTEGER_32 *)(loc20 + RTVA(19155, "row_height", loc20));
				loc1 = (EIF_INTEGER_32) ((EIF_INTEGER_32) (((EIF_INTEGER_32) (loc4 / ti4_1)) + ((EIF_INTEGER_32) 1L)));
				RTHOOK(13);
				RTDBGAL(Current, 2, 0x10000000, 1, 0); /* loc2 */
				ti4_1 = *(EIF_INTEGER_32 *)(loc20 + RTVA(19155, "row_height", loc20));
				ui4_1 = loc8;
				ti4_2 = eif_min_int32 (((EIF_INTEGER_32) (((EIF_INTEGER_32) (loc5 / ti4_1)) + ((EIF_INTEGER_32) 1L))),ui4_1);
				loc2 = (EIF_INTEGER_32) ti4_2;
				RTHOOK(14);
				if ((EIF_BOOLEAN) (loc1 <= loc8)) {
					RTHOOK(15);
					RTDBGAL(Current, 7, 0x10000000, 1, 0); /* loc7 */
					loc7 = (EIF_INTEGER_32) loc8;
					RTHOOK(16);
					RTDBGAL(Current, 6, 0x10000000, 1, 0); /* loc6 */
					loc6 = (EIF_INTEGER_32) loc1;
					for (;;) {
						RTHOOK(17);
						if ((EIF_BOOLEAN) (loc6 > loc2)) break;
						RTHOOK(18);
						ui4_1 = loc6;
						(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8520, "extend", Result))(Result, ui4_1x);
						if (RTAL & CK_CHECK) {
							RTHOOK(19);
							RTCT("i_positive", EX_CHECK);
							if ((EIF_BOOLEAN) (loc6 >= ((EIF_INTEGER_32) 0L))) {
								RTCK;
							} else {
								RTCF;
							}
						}
						RTHOOK(20);
						RTDBGAL(Current, 6, 0x10000000, 1, 0); /* loc6 */
						loc6++;
					}
				}
			} else {
				RTHOOK(21);
				RTDBGAL(Current, 16, 0xF800084F, 0, 0); /* loc16 */
				tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19358, "visible_indexes_to_row_indexes", loc20))(loc20)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				loc16 = (EIF_REFERENCE) RTCCL(tr1);
				RTHOOK(22);
				RTCT0(NULL, EX_CHECK);
				if ((EIF_BOOLEAN)(loc16 != NULL)) {
					RTCK0;
				} else {
					RTCF0;
				}
				RTHOOK(23);
				RTDBGAL(Current, 3, 0xF8000686, 0, 0); /* loc3 */
				tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19356, "row_offsets", loc20))(loc20)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				loc3 = (EIF_REFERENCE) RTCCL(tr1);
				RTHOOK(24);
				RTCT0(NULL, EX_CHECK);
				if ((EIF_BOOLEAN)(loc3 != NULL)) {
					RTCK0;
				} else {
					RTCF0;
				}
				RTHOOK(25);
				RTDBGAL(Current, 10, 0x10000000, 1, 0); /* loc10 */
				loc10 = (EIF_INTEGER_32) loc8;
				RTHOOK(26);
				RTDBGAL(Current, 11, 0x10000000, 1, 0); /* loc11 */
				loc11 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
				for (;;) {
					RTHOOK(27);
					if ((EIF_BOOLEAN) !(EIF_BOOLEAN) ((EIF_INTEGER_32) (loc10 - loc11) > ((EIF_INTEGER_32) 1L))) break;
					RTHOOK(28);
					RTDBGAL(Current, 9, 0x10000000, 1, 0); /* loc9 */
					loc9 = (EIF_INTEGER_32) (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc11 + loc10) / ((EIF_INTEGER_32) 2L));
					RTHOOK(29);
					ui4_1 = loc9;
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", loc3))(loc3, ui4_1x)).it_i4);
					if ((EIF_BOOLEAN) (ti4_1 <= loc4)) {
						RTHOOK(30);
						RTDBGAL(Current, 11, 0x10000000, 1, 0); /* loc11 */
						loc11 = (EIF_INTEGER_32) loc9;
					} else {
						RTHOOK(31);
						RTDBGAL(Current, 10, 0x10000000, 1, 0); /* loc10 */
						loc10 = (EIF_INTEGER_32) loc9;
					}
				}
				RTHOOK(32);
				RTDBGAL(Current, 15, 0x10000000, 1, 0); /* loc15 */
				ui4_1 = loc10;
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", loc3))(loc3, ui4_1x)).it_i4);
				loc15 = (EIF_INTEGER_32) ti4_1;
				RTHOOK(33);
				if ((EIF_BOOLEAN) (loc15 <= loc4)) {
					RTHOOK(34);
					RTDBGAL(Current, 14, 0x10000000, 1, 0); /* loc14 */
					loc14 = (EIF_INTEGER_32) loc10;
				} else {
					RTHOOK(35);
					RTDBGAL(Current, 14, 0x10000000, 1, 0); /* loc14 */
					ui4_1 = ((EIF_INTEGER_32) 1L);
					ti4_1 = eif_max_int32 (loc11,ui4_1);
					loc14 = (EIF_INTEGER_32) ti4_1;
					RTHOOK(36);
					RTDBGAL(Current, 15, 0x10000000, 1, 0); /* loc15 */
					ui4_1 = loc14;
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", loc3))(loc3, ui4_1x)).it_i4);
					loc15 = (EIF_INTEGER_32) ti4_1;
				}
				RTHOOK(37);
				tb1 = '\0';
				if ((EIF_BOOLEAN) (loc14 > ((EIF_INTEGER_32) 1L))) {
					ui4_1 = (EIF_INTEGER_32) (loc14 - ((EIF_INTEGER_32) 1L));
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", loc3))(loc3, ui4_1x)).it_i4);
					tb1 = (EIF_BOOLEAN)(ti4_1 == loc15);
				}
				if (tb1) {
					if (RTAL & CK_CHECK) {
						RTHOOK(38);
						RTCT("duplicates_found_implies_next_offset_greater", EX_CHECK);
						tb1 = '\01';
						ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19318, "row_count", loc20))(loc20)).it_i4);
						if ((EIF_BOOLEAN) (loc14 < (EIF_INTEGER_32) (ti4_1 + ((EIF_INTEGER_32) 1L)))) {
							ui4_1 = (EIF_INTEGER_32) (loc14 + ((EIF_INTEGER_32) 1L));
							ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", loc3))(loc3, ui4_1x)).it_i4);
							tb1 = (EIF_BOOLEAN) (ti4_1 > loc15);
						}
						if (tb1) {
							RTCK;
						} else {
							RTCF;
						}
					}
					RTHOOK(39);
					RTDBGAL(Current, 10, 0x10000000, 1, 0); /* loc10 */
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19319, "visible_row_count", loc20))(loc20)).it_i4);
					loc10 = (EIF_INTEGER_32) ti4_1;
					RTHOOK(40);
					RTDBGAL(Current, 11, 0x10000000, 1, 0); /* loc11 */
					loc11 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
					for (;;) {
						RTHOOK(41);
						if ((EIF_BOOLEAN) !(EIF_BOOLEAN) ((EIF_INTEGER_32) (loc10 - loc11) > ((EIF_INTEGER_32) 1L))) break;
						RTHOOK(42);
						RTDBGAL(Current, 9, 0x10000000, 1, 0); /* loc9 */
						loc9 = (EIF_INTEGER_32) (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc11 + loc10) / ((EIF_INTEGER_32) 2L));
						RTHOOK(43);
						ui4_1 = loc9;
						ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", loc16))(loc16, ui4_1x)).it_i4);
						if ((EIF_BOOLEAN) (ti4_1 < loc14)) {
							RTHOOK(44);
							RTDBGAL(Current, 11, 0x10000000, 1, 0); /* loc11 */
							loc11 = (EIF_INTEGER_32) loc9;
						} else {
							RTHOOK(45);
							RTDBGAL(Current, 10, 0x10000000, 1, 0); /* loc10 */
							loc10 = (EIF_INTEGER_32) loc9;
						}
					}
					RTHOOK(46);
					ui4_1 = loc10;
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", loc16))(loc16, ui4_1x)).it_i4);
					if ((EIF_BOOLEAN) (ti4_1 <= loc14)) {
						RTHOOK(47);
						RTDBGAL(Current, 14, 0x10000000, 1, 0); /* loc14 */
						ui4_1 = loc10;
						ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", loc16))(loc16, ui4_1x)).it_i4);
						loc14 = (EIF_INTEGER_32) ti4_1;
					} else {
						RTHOOK(48);
						RTDBGAL(Current, 14, 0x10000000, 1, 0); /* loc14 */
						ui4_1 = ((EIF_INTEGER_32) 1L);
						ti4_1 = eif_max_int32 (loc11,ui4_1);
						ui4_1 = ti4_1;
						ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", loc16))(loc16, ui4_1x)).it_i4);
						loc14 = (EIF_INTEGER_32) ti4_1;
					}
				}
				RTHOOK(49);
				RTDBGAL(Current, 17, 0xF800084F, 0, 0); /* loc17 */
				tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19357, "row_indexes_to_visible_indexes", loc20))(loc20)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				loc17 = (EIF_REFERENCE) RTCCL(tr1);
				RTHOOK(50);
				RTCT0(NULL, EX_CHECK);
				if ((EIF_BOOLEAN)(loc17 != NULL)) {
					RTCK0;
				} else {
					RTCF0;
				}
				RTHOOK(51);
				RTDBGAL(Current, 12, 0x10000000, 1, 0); /* loc12 */
				ui4_1 = loc14;
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", loc17))(loc17, ui4_1x)).it_i4);
				loc12 = (EIF_INTEGER_32) (EIF_INTEGER_32) (ti4_1 + ((EIF_INTEGER_32) 1L));
				RTHOOK(52);
				RTDBGAL(Current, 14, 0x10000000, 1, 0); /* loc14 */
				ui4_1 = loc12;
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", loc16))(loc16, ui4_1x)).it_i4);
				loc14 = (EIF_INTEGER_32) ti4_1;
				RTHOOK(53);
				ui4_1 = loc14;
				(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8520, "extend", Result))(Result, ui4_1x);
				RTHOOK(54);
				RTDBGAL(Current, 18, 0x04000000, 1, 0); /* loc18 */
				tb1 = *(EIF_BOOLEAN *)(loc20 + RTVA(19153, "is_row_height_fixed", loc20));
				loc18 = (EIF_BOOLEAN) tb1;
				RTHOOK(55);
				RTDBGAL(Current, 19, 0x10000000, 1, 0); /* loc19 */
				ti4_1 = *(EIF_INTEGER_32 *)(loc20 + RTVA(19155, "row_height", loc20));
				loc19 = (EIF_INTEGER_32) ti4_1;
				RTHOOK(56);
				RTDBGAL(Current, 13, 0x10000000, 1, 0); /* loc13 */
				ui4_1 = loc14;
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", loc3))(loc3, ui4_1x)).it_i4);
				loc13 = (EIF_INTEGER_32) ti4_1;
				RTHOOK(57);
				if (loc18) {
					RTHOOK(58);
					RTDBGAL(Current, 13, 0x10000000, 1, 0); /* loc13 */
					loc13 += loc19;
				} else {
					RTHOOK(59);
					RTDBGAL(Current, 13, 0x10000000, 1, 0); /* loc13 */
					ui4_1 = loc14;
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(19134, "row", loc20))(loc20, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					RTNHOOK(59,1);
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16632, "height", tr1))(tr1)).it_i4);
					loc13 += ti4_1;
				}
				RTHOOK(60);
				RTDBGAL(Current, 6, 0x10000000, 1, 0); /* loc6 */
				loc6 = (EIF_INTEGER_32) (EIF_INTEGER_32) (loc12 + ((EIF_INTEGER_32) 1L));
				for (;;) {
					RTHOOK(61);
					tb1 = '\01';
					if (!((EIF_BOOLEAN) (loc13 > loc5))) {
						ti4_1 = *(EIF_INTEGER_32 *)(loc20 + RTVA(19366, "computed_visible_row_count", loc20));
						tb1 = (EIF_BOOLEAN) (loc6 > ti4_1);
					}
					if (tb1) break;
					RTHOOK(62);
					RTDBGAL(Current, 14, 0x10000000, 1, 0); /* loc14 */
					ui4_1 = loc6;
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", loc16))(loc16, ui4_1x)).it_i4);
					loc14 = (EIF_INTEGER_32) ti4_1;
					RTHOOK(63);
					ui4_1 = loc14;
					(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8520, "extend", Result))(Result, ui4_1x);
					RTHOOK(64);
					if (loc18) {
						RTHOOK(65);
						RTDBGAL(Current, 13, 0x10000000, 1, 0); /* loc13 */
						loc13 += loc19;
					} else {
						RTHOOK(66);
						RTDBGAL(Current, 13, 0x10000000, 1, 0); /* loc13 */
						ui4_1 = loc14;
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(19134, "row", loc20))(loc20, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(66,1);
						ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16632, "height", tr1))(tr1)).it_i4);
						loc13 += ti4_1;
					}
					RTHOOK(67);
					RTDBGAL(Current, 6, 0x10000000, 1, 0); /* loc6 */
					loc6++;
				}
			}
		}
	}
	if (RTAL & CK_ENSURE) {
		RTHOOK(68);
		RTCT("result_not_void", EX_POST);
		if ((EIF_BOOLEAN)(Result != NULL)) {
			RTCK;
		} else {
			RTCF;
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(69);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(24);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ui4_1
#undef arg2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.item_at_virtual_position */
EIF_TYPED_VALUE F658_12102 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x)
{
	GTCX
	char *l_feature_name = "item_at_virtual_position";
	RTEX;
	EIF_REFERENCE loc1 = (EIF_REFERENCE) 0;
#define arg1 arg1x.it_i4
#define arg2 arg2x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_INTEGER_32 ti4_3;
	EIF_INTEGER_32 ti4_4;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_i4 = * (EIF_INTEGER_32 *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(3);
	RTLR(0,loc1);
	RTLR(1,Current);
	RTLR(2,Result);
	RTLIU(3);
	RTLU (SK_REF, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU(SK_INT32,&arg2);
	RTLU (SK_REF, &Current);
	RTLU(SK_REF, &loc1);
	
	RTEAA(l_feature_name, 657, Current, 1, 2, 16013);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16013);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 1, 0xF80004B3, 0, 0); /* loc1 */
	loc1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(2);
	RTDBGAL(Current, 0, 0xF800049E, 0,0); /* Result */
	ti4_1 = *(EIF_INTEGER_32 *)(loc1 + RTVA(19360, "internal_client_x", loc1));
	ti4_2 = *(EIF_INTEGER_32 *)(loc1 + RTVA(19362, "viewport_x_offset", loc1));
	ui4_1 = (EIF_INTEGER_32) ((EIF_INTEGER_32) (arg1 - ti4_1) + ti4_2);
	ti4_3 = *(EIF_INTEGER_32 *)(loc1 + RTVA(19361, "internal_client_y", loc1));
	ti4_4 = *(EIF_INTEGER_32 *)(loc1 + RTVA(19363, "viewport_y_offset", loc1));
	ui4_2 = (EIF_INTEGER_32) ((EIF_INTEGER_32) (arg2 - ti4_3) + ti4_4);
	Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10842, dtype))(Current, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(3);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(5);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ui4_1
#undef ui4_2
#undef arg2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.row_at_virtual_position */
EIF_TYPED_VALUE F658_12103 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x)
{
	GTCX
	char *l_feature_name = "row_at_virtual_position";
	RTEX;
#define arg1 arg1x.it_i4
#define arg2 arg2x.it_b
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ub1x = {{0}, SK_BOOL};
#define ub1 ub1x.it_b
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_b = * (EIF_BOOLEAN *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(3);
	RTLR(0,Current);
	RTLR(1,tr1);
	RTLR(2,Result);
	RTLIU(3);
	RTLU (SK_REF, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU(SK_BOOL,&arg2);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 2, 16014);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16014);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 0, 0xF8000491, 0,0); /* Result */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,1);
	ti4_1 = *(EIF_INTEGER_32 *)(tr1 + RTVA(19361, "internal_client_y", tr1));
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,2);
	ti4_2 = *(EIF_INTEGER_32 *)(tr1 + RTVA(19363, "viewport_y_offset", tr1));
	ui4_1 = (EIF_INTEGER_32) ((EIF_INTEGER_32) (arg1 - ti4_1) + ti4_2);
	ub1 = arg2;
	Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10843, dtype))(Current, ui4_1x, ub1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(4);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ui4_1
#undef ub1
#undef arg2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.column_at_virtual_position */
EIF_TYPED_VALUE F658_12104 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x)
{
	GTCX
	char *l_feature_name = "column_at_virtual_position";
	RTEX;
#define arg1 arg1x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(3);
	RTLR(0,Current);
	RTLR(1,tr1);
	RTLR(2,Result);
	RTLIU(3);
	RTLU (SK_REF, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 1, 16015);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16015);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 0, 0xF8000490, 0,0); /* Result */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,1);
	ti4_1 = *(EIF_INTEGER_32 *)(tr1 + RTVA(19360, "internal_client_x", tr1));
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,2);
	ti4_2 = *(EIF_INTEGER_32 *)(tr1 + RTVA(19362, "viewport_x_offset", tr1));
	ui4_1 = (EIF_INTEGER_32) ((EIF_INTEGER_32) (arg1 - ti4_1) + ti4_2);
	Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10844, dtype))(Current, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(3);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ui4_1
#undef arg1
}

/* {EV_GRID_DRAWER_I}.item_coordinates_at_position */
EIF_TYPED_VALUE F658_12105 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x)
{
	GTCX
	char *l_feature_name = "item_coordinates_at_position";
	RTEX;
	EIF_REFERENCE loc1 = (EIF_REFERENCE) 0;
#define arg1 arg1x.it_i4
#define arg2 arg2x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_REFERENCE tr2 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_i4 = * (EIF_INTEGER_32 *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(5);
	RTLR(0,loc1);
	RTLR(1,Current);
	RTLR(2,tr1);
	RTLR(3,tr2);
	RTLR(4,Result);
	RTLIU(5);
	RTLU (SK_REF, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU(SK_INT32,&arg2);
	RTLU (SK_REF, &Current);
	RTLU(SK_REF, &loc1);
	
	RTEAA(l_feature_name, 657, Current, 1, 2, 16016);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 0);
	RTGC;
	RTDBGEAA(657, Current, 16016);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 1, 0xF800049E, 0, 0); /* loc1 */
	ui4_1 = arg1;
	ui4_2 = arg2;
	loc1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10842, Dtype(Current)))(Current, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(2);
	if ((EIF_BOOLEAN)(loc1 != NULL)) {
		RTHOOK(3);
		RTDBGAL(Current, 0, 0xF800026F, 0,0); /* Result */
		tr1 = RTLN(eif_new_type(623, 0x01).id);
		tr2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18841, "column", loc1))(loc1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		RTNHOOK(3,1);
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16610, "index", tr2))(tr2)).it_i4);
		ui4_1 = ti4_1;
		tr2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18843, "row", loc1))(loc1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		RTNHOOK(3,2);
		ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16643, "index", tr2))(tr2)).it_i4);
		ui4_2 = ti4_2;
		(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWC(10646, Dtype(tr1)))(tr1, ui4_1x, ui4_2x);
		RTNHOOK(3,3);
		Result = (EIF_REFERENCE) RTCCL(tr1);
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(4);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(5);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ui4_1
#undef ui4_2
#undef arg2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.item_at_position */
EIF_TYPED_VALUE F658_12106 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x)
{
	GTCX
	char *l_feature_name = "item_at_position";
	RTEX;
	EIF_REFERENCE loc1 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc2 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc3 = (EIF_REFERENCE) 0;
#define arg1 arg1x.it_i4
#define arg2 arg2x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_BOOLEAN tb1;
	EIF_BOOLEAN tb2;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_i4 = * (EIF_INTEGER_32 *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(6);
	RTLR(0,loc3);
	RTLR(1,Current);
	RTLR(2,Result);
	RTLR(3,loc1);
	RTLR(4,loc2);
	RTLR(5,tr1);
	RTLIU(6);
	RTLU (SK_REF, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU(SK_INT32,&arg2);
	RTLU (SK_REF, &Current);
	RTLU(SK_REF, &loc1);
	RTLU(SK_REF, &loc2);
	RTLU(SK_REF, &loc3);
	
	RTEAA(l_feature_name, 657, Current, 3, 2, 16017);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16017);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 3, 0xF80004B3, 0, 0); /* loc3 */
	loc3 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(2);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(19333, "perform_horizontal_computation", loc3))(loc3);
	RTHOOK(3);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(19332, "perform_vertical_computation", loc3))(loc3);
	RTHOOK(4);
	RTDBGAL(Current, 0, 0xF800049E, 0,0); /* Result */
	ui4_1 = arg1;
	ui4_2 = arg2;
	Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10846, dtype))(Current, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(5);
	if ((EIF_BOOLEAN)(Result == NULL)) {
		RTHOOK(6);
		RTDBGAL(Current, 1, 0xF8000686, 0, 0); /* loc1 */
		ui4_1 = arg1;
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10863, dtype))(Current, ui4_1x)).it_i4);
		ui4_1 = ti4_1;
		ui4_2 = ((EIF_INTEGER_32) 0L);
		loc1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10836, dtype))(Current, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		RTHOOK(7);
		RTDBGAL(Current, 2, 0xF8000686, 0, 0); /* loc2 */
		ui4_1 = arg2;
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10864, dtype))(Current, ui4_1x)).it_i4);
		ui4_1 = ti4_1;
		ui4_2 = ((EIF_INTEGER_32) 0L);
		loc2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10837, dtype))(Current, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		RTHOOK(8);
		tb1 = '\0';
		tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8471, "is_empty", loc1))(loc1)).it_b);
		if ((EIF_BOOLEAN) !tb2) {
			tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8471, "is_empty", loc2))(loc2)).it_b);
			tb1 = (EIF_BOOLEAN) !tb2;
		}
		if (tb1) {
			RTHOOK(9);
			RTDBGAL(Current, 0, 0xF800049E, 0,0); /* Result */
			ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(9851, "first", loc1))(loc1)).it_i4);
			ui4_1 = ti4_1;
			ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(9851, "first", loc2))(loc2)).it_i4);
			ui4_2 = ti4_2;
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(19501, "item_internal", loc3))(loc3, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			Result = (EIF_REFERENCE) RTCCL(tr1);
			RTHOOK(10);
			if ((EIF_BOOLEAN)(Result != NULL)) {
				RTHOOK(11);
				tb1 = '\01';
				tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18841, "column", Result))(Result)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				RTNHOOK(11,1);
				tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16600, "is_locked", tr1))(tr1)).it_b);
				if (!tb2) {
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18843, "row", Result))(Result)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					RTNHOOK(11,2);
					tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16645, "is_locked", tr1))(tr1)).it_b);
					tb1 = tb2;
				}
				if (tb1) {
					RTHOOK(12);
					RTDBGAL(Current, 0, 0xF800049E, 0,0); /* Result */
					Result = (EIF_REFERENCE) NULL;
				}
			}
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(13);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(7);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ui4_1
#undef ui4_2
#undef arg2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.row_at_position */
EIF_TYPED_VALUE F658_12107 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x)
{
	GTCX
	char *l_feature_name = "row_at_position";
	RTEX;
	EIF_REFERENCE loc1 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc2 = (EIF_REFERENCE) 0;
#define arg1 arg1x.it_i4
#define arg2 arg2x.it_b
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_BOOLEAN tb1;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_b = * (EIF_BOOLEAN *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(5);
	RTLR(0,loc2);
	RTLR(1,Current);
	RTLR(2,Result);
	RTLR(3,loc1);
	RTLR(4,tr1);
	RTLIU(5);
	RTLU (SK_REF, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU(SK_BOOL,&arg2);
	RTLU (SK_REF, &Current);
	RTLU(SK_REF, &loc1);
	RTLU(SK_REF, &loc2);
	
	RTEAA(l_feature_name, 657, Current, 2, 2, 16018);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16018);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 2, 0xF80004B3, 0, 0); /* loc2 */
	loc2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(2);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(19333, "perform_horizontal_computation", loc2))(loc2);
	RTHOOK(3);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(19332, "perform_vertical_computation", loc2))(loc2);
	RTHOOK(4);
	if ((EIF_BOOLEAN) !arg2) {
		RTHOOK(5);
		RTDBGAL(Current, 0, 0xF8000491, 0,0); /* Result */
		ui4_1 = arg1;
		Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10847, dtype))(Current, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	}
	RTHOOK(6);
	if ((EIF_BOOLEAN)(Result == NULL)) {
		RTHOOK(7);
		RTDBGAL(Current, 1, 0xF8000686, 0, 0); /* loc1 */
		ui4_1 = arg1;
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10864, dtype))(Current, ui4_1x)).it_i4);
		ui4_1 = ti4_1;
		ui4_2 = ((EIF_INTEGER_32) 0L);
		loc1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10837, dtype))(Current, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		RTHOOK(8);
		tb1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8471, "is_empty", loc1))(loc1)).it_b);
		if ((EIF_BOOLEAN) !tb1) {
			RTHOOK(9);
			RTDBGAL(Current, 0, 0xF8000491, 0,0); /* Result */
			ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(9851, "first", loc1))(loc1)).it_i4);
			ui4_1 = ti4_1;
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(19470, "row_internal", loc2))(loc2, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			Result = (EIF_REFERENCE) RTCCL(tr1);
			RTHOOK(10);
			if ((EIF_BOOLEAN) !arg2) {
				if (
					WDBG(657, (char *) 0)
				) {
					RTHOOK(11);
					tb1 = *(EIF_BOOLEAN *)(Result + RTVA(18750, "is_locked", Result));
					if (tb1) {
					}
					RTHOOK(12);
					RTDBGAL(Current, 0, 0xF8000491, 0,0); /* Result */
					Result = (EIF_REFERENCE) NULL;
				}
			}
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(13);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(6);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ui4_1
#undef ui4_2
#undef arg2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.column_at_position */
EIF_TYPED_VALUE F658_12108 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x)
{
	GTCX
	char *l_feature_name = "column_at_position";
	RTEX;
	EIF_REFERENCE loc1 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc2 = (EIF_REFERENCE) 0;
#define arg1 arg1x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_BOOLEAN tb1;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(5);
	RTLR(0,loc2);
	RTLR(1,Current);
	RTLR(2,Result);
	RTLR(3,loc1);
	RTLR(4,tr1);
	RTLIU(5);
	RTLU (SK_REF, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU (SK_REF, &Current);
	RTLU(SK_REF, &loc1);
	RTLU(SK_REF, &loc2);
	
	RTEAA(l_feature_name, 657, Current, 2, 1, 16019);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16019);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 2, 0xF80004B3, 0, 0); /* loc2 */
	loc2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(2);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(19333, "perform_horizontal_computation", loc2))(loc2);
	RTHOOK(3);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(19332, "perform_vertical_computation", loc2))(loc2);
	RTHOOK(4);
	RTDBGAL(Current, 0, 0xF8000490, 0,0); /* Result */
	ui4_1 = arg1;
	Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10848, dtype))(Current, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(5);
	if ((EIF_BOOLEAN)(Result == NULL)) {
		RTHOOK(6);
		RTDBGAL(Current, 1, 0xF8000686, 0, 0); /* loc1 */
		ui4_1 = arg1;
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10863, dtype))(Current, ui4_1x)).it_i4);
		ui4_1 = ti4_1;
		ui4_2 = ((EIF_INTEGER_32) 0L);
		loc1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10836, dtype))(Current, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		RTHOOK(7);
		tb1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8471, "is_empty", loc1))(loc1)).it_b);
		if ((EIF_BOOLEAN) !tb1) {
			RTHOOK(8);
			RTDBGAL(Current, 0, 0xF8000490, 0,0); /* Result */
			ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(9851, "first", loc1))(loc1)).it_i4);
			ui4_1 = ti4_1;
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(19471, "column_internal", loc2))(loc2, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			Result = (EIF_REFERENCE) RTCCL(tr1);
			RTHOOK(9);
			tb1 = *(EIF_BOOLEAN *)(Result + RTVA(18685, "is_locked", Result));
			if (tb1) {
				RTHOOK(10);
				RTDBGAL(Current, 0, 0xF8000490, 0,0); /* Result */
				Result = (EIF_REFERENCE) NULL;
			}
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(11);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(5);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ui4_1
#undef ui4_2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.item_at_position_strict */
EIF_TYPED_VALUE F658_12109 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x)
{
	GTCX
	char *l_feature_name = "item_at_position_strict";
	RTEX;
	EIF_INTEGER_32 loc1 = (EIF_INTEGER_32) 0;
#define arg1 arg1x.it_i4
#define arg2 arg2x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ur1x = {{0}, SK_REF};
#define ur1 ur1x.it_r
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_i4 = * (EIF_INTEGER_32 *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(4);
	RTLR(0,Current);
	RTLR(1,Result);
	RTLR(2,tr1);
	RTLR(3,ur1);
	RTLIU(4);
	RTLU (SK_REF, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU(SK_INT32,&arg2);
	RTLU (SK_REF, &Current);
	RTLU(SK_INT32, &loc1);
	
	RTEAA(l_feature_name, 657, Current, 1, 2, 16020);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16020);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 0, 0xF800049E, 0,0); /* Result */
	ui4_1 = arg1;
	ui4_2 = arg2;
	Result = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10842, dtype))(Current, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(2);
	if ((EIF_BOOLEAN)(Result != NULL)) {
		RTHOOK(3);
		RTDBGAL(Current, 1, 0x10000000, 1, 0); /* loc1 */
		tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		RTNHOOK(3,1);
		ur1 = RTCCL(Result);
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(19344, "item_indent", tr1))(tr1, ur1x)).it_i4);
		loc1 = (EIF_INTEGER_32) ti4_1;
		RTHOOK(4);
		ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18844, "virtual_x_position", Result))(Result)).it_i4);
		if ((EIF_BOOLEAN) ((EIF_INTEGER_32) (arg1 - ti4_1) < ((EIF_INTEGER_32) 0L))) {
			RTHOOK(5);
			RTDBGAL(Current, 0, 0xF800049E, 0,0); /* Result */
			Result = (EIF_REFERENCE) NULL;
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(6);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(5);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ur1
#undef ui4_1
#undef ui4_2
#undef arg2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.locked_item_at_position */
EIF_TYPED_VALUE F658_12110 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x)
{
	GTCX
	char *l_feature_name = "locked_item_at_position";
	RTEX;
	EIF_REFERENCE loc1 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc2 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc3 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc4 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc5 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc6 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc7 = (EIF_REFERENCE) 0;
	EIF_BOOLEAN loc8 = (EIF_BOOLEAN) 0;
	EIF_REFERENCE loc9 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc10 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc11 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc12 = (EIF_REFERENCE) 0;
#define arg1 arg1x.it_i4
#define arg2 arg2x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ur1x = {{0}, SK_REF};
#define ur1 ur1x.it_r
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_REFERENCE tr2 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_BOOLEAN tb1;
	EIF_BOOLEAN tb2;
	EIF_BOOLEAN tb3;
	EIF_BOOLEAN tb4;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_i4 = * (EIF_INTEGER_32 *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(14);
	RTLR(0,loc10);
	RTLR(1,Current);
	RTLR(2,loc1);
	RTLR(3,tr1);
	RTLR(4,loc9);
	RTLR(5,loc2);
	RTLR(6,loc6);
	RTLR(7,Result);
	RTLR(8,loc11);
	RTLR(9,tr2);
	RTLR(10,loc3);
	RTLR(11,loc7);
	RTLR(12,loc12);
	RTLR(13,ur1);
	RTLIU(14);
	RTLU (SK_REF, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU(SK_INT32,&arg2);
	RTLU (SK_REF, &Current);
	RTLU(SK_REF, &loc1);
	RTLU(SK_REF, &loc2);
	RTLU(SK_REF, &loc3);
	RTLU(SK_INT32, &loc4);
	RTLU(SK_INT32, &loc5);
	RTLU(SK_REF, &loc6);
	RTLU(SK_REF, &loc7);
	RTLU(SK_BOOL, &loc8);
	RTLU(SK_REF, &loc9);
	RTLU(SK_REF, &loc10);
	RTLU(SK_REF, &loc11);
	RTLU(SK_REF, &loc12);
	
	RTEAA(l_feature_name, 657, Current, 12, 2, 16021);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16021);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 10, 0xF80004B3, 0, 0); /* loc10 */
	loc10 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(2);
	RTDBGAL(Current, 1, 0xF800061F, 0, 0); /* loc1 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19173, "locked_indexes", loc10))(loc10)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	loc1 = (EIF_REFERENCE) RTCCL(tr1);
	RTHOOK(3);
	RTDBGAL(Current, 9, 0xF8000259, 0, 0); /* loc9 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8571, "cursor", loc1))(loc1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	loc9 = (EIF_REFERENCE) RTCCL(tr1);
	RTHOOK(4);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(8496, "finish", loc1))(loc1);
	for (;;) {
		RTHOOK(5);
		tb1 = '\01';
		tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8480, "off", loc1))(loc1)).it_b);
		if (!tb2) {
			tb1 = loc8;
		}
		if (tb1) break;
		RTHOOK(6);
		RTDBGAL(Current, 2, 0xF80001AA, 0, 0); /* loc2 */
		tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc1))(loc1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		loc2 = RTCCL(tr1);
		loc2 = RTRV(eif_new_type(426, 0x00), loc2);
		RTHOOK(7);
		if ((EIF_BOOLEAN)(loc2 != NULL)) {
			RTHOOK(8);
			tb2 = *(EIF_BOOLEAN *)(loc10 + RTVA(19153, "is_row_height_fixed", loc10));
			if (tb2) {
				RTHOOK(9);
				RTDBGAL(Current, 4, 0x10000000, 1, 0); /* loc4 */
				ti4_1 = *(EIF_INTEGER_32 *)(loc10 + RTVA(19155, "row_height", loc10));
				loc4 = (EIF_INTEGER_32) ti4_1;
			} else {
				RTHOOK(10);
				RTDBGAL(Current, 4, 0x10000000, 1, 0); /* loc4 */
				tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(7249, "row_i", loc2))(loc2)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				RTNHOOK(10,1);
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18732, "height", tr1))(tr1)).it_i4);
				loc4 = (EIF_INTEGER_32) ti4_1;
			}
			RTHOOK(11);
			tb2 = '\0';
			ti4_1 = *(EIF_INTEGER_32 *)(loc10 + RTVA(19363, "viewport_y_offset", loc10));
			ti4_2 = *(EIF_INTEGER_32 *)(loc2 + RTVA(7216, "offset", loc2));
			if ((EIF_BOOLEAN) (arg2 >= (EIF_INTEGER_32) (ti4_1 + ti4_2))) {
				ti4_1 = *(EIF_INTEGER_32 *)(loc10 + RTVA(19363, "viewport_y_offset", loc10));
				ti4_2 = *(EIF_INTEGER_32 *)(loc2 + RTVA(7216, "offset", loc2));
				tb2 = (EIF_BOOLEAN) (arg2 < (EIF_INTEGER_32) ((EIF_INTEGER_32) (ti4_1 + ti4_2) + loc4));
			}
			if (tb2) {
				RTHOOK(12);
				RTDBGAL(Current, 8, 0x04000000, 1, 0); /* loc8 */
				loc8 = (EIF_BOOLEAN) (EIF_BOOLEAN) 1;
				RTHOOK(13);
				RTDBGAL(Current, 6, 0xF8000686, 0, 0); /* loc6 */
				ui4_1 = arg1;
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10863, dtype))(Current, ui4_1x)).it_i4);
				ui4_1 = ti4_1;
				ui4_2 = ((EIF_INTEGER_32) 0L);
				loc6 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10836, dtype))(Current, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				RTHOOK(14);
				tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8471, "is_empty", loc6))(loc6)).it_b);
				if ((EIF_BOOLEAN) !tb2) {
					RTHOOK(15);
					RTDBGAL(Current, 0, 0xF800049E, 0,0); /* Result */
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(9851, "first", loc6))(loc6)).it_i4);
					ui4_1 = ti4_1;
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(7249, "row_i", loc2))(loc2)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					RTNHOOK(15,1);
					ti4_2 = *(EIF_INTEGER_32 *)(tr1 + RTVA(18743, "index", tr1));
					ui4_2 = ti4_2;
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(19501, "item_internal", loc10))(loc10, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					Result = (EIF_REFERENCE) RTCCL(tr1);
					RTHOOK(16);
					if ((EIF_BOOLEAN)(Result != NULL)) {
						RTHOOK(17);
						tb2 = '\0';
						tb3 = '\0';
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18841, "column", Result))(Result)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(17,1);
						tb4 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16600, "is_locked", tr1))(tr1)).it_b);
						if (tb4) {
							tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18841, "column", Result))(Result)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							RTNHOOK(17,2);
							tr2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16281, "implementation", tr1))(tr1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							RTNHOOK(17,3);
							tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18693, "locked_column", tr2))(tr2)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							loc11 = RTCCL(tr1);
							tb3 = EIF_TEST(loc11);
						}
						if (tb3) {
							ti4_1 = *(EIF_INTEGER_32 *)(loc11 + RTVA(7222, "locked_index", loc11));
							ti4_2 = *(EIF_INTEGER_32 *)(loc2 + RTVA(7222, "locked_index", loc2));
							tb2 = (EIF_BOOLEAN) (ti4_1 > ti4_2);
						}
						if (tb2) {
							RTHOOK(18);
							RTDBGAL(Current, 0, 0xF800049E, 0,0); /* Result */
							Result = (EIF_REFERENCE) NULL;
						}
					}
				}
			}
		} else {
			RTHOOK(19);
			RTDBGAL(Current, 3, 0xF80001AB, 0, 0); /* loc3 */
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc1))(loc1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			loc3 = RTCCL(tr1);
			loc3 = RTRV(eif_new_type(427, 0x00), loc3);
			RTHOOK(20);
			if ((EIF_BOOLEAN)(loc3 != NULL)) {
				RTHOOK(21);
				RTDBGAL(Current, 5, 0x10000000, 1, 0); /* loc5 */
				tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(7251, "column_i", loc3))(loc3)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				RTNHOOK(21,1);
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18679, "width", tr1))(tr1)).it_i4);
				loc5 = (EIF_INTEGER_32) ti4_1;
				RTHOOK(22);
				tb2 = '\0';
				ti4_1 = *(EIF_INTEGER_32 *)(loc10 + RTVA(19362, "viewport_x_offset", loc10));
				ti4_2 = *(EIF_INTEGER_32 *)(loc3 + RTVA(7216, "offset", loc3));
				if ((EIF_BOOLEAN) (arg1 >= (EIF_INTEGER_32) (ti4_1 + ti4_2))) {
					ti4_1 = *(EIF_INTEGER_32 *)(loc10 + RTVA(19362, "viewport_x_offset", loc10));
					ti4_2 = *(EIF_INTEGER_32 *)(loc3 + RTVA(7216, "offset", loc3));
					tb2 = (EIF_BOOLEAN) (arg1 < (EIF_INTEGER_32) ((EIF_INTEGER_32) (ti4_1 + ti4_2) + loc5));
				}
				if (tb2) {
					RTHOOK(23);
					RTDBGAL(Current, 8, 0x04000000, 1, 0); /* loc8 */
					loc8 = (EIF_BOOLEAN) (EIF_BOOLEAN) 1;
					RTHOOK(24);
					RTDBGAL(Current, 7, 0xF8000686, 0, 0); /* loc7 */
					ui4_1 = arg2;
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10864, dtype))(Current, ui4_1x)).it_i4);
					ui4_1 = ti4_1;
					ui4_2 = ((EIF_INTEGER_32) 0L);
					loc7 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10837, dtype))(Current, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					RTHOOK(25);
					tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8471, "is_empty", loc7))(loc7)).it_b);
					if ((EIF_BOOLEAN) !tb2) {
						RTHOOK(26);
						RTDBGAL(Current, 0, 0xF800049E, 0,0); /* Result */
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(7251, "column_i", loc3))(loc3)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(26,1);
						ti4_1 = *(EIF_INTEGER_32 *)(tr1 + RTVA(18694, "index", tr1));
						ui4_1 = ti4_1;
						ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(9851, "first", loc7))(loc7)).it_i4);
						ui4_2 = ti4_2;
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(19501, "item_internal", loc10))(loc10, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						Result = (EIF_REFERENCE) RTCCL(tr1);
						RTHOOK(27);
						if ((EIF_BOOLEAN)(Result != NULL)) {
							RTHOOK(28);
							tb2 = '\0';
							tb3 = '\0';
							tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18843, "row", Result))(Result)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							RTNHOOK(28,1);
							tb4 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16645, "is_locked", tr1))(tr1)).it_b);
							if (tb4) {
								tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18843, "row", Result))(Result)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
								RTNHOOK(28,2);
								tr2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10869, "implementation", tr1))(tr1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
								RTNHOOK(28,3);
								tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18740, "locked_row", tr2))(tr2)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
								loc12 = RTCCL(tr1);
								tb3 = EIF_TEST(loc12);
							}
							if (tb3) {
								ti4_1 = *(EIF_INTEGER_32 *)(loc12 + RTVA(7222, "locked_index", loc12));
								ti4_2 = *(EIF_INTEGER_32 *)(loc3 + RTVA(7222, "locked_index", loc3));
								tb2 = (EIF_BOOLEAN) (ti4_1 > ti4_2);
							}
							if (tb2) {
								RTHOOK(29);
								RTDBGAL(Current, 0, 0xF800049E, 0,0); /* Result */
								Result = (EIF_REFERENCE) NULL;
							}
						}
					}
				}
			}
		}
		RTHOOK(30);
		(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(8499, "back", loc1))(loc1);
	}
	RTHOOK(31);
	ur1 = RTCCL(loc9);
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8573, "go_to", loc1))(loc1, ur1x);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(32);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(16);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ur1
#undef ui4_1
#undef ui4_2
#undef arg2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.locked_row_at_position */
EIF_TYPED_VALUE F658_12111 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x)
{
	GTCX
	char *l_feature_name = "locked_row_at_position";
	RTEX;
	EIF_REFERENCE loc1 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc2 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc3 = (EIF_INTEGER_32) 0;
	EIF_BOOLEAN loc4 = (EIF_BOOLEAN) 0;
	EIF_REFERENCE loc5 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc6 = (EIF_REFERENCE) 0;
#define arg1 arg1x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ur1x = {{0}, SK_REF};
#define ur1 ur1x.it_r
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_BOOLEAN tb1;
	EIF_BOOLEAN tb2;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(8);
	RTLR(0,loc6);
	RTLR(1,Current);
	RTLR(2,loc1);
	RTLR(3,tr1);
	RTLR(4,loc5);
	RTLR(5,loc2);
	RTLR(6,Result);
	RTLR(7,ur1);
	RTLIU(8);
	RTLU (SK_REF, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU (SK_REF, &Current);
	RTLU(SK_REF, &loc1);
	RTLU(SK_REF, &loc2);
	RTLU(SK_INT32, &loc3);
	RTLU(SK_BOOL, &loc4);
	RTLU(SK_REF, &loc5);
	RTLU(SK_REF, &loc6);
	
	RTEAA(l_feature_name, 657, Current, 6, 1, 16022);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 0);
	RTGC;
	RTDBGEAA(657, Current, 16022);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 6, 0xF80004B3, 0, 0); /* loc6 */
	loc6 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, Dtype(Current)))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(2);
	RTDBGAL(Current, 1, 0xF800061F, 0, 0); /* loc1 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19173, "locked_indexes", loc6))(loc6)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	loc1 = (EIF_REFERENCE) RTCCL(tr1);
	RTHOOK(3);
	RTDBGAL(Current, 5, 0xF8000259, 0, 0); /* loc5 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8571, "cursor", loc1))(loc1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	loc5 = (EIF_REFERENCE) RTCCL(tr1);
	RTHOOK(4);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(8496, "finish", loc1))(loc1);
	for (;;) {
		RTHOOK(5);
		tb1 = '\01';
		tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8480, "off", loc1))(loc1)).it_b);
		if (!tb2) {
			tb1 = loc4;
		}
		if (tb1) break;
		RTHOOK(6);
		RTDBGAL(Current, 2, 0xF80001AA, 0, 0); /* loc2 */
		tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc1))(loc1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		loc2 = RTCCL(tr1);
		loc2 = RTRV(eif_new_type(426, 0x00), loc2);
		RTHOOK(7);
		if ((EIF_BOOLEAN)(loc2 != NULL)) {
			RTHOOK(8);
			tb2 = *(EIF_BOOLEAN *)(loc6 + RTVA(19153, "is_row_height_fixed", loc6));
			if (tb2) {
				RTHOOK(9);
				RTDBGAL(Current, 3, 0x10000000, 1, 0); /* loc3 */
				ti4_1 = *(EIF_INTEGER_32 *)(loc6 + RTVA(19155, "row_height", loc6));
				loc3 = (EIF_INTEGER_32) ti4_1;
			} else {
				RTHOOK(10);
				RTDBGAL(Current, 3, 0x10000000, 1, 0); /* loc3 */
				tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(7249, "row_i", loc2))(loc2)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				RTNHOOK(10,1);
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18732, "height", tr1))(tr1)).it_i4);
				loc3 = (EIF_INTEGER_32) ti4_1;
			}
			RTHOOK(11);
			tb2 = '\0';
			ti4_1 = *(EIF_INTEGER_32 *)(loc6 + RTVA(19363, "viewport_y_offset", loc6));
			ti4_2 = *(EIF_INTEGER_32 *)(loc2 + RTVA(7216, "offset", loc2));
			if ((EIF_BOOLEAN) (arg1 >= (EIF_INTEGER_32) (ti4_1 + ti4_2))) {
				ti4_1 = *(EIF_INTEGER_32 *)(loc6 + RTVA(19363, "viewport_y_offset", loc6));
				ti4_2 = *(EIF_INTEGER_32 *)(loc2 + RTVA(7216, "offset", loc2));
				tb2 = (EIF_BOOLEAN) (arg1 < (EIF_INTEGER_32) ((EIF_INTEGER_32) (ti4_1 + ti4_2) + loc3));
			}
			if (tb2) {
				RTHOOK(12);
				RTDBGAL(Current, 0, 0xF8000491, 0,0); /* Result */
				tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(7249, "row_i", loc2))(loc2)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				Result = (EIF_REFERENCE) RTCCL(tr1);
			}
		}
		RTHOOK(13);
		(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(8499, "back", loc1))(loc1);
	}
	RTHOOK(14);
	ur1 = RTCCL(loc5);
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8573, "go_to", loc1))(loc1, ur1x);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(15);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(9);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ur1
#undef arg1
}

/* {EV_GRID_DRAWER_I}.locked_column_at_position */
EIF_TYPED_VALUE F658_12112 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x)
{
	GTCX
	char *l_feature_name = "locked_column_at_position";
	RTEX;
	EIF_REFERENCE loc1 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc2 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc3 = (EIF_INTEGER_32) 0;
	EIF_BOOLEAN loc4 = (EIF_BOOLEAN) 0;
	EIF_REFERENCE loc5 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc6 = (EIF_REFERENCE) 0;
#define arg1 arg1x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ur1x = {{0}, SK_REF};
#define ur1 ur1x.it_r
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_BOOLEAN tb1;
	EIF_BOOLEAN tb2;
	EIF_REFERENCE Result = ((EIF_REFERENCE) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(8);
	RTLR(0,loc6);
	RTLR(1,Current);
	RTLR(2,loc1);
	RTLR(3,tr1);
	RTLR(4,loc5);
	RTLR(5,loc2);
	RTLR(6,Result);
	RTLR(7,ur1);
	RTLIU(8);
	RTLU (SK_REF, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU (SK_REF, &Current);
	RTLU(SK_REF, &loc1);
	RTLU(SK_REF, &loc2);
	RTLU(SK_INT32, &loc3);
	RTLU(SK_BOOL, &loc4);
	RTLU(SK_REF, &loc5);
	RTLU(SK_REF, &loc6);
	
	RTEAA(l_feature_name, 657, Current, 6, 1, 16023);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 0);
	RTGC;
	RTDBGEAA(657, Current, 16023);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 6, 0xF80004B3, 0, 0); /* loc6 */
	loc6 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, Dtype(Current)))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(2);
	RTDBGAL(Current, 1, 0xF800061F, 0, 0); /* loc1 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19173, "locked_indexes", loc6))(loc6)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	loc1 = (EIF_REFERENCE) RTCCL(tr1);
	RTHOOK(3);
	RTDBGAL(Current, 5, 0xF8000259, 0, 0); /* loc5 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8571, "cursor", loc1))(loc1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	loc5 = (EIF_REFERENCE) RTCCL(tr1);
	RTHOOK(4);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(8496, "finish", loc1))(loc1);
	for (;;) {
		RTHOOK(5);
		tb1 = '\01';
		tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8480, "off", loc1))(loc1)).it_b);
		if (!tb2) {
			tb1 = loc4;
		}
		if (tb1) break;
		RTHOOK(6);
		RTDBGAL(Current, 2, 0xF80001AB, 0, 0); /* loc2 */
		tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc1))(loc1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		loc2 = RTCCL(tr1);
		loc2 = RTRV(eif_new_type(427, 0x00), loc2);
		RTHOOK(7);
		if ((EIF_BOOLEAN)(loc2 != NULL)) {
			RTHOOK(8);
			RTDBGAL(Current, 3, 0x10000000, 1, 0); /* loc3 */
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(7251, "column_i", loc2))(loc2)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			RTNHOOK(8,1);
			ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18679, "width", tr1))(tr1)).it_i4);
			loc3 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(9);
			tb2 = '\0';
			ti4_1 = *(EIF_INTEGER_32 *)(loc6 + RTVA(19362, "viewport_x_offset", loc6));
			ti4_2 = *(EIF_INTEGER_32 *)(loc2 + RTVA(7216, "offset", loc2));
			if ((EIF_BOOLEAN) (arg1 >= (EIF_INTEGER_32) (ti4_1 + ti4_2))) {
				ti4_1 = *(EIF_INTEGER_32 *)(loc6 + RTVA(19362, "viewport_x_offset", loc6));
				ti4_2 = *(EIF_INTEGER_32 *)(loc2 + RTVA(7216, "offset", loc2));
				tb2 = (EIF_BOOLEAN) (arg1 < (EIF_INTEGER_32) ((EIF_INTEGER_32) (ti4_1 + ti4_2) + loc3));
			}
			if (tb2) {
				RTHOOK(10);
				RTDBGAL(Current, 0, 0xF8000490, 0,0); /* Result */
				tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(7251, "column_i", loc2))(loc2)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				Result = (EIF_REFERENCE) RTCCL(tr1);
			}
		}
		RTHOOK(11);
		(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(8499, "back", loc1))(loc1);
	}
	RTHOOK(12);
	ur1 = RTCCL(loc5);
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8573, "go_to", loc1))(loc1, ur1x);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(13);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(9);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef ur1
#undef arg1
}

/* {EV_GRID_DRAWER_I}.redraw_area_in_virtual_coordinates */
void F658_12113 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x, EIF_TYPED_VALUE arg3x, EIF_TYPED_VALUE arg4x)
{
	GTCX
	char *l_feature_name = "redraw_area_in_virtual_coordinates";
	RTEX;
#define arg1 arg1x.it_i4
#define arg2 arg2x.it_i4
#define arg3 arg3x.it_i4
#define arg4 arg4x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_TYPED_VALUE ui4_3x = {{0}, SK_INT32};
#define ui4_3 ui4_3x.it_i4
	EIF_TYPED_VALUE ui4_4x = {{0}, SK_INT32};
#define ui4_4 ui4_4x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_INTEGER_32 ti4_3;
	EIF_INTEGER_32 ti4_4;
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg4x.type & SK_HEAD) == SK_REF) arg4x.it_i4 = * (EIF_INTEGER_32 *) arg4x.it_r;
	if ((arg3x.type & SK_HEAD) == SK_REF) arg3x.it_i4 = * (EIF_INTEGER_32 *) arg3x.it_r;
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_i4 = * (EIF_INTEGER_32 *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(2);
	RTLR(0,Current);
	RTLR(1,tr1);
	RTLIU(2);
	RTLU (SK_VOID, NULL);
	RTLU(SK_INT32,&arg1);
	RTLU(SK_INT32,&arg2);
	RTLU(SK_INT32,&arg3);
	RTLU(SK_INT32,&arg4);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 4, 16024);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16024);
	RTIV(Current, RTAL);
	RTHOOK(1);
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,1);
	ti4_1 = *(EIF_INTEGER_32 *)(tr1 + RTVA(19360, "internal_client_x", tr1));
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,2);
	ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19169, "viewable_x_offset", tr1))(tr1)).it_i4);
	ui4_1 = (EIF_INTEGER_32) ((EIF_INTEGER_32) (arg1 - ti4_1) + ti4_2);
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,3);
	ti4_3 = *(EIF_INTEGER_32 *)(tr1 + RTVA(19361, "internal_client_y", tr1));
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,4);
	ti4_4 = *(EIF_INTEGER_32 *)(tr1 + RTVA(19363, "viewport_y_offset", tr1));
	ui4_2 = (EIF_INTEGER_32) ((EIF_INTEGER_32) (arg2 - ti4_3) + ti4_4);
	ui4_3 = arg3;
	ui4_4 = arg4;
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10850, dtype))(Current, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(6);
	RTEE;
#undef up1
#undef ui4_1
#undef ui4_2
#undef ui4_3
#undef ui4_4
#undef arg4
#undef arg3
#undef arg2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.redraw_area_in_drawable_coordinates_wrapper */
void F658_12114 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x, EIF_TYPED_VALUE arg3x, EIF_TYPED_VALUE arg4x)
{
	GTCX
	char *l_feature_name = "redraw_area_in_drawable_coordinates_wrapper";
	RTEX;
#define arg1 arg1x.it_i4
#define arg2 arg2x.it_i4
#define arg3 arg3x.it_i4
#define arg4 arg4x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE up2x = {{0}, SK_POINTER};
#define up2 up2x.it_p
	EIF_TYPED_VALUE ur1x = {{0}, SK_REF};
#define ur1 ur1x.it_r
	EIF_TYPED_VALUE ur2x = {{0}, SK_REF};
#define ur2 ur2x.it_r
	EIF_TYPED_VALUE ur3x = {{0}, SK_REF};
#define ur3 ur3x.it_r
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_TYPED_VALUE ui4_3x = {{0}, SK_INT32};
#define ui4_3 ui4_3x.it_i4
	EIF_TYPED_VALUE ui4_4x = {{0}, SK_INT32};
#define ui4_4 ui4_4x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_REFERENCE tr2 = NULL;
	EIF_REFERENCE tr3 = NULL;
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg4x.type & SK_HEAD) == SK_REF) arg4x.it_i4 = * (EIF_INTEGER_32 *) arg4x.it_r;
	if ((arg3x.type & SK_HEAD) == SK_REF) arg3x.it_i4 = * (EIF_INTEGER_32 *) arg3x.it_r;
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_i4 = * (EIF_INTEGER_32 *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(7);
	RTLR(0,Current);
	RTLR(1,tr1);
	RTLR(2,tr2);
	RTLR(3,ur1);
	RTLR(4,tr3);
	RTLR(5,ur2);
	RTLR(6,ur3);
	RTLIU(7);
	RTLU (SK_VOID, NULL);
	RTLU(SK_INT32,&arg1);
	RTLU(SK_INT32,&arg2);
	RTLU(SK_INT32,&arg3);
	RTLU(SK_INT32,&arg4);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 4, 16025);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16025);
	RTIV(Current, RTAL);
	RTHOOK(1);
	ui4_1 = arg1;
	ui4_2 = arg2;
	ui4_3 = arg3;
	ui4_4 = arg4;
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,1);
	tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19359, "drawable", tr1))(tr1)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
	ur1 = RTCCL(tr2);
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,2);
	tr3 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19364, "viewport", tr1))(tr1)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
	ur2 = RTCCL(tr3);
	ur3 = NULL;
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10851, dtype))(Current, ui4_1x, ui4_2x, ui4_3x, ui4_4x, ur1x, ur2x, ur3x);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(6);
	RTEE;
#undef up1
#undef up2
#undef ur1
#undef ur2
#undef ur3
#undef ui4_1
#undef ui4_2
#undef ui4_3
#undef ui4_4
#undef arg4
#undef arg3
#undef arg2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.redraw_area_in_drawable_coordinates */
void F658_12115 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x, EIF_TYPED_VALUE arg3x, EIF_TYPED_VALUE arg4x, EIF_TYPED_VALUE arg5x, EIF_TYPED_VALUE arg6x, EIF_TYPED_VALUE arg7x)
{
	GTCX
	char *l_feature_name = "redraw_area_in_drawable_coordinates";
	RTEX;
	EIF_INTEGER_32 loc1 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc2 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc3 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc4 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc5 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc6 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc7 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc8 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc9 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc10 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc11 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc12 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc13 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc14 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc15 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc16 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc17 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc18 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc19 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc20 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc21 = (EIF_INTEGER_32) 0;
	EIF_BOOLEAN loc22 = (EIF_BOOLEAN) 0;
	EIF_INTEGER_32 loc23 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc24 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc25 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc26 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc27 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc28 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc29 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc30 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc31 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc32 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc33 = (EIF_INTEGER_32) 0;
	EIF_BOOLEAN loc34 = (EIF_BOOLEAN) 0;
	EIF_BOOLEAN loc35 = (EIF_BOOLEAN) 0;
	EIF_INTEGER_32 loc36 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc37 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc38 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc39 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc40 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc41 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc42 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc43 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc44 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc45 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc46 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc47 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc48 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc49 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc50 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc51 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc52 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc53 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc54 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc55 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc56 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc57 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc58 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc59 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc60 = (EIF_REFERENCE) 0;
	EIF_BOOLEAN loc61 = (EIF_BOOLEAN) 0;
	EIF_INTEGER_32 loc62 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc63 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc64 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc65 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc66 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc67 = (EIF_INTEGER_32) 0;
	EIF_INTEGER_32 loc68 = (EIF_INTEGER_32) 0;
	EIF_BOOLEAN loc69 = (EIF_BOOLEAN) 0;
	EIF_BOOLEAN loc70 = (EIF_BOOLEAN) 0;
	EIF_BOOLEAN loc71 = (EIF_BOOLEAN) 0;
	EIF_REFERENCE loc72 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc73 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc74 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc75 = (EIF_REFERENCE) 0;
	EIF_INTEGER_32 loc76 = (EIF_INTEGER_32) 0;
	EIF_REFERENCE loc77 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc78 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc79 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc80 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc81 = (EIF_REFERENCE) 0;
	EIF_REFERENCE loc82 = (EIF_REFERENCE) 0;
#define arg1 arg1x.it_i4
#define arg2 arg2x.it_i4
#define arg3 arg3x.it_i4
#define arg4 arg4x.it_i4
#define arg5 arg5x.it_r
#define arg6 arg6x.it_r
#define arg7 arg7x.it_r
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE up2x = {{0}, SK_POINTER};
#define up2 up2x.it_p
	EIF_TYPED_VALUE ur1x = {{0}, SK_REF};
#define ur1 ur1x.it_r
	EIF_TYPED_VALUE ur2x = {{0}, SK_REF};
#define ur2 ur2x.it_r
	EIF_TYPED_VALUE ur3x = {{0}, SK_REF};
#define ur3 ur3x.it_r
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_TYPED_VALUE ui4_3x = {{0}, SK_INT32};
#define ui4_3 ui4_3x.it_i4
	EIF_TYPED_VALUE ui4_4x = {{0}, SK_INT32};
#define ui4_4 ui4_4x.it_i4
	EIF_TYPED_VALUE ui4_5x = {{0}, SK_INT32};
#define ui4_5 ui4_5x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_REFERENCE tr2 = NULL;
	EIF_REFERENCE tr3 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_BOOLEAN tb1;
	EIF_BOOLEAN tb2;
	EIF_BOOLEAN tb3;
	EIF_BOOLEAN tb4;
	EIF_BOOLEAN tb5;
	EIF_BOOLEAN tb6;
	EIF_BOOLEAN tb7;
	RTCFDT;
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg4x.type & SK_HEAD) == SK_REF) arg4x.it_i4 = * (EIF_INTEGER_32 *) arg4x.it_r;
	if ((arg3x.type & SK_HEAD) == SK_REF) arg3x.it_i4 = * (EIF_INTEGER_32 *) arg3x.it_r;
	if ((arg2x.type & SK_HEAD) == SK_REF) arg2x.it_i4 = * (EIF_INTEGER_32 *) arg2x.it_r;
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(40);
	RTLR(0,arg5);
	RTLR(1,arg6);
	RTLR(2,arg7);
	RTLR(3,loc78);
	RTLR(4,Current);
	RTLR(5,tr1);
	RTLR(6,loc27);
	RTLR(7,loc5);
	RTLR(8,loc43);
	RTLR(9,loc42);
	RTLR(10,loc64);
	RTLR(11,loc8);
	RTLR(12,loc74);
	RTLR(13,loc75);
	RTLR(14,loc16);
	RTLR(15,loc17);
	RTLR(16,loc31);
	RTLR(17,loc32);
	RTLR(18,loc65);
	RTLR(19,loc7);
	RTLR(20,loc6);
	RTLR(21,loc47);
	RTLR(22,ur1);
	RTLR(23,loc72);
	RTLR(24,loc66);
	RTLR(25,tr2);
	RTLR(26,loc12);
	RTLR(27,loc13);
	RTLR(28,tr3);
	RTLR(29,loc48);
	RTLR(30,ur2);
	RTLR(31,loc58);
	RTLR(32,loc59);
	RTLR(33,loc77);
	RTLR(34,loc60);
	RTLR(35,ur3);
	RTLR(36,loc79);
	RTLR(37,loc80);
	RTLR(38,loc81);
	RTLR(39,loc82);
	RTLIU(40);
	RTLU (SK_VOID, NULL);
	RTLU(SK_INT32,&arg1);
	RTLU(SK_INT32,&arg2);
	RTLU(SK_INT32,&arg3);
	RTLU(SK_INT32,&arg4);
	RTLU(SK_REF,&arg5);
	RTLU(SK_REF,&arg6);
	RTLU(SK_REF,&arg7);
	RTLU (SK_REF, &Current);
	RTLU(SK_INT32, &loc1);
	RTLU(SK_INT32, &loc2);
	RTLU(SK_INT32, &loc3);
	RTLU(SK_INT32, &loc4);
	RTLU(SK_REF, &loc5);
	RTLU(SK_REF, &loc6);
	RTLU(SK_REF, &loc7);
	RTLU(SK_REF, &loc8);
	RTLU(SK_INT32, &loc9);
	RTLU(SK_INT32, &loc10);
	RTLU(SK_INT32, &loc11);
	RTLU(SK_REF, &loc12);
	RTLU(SK_REF, &loc13);
	RTLU(SK_INT32, &loc14);
	RTLU(SK_INT32, &loc15);
	RTLU(SK_REF, &loc16);
	RTLU(SK_REF, &loc17);
	RTLU(SK_INT32, &loc18);
	RTLU(SK_INT32, &loc19);
	RTLU(SK_INT32, &loc20);
	RTLU(SK_INT32, &loc21);
	RTLU(SK_BOOL, &loc22);
	RTLU(SK_INT32, &loc23);
	RTLU(SK_INT32, &loc24);
	RTLU(SK_INT32, &loc25);
	RTLU(SK_INT32, &loc26);
	RTLU(SK_REF, &loc27);
	RTLU(SK_INT32, &loc28);
	RTLU(SK_INT32, &loc29);
	RTLU(SK_INT32, &loc30);
	RTLU(SK_REF, &loc31);
	RTLU(SK_REF, &loc32);
	RTLU(SK_INT32, &loc33);
	RTLU(SK_BOOL, &loc34);
	RTLU(SK_BOOL, &loc35);
	RTLU(SK_INT32, &loc36);
	RTLU(SK_INT32, &loc37);
	RTLU(SK_INT32, &loc38);
	RTLU(SK_INT32, &loc39);
	RTLU(SK_INT32, &loc40);
	RTLU(SK_INT32, &loc41);
	RTLU(SK_REF, &loc42);
	RTLU(SK_REF, &loc43);
	RTLU(SK_INT32, &loc44);
	RTLU(SK_INT32, &loc45);
	RTLU(SK_INT32, &loc46);
	RTLU(SK_REF, &loc47);
	RTLU(SK_REF, &loc48);
	RTLU(SK_INT32, &loc49);
	RTLU(SK_INT32, &loc50);
	RTLU(SK_INT32, &loc51);
	RTLU(SK_INT32, &loc52);
	RTLU(SK_INT32, &loc53);
	RTLU(SK_INT32, &loc54);
	RTLU(SK_INT32, &loc55);
	RTLU(SK_INT32, &loc56);
	RTLU(SK_INT32, &loc57);
	RTLU(SK_REF, &loc58);
	RTLU(SK_REF, &loc59);
	RTLU(SK_REF, &loc60);
	RTLU(SK_BOOL, &loc61);
	RTLU(SK_INT32, &loc62);
	RTLU(SK_INT32, &loc63);
	RTLU(SK_REF, &loc64);
	RTLU(SK_REF, &loc65);
	RTLU(SK_REF, &loc66);
	RTLU(SK_INT32, &loc67);
	RTLU(SK_INT32, &loc68);
	RTLU(SK_BOOL, &loc69);
	RTLU(SK_BOOL, &loc70);
	RTLU(SK_BOOL, &loc71);
	RTLU(SK_REF, &loc72);
	RTLU(SK_INT32, &loc73);
	RTLU(SK_REF, &loc74);
	RTLU(SK_REF, &loc75);
	RTLU(SK_INT32, &loc76);
	RTLU(SK_REF, &loc77);
	RTLU(SK_REF, &loc78);
	RTLU(SK_REF, &loc79);
	RTLU(SK_REF, &loc80);
	RTLU(SK_REF, &loc81);
	RTLU(SK_REF, &loc82);
	
	RTEAA(l_feature_name, 657, Current, 82, 7, 16026);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTAOMS(12114,1);
	RTDBGEAA(657, Current, 16026);
	RTCC(arg5, 657, l_feature_name, 5, eif_new_type(994, 0x01), 0x01);
	RTCC(arg6, 657, l_feature_name, 6, eif_new_type(1034, 0x01), 0x01);
	if (arg7) {
		RTCC(arg7, 657, l_feature_name, 7, eif_new_type(425, 0x00), 0x00);
	}
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 78, 0xF80004B3, 0, 0); /* loc78 */
	loc78 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(2);
	tb1 = *(EIF_BOOLEAN *)(loc78 + RTVA(19383, "is_locked", loc78));
	if ((EIF_BOOLEAN) !tb1) {
		RTHOOK(3);
		(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(19349, "reset_redraw_object_counter", loc78))(loc78);
		RTHOOK(4);
		if ((EIF_BOOLEAN) ((EIF_BOOLEAN) (arg3 > ((EIF_INTEGER_32) 0L)) && (EIF_BOOLEAN) (arg4 > ((EIF_INTEGER_32) 0L)))) {
			RTHOOK(5);
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			RTNHOOK(5,1);
			(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(16727, "set_copy_mode", tr1))(tr1);
			RTHOOK(6);
			RTDBGAL(Current, 27, 0xF800063A, 0, 0); /* loc27 */
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19156, "dynamic_content_function", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			loc27 = (EIF_REFERENCE) RTCCL(tr1);
			RTHOOK(7);
			(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(19333, "perform_horizontal_computation", loc78))(loc78);
			RTHOOK(8);
			(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(19332, "perform_vertical_computation", loc78))(loc78);
			RTHOOK(9);
			RTDBGAL(Current, 5, 0xF8000686, 0, 0); /* loc5 */
			{
				static EIF_TYPE_INDEX typarr0[] = {0xFF01,1670,869,0xFFFF};
				EIF_TYPE typres0;
				static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
				
				typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(dftype, typarr0)));
				tr1 = RTLN(typres0.id);
			}
			ui4_1 = ((EIF_INTEGER_32) 8L);
			(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWC(10419, Dtype(tr1)))(tr1, ui4_1x);
			RTNHOOK(9,1);
			loc5 = (EIF_REFERENCE) RTCCL(tr1);
			RTHOOK(10);
			ui4_1 = ((EIF_INTEGER_32) 0L);
			(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8520, "extend", loc5))(loc5, ui4_1x);
			RTHOOK(11);
			RTDBGAL(Current, 43, 0xF800041D, 0, 0); /* loc43 */
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19370, "expand_node_pixmap", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			loc43 = (EIF_REFERENCE) RTCCL(tr1);
			RTHOOK(12);
			RTDBGAL(Current, 42, 0xF800041D, 0, 0); /* loc42 */
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19371, "collapse_node_pixmap", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			loc42 = (EIF_REFERENCE) RTCCL(tr1);
			RTHOOK(13);
			RTDBGAL(Current, 61, 0x04000000, 1, 0); /* loc61 */
			tb1 = *(EIF_BOOLEAN *)(loc78 + RTVA(19158, "are_tree_node_connectors_shown", loc78));
			loc61 = (EIF_BOOLEAN) tb1;
			RTHOOK(14);
			RTDBGAL(Current, 64, 0xF80003C2, 0, 0); /* loc64 */
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19290, "tree_node_connector_color", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			loc64 = (EIF_REFERENCE) RTCCL(tr1);
			RTHOOK(15);
			RTDBGAL(Current, 40, 0x10000000, 1, 0); /* loc40 */
			ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19369, "tree_node_spacing", loc78))(loc78)).it_i4);
			loc40 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(16);
			RTDBGAL(Current, 45, 0x10000000, 1, 0); /* loc45 */
			ti4_1 = *(EIF_INTEGER_32 *)(loc78 + RTVA(19378, "node_pixmap_width", loc78));
			loc45 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(17);
			RTDBGAL(Current, 46, 0x10000000, 1, 0); /* loc46 */
			ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16694, "height", loc43))(loc43)).it_i4);
			loc46 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(18);
			RTDBGAL(Current, 33, 0x10000000, 1, 0); /* loc33 */
			ti4_1 = *(EIF_INTEGER_32 *)(loc78 + RTVA(19381, "tree_subrow_indent", loc78));
			loc33 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(19);
			RTDBGAL(Current, 41, 0x10000000, 1, 0); /* loc41 */
			ti4_1 = *(EIF_INTEGER_32 *)(loc78 + RTVA(19379, "total_tree_node_width", loc78));
			loc41 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(20);
			RTDBGAL(Current, 44, 0x10000000, 1, 0); /* loc44 */
			ti4_1 = *(EIF_INTEGER_32 *)(loc78 + RTVA(19380, "first_tree_node_indent", loc78));
			loc44 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(21);
			RTDBGAL(Current, 8, 0xF8000684, 0, 0); /* loc8 */
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19321, "physical_column_indexes", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			loc8 = (EIF_REFERENCE) tr1;
			RTHOOK(22);
			if ((EIF_BOOLEAN)(arg7 == NULL)) {
				RTHOOK(23);
				RTDBGAL(Current, 1, 0x10000000, 1, 0); /* loc1 */
				ti4_1 = *(EIF_INTEGER_32 *)(loc78 + RTVA(19360, "internal_client_x", loc78));
				loc1 = (EIF_INTEGER_32) ti4_1;
				RTHOOK(24);
				RTDBGAL(Current, 2, 0x10000000, 1, 0); /* loc2 */
				ti4_1 = *(EIF_INTEGER_32 *)(loc78 + RTVA(19361, "internal_client_y", loc78));
				loc2 = (EIF_INTEGER_32) ti4_1;
			} else {
				RTHOOK(25);
				RTDBGAL(Current, 74, 0xF80001AB, 0, 0); /* loc74 */
				loc74 = RTCCL(arg7);
				loc74 = RTRV(eif_new_type(427, 0x00), loc74);
				RTHOOK(26);
				RTDBGAL(Current, 75, 0xF80001AA, 0, 0); /* loc75 */
				loc75 = RTCCL(arg7);
				loc75 = RTRV(eif_new_type(426, 0x00), loc75);
				RTHOOK(27);
				if ((EIF_BOOLEAN)(loc74 != NULL)) {
					RTHOOK(28);
					RTDBGAL(Current, 1, 0x10000000, 1, 0); /* loc1 */
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(7251, "column_i", loc74))(loc74)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					RTNHOOK(28,1);
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18680, "virtual_x_position", tr1))(tr1)).it_i4);
					loc1 = (EIF_INTEGER_32) ti4_1;
					RTHOOK(29);
					RTDBGAL(Current, 2, 0x10000000, 1, 0); /* loc2 */
					ti4_1 = *(EIF_INTEGER_32 *)(arg7 + RTVA(7242, "internal_client_y", arg7));
					loc2 = (EIF_INTEGER_32) ti4_1;
				} else {
					RTHOOK(30);
					if ((EIF_BOOLEAN)(loc75 != NULL)) {
						RTHOOK(31);
						RTDBGAL(Current, 2, 0x10000000, 1, 0); /* loc2 */
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(7249, "row_i", loc75))(loc75)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(31,1);
						ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18737, "virtual_y_position", tr1))(tr1)).it_i4);
						loc2 = (EIF_INTEGER_32) ti4_1;
						RTHOOK(32);
						RTDBGAL(Current, 1, 0x10000000, 1, 0); /* loc1 */
						ti4_1 = *(EIF_INTEGER_32 *)(arg7 + RTVA(7243, "internal_client_x", arg7));
						loc1 = (EIF_INTEGER_32) ti4_1;
					} else {
						if (RTAL & CK_CHECK) {
							RTHOOK(33);
							RTCT(NULL, EX_CHECK);
								RTCF;
						}
					}
				}
			}
			RTHOOK(34);
			RTDBGAL(Current, 28, 0x10000000, 1, 0); /* loc28 */
			ti4_1 = *(EIF_INTEGER_32 *)(loc78 + RTVA(19167, "viewable_width", loc78));
			loc28 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(35);
			RTDBGAL(Current, 29, 0x10000000, 1, 0); /* loc29 */
			ti4_1 = *(EIF_INTEGER_32 *)(loc78 + RTVA(19168, "viewable_height", loc78));
			loc29 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(36);
			RTDBGAL(Current, 3, 0x10000000, 1, 0); /* loc3 */
			ti4_1 = *(EIF_INTEGER_32 *)(loc78 + RTVA(19363, "viewport_y_offset", loc78));
			loc3 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(37);
			RTDBGAL(Current, 4, 0x10000000, 1, 0); /* loc4 */
			ti4_1 = *(EIF_INTEGER_32 *)(loc78 + RTVA(19362, "viewport_x_offset", loc78));
			loc4 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(38);
			RTDBGAL(Current, 67, 0x10000000, 1, 0); /* loc67 */
			ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19318, "row_count", loc78))(loc78)).it_i4);
			loc67 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(39);
			RTDBGAL(Current, 69, 0x04000000, 1, 0); /* loc69 */
			tb1 = *(EIF_BOOLEAN *)(loc78 + RTVA(19275, "is_tree_enabled", loc78));
			loc69 = (EIF_BOOLEAN) tb1;
			RTHOOK(40);
			RTDBGAL(Current, 70, 0x04000000, 1, 0); /* loc70 */
			tb1 = *(EIF_BOOLEAN *)(loc78 + RTVA(19152, "is_content_partially_dynamic", loc78));
			loc70 = (EIF_BOOLEAN) tb1;
			RTHOOK(41);
			RTDBGAL(Current, 68, 0x10000000, 1, 0); /* loc68 */
			ti4_1 = *(EIF_INTEGER_32 *)(loc78 + RTVA(19155, "row_height", loc78));
			loc68 = (EIF_INTEGER_32) ti4_1;
			RTHOOK(42);
			tb1 = '\0';
			if ((EIF_BOOLEAN) (loc67 > ((EIF_INTEGER_32) 0L))) {
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19317, "column_count", loc78))(loc78)).it_i4);
				tb1 = (EIF_BOOLEAN) (ti4_1 > ((EIF_INTEGER_32) 0L));
			}
			if (tb1) {
				RTHOOK(43);
				RTDBGAL(Current, 16, 0xF8000686, 0, 0); /* loc16 */
				tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19355, "column_offsets", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				loc16 = (EIF_REFERENCE) RTCCL(tr1);
				RTHOOK(44);
				RTDBGAL(Current, 17, 0xF8000686, 0, 0); /* loc17 */
				tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19356, "row_offsets", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				loc17 = (EIF_REFERENCE) RTCCL(tr1);
				RTHOOK(45);
				if ((EIF_BOOLEAN)(loc74 == NULL)) {
					RTHOOK(46);
					RTDBGAL(Current, 31, 0xF8000686, 0, 0); /* loc31 */
					ui4_1 = arg1;
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10863, dtype))(Current, ui4_1x)).it_i4);
					ui4_1 = ti4_1;
					ui4_2 = (EIF_INTEGER_32) (arg3 - ((EIF_INTEGER_32) 1L));
					loc31 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10836, dtype))(Current, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				} else {
					RTHOOK(47);
					RTDBGAL(Current, 31, 0xF8000686, 0, 0); /* loc31 */
					{
						static EIF_TYPE_INDEX typarr0[] = {0xFF01,1670,869,0xFFFF};
						EIF_TYPE typres0;
						static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
						
						typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(dftype, typarr0)));
						tr1 = RTLN(typres0.id);
					}
					ui4_1 = ((EIF_INTEGER_32) 1L);
					(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWC(10419, Dtype(tr1)))(tr1, ui4_1x);
					RTNHOOK(47,1);
					loc31 = (EIF_REFERENCE) RTCCL(tr1);
					RTHOOK(48);
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(7251, "column_i", loc74))(loc74)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					RTNHOOK(48,1);
					ti4_1 = *(EIF_INTEGER_32 *)(tr1 + RTVA(18694, "index", tr1));
					ui4_1 = ti4_1;
					(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8520, "extend", loc31))(loc31, ui4_1x);
				}
				RTHOOK(49);
				if ((EIF_BOOLEAN)(loc75 == NULL)) {
					RTHOOK(50);
					RTDBGAL(Current, 32, 0xF8000686, 0, 0); /* loc32 */
					ui4_1 = arg2;
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10864, dtype))(Current, ui4_1x)).it_i4);
					ui4_1 = ti4_1;
					ui4_2 = (EIF_INTEGER_32) (arg4 - ((EIF_INTEGER_32) 1L));
					loc32 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10837, dtype))(Current, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				} else {
					RTHOOK(51);
					RTDBGAL(Current, 32, 0xF8000686, 0, 0); /* loc32 */
					{
						static EIF_TYPE_INDEX typarr0[] = {0xFF01,1670,869,0xFFFF};
						EIF_TYPE typres0;
						static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
						
						typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(dftype, typarr0)));
						tr1 = RTLN(typres0.id);
					}
					ui4_1 = ((EIF_INTEGER_32) 1L);
					(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWC(10419, Dtype(tr1)))(tr1, ui4_1x);
					RTNHOOK(51,1);
					loc32 = (EIF_REFERENCE) RTCCL(tr1);
					RTHOOK(52);
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(7249, "row_i", loc75))(loc75)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					RTNHOOK(52,1);
					ti4_1 = *(EIF_INTEGER_32 *)(tr1 + RTVA(18743, "index", tr1));
					ui4_1 = ti4_1;
					(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8520, "extend", loc32))(loc32, ui4_1x);
				}
				RTHOOK(53);
				tb1 = '\01';
				tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8471, "is_empty", loc31))(loc31)).it_b);
				if (!tb2) {
					tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8471, "is_empty", loc32))(loc32)).it_b);
					tb1 = tb2;
				}
				if ((EIF_BOOLEAN) !tb1) {
					RTHOOK(54);
					RTDBGAL(Current, 9, 0x10000000, 1, 0); /* loc9 */
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(9851, "first", loc31))(loc31)).it_i4);
					loc9 = (EIF_INTEGER_32) ti4_1;
					RTHOOK(55);
					RTDBGAL(Current, 11, 0x10000000, 1, 0); /* loc11 */
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(9852, "last", loc31))(loc31)).it_i4);
					loc11 = (EIF_INTEGER_32) ti4_1;
					RTHOOK(56);
					RTDBGAL(Current, 10, 0x10000000, 1, 0); /* loc10 */
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(9851, "first", loc32))(loc32)).it_i4);
					loc10 = (EIF_INTEGER_32) ti4_1;
					RTHOOK(57);
					RTDBGAL(Current, 23, 0x10000000, 1, 0); /* loc23 */
					loc23 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
					RTHOOK(58);
					(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(8481, "start", loc32))(loc32);
					RTHOOK(59);
					RTDBGAL(Current, 65, 0xF800063E, 0, 0); /* loc65 */
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19320, "internal_row_data", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					loc65 = (EIF_REFERENCE) RTCCL(tr1);
					for (;;) {
						RTHOOK(60);
						tb1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8480, "off", loc32))(loc32)).it_b);
						if (tb1) break;
						RTHOOK(61);
						RTDBGAL(Current, 15, 0x10000000, 1, 0); /* loc15 */
						ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc32))(loc32)).it_i4);
						loc15 = (EIF_INTEGER_32) ti4_1;
						RTHOOK(62);
						RTDBGAL(Current, 7, 0xF8000491, 0, 0); /* loc7 */
						ui4_1 = loc15;
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(19470, "row_internal", loc78))(loc78, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						loc7 = (EIF_REFERENCE) RTCCL(tr1);
						RTHOOK(63);
						RTDBGAL(Current, 6, 0xF800060B, 0, 0); /* loc6 */
						ui4_1 = loc15;
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8560, "at", loc65))(loc65, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						loc6 = (EIF_REFERENCE) tr1;
						if (RTAL & CK_CHECK) {
							RTHOOK(64);
							RTCT(NULL, EX_CHECK);
							if ((EIF_BOOLEAN)(loc6 != NULL)) {
								RTCK;
							} else {
								RTCF;
							}
						}
						RTHOOK(65);
						tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19502, "uses_row_offsets", loc78))(loc78)).it_b);
						if ((EIF_BOOLEAN) !tb2) {
							RTHOOK(66);
							RTDBGAL(Current, 23, 0x10000000, 1, 0); /* loc23 */
							loc23 = (EIF_INTEGER_32) (EIF_INTEGER_32) (((EIF_INTEGER_32) (loc68 * (EIF_INTEGER_32) (loc15 - ((EIF_INTEGER_32) 1L)))) - (EIF_INTEGER_32) (loc2 - loc3));
							RTHOOK(67);
							RTDBGAL(Current, 19, 0x10000000, 1, 0); /* loc19 */
							loc19 = (EIF_INTEGER_32) loc68;
						} else {
							RTHOOK(68);
							if ((EIF_BOOLEAN)(loc17 != NULL)) {
								RTHOOK(69);
								RTDBGAL(Current, 23, 0x10000000, 1, 0); /* loc23 */
								ui4_1 = loc15;
								ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8560, "at", loc17))(loc17, ui4_1x)).it_i4);
								loc23 = (EIF_INTEGER_32) (EIF_INTEGER_32) (ti4_1 - (EIF_INTEGER_32) (loc2 - loc3));
								RTHOOK(70);
								tb2 = *(EIF_BOOLEAN *)(loc78 + RTVA(19153, "is_row_height_fixed", loc78));
								if (tb2) {
									RTHOOK(71);
									RTDBGAL(Current, 19, 0x10000000, 1, 0); /* loc19 */
									loc19 = (EIF_INTEGER_32) loc68;
								} else {
									RTHOOK(72);
									RTDBGAL(Current, 19, 0x10000000, 1, 0); /* loc19 */
									ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18732, "height", loc7))(loc7)).it_i4);
									loc19 = (EIF_INTEGER_32) ti4_1;
								}
							} else {
								if (RTAL & CK_CHECK) {
									RTHOOK(73);
									RTCT(NULL, EX_CHECK);
									if ((EIF_BOOLEAN)(loc17 != NULL)) {
										RTCK;
									} else {
										RTCF;
									}
								}
							}
						}
						RTHOOK(74);
						if (loc69) {
							RTHOOK(75);
							RTDBGAL(Current, 47, 0xF8000491, 0, 0); /* loc47 */
							tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18782, "parent_row_i", loc7))(loc7)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							loc47 = (EIF_REFERENCE) RTCCL(tr1);
							RTHOOK(76);
							RTDBGAL(Current, 34, 0x04000000, 1, 0); /* loc34 */
							loc34 = (EIF_BOOLEAN) (EIF_BOOLEAN)(loc47 != NULL);
							RTHOOK(77);
							RTDBGAL(Current, 35, 0x04000000, 1, 0); /* loc35 */
							tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18739, "is_expandable", loc7))(loc7)).it_b);
							loc35 = (EIF_BOOLEAN) tb2;
							RTHOOK(78);
							if ((EIF_BOOLEAN) (loc34 || loc35)) {
								RTHOOK(79);
								RTDBGAL(Current, 36, 0x10000000, 1, 0); /* loc36 */
								ur1 = RTCCL(loc7);
								loc36 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10854, dtype))(Current, ur1x)).it_i4);
								RTHOOK(80);
								if ((EIF_BOOLEAN)(loc47 != NULL)) {
									RTHOOK(81);
									RTDBGAL(Current, 37, 0x10000000, 1, 0); /* loc37 */
									ur1 = RTCCL(loc47);
									loc37 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10854, dtype))(Current, ur1x)).it_i4);
									RTHOOK(82);
									RTDBGAL(Current, 72, 0xF8000491, 0, 0); /* loc72 */
									loc72 = (EIF_REFERENCE) RTCCL(loc47);
									for (;;) {
										RTHOOK(83);
										if ((EIF_BOOLEAN)(loc72 == NULL)) break;
										RTHOOK(84);
										RTDBGAL(Current, 37, 0x10000000, 1, 0); /* loc37 */
										ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18746, "index_of_first_item", loc72))(loc72)).it_i4);
										ui4_1 = ti4_1;
										ti4_1 = eif_max_int32 (loc37,ui4_1);
										loc37 = (EIF_INTEGER_32) ti4_1;
										RTHOOK(85);
										RTDBGAL(Current, 72, 0xF8000491, 0, 0); /* loc72 */
										tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18782, "parent_row_i", loc72))(loc72)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										loc72 = (EIF_REFERENCE) RTCCL(tr1);
									}
									RTHOOK(86);
									RTDBGAL(Current, 38, 0x10000000, 1, 0); /* loc38 */
									ui4_1 = loc37;
									ti4_1 = *(EIF_INTEGER_32 *)(loc47 + RTVA(18743, "index", loc47));
									ui4_2 = ti4_1;
									ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(19345, "item_cell_indent", loc78))(loc78, ui4_1x, ui4_2x)).it_i4);
									loc38 = (EIF_INTEGER_32) (EIF_INTEGER_32) (ti4_1 + ((EIF_INTEGER_32) ((EIF_INTEGER_32) (loc45 + ((EIF_INTEGER_32) 1L)) / ((EIF_INTEGER_32) 2L))));
									RTHOOK(87);
									RTDBGAL(Current, 39, 0x10000000, 1, 0); /* loc39 */
									loc39 = (EIF_INTEGER_32) loc38;
									RTHOOK(88);
									RTDBGAL(Current, 36, 0x10000000, 1, 0); /* loc36 */
									ui4_1 = loc37;
									ti4_1 = eif_max_int32 (loc36,ui4_1);
									loc36 = (EIF_INTEGER_32) ti4_1;
								}
							} else {
								RTHOOK(89);
								RTDBGAL(Current, 36, 0x10000000, 1, 0); /* loc36 */
								loc36 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
							}
							RTHOOK(90);
							RTDBGAL(Current, 49, 0x10000000, 1, 0); /* loc49 */
							loc49 = (EIF_INTEGER_32) (EIF_INTEGER_32) (loc19 / ((EIF_INTEGER_32) 2L));
							RTHOOK(91);
							RTDBGAL(Current, 50, 0x10000000, 1, 0); /* loc50 */
							loc50 = (EIF_INTEGER_32) loc19;
							RTHOOK(92);
							RTDBGAL(Current, 51, 0x10000000, 1, 0); /* loc51 */
							loc51 = (EIF_INTEGER_32) ((EIF_INTEGER_32) ((EIF_INTEGER_32) ((EIF_INTEGER_32) (loc19 - loc46) + ((EIF_INTEGER_32) 1L)) / ((EIF_INTEGER_32) 2L)));
							RTHOOK(93);
							RTDBGAL(Current, 52, 0x10000000, 1, 0); /* loc52 */
							loc52 = (EIF_INTEGER_32) (EIF_INTEGER_32) (loc51 + loc46);
							RTHOOK(94);
							RTDBGAL(Current, 30, 0x10000000, 1, 0); /* loc30 */
							ur1 = RTCCL(loc7);
							loc30 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10853, dtype))(Current, ur1x)).it_i4);
						}
						RTHOOK(95);
						(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(8481, "start", loc31))(loc31);
						RTHOOK(96);
						RTDBGAL(Current, 24, 0x10000000, 1, 0); /* loc24 */
						loc24 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
						RTHOOK(97);
						RTDBGAL(Current, 71, 0x04000000, 1, 0); /* loc71 */
						loc71 = (EIF_BOOLEAN) (EIF_BOOLEAN) 0;
						for (;;) {
							RTHOOK(98);
							tb2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8480, "off", loc31))(loc31)).it_b);
							if (tb2) break;
							if (RTAL & CK_CHECK) {
								RTHOOK(99);
								RTCT("lists_valid_lengths", EX_CHECK);
								ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10700, "count", loc8))(loc8)).it_i4);
								ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8735, "count", loc31))(loc31)).it_i4);
								if ((EIF_BOOLEAN) (ti4_1 >= ti4_2)) {
									RTCK;
								} else {
									RTCF;
								}
							}
							RTHOOK(100);
							RTDBGAL(Current, 14, 0x10000000, 1, 0); /* loc14 */
							ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc31))(loc31)).it_i4);
							loc14 = (EIF_INTEGER_32) ti4_1;
							RTHOOK(101);
							RTDBGAL(Current, 66, 0xF8000490, 0, 0); /* loc66 */
							tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19324, "columns", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							ui4_1 = loc14;
							tr2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8560, "at", tr1))(tr1, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							loc66 = (EIF_REFERENCE) RTCCL(tr2);
							RTHOOK(102);
							if ((EIF_BOOLEAN)(loc66 == NULL)) {
								RTHOOK(103);
								RTCT0(NULL, EX_CHECK);
								if ((EIF_BOOLEAN)(loc66 != NULL)) {
									RTCK0;
								} else {
									RTCF0;
								}
							}
							RTHOOK(104);
							RTDBGAL(Current, 18, 0x10000000, 1, 0); /* loc18 */
							ui4_1 = (EIF_INTEGER_32) (loc14 + ((EIF_INTEGER_32) 1L));
							ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8560, "at", loc16))(loc16, ui4_1x)).it_i4);
							ui4_1 = loc14;
							ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8560, "at", loc16))(loc16, ui4_1x)).it_i4);
							loc18 = (EIF_INTEGER_32) (EIF_INTEGER_32) (ti4_1 - ti4_2);
							RTHOOK(105);
							if ((EIF_BOOLEAN) (loc18 > ((EIF_INTEGER_32) 0L))) {
								RTHOOK(106);
								RTDBGAL(Current, 62, 0x10000000, 1, 0); /* loc62 */
								ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc31))(loc31)).it_i4);
								ui4_1 = (EIF_INTEGER_32) (ti4_1 - ((EIF_INTEGER_32) 1L));
								ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(9749, "item", loc8))(loc8, ui4_1x)).it_i4);
								loc62 = (EIF_INTEGER_32) ti4_1;
								RTHOOK(107);
								RTDBGAL(Current, 22, 0x04000000, 1, 0); /* loc22 */
								loc22 = (EIF_BOOLEAN) (EIF_BOOLEAN) 0;
								RTHOOK(108);
								tb3 = '\0';
								if ((EIF_BOOLEAN)(loc6 != NULL)) {
									ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10700, "count", loc6))(loc6)).it_i4);
									tb3 = (EIF_BOOLEAN) (loc62 < ti4_1);
								}
								if (tb3) {
									RTHOOK(109);
									RTDBGAL(Current, 12, 0xF800049E, 0, 0); /* loc12 */
									ui4_1 = loc62;
									tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(10706, "at", loc6))(loc6, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
									loc12 = (EIF_REFERENCE) RTCCL(tr1);
									RTHOOK(110);
									if ((EIF_BOOLEAN)(loc12 != NULL)) {
										RTHOOK(111);
										RTDBGAL(Current, 22, 0x04000000, 1, 0); /* loc22 */
										loc22 = (EIF_BOOLEAN) (EIF_BOOLEAN) 1;
									}
								}
								RTHOOK(112);
								RTDBGAL(Current, 24, 0x10000000, 1, 0); /* loc24 */
								ui4_1 = loc14;
								ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8560, "at", loc16))(loc16, ui4_1x)).it_i4);
								loc24 = (EIF_INTEGER_32) (EIF_INTEGER_32) (ti4_1 - (EIF_INTEGER_32) (loc1 - loc4));
								RTHOOK(113);
								if ((EIF_BOOLEAN) ((EIF_BOOLEAN) (loc70 && (EIF_BOOLEAN) !loc22) && (EIF_BOOLEAN)(loc27 != NULL))) {
									RTHOOK(114);
									RTDBGAL(Current, 13, 0xF80003E9, 0, 0); /* loc13 */
									{
										static EIF_TYPE_INDEX typarr0[] = {0xFF01,0xFFF9,2,840,869,869,0xFFFF};
										EIF_TYPE typres0;
										static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
										
										typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(dftype, typarr0)));
										tr1 = RTLNTS(typres0.id, 3, 1);
									}
									((EIF_TYPED_VALUE *)tr1+1)->it_i4 = loc14;
									((EIF_TYPED_VALUE *)tr1+2)->it_i4 = loc15;
									ur1 = RTCCL(tr1);
									tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(14731, "item", loc27))(loc27, ur1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
									loc13 = (EIF_REFERENCE) RTCCL(tr1);
									if (RTAL & CK_CHECK) {
										RTHOOK(115);
										RTCT("item_set_implies_set_item_is_returned_item", EX_CHECK);
										tb3 = '\01';
										ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc31))(loc31)).it_i4);
										ui4_1 = ti4_1;
										ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc32))(loc32)).it_i4);
										ui4_2 = ti4_2;
										tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(19501, "item_internal", loc78))(loc78, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										if ((EIF_BOOLEAN)(tr1 != NULL)) {
											ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc31))(loc31)).it_i4);
											ui4_1 = ti4_1;
											ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc32))(loc32)).it_i4);
											ui4_2 = ti4_2;
											tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(19501, "item_internal", loc78))(loc78, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											tr2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16281, "implementation", loc13))(loc13)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											tb3 = RTCEQ(tr1, tr2);
										}
										if (tb3) {
											RTCK;
										} else {
											RTCF;
										}
									}
									RTHOOK(116);
									if ((EIF_BOOLEAN)(loc13 != NULL)) {
										RTHOOK(117);
										RTDBGAL(Current, 12, 0xF800049E, 0, 0); /* loc12 */
										tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16281, "implementation", loc13))(loc13)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										loc12 = (EIF_REFERENCE) RTCCL(tr1);
										RTHOOK(118);
										ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc31))(loc31)).it_i4);
										ui4_1 = ti4_1;
										ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc32))(loc32)).it_i4);
										ui4_2 = ti4_2;
										tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(19137, "item", loc78))(loc78, ui4_1x, ui4_2x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										if ((EIF_BOOLEAN)(tr1 == NULL)) {
											RTHOOK(119);
											ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc31))(loc31)).it_i4);
											ui4_1 = ti4_1;
											ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8565, "item", loc32))(loc32)).it_i4);
											ui4_2 = ti4_2;
											tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18088, "interface", loc12))(loc12)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											ur1 = RTCCL(tr1);
											(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(19310, "set_item", loc78))(loc78, ui4_1x, ui4_2x, ur1x);
										}
										RTHOOK(120);
										RTDBGAL(Current, 22, 0x04000000, 1, 0); /* loc22 */
										loc22 = (EIF_BOOLEAN) (EIF_BOOLEAN) 1;
									}
									RTHOOK(121);
									if (loc69) {
										RTHOOK(122);
										RTDBGAL(Current, 47, 0xF8000491, 0, 0); /* loc47 */
										tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18782, "parent_row_i", loc7))(loc7)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										loc47 = (EIF_REFERENCE) RTCCL(tr1);
										RTHOOK(123);
										RTDBGAL(Current, 34, 0x04000000, 1, 0); /* loc34 */
										loc34 = (EIF_BOOLEAN) (EIF_BOOLEAN)(loc47 != NULL);
										RTHOOK(124);
										RTDBGAL(Current, 35, 0x04000000, 1, 0); /* loc35 */
										tb3 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18739, "is_expandable", loc7))(loc7)).it_b);
										loc35 = (EIF_BOOLEAN) tb3;
										RTHOOK(125);
										if ((EIF_BOOLEAN) (loc34 || loc35)) {
											RTHOOK(126);
											RTDBGAL(Current, 36, 0x10000000, 1, 0); /* loc36 */
											ur1 = RTCCL(loc7);
											loc36 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10854, dtype))(Current, ur1x)).it_i4);
											RTHOOK(127);
											if ((EIF_BOOLEAN)(loc47 != NULL)) {
												RTHOOK(128);
												RTDBGAL(Current, 37, 0x10000000, 1, 0); /* loc37 */
												ur1 = RTCCL(loc47);
												loc37 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10854, dtype))(Current, ur1x)).it_i4);
												RTHOOK(129);
												RTDBGAL(Current, 72, 0xF8000491, 0, 0); /* loc72 */
												loc72 = (EIF_REFERENCE) RTCCL(loc47);
												for (;;) {
													RTHOOK(130);
													if ((EIF_BOOLEAN)(loc72 == NULL)) break;
													RTHOOK(131);
													RTDBGAL(Current, 37, 0x10000000, 1, 0); /* loc37 */
													ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18746, "index_of_first_item", loc72))(loc72)).it_i4);
													ui4_1 = ti4_1;
													ti4_1 = eif_max_int32 (loc37,ui4_1);
													loc37 = (EIF_INTEGER_32) ti4_1;
													RTHOOK(132);
													RTDBGAL(Current, 72, 0xF8000491, 0, 0); /* loc72 */
													tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18782, "parent_row_i", loc72))(loc72)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
													loc72 = (EIF_REFERENCE) RTCCL(tr1);
												}
												RTHOOK(133);
												RTDBGAL(Current, 38, 0x10000000, 1, 0); /* loc38 */
												ui4_1 = loc37;
												ti4_1 = *(EIF_INTEGER_32 *)(loc47 + RTVA(18743, "index", loc47));
												ui4_2 = ti4_1;
												ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(19345, "item_cell_indent", loc78))(loc78, ui4_1x, ui4_2x)).it_i4);
												loc38 = (EIF_INTEGER_32) (EIF_INTEGER_32) (ti4_1 + ((EIF_INTEGER_32) ((EIF_INTEGER_32) (loc45 + ((EIF_INTEGER_32) 1L)) / ((EIF_INTEGER_32) 2L))));
												RTHOOK(134);
												RTDBGAL(Current, 39, 0x10000000, 1, 0); /* loc39 */
												loc39 = (EIF_INTEGER_32) loc38;
												RTHOOK(135);
												RTDBGAL(Current, 36, 0x10000000, 1, 0); /* loc36 */
												ui4_1 = loc37;
												ti4_1 = eif_max_int32 (loc36,ui4_1);
												loc36 = (EIF_INTEGER_32) ti4_1;
											}
										} else {
											RTHOOK(136);
											RTDBGAL(Current, 36, 0x10000000, 1, 0); /* loc36 */
											loc36 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
										}
										RTHOOK(137);
										RTDBGAL(Current, 49, 0x10000000, 1, 0); /* loc49 */
										loc49 = (EIF_INTEGER_32) (EIF_INTEGER_32) (loc19 / ((EIF_INTEGER_32) 2L));
										RTHOOK(138);
										RTDBGAL(Current, 50, 0x10000000, 1, 0); /* loc50 */
										loc50 = (EIF_INTEGER_32) loc19;
										RTHOOK(139);
										RTDBGAL(Current, 51, 0x10000000, 1, 0); /* loc51 */
										loc51 = (EIF_INTEGER_32) ((EIF_INTEGER_32) ((EIF_INTEGER_32) ((EIF_INTEGER_32) (loc19 - loc46) + ((EIF_INTEGER_32) 1L)) / ((EIF_INTEGER_32) 2L)));
										RTHOOK(140);
										RTDBGAL(Current, 52, 0x10000000, 1, 0); /* loc52 */
										loc52 = (EIF_INTEGER_32) (EIF_INTEGER_32) (loc51 + loc46);
										RTHOOK(141);
										RTDBGAL(Current, 30, 0x10000000, 1, 0); /* loc30 */
										ur1 = RTCCL(loc7);
										loc30 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10853, dtype))(Current, ur1x)).it_i4);
									}
									RTHOOK(142);
									RTDBGAL(Current, 6, 0xF800060B, 0, 0); /* loc6 */
									ui4_1 = loc15;
									tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8560, "at", loc65))(loc65, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
									loc6 = (EIF_REFERENCE) tr1;
								}
								RTHOOK(143);
								tb3 = '\01';
								tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
								RTNHOOK(143,1);
								ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16693, "width", tr1))(tr1)).it_i4);
								if (!(EIF_BOOLEAN) (ti4_1 < loc18)) {
									tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
									RTNHOOK(143,2);
									ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16694, "height", tr1))(tr1)).it_i4);
									tb3 = (EIF_BOOLEAN) (ti4_1 < loc19);
								}
								if (tb3) {
									RTHOOK(144);
									tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
									RTNHOOK(144,1);
									ui4_1 = loc18;
									ui4_2 = loc19;
									(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(17674, "set_size", tr1))(tr1, ui4_1x, ui4_2x);
								}
								RTHOOK(145);
								if (loc35) {
									RTHOOK(146);
									RTDBGAL(Current, 53, 0x10000000, 1, 0); /* loc53 */
									loc53 = (EIF_INTEGER_32) (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc30 - (EIF_INTEGER_32) (loc40 * ((EIF_INTEGER_32) 2L))) - loc45);
								} else {
									RTHOOK(147);
									if (loc34) {
										RTHOOK(148);
										RTDBGAL(Current, 53, 0x10000000, 1, 0); /* loc53 */
										loc53 = (EIF_INTEGER_32) loc30;
									} else {
										RTHOOK(149);
										RTDBGAL(Current, 53, 0x10000000, 1, 0); /* loc53 */
										loc53 = (EIF_INTEGER_32) loc18;
									}
								}
								RTHOOK(150);
								RTDBGAL(Current, 54, 0x10000000, 1, 0); /* loc54 */
								loc54 = (EIF_INTEGER_32) (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc30 - (EIF_INTEGER_32) (loc40 * ((EIF_INTEGER_32) 2L))) - (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc45 + ((EIF_INTEGER_32) 1L)) / ((EIF_INTEGER_32) 2L)));
								RTHOOK(151);
								RTDBGAL(Current, 25, 0x10000000, 1, 0); /* loc25 */
								loc25 = (EIF_INTEGER_32) loc24;
								RTHOOK(152);
								RTDBGAL(Current, 26, 0x10000000, 1, 0); /* loc26 */
								loc26 = (EIF_INTEGER_32) loc18;
								RTHOOK(153);
								if ((EIF_BOOLEAN) (loc22 && (EIF_BOOLEAN)(loc12 != NULL))) {
									RTHOOK(154);
									tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
									RTNHOOK(154,1);
									tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18877, "displayed_background_color", loc12))(loc12)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
									ur1 = RTCCL(tr2);
									(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", tr1))(tr1, ur1x);
								} else {
									RTHOOK(155);
									tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
									RTNHOOK(155,1);
									ui4_1 = loc14;
									ui4_2 = loc15;
									tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(19295, "displayed_background_color", loc78))(loc78, ui4_1x, ui4_2x)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
									ur1 = RTCCL(tr2);
									(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", tr1))(tr1, ur1x);
								}
								RTHOOK(156);
								tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
								RTNHOOK(156,1);
								ui4_1 = ((EIF_INTEGER_32) 0L);
								ui4_2 = ((EIF_INTEGER_32) 0L);
								ui4_3 = loc18;
								ui4_4 = loc19;
								(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16723, "fill_rectangle", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
								RTHOOK(157);
								tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10914, "pre_draw_overlay_actions_internal", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
								if ((EIF_BOOLEAN)(tr1 != NULL)) {
									RTHOOK(158);
									if ((EIF_BOOLEAN) (loc22 && (EIF_BOOLEAN)(loc12 != NULL))) {
										RTHOOK(159);
										tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10892, "pre_draw_overlay_actions", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										RTNHOOK(159,1);
										{
											static EIF_TYPE_INDEX typarr0[] = {0xFF01,0xFFF9,4,840,0xFF01,1053,1001,869,869,0xFFFF};
											EIF_TYPE typres0;
											static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
											
											typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(dftype, typarr0)));
											tr2 = RTLNTS(typres0.id, 5, 0);
										}
										tr3 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										((EIF_TYPED_VALUE *)tr2+1)->it_r = tr3;
										RTAR(tr2,tr3);
										tr3 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18088, "interface", loc12))(loc12)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										((EIF_TYPED_VALUE *)tr2+2)->it_r = tr3;
										RTAR(tr2,tr3);
										((EIF_TYPED_VALUE *)tr2+3)->it_i4 = loc14;
										((EIF_TYPED_VALUE *)tr2+4)->it_i4 = loc15;
										ur1 = RTCCL(tr2);
										(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(10459, "call", tr1))(tr1, ur1x);
									} else {
										RTHOOK(160);
										tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10892, "pre_draw_overlay_actions", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										RTNHOOK(160,1);
										{
											static EIF_TYPE_INDEX typarr0[] = {0xFF01,0xFFF9,4,840,0xFF01,1053,65534,869,869,0xFFFF};
											EIF_TYPE typres0;
											static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
											
											typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(dftype, typarr0)));
											tr2 = RTLNTS(typres0.id, 5, 0);
										}
										tr3 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										((EIF_TYPED_VALUE *)tr2+1)->it_r = tr3;
										RTAR(tr2,tr3);
										((EIF_TYPED_VALUE *)tr2+3)->it_i4 = loc14;
										((EIF_TYPED_VALUE *)tr2+4)->it_i4 = loc15;
										ur1 = RTCCL(tr2);
										(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(10459, "call", tr1))(tr1, ur1x);
									}
								}
								RTHOOK(161);
								if (loc69) {
									RTHOOK(162);
									if ((EIF_BOOLEAN)(loc14 == loc36)) {
										RTHOOK(163);
										RTDBGAL(Current, 25, 0x10000000, 1, 0); /* loc25 */
										loc25 += loc30;
										RTHOOK(164);
										RTDBGAL(Current, 26, 0x10000000, 1, 0); /* loc26 */
										loc26 -= loc30;
										RTHOOK(165);
										tb3 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18739, "is_expandable", loc7))(loc7)).it_b);
										if (tb3) {
											RTHOOK(166);
											tb3 = *(EIF_BOOLEAN *)(loc7 + RTVA(18731, "is_expanded", loc7));
											if (tb3) {
												RTHOOK(167);
												RTDBGAL(Current, 48, 0xF800041D, 0, 0); /* loc48 */
												loc48 = (EIF_REFERENCE) RTCCL(loc42);
											} else {
												RTHOOK(168);
												RTDBGAL(Current, 48, 0xF800041D, 0, 0); /* loc48 */
												loc48 = (EIF_REFERENCE) RTCCL(loc43);
											}
											RTHOOK(169);
											RTCOMS(tr1,12114,0,"Add horizontal clipping for pixmaps.",36,1728891694);
											ur1 = tr1;
											(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10831, dtype))(Current, ur1x);
											RTHOOK(170);
											if ((EIF_BOOLEAN) (loc53 < loc18)) {
												RTHOOK(171);
												if ((EIF_BOOLEAN) (loc46 > loc19)) {
													RTHOOK(172);
													tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
													RTNHOOK(172,1);
													ui4_1 = ((EIF_INTEGER_32) 0L);
													ui4_2 = (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc46 - loc19) / ((EIF_INTEGER_32) 2L));
													ui4_3 = loc46;
													ui4_4 = loc19;
													(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(10699, "move_and_resize", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
													RTHOOK(173);
													tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
													RTNHOOK(173,1);
													ui4_1 = loc53;
													ui4_2 = ((EIF_INTEGER_32) 0L);
													ur1 = RTCCL(loc48);
													tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
													ur2 = RTCCL(tr2);
													(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16716, "draw_sub_pixmap", tr1))(tr1, ui4_1x, ui4_2x, ur1x, ur2x);
												} else {
													RTHOOK(174);
													tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
													RTNHOOK(174,1);
													ui4_1 = loc53;
													ui4_2 = loc51;
													ur1 = RTCCL(loc48);
													(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16715, "draw_pixmap", tr1))(tr1, ui4_1x, ui4_2x, ur1x);
												}
											}
										}
										RTHOOK(175);
										if (loc61) {
											RTHOOK(176);
											if ((EIF_BOOLEAN) (loc35 || loc34)) {
												RTHOOK(177);
												RTDBGAL(Current, 55, 0x10000000, 1, 0); /* loc55 */
												loc55 = (EIF_INTEGER_32) loc30;
												RTHOOK(178);
												tb3 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18739, "is_expandable", loc7))(loc7)).it_b);
												if (tb3) {
													RTHOOK(179);
													RTDBGAL(Current, 56, 0x10000000, 1, 0); /* loc56 */
													loc56 = (EIF_INTEGER_32) (EIF_INTEGER_32) (loc53 + loc45);
												} else {
													RTHOOK(180);
													if ((EIF_BOOLEAN)(loc37 == loc36)) {
														RTHOOK(181);
														RTDBGAL(Current, 56, 0x10000000, 1, 0); /* loc56 */
														loc56 = (EIF_INTEGER_32) loc54;
													} else {
														RTHOOK(182);
														RTDBGAL(Current, 56, 0x10000000, 1, 0); /* loc56 */
														loc56 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
													}
												}
												RTHOOK(183);
												tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
												RTNHOOK(183,1);
												ur1 = RTCCL(loc64);
												(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", tr1))(tr1, ur1x);
												RTHOOK(184);
												if ((EIF_BOOLEAN) (loc55 < loc18)) {
													RTHOOK(185);
													tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
													RTNHOOK(185,1);
													ui4_1 = loc55;
													ui4_2 = loc49;
													ui4_3 = loc56;
													ui4_4 = loc49;
													(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
													RTHOOK(186);
													tb3 = '\0';
													if ((EIF_BOOLEAN) ((EIF_BOOLEAN) (loc37 > ((EIF_INTEGER_32) 0L)) && (EIF_BOOLEAN)(loc37 != loc36))) {
														tb4 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18739, "is_expandable", loc7))(loc7)).it_b);
														tb3 = tb4;
													}
													if (tb3) {
														RTHOOK(187);
														tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
														RTNHOOK(187,1);
														ui4_1 = ((EIF_INTEGER_32) 0L);
														ui4_2 = loc49;
														ui4_3 = loc53;
														ui4_4 = loc49;
														(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
													}
												} else {
													RTHOOK(188);
													ui4_1 = (EIF_INTEGER_32) (loc24 + loc18);
													ti4_1 = eif_min_int32 (loc56,ui4_1);
													if ((EIF_BOOLEAN)(ti4_1 != (EIF_INTEGER_32) (loc24 + loc18))) {
														RTHOOK(189);
														tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
														RTNHOOK(189,1);
														ui4_1 = loc18;
														ti4_1 = eif_min_int32 (loc56,ui4_1);
														ui4_1 = ti4_1;
														ui4_2 = loc49;
														ui4_3 = loc18;
														ui4_4 = loc49;
														(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
													}
												}
											}
											RTHOOK(190);
											if ((EIF_BOOLEAN) (loc34 && (EIF_BOOLEAN)(loc37 == loc14))) {
												RTHOOK(191);
												RTDBGAL(Current, 57, 0x10000000, 1, 0); /* loc57 */
												loc57 = (EIF_INTEGER_32) loc54;
												RTHOOK(192);
												if (loc61) {
													RTHOOK(193);
													tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
													RTNHOOK(193,1);
													ur1 = RTCCL(loc64);
													(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", tr1))(tr1, ur1x);
													RTHOOK(194);
													ui4_1 = (EIF_INTEGER_32) (loc36 + ((EIF_INTEGER_32) 1L));
													ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8560, "at", loc16))(loc16, ui4_1x)).it_i4);
													if ((EIF_BOOLEAN) (loc57 < ti4_1)) {
														RTHOOK(195);
														tb3 = '\0';
														if ((EIF_BOOLEAN)(loc47 != NULL)) {
															ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18741, "subrow_count", loc47))(loc47)).it_i4);
															ui4_1 = ti4_1;
															tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(18724, "subrow", loc47))(loc47, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
															tr2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18088, "interface", loc7))(loc7)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
															tb3 = RTCEQ(tr1, tr2);
														}
														if (tb3) {
															RTHOOK(196);
															tb3 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18739, "is_expandable", loc7))(loc7)).it_b);
															if (tb3) {
																RTHOOK(197);
																tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
																RTNHOOK(197,1);
																ui4_1 = loc54;
																ui4_2 = (EIF_INTEGER_32) (loc51 - ((EIF_INTEGER_32) 1L));
																ui4_3 = loc54;
																ui4_4 = ((EIF_INTEGER_32) 0L);
																(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
															} else {
																RTHOOK(198);
																tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
																RTNHOOK(198,1);
																ui4_1 = loc54;
																ui4_2 = loc49;
																ui4_3 = loc54;
																ui4_4 = ((EIF_INTEGER_32) 0L);
																(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
															}
														} else {
															RTHOOK(199);
															tb3 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18739, "is_expandable", loc7))(loc7)).it_b);
															if (tb3) {
																RTHOOK(200);
																tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
																RTNHOOK(200,1);
																ui4_1 = loc54;
																ui4_2 = (EIF_INTEGER_32) (loc51 - ((EIF_INTEGER_32) 1L));
																ui4_3 = loc54;
																ui4_4 = ((EIF_INTEGER_32) 0L);
																(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
																RTHOOK(201);
																tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
																RTNHOOK(201,1);
																ui4_1 = loc54;
																ui4_2 = loc52;
																ui4_3 = loc54;
																ui4_4 = loc50;
																(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
															} else {
																RTHOOK(202);
																tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
																RTNHOOK(202,1);
																ui4_1 = loc54;
																ui4_2 = ((EIF_INTEGER_32) 0L);
																ui4_3 = loc54;
																ui4_4 = loc50;
																(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
															}
														}
													}
													RTHOOK(203);
													RTCT0(NULL, EX_CHECK);
													if ((EIF_BOOLEAN)(loc47 != NULL)) {
														RTCK0;
													} else {
														RTCF0;
													}
													RTHOOK(204);
													RTDBGAL(Current, 58, 0xF8000491, 0, 0); /* loc58 */
													loc58 = (EIF_REFERENCE) RTCCL(loc47);
													RTHOOK(205);
													RTDBGAL(Current, 59, 0xF8000491, 0, 0); /* loc59 */
													tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18782, "parent_row_i", loc47))(loc47)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
													loc59 = (EIF_REFERENCE) RTCCL(tr1);
													for (;;) {
														RTHOOK(206);
														if ((EIF_BOOLEAN) ((EIF_BOOLEAN) (loc57 < loc24) || (EIF_BOOLEAN)(loc59 == NULL))) break;
														RTHOOK(207);
														RTDBGAL(Current, 57, 0x10000000, 1, 0); /* loc57 */
														loc57 -= loc33;
														RTHOOK(208);
														if ((EIF_BOOLEAN) (loc57 < (EIF_INTEGER_32) (loc24 + loc18))) {
															RTHOOK(209);
															RTDBGAL(Current, 77, 0xF80003DD, 0, 0); /* loc77 */
															tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18087, "attached_interface", loc59))(loc59)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
															loc77 = (EIF_REFERENCE) RTCCL(tr1);
															RTHOOK(210);
															RTDBGAL(Current, 76, 0x10000000, 1, 0); /* loc76 */
															ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16641, "subrow_count", loc77))(loc77)).it_i4);
															loc76 = (EIF_INTEGER_32) ti4_1;
															RTHOOK(211);
															if ((EIF_BOOLEAN) (loc76 > ((EIF_INTEGER_32) 0L))) {
																RTHOOK(212);
																RTDBGAL(Current, 60, 0xF80003DD, 0, 0); /* loc60 */
																ui4_1 = loc76;
																tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16624, "subrow", loc77))(loc77, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
																loc60 = (EIF_REFERENCE) RTCCL(tr1);
																RTHOOK(213);
																tb3 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16640, "is_show_requested", loc60))(loc60)).it_b);
																if ((EIF_BOOLEAN) !tb3) {
																	for (;;) {
																		RTHOOK(214);
																		tb3 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16640, "is_show_requested", loc60))(loc60)).it_b);
																		if (tb3) break;
																		RTHOOK(215);
																		RTDBGAL(Current, 76, 0x10000000, 1, 0); /* loc76 */
																		loc76--;
																		RTHOOK(216);
																		RTDBGAL(Current, 60, 0xF80003DD, 0, 0); /* loc60 */
																		ui4_1 = loc76;
																		tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16624, "subrow", loc77))(loc77, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
																		loc60 = (EIF_REFERENCE) RTCCL(tr1);
																	}
																}
																RTHOOK(217);
																ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16643, "index", loc60))(loc60)).it_i4);
																ti4_2 = *(EIF_INTEGER_32 *)(loc58 + RTVA(18743, "index", loc58));
																if ((EIF_BOOLEAN) (ti4_1 > ti4_2)) {
																	RTHOOK(218);
																	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
																	RTNHOOK(218,1);
																	ui4_1 = loc57;
																	ui4_2 = loc50;
																	ui4_3 = loc57;
																	ui4_4 = ((EIF_INTEGER_32) 0L);
																	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
																}
															}
														}
														RTHOOK(219);
														RTDBGAL(Current, 58, 0xF8000491, 0, 0); /* loc58 */
														loc58 = (EIF_REFERENCE) RTCCL(loc59);
														RTHOOK(220);
														RTDBGAL(Current, 59, 0xF8000491, 0, 0); /* loc59 */
														tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18782, "parent_row_i", loc59))(loc59)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
														loc59 = (EIF_REFERENCE) RTCCL(tr1);
													}
												}
											}
										}
									} else {
										RTHOOK(221);
										if ((EIF_BOOLEAN) (loc34 && (EIF_BOOLEAN) (loc14 < loc36))) {
											RTHOOK(222);
											RTCT0(NULL, EX_CHECK);
											if ((EIF_BOOLEAN)(loc47 != NULL)) {
												RTCK0;
											} else {
												RTCF0;
											}
											RTHOOK(223);
											RTDBGAL(Current, 59, 0xF8000491, 0, 0); /* loc59 */
											tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18782, "parent_row_i", loc47))(loc47)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											loc59 = (EIF_REFERENCE) RTCCL(tr1);
											RTHOOK(224);
											tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											RTNHOOK(224,1);
											ur1 = RTCCL(loc64);
											(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", tr1))(tr1, ur1x);
											for (;;) {
												RTHOOK(225);
												if ((EIF_BOOLEAN)(loc59 == NULL)) break;
												RTHOOK(226);
												RTDBGAL(Current, 77, 0xF80003DD, 0, 0); /* loc77 */
												tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18087, "attached_interface", loc59))(loc59)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
												loc77 = (EIF_REFERENCE) RTCCL(tr1);
												RTHOOK(227);
												RTDBGAL(Current, 76, 0x10000000, 1, 0); /* loc76 */
												ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16641, "subrow_count", loc77))(loc77)).it_i4);
												loc76 = (EIF_INTEGER_32) ti4_1;
												RTHOOK(228);
												tb4 = '\0';
												if ((EIF_BOOLEAN) (loc76 > ((EIF_INTEGER_32) 0L))) {
													ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16635, "index_of_first_item", loc77))(loc77)).it_i4);
													tb4 = (EIF_BOOLEAN)(ti4_1 == loc14);
												}
												if (tb4) {
													RTHOOK(229);
													RTDBGAL(Current, 60, 0xF80003DD, 0, 0); /* loc60 */
													ui4_1 = loc76;
													tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16624, "subrow", loc77))(loc77, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
													loc60 = (EIF_REFERENCE) RTCCL(tr1);
													RTHOOK(230);
													tb4 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16640, "is_show_requested", loc60))(loc60)).it_b);
													if ((EIF_BOOLEAN) !tb4) {
														for (;;) {
															RTHOOK(231);
															tb4 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16640, "is_show_requested", loc60))(loc60)).it_b);
															if (tb4) break;
															RTHOOK(232);
															RTDBGAL(Current, 76, 0x10000000, 1, 0); /* loc76 */
															loc76--;
															RTHOOK(233);
															RTDBGAL(Current, 60, 0xF80003DD, 0, 0); /* loc60 */
															ui4_1 = loc76;
															tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16624, "subrow", loc77))(loc77, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
															loc60 = (EIF_REFERENCE) RTCCL(tr1);
														}
													}
													RTHOOK(234);
													ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16643, "index", loc60))(loc60)).it_i4);
													ti4_2 = *(EIF_INTEGER_32 *)(loc7 + RTVA(18743, "index", loc7));
													if ((EIF_BOOLEAN) (ti4_1 > ti4_2)) {
														RTHOOK(235);
														RTDBGAL(Current, 55, 0x10000000, 1, 0); /* loc55 */
														ur1 = RTCCL(loc59);
														loc55 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTWF(10853, dtype))(Current, ur1x)).it_i4);
														loc55 = (EIF_INTEGER_32) (EIF_INTEGER_32) (loc55 + ((EIF_INTEGER_32) (loc45 / ((EIF_INTEGER_32) 2L))));
														RTHOOK(236);
														tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
														RTNHOOK(236,1);
														ui4_1 = loc55;
														ui4_2 = loc50;
														ui4_3 = loc55;
														ui4_4 = ((EIF_INTEGER_32) 0L);
														(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
													}
												}
												RTHOOK(237);
												RTDBGAL(Current, 59, 0xF8000491, 0, 0); /* loc59 */
												tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18782, "parent_row_i", loc59))(loc59)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
												loc59 = (EIF_REFERENCE) RTCCL(tr1);
											}
										}
									}
								}
								RTHOOK(238);
								if ((EIF_BOOLEAN) (loc22 && (EIF_BOOLEAN)(loc12 != NULL))) {
									RTHOOK(239);
									if ((EIF_BOOLEAN) ((EIF_INTEGER_32) (loc25 - loc24) < loc18)) {
										RTHOOK(240);
										if ((EIF_BOOLEAN)(loc14 == loc36)) {
											RTHOOK(241);
											ui4_1 = loc25;
											ui4_2 = loc23;
											ui4_3 = loc26;
											ui4_4 = loc19;
											ui4_5 = loc30;
											tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											ur1 = RTCCL(tr1);
											(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(18876, "perform_redraw", loc12))(loc12, ui4_1x, ui4_2x, ui4_3x, ui4_4x, ui4_5x, ur1x);
										} else {
											RTHOOK(242);
											ui4_1 = loc25;
											ui4_2 = loc23;
											ui4_3 = loc26;
											ui4_4 = loc19;
											ui4_5 = ((EIF_INTEGER_32) 0L);
											tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											ur1 = RTCCL(tr1);
											(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(18876, "perform_redraw", loc12))(loc12, ui4_1x, ui4_2x, ui4_3x, ui4_4x, ui4_5x, ur1x);
										}
									}
									RTHOOK(243);
									RTDBGAL(Current, 71, 0x04000000, 1, 0); /* loc71 */
									loc71 = (EIF_BOOLEAN) (EIF_BOOLEAN) 1;
								} else {
									RTHOOK(244);
									RTDBGAL(Current, 63, 0x10000000, 1, 0); /* loc63 */
									ti4_1 = *(EIF_INTEGER_32 *)(loc78 + RTVA(19157, "subrow_indent", loc78));
									loc63 = (EIF_INTEGER_32) (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc39 + ti4_1) - ((EIF_INTEGER_32) 1L));
									RTHOOK(245);
									if ((EIF_BOOLEAN) (loc37 < loc14)) {
										RTHOOK(246);
										RTDBGAL(Current, 63, 0x10000000, 1, 0); /* loc63 */
										loc63 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
									}
									RTHOOK(247);
									tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
									RTNHOOK(247,1);
									ur1 = RTCCL(loc64);
									(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", tr1))(tr1, ur1x);
									RTHOOK(248);
									tb5 = '\0';
									if (loc34) {
										ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18746, "index_of_first_item", loc7))(loc7)).it_i4);
										tb5 = ((EIF_BOOLEAN)(loc14 == ti4_1));
									}
									if (tb5) {
										RTHOOK(249);
										RTDBGAL(Current, 56, 0x10000000, 1, 0); /* loc56 */
										loc56 = (EIF_INTEGER_32) loc18;
										RTHOOK(250);
										if ((EIF_BOOLEAN)(loc37 != loc14)) {
											RTHOOK(251);
											RTDBGAL(Current, 55, 0x10000000, 1, 0); /* loc55 */
											loc55 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
										}
										RTHOOK(252);
										tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										RTNHOOK(252,1);
										ui4_1 = loc55;
										ui4_2 = loc49;
										ui4_3 = loc56;
										ui4_4 = loc49;
										(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
									}
									RTHOOK(253);
									if ((EIF_BOOLEAN) ((EIF_BOOLEAN) ((EIF_BOOLEAN) (loc61 && (EIF_BOOLEAN) (loc34 || loc35)) && (EIF_BOOLEAN)(loc14 != loc36)) && (EIF_BOOLEAN) (loc14 >= loc37))) {
										RTHOOK(254);
										if ((EIF_BOOLEAN) (loc14 < loc36)) {
											RTHOOK(255);
											tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											RTNHOOK(255,1);
											ui4_1 = loc18;
											ti4_1 = eif_min_int32 (loc63,ui4_1);
											ui4_1 = ti4_1;
											ui4_2 = loc49;
											ui4_3 = loc18;
											ui4_4 = loc49;
											(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
										}
										RTHOOK(256);
										if ((EIF_BOOLEAN) ((EIF_BOOLEAN) (loc34 && (EIF_BOOLEAN)(loc37 == loc14)) && (EIF_BOOLEAN) (loc63 < loc18))) {
											RTHOOK(257);
											RTCT0(NULL, EX_CHECK);
											if ((EIF_BOOLEAN)(loc47 != NULL)) {
												RTCK0;
											} else {
												RTCF0;
											}
											RTHOOK(258);
											tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18780, "subrows", loc47))(loc47)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18741, "subrow_count", loc47))(loc47)).it_i4);
											ui4_1 = ti4_1;
											tr2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", tr1))(tr1, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											if (RTCEQ(loc7, tr2)) {
												RTHOOK(259);
												tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
												RTNHOOK(259,1);
												ui4_1 = loc63;
												ui4_2 = loc49;
												ui4_3 = loc63;
												ui4_4 = ((EIF_INTEGER_32) 0L);
												(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
											} else {
												RTHOOK(260);
												tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
												RTNHOOK(260,1);
												ui4_1 = loc63;
												ui4_2 = loc50;
												ui4_3 = loc63;
												ui4_4 = ((EIF_INTEGER_32) 0L);
												(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
											}
										}
									}
								}
								RTHOOK(261);
								ur1 = RTCCL(loc66);
								ur2 = RTCCL(loc7);
								ur3 = RTCCL(loc12);
								ui4_1 = loc24;
								ui4_2 = loc23;
								ui4_3 = loc18;
								ui4_4 = loc19;
								(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTWF(10855, dtype))(Current, ur1x, ur2x, ur3x, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
								RTHOOK(262);
								tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10915, "post_draw_overlay_actions_internal", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
								if ((EIF_BOOLEAN)(tr1 != NULL)) {
									RTHOOK(263);
									if ((EIF_BOOLEAN) (loc22 && (EIF_BOOLEAN)(loc12 != NULL))) {
										RTHOOK(264);
										tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10893, "post_draw_overlay_actions", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										RTNHOOK(264,1);
										{
											static EIF_TYPE_INDEX typarr0[] = {0xFF01,0xFFF9,4,840,0xFF01,1053,1001,869,869,0xFFFF};
											EIF_TYPE typres0;
											static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
											
											typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(dftype, typarr0)));
											tr2 = RTLNTS(typres0.id, 5, 0);
										}
										tr3 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										((EIF_TYPED_VALUE *)tr2+1)->it_r = tr3;
										RTAR(tr2,tr3);
										tr3 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18088, "interface", loc12))(loc12)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										((EIF_TYPED_VALUE *)tr2+2)->it_r = tr3;
										RTAR(tr2,tr3);
										((EIF_TYPED_VALUE *)tr2+3)->it_i4 = loc14;
										((EIF_TYPED_VALUE *)tr2+4)->it_i4 = loc15;
										ur1 = RTCCL(tr2);
										(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(10459, "call", tr1))(tr1, ur1x);
									} else {
										RTHOOK(265);
										tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10893, "post_draw_overlay_actions", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										RTNHOOK(265,1);
										{
											static EIF_TYPE_INDEX typarr0[] = {0xFF01,0xFFF9,4,840,0xFF01,1053,65534,869,869,0xFFFF};
											EIF_TYPE typres0;
											static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
											
											typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(dftype, typarr0)));
											tr2 = RTLNTS(typres0.id, 5, 0);
										}
										tr3 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										((EIF_TYPED_VALUE *)tr2+1)->it_r = tr3;
										RTAR(tr2,tr3);
										((EIF_TYPED_VALUE *)tr2+3)->it_i4 = loc14;
										((EIF_TYPED_VALUE *)tr2+4)->it_i4 = loc15;
										ur1 = RTCCL(tr2);
										(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(10459, "call", tr1))(tr1, ur1x);
									}
								}
								RTHOOK(266);
								tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
								RTNHOOK(266,1);
								ui4_1 = ((EIF_INTEGER_32) 0L);
								ui4_2 = ((EIF_INTEGER_32) 0L);
								ui4_3 = loc18;
								ui4_4 = loc19;
								(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(10699, "move_and_resize", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
								RTHOOK(267);
								if ((EIF_BOOLEAN)(arg7 == NULL)) {
									RTHOOK(268);
									tb5 = '\01';
									tb6 = *(EIF_BOOLEAN *)(loc66 + RTVA(18685, "is_locked", loc66));
									if (!tb6) {
										tb6 = *(EIF_BOOLEAN *)(loc7 + RTVA(18750, "is_locked", loc7));
										tb5 = tb6;
									}
									if (tb5) {
										RTHOOK(269);
										tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10858, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										ur1 = RTCCL(tr1);
										(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", arg5))(arg5, ur1x);
										RTHOOK(270);
										ui4_1 = loc24;
										ui4_2 = loc23;
										ui4_3 = loc18;
										ui4_4 = loc19;
										(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16723, "fill_rectangle", arg5))(arg5, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
									} else {
										RTHOOK(271);
										ui4_1 = loc24;
										ui4_2 = loc23;
										tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										ur1 = RTCCL(tr1);
										tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
										ur2 = RTCCL(tr2);
										(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16716, "draw_sub_pixmap", arg5))(arg5, ui4_1x, ui4_2x, ur1x, ur2x);
									}
								} else {
									RTHOOK(272);
									if ((EIF_BOOLEAN)(loc74 != NULL)) {
										RTHOOK(273);
										tb5 = '\0';
										tb6 = '\0';
										ui4_1 = loc15;
										tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(19470, "row_internal", loc78))(loc78, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
										RTNHOOK(273,1);
										tb7 = *(EIF_BOOLEAN *)(tr1 + RTVA(18750, "is_locked", tr1));
										if (tb7) {
											ui4_1 = loc15;
											tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(19470, "row_internal", loc78))(loc78, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											RTNHOOK(273,2);
											tr2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18740, "locked_row", tr1))(tr1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											loc79 = RTCCL(tr2);
											tb6 = EIF_TEST(loc79);
										}
										if (tb6) {
											ti4_1 = *(EIF_INTEGER_32 *)(loc79 + RTVA(7222, "locked_index", loc79));
											ti4_2 = *(EIF_INTEGER_32 *)(loc74 + RTVA(7222, "locked_index", loc74));
											tb5 = (EIF_BOOLEAN) (ti4_1 > ti4_2);
										}
										if (tb5) {
											RTHOOK(274);
											tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18682, "background_color", loc66))(loc66)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											loc80 = RTCCL(tr1);
											if (EIF_TEST(loc80)) {
												RTHOOK(275);
												ur1 = RTCCL(loc80);
												(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", arg5))(arg5, ur1x);
											} else {
												RTHOOK(276);
												tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10858, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
												ur1 = RTCCL(tr1);
												(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", arg5))(arg5, ur1x);
											}
											RTHOOK(277);
											ui4_1 = ((EIF_INTEGER_32) 0L);
											ui4_2 = loc23;
											ui4_3 = loc18;
											ui4_4 = loc19;
											(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16723, "fill_rectangle", arg5))(arg5, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
										} else {
											RTHOOK(278);
											ui4_1 = ((EIF_INTEGER_32) 0L);
											ui4_2 = loc23;
											tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											ur1 = RTCCL(tr1);
											tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
											ur2 = RTCCL(tr2);
											(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16716, "draw_sub_pixmap", arg5))(arg5, ui4_1x, ui4_2x, ur1x, ur2x);
										}
									} else {
										RTHOOK(279);
										if ((EIF_BOOLEAN)(loc75 != NULL)) {
											RTHOOK(280);
											tb5 = '\0';
											tb6 = '\0';
											ui4_1 = loc14;
											tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(19471, "column_internal", loc78))(loc78, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
											RTNHOOK(280,1);
											tb7 = *(EIF_BOOLEAN *)(tr1 + RTVA(18685, "is_locked", tr1));
											if (tb7) {
												ui4_1 = loc14;
												tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(19471, "column_internal", loc78))(loc78, ui4_1x)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
												RTNHOOK(280,2);
												tr2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18693, "locked_column", tr1))(tr1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
												loc81 = RTCCL(tr2);
												tb6 = EIF_TEST(loc81);
											}
											if (tb6) {
												ti4_1 = *(EIF_INTEGER_32 *)(loc81 + RTVA(7222, "locked_index", loc81));
												ti4_2 = *(EIF_INTEGER_32 *)(loc75 + RTVA(7222, "locked_index", loc75));
												tb5 = (EIF_BOOLEAN) (ti4_1 > ti4_2);
											}
											if (tb5) {
												RTHOOK(281);
												tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18748, "background_color", loc7))(loc7)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
												loc82 = RTCCL(tr1);
												if (EIF_TEST(loc82)) {
													RTHOOK(282);
													ur1 = RTCCL(loc82);
													(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", arg5))(arg5, ur1x);
												} else {
													RTHOOK(283);
													tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10858, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
													ur1 = RTCCL(tr1);
													(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", arg5))(arg5, ur1x);
												}
												RTHOOK(284);
												ui4_1 = loc24;
												ui4_2 = ((EIF_INTEGER_32) 0L);
												ui4_3 = loc18;
												ui4_4 = loc19;
												(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16723, "fill_rectangle", arg5))(arg5, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
											} else {
												RTHOOK(285);
												ui4_1 = loc24;
												ui4_2 = ((EIF_INTEGER_32) 0L);
												tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
												ur1 = RTCCL(tr1);
												tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
												ur2 = RTCCL(tr2);
												(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16716, "draw_sub_pixmap", arg5))(arg5, ui4_1x, ui4_2x, ur1x, ur2x);
											}
										} else {
											if (RTAL & CK_CHECK) {
												RTHOOK(286);
												RTCT(NULL, EX_CHECK);
													RTCF;
											}
										}
									}
								}
							}
							RTHOOK(287);
							(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(8497, "forth", loc31))(loc31);
						}
						RTHOOK(288);
						(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(8497, "forth", loc32))(loc32);
					}
				}
				RTHOOK(289);
				RTDBGAL(Current, 20, 0x10000000, 1, 0); /* loc20 */
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8735, "count", loc16))(loc16)).it_i4);
				ui4_1 = ti4_1;
				ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8560, "at", loc16))(loc16, ui4_1x)).it_i4);
				loc20 = (EIF_INTEGER_32) (EIF_INTEGER_32) (loc28 - (EIF_INTEGER_32) (ti4_1 - loc1));
				RTHOOK(290);
				if ((EIF_BOOLEAN) (loc20 > ((EIF_INTEGER_32) 0L))) {
					RTHOOK(291);
					tb5 = '\01';
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					RTNHOOK(291,1);
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16693, "width", tr1))(tr1)).it_i4);
					if (!(EIF_BOOLEAN) (ti4_1 < loc20)) {
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(291,2);
						ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16694, "height", tr1))(tr1)).it_i4);
						tb5 = (EIF_BOOLEAN) (ti4_1 < loc29);
					}
					if (tb5) {
						RTHOOK(292);
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(292,1);
						ui4_1 = loc20;
						ui4_2 = loc29;
						(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(17674, "set_size", tr1))(tr1, ui4_1x, ui4_2x);
					}
					RTHOOK(293);
					tb5 = '\0';
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10916, "fill_background_actions_internal", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					if ((EIF_BOOLEAN)(tr1 != NULL)) {
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10894, "fill_background_actions", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(293,1);
						tb6 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8471, "is_empty", tr1))(tr1)).it_b);
						tb5 = (EIF_BOOLEAN) !tb6;
					}
					if (tb5) {
						RTHOOK(294);
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10894, "fill_background_actions", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(294,1);
						{
							static EIF_TYPE_INDEX typarr0[] = {0xFF01,0xFFF9,5,840,0xFF01,1053,869,869,869,869,0xFFFF};
							EIF_TYPE typres0;
							static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
							
							typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(dftype, typarr0)));
							tr2 = RTLNTS(typres0.id, 6, 0);
						}
						tr3 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						((EIF_TYPED_VALUE *)tr2+1)->it_r = tr3;
						RTAR(tr2,tr3);
						ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8735, "count", loc16))(loc16)).it_i4);
						ui4_1 = ti4_1;
						ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8560, "at", loc16))(loc16, ui4_1x)).it_i4);
						((EIF_TYPED_VALUE *)tr2+2)->it_i4 = ti4_1;
						((EIF_TYPED_VALUE *)tr2+3)->it_i4 = loc2;
						((EIF_TYPED_VALUE *)tr2+4)->it_i4 = loc20;
						((EIF_TYPED_VALUE *)tr2+5)->it_i4 = loc29;
						ur1 = RTCCL(tr2);
						(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(10459, "call", tr1))(tr1, ur1x);
					} else {
						RTHOOK(295);
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(295,1);
						tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18425, "background_color", loc78))(loc78)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
						ur1 = RTCCL(tr2);
						(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", tr1))(tr1, ur1x);
						RTHOOK(296);
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(296,1);
						ui4_1 = ((EIF_INTEGER_32) 0L);
						ui4_2 = ((EIF_INTEGER_32) 0L);
						ui4_3 = loc20;
						ui4_4 = loc29;
						(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16723, "fill_rectangle", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
					}
					RTHOOK(297);
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					RTNHOOK(297,1);
					ui4_1 = ((EIF_INTEGER_32) 0L);
					ui4_2 = ((EIF_INTEGER_32) 0L);
					ui4_3 = loc20;
					ui4_4 = loc29;
					(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(10699, "move_and_resize", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
					RTHOOK(298);
					if ((EIF_BOOLEAN)(loc75 == NULL)) {
						RTHOOK(299);
						ui4_1 = (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc4 + loc28) - loc20);
						ui4_2 = loc3;
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						ur1 = RTCCL(tr1);
						tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
						ur2 = RTCCL(tr2);
						(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16716, "draw_sub_pixmap", arg5))(arg5, ui4_1x, ui4_2x, ur1x, ur2x);
					} else {
						RTHOOK(300);
						ui4_1 = (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc4 + loc28) - loc20);
						ui4_2 = ((EIF_INTEGER_32) 0L);
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						ur1 = RTCCL(tr1);
						tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
						ur2 = RTCCL(tr2);
						(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16716, "draw_sub_pixmap", arg5))(arg5, ui4_1x, ui4_2x, ur1x, ur2x);
					}
				}
				RTHOOK(301);
				tb5 = '\01';
				if (!(EIF_BOOLEAN)(loc7 == NULL)) {
					ti4_1 = *(EIF_INTEGER_32 *)(loc7 + RTVA(18743, "index", loc7));
					ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19319, "visible_row_count", loc78))(loc78)).it_i4);
					tb5 = (EIF_BOOLEAN) (ti4_1 >= ti4_2);
				}
				if (tb5) {
					RTHOOK(302);
					tb5 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19502, "uses_row_offsets", loc78))(loc78)).it_b);
					if ((EIF_BOOLEAN) !tb5) {
						RTHOOK(303);
						RTDBGAL(Current, 73, 0x10000000, 1, 0); /* loc73 */
						loc73 = (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc68 * loc67));
						RTHOOK(304);
						RTDBGAL(Current, 21, 0x10000000, 1, 0); /* loc21 */
						loc21 = (EIF_INTEGER_32) (EIF_INTEGER_32) (loc29 - (EIF_INTEGER_32) (loc73 - loc2));
					} else {
						RTHOOK(305);
						RTCT0(NULL, EX_CHECK);
						if ((EIF_BOOLEAN)(loc17 != NULL)) {
							RTCK0;
						} else {
							RTCF0;
						}
						RTHOOK(306);
						RTDBGAL(Current, 73, 0x10000000, 1, 0); /* loc73 */
						ui4_1 = (EIF_INTEGER_32) (loc67 + ((EIF_INTEGER_32) 1L));
						ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8560, "at", loc17))(loc17, ui4_1x)).it_i4);
						loc73 = (EIF_INTEGER_32) ti4_1;
						RTHOOK(307);
						RTDBGAL(Current, 21, 0x10000000, 1, 0); /* loc21 */
						loc21 = (EIF_INTEGER_32) (EIF_INTEGER_32) (loc29 - (EIF_INTEGER_32) (loc73 - loc2));
					}
					RTHOOK(308);
					if ((EIF_BOOLEAN) (loc21 > ((EIF_INTEGER_32) 0L))) {
						RTHOOK(309);
						tb5 = '\01';
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(309,1);
						ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16693, "width", tr1))(tr1)).it_i4);
						if (!(EIF_BOOLEAN) (ti4_1 < loc28)) {
							tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							RTNHOOK(309,2);
							ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16694, "height", tr1))(tr1)).it_i4);
							tb5 = (EIF_BOOLEAN) (ti4_1 < loc21);
						}
						if (tb5) {
							RTHOOK(310);
							tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							RTNHOOK(310,1);
							ui4_1 = loc28;
							ui4_2 = loc21;
							(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(17674, "set_size", tr1))(tr1, ui4_1x, ui4_2x);
						}
						RTHOOK(311);
						tb5 = '\0';
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10916, "fill_background_actions_internal", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						if ((EIF_BOOLEAN)(tr1 != NULL)) {
							tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10894, "fill_background_actions", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							RTNHOOK(311,1);
							tb6 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8471, "is_empty", tr1))(tr1)).it_b);
							tb5 = (EIF_BOOLEAN) !tb6;
						}
						if (tb5) {
							RTHOOK(312);
							tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10894, "fill_background_actions", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							RTNHOOK(312,1);
							{
								static EIF_TYPE_INDEX typarr0[] = {0xFF01,0xFFF9,5,840,0xFF01,1053,869,869,869,869,0xFFFF};
								EIF_TYPE typres0;
								static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
								
								typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(dftype, typarr0)));
								tr2 = RTLNTS(typres0.id, 6, 0);
							}
							tr3 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							((EIF_TYPED_VALUE *)tr2+1)->it_r = tr3;
							RTAR(tr2,tr3);
							((EIF_TYPED_VALUE *)tr2+2)->it_i4 = loc1;
							((EIF_TYPED_VALUE *)tr2+3)->it_i4 = loc73;
							((EIF_TYPED_VALUE *)tr2+4)->it_i4 = loc28;
							((EIF_TYPED_VALUE *)tr2+5)->it_i4 = loc21;
							ur1 = RTCCL(tr2);
							(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(10459, "call", tr1))(tr1, ur1x);
						} else {
							RTHOOK(313);
							tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							RTNHOOK(313,1);
							tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18425, "background_color", loc78))(loc78)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
							ur1 = RTCCL(tr2);
							(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", tr1))(tr1, ur1x);
							RTHOOK(314);
							tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							RTNHOOK(314,1);
							ui4_1 = ((EIF_INTEGER_32) 0L);
							ui4_2 = ((EIF_INTEGER_32) 0L);
							ui4_3 = loc28;
							ui4_4 = loc21;
							(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16723, "fill_rectangle", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
						}
						RTHOOK(315);
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(315,1);
						ui4_1 = ((EIF_INTEGER_32) 0L);
						ui4_2 = ((EIF_INTEGER_32) 0L);
						ui4_3 = loc28;
						ui4_4 = loc21;
						(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(10699, "move_and_resize", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
						RTHOOK(316);
						if ((EIF_BOOLEAN)(loc74 == NULL)) {
							RTHOOK(317);
							ui4_1 = loc4;
							ui4_2 = (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc3 + loc29) - loc21);
							tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							ur1 = RTCCL(tr1);
							tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
							ur2 = RTCCL(tr2);
							(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16716, "draw_sub_pixmap", arg5))(arg5, ui4_1x, ui4_2x, ur1x, ur2x);
						} else {
							RTHOOK(318);
							ui4_1 = ((EIF_INTEGER_32) 0L);
							ui4_2 = (EIF_INTEGER_32) ((EIF_INTEGER_32) (loc3 + loc29) - loc21);
							tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
							ur1 = RTCCL(tr1);
							tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
							ur2 = RTCCL(tr2);
							(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16716, "draw_sub_pixmap", arg5))(arg5, ui4_1x, ui4_2x, ur1x, ur2x);
						}
					}
				}
			} else {
				RTHOOK(319);
				tb5 = '\0';
				tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10916, "fill_background_actions_internal", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
				if ((EIF_BOOLEAN)(tr1 != NULL)) {
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10894, "fill_background_actions", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					RTNHOOK(319,1);
					tb6 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(8471, "is_empty", tr1))(tr1)).it_b);
					tb5 = (EIF_BOOLEAN) !tb6;
				}
				if (tb5) {
					RTHOOK(320);
					tb5 = '\01';
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					RTNHOOK(320,1);
					ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16693, "width", tr1))(tr1)).it_i4);
					if (!(EIF_BOOLEAN) (ti4_1 < arg3)) {
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(320,2);
						ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(16694, "height", tr1))(tr1)).it_i4);
						tb5 = (EIF_BOOLEAN) (ti4_1 < arg4);
					}
					if (tb5) {
						RTHOOK(321);
						tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
						RTNHOOK(321,1);
						ui4_1 = arg3;
						ui4_2 = arg4;
						(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(17674, "set_size", tr1))(tr1, ui4_1x, ui4_2x);
					}
					RTHOOK(322);
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(10894, "fill_background_actions", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					RTNHOOK(322,1);
					{
						static EIF_TYPE_INDEX typarr0[] = {0xFF01,0xFFF9,5,840,0xFF01,1053,869,869,869,869,0xFFFF};
						EIF_TYPE typres0;
						static EIF_TYPE typcache0 = {INVALID_DTYPE, 0};
						
						typres0 = (typcache0.id != INVALID_DTYPE ? typcache0 : (typcache0 = eif_compound_id(dftype, typarr0)));
						tr2 = RTLNTS(typres0.id, 6, 0);
					}
					tr3 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					((EIF_TYPED_VALUE *)tr2+1)->it_r = tr3;
					RTAR(tr2,tr3);
					((EIF_TYPED_VALUE *)tr2+2)->it_i4 = loc1;
					((EIF_TYPED_VALUE *)tr2+3)->it_i4 = loc2;
					((EIF_TYPED_VALUE *)tr2+4)->it_i4 = arg3;
					((EIF_TYPED_VALUE *)tr2+5)->it_i4 = arg4;
					ur1 = RTCCL(tr2);
					(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(10459, "call", tr1))(tr1, ur1x);
					RTHOOK(323);
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					RTNHOOK(323,1);
					ui4_1 = ((EIF_INTEGER_32) 0L);
					ui4_2 = ((EIF_INTEGER_32) 0L);
					ui4_3 = arg3;
					ui4_4 = arg4;
					(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(10699, "move_and_resize", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
					RTHOOK(324);
					ui4_1 = arg1;
					ui4_2 = arg2;
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					ur1 = RTCCL(tr1);
					tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
					ur2 = RTCCL(tr2);
					(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16716, "draw_sub_pixmap", arg5))(arg5, ui4_1x, ui4_2x, ur1x, ur2x);
				} else {
					RTHOOK(325);
					tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18425, "background_color", loc78))(loc78)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
					ur1 = RTCCL(tr1);
					(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", arg5))(arg5, ur1x);
					RTHOOK(326);
					ui4_1 = arg1;
					ui4_2 = arg2;
					ui4_3 = arg3;
					ui4_4 = arg4;
					(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16723, "fill_rectangle", arg5))(arg5, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
				}
			}
			RTHOOK(327);
			tb5 = '\0';
			tb6 = '\0';
			tb7 = *(EIF_BOOLEAN *)(loc78 + RTVA(19154, "is_column_resize_immediate", loc78));
			if ((EIF_BOOLEAN) !tb7) {
				tb7 = *(EIF_BOOLEAN *)(loc78 + RTVA(19434, "is_header_item_resizing", loc78));
				tb6 = tb7;
			}
			if (tb6) {
				tb6 = *(EIF_BOOLEAN *)(loc78 + RTVA(19146, "is_resizing_divider_enabled", loc78));
				tb5 = tb6;
			}
			if (tb5) {
				RTHOOK(328);
				(FUNCTION_CAST(void, (EIF_REFERENCE)) RTVF(19392, "redraw_resizing_line", loc78))(loc78);
			}
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(329);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(91);
	RTEE;
#undef up1
#undef up2
#undef ur1
#undef ur2
#undef ur3
#undef ui4_1
#undef ui4_2
#undef ui4_3
#undef ui4_4
#undef ui4_5
#undef arg7
#undef arg6
#undef arg5
#undef arg4
#undef arg3
#undef arg2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.drawable_item_buffer_pixmap */
RTOID (F658_12116)
EIF_TYPED_VALUE F658_12116 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "drawable_item_buffer_pixmap";
	RTEX;
	EIF_REFERENCE tr1 = NULL;
	RTSN;
	RTDA;
	RTLD;
	
#define Result RTOTRR
	RTOTDR(F658_12116);

	RTLI(2);
	RTLR(0,tr1);
	RTLR(1,Current);
	RTLIU(2);
	RTLU (SK_REF, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 0, 16027);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 0);
	RTGC;
	RTDBGEAA(657, Current, 16027);
	RTIV(Current, RTAL);
	RTOTP;
	RTHOOK(1);
	RTDBGAL(Current, 0, 0xF800041D, 0,0); /* Result */
	tr1 = RTLN(eif_new_type(1053, 0x01).id);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTWC(32, Dtype(tr1)))(tr1);
	RTNHOOK(1,1);
	Result = (EIF_REFERENCE) RTCCL(tr1);
	RTVI(Current, RTAL);
	RTRS;
	RTOTE;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef Result
}

/* {EV_GRID_DRAWER_I}.subrow_indent */
EIF_TYPED_VALUE F658_12117 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x)
{
	GTCX
	char *l_feature_name = "subrow_indent";
	RTEX;
#define arg1 arg1x.it_r
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	
	RTLI(3);
	RTLR(0,arg1);
	RTLR(1,Current);
	RTLR(2,tr1);
	RTLIU(3);
	RTLU (SK_INT32, &Result);
	RTLU(SK_REF,&arg1);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 1, 16028);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16028);
	RTCC(arg1, 657, l_feature_name, 1, eif_new_type(1169, 0x01), 0x01);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("current_row_not_void", EX_PRE);
		RTTE((EIF_BOOLEAN)(arg1 != NULL), label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTHOOK(2);
	RTDBGAL(Current, 0, 0x10000000, 1,0); /* Result */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(2,1);
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18746, "index_of_first_item", arg1))(arg1)).it_i4);
	ui4_1 = ti4_1;
	ti4_2 = *(EIF_INTEGER_32 *)(arg1 + RTVA(18743, "index", arg1));
	ui4_2 = ti4_2;
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(19345, "item_cell_indent", tr1))(tr1, ui4_1x, ui4_2x)).it_i4);
	Result = (EIF_INTEGER_32) ti4_1;
	if (RTAL & CK_ENSURE) {
		RTHOOK(3);
		RTCT("result_non_negative", EX_POST);
		if ((EIF_BOOLEAN) (Result >= ((EIF_INTEGER_32) 0L))) {
			RTCK;
		} else {
			RTCF;
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(4);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(3);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
#undef up1
#undef ui4_1
#undef ui4_2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.retrieve_node_index */
EIF_TYPED_VALUE F658_12118 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x)
{
	GTCX
	char *l_feature_name = "retrieve_node_index";
	RTEX;
#define arg1 arg1x.it_r
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_BOOLEAN tb1;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	
	RTLI(3);
	RTLR(0,arg1);
	RTLR(1,Current);
	RTLR(2,tr1);
	RTLIU(3);
	RTLU (SK_INT32, &Result);
	RTLU(SK_REF,&arg1);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 1, 16029);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16029);
	RTCC(arg1, 657, l_feature_name, 1, eif_new_type(1169, 0x01), 0x01);
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("current_row_not_void", EX_PRE);
		RTTE((EIF_BOOLEAN)(arg1 != NULL), label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTHOOK(2);
	RTDBGAL(Current, 0, 0x10000000, 1,0); /* Result */
	ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(18746, "index_of_first_item", arg1))(arg1)).it_i4);
	Result = (EIF_INTEGER_32) ti4_1;
	RTHOOK(3);
	if ((EIF_BOOLEAN)(Result == ((EIF_INTEGER_32) 0L))) {
		RTHOOK(4);
		RTDBGAL(Current, 0, 0x10000000, 1,0); /* Result */
		Result = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	}
	if (RTAL & CK_ENSURE) {
		RTHOOK(5);
		RTCT("valid_result", EX_POST);
		tb1 = '\0';
		if ((EIF_BOOLEAN) (Result > ((EIF_INTEGER_32) 0L))) {
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			RTNHOOK(5,1);
			ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19317, "column_count", tr1))(tr1)).it_i4);
			tb1 = (EIF_BOOLEAN) (Result <= ti4_1);
		}
		if (tb1) {
			RTCK;
		} else {
			RTCF;
		}
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(6);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(3);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
#undef up1
#undef arg1
}

/* {EV_GRID_DRAWER_I}.draw_item_border */
void F658_12119 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x, EIF_TYPED_VALUE arg2x, EIF_TYPED_VALUE arg3x, EIF_TYPED_VALUE arg4x, EIF_TYPED_VALUE arg5x, EIF_TYPED_VALUE arg6x, EIF_TYPED_VALUE arg7x)
{
	GTCX
	char *l_feature_name = "draw_item_border";
	RTEX;
	EIF_REFERENCE loc1 = (EIF_REFERENCE) 0;
	EIF_BOOLEAN loc2 = (EIF_BOOLEAN) 0;
	EIF_REFERENCE loc3 = (EIF_REFERENCE) 0;
#define arg1 arg1x.it_r
#define arg2 arg2x.it_r
#define arg3 arg3x.it_r
#define arg4 arg4x.it_i4
#define arg5 arg5x.it_i4
#define arg6 arg6x.it_i4
#define arg7 arg7x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_TYPED_VALUE up2x = {{0}, SK_POINTER};
#define up2 up2x.it_p
	EIF_TYPED_VALUE ur1x = {{0}, SK_REF};
#define ur1 ur1x.it_r
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_TYPED_VALUE ui4_3x = {{0}, SK_INT32};
#define ui4_3 ui4_3x.it_i4
	EIF_TYPED_VALUE ui4_4x = {{0}, SK_INT32};
#define ui4_4 ui4_4x.it_i4
	EIF_REFERENCE tr1 = NULL;
	EIF_REFERENCE tr2 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_BOOLEAN tb1;
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg7x.type & SK_HEAD) == SK_REF) arg7x.it_i4 = * (EIF_INTEGER_32 *) arg7x.it_r;
	if ((arg6x.type & SK_HEAD) == SK_REF) arg6x.it_i4 = * (EIF_INTEGER_32 *) arg6x.it_r;
	if ((arg5x.type & SK_HEAD) == SK_REF) arg5x.it_i4 = * (EIF_INTEGER_32 *) arg5x.it_r;
	if ((arg4x.type & SK_HEAD) == SK_REF) arg4x.it_i4 = * (EIF_INTEGER_32 *) arg4x.it_r;
	
	RTLI(9);
	RTLR(0,arg1);
	RTLR(1,arg2);
	RTLR(2,arg3);
	RTLR(3,loc3);
	RTLR(4,Current);
	RTLR(5,loc1);
	RTLR(6,tr1);
	RTLR(7,tr2);
	RTLR(8,ur1);
	RTLIU(9);
	RTLU (SK_VOID, NULL);
	RTLU(SK_REF,&arg1);
	RTLU(SK_REF,&arg2);
	RTLU(SK_REF,&arg3);
	RTLU(SK_INT32,&arg4);
	RTLU(SK_INT32,&arg5);
	RTLU(SK_INT32,&arg6);
	RTLU(SK_INT32,&arg7);
	RTLU (SK_REF, &Current);
	RTLU(SK_REF, &loc1);
	RTLU(SK_BOOL, &loc2);
	RTLU(SK_REF, &loc3);
	
	RTEAA(l_feature_name, 657, Current, 3, 7, 16030);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16030);
	RTCC(arg1, 657, l_feature_name, 1, eif_new_type(1168, 0x01), 0x01);
	RTCC(arg2, 657, l_feature_name, 2, eif_new_type(1169, 0x01), 0x01);
	if (arg3) {
		RTCC(arg3, 657, l_feature_name, 3, eif_new_type(1182, 0x00), 0x00);
	}
	RTIV(Current, RTAL);
	if ((RTAL & CK_REQUIRE) || RTAC) {
		RTHOOK(1);
		RTCT("current_column_not_void", EX_PRE);
		RTTE((EIF_BOOLEAN)(arg1 != NULL), label_1);
		RTCK;
		RTHOOK(2);
		RTCT("current_row_not_void", EX_PRE);
		RTTE((EIF_BOOLEAN)(arg2 != NULL), label_1);
		RTCK;
		RTHOOK(3);
		RTCT("current_column_width_greater_than_zero", EX_PRE);
		RTTE((EIF_BOOLEAN) (arg6 > ((EIF_INTEGER_32) 0L)), label_1);
		RTCK;
		RTJB;
label_1:
		RTCF;
	}
body:;
	RTHOOK(4);
	RTDBGAL(Current, 3, 0xF80004B3, 0, 0); /* loc3 */
	loc3 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTHOOK(5);
	RTDBGAL(Current, 1, 0xF800041F, 0, 0); /* loc1 */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19359, "drawable", loc3))(loc3)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	loc1 = (EIF_REFERENCE) RTCCL(tr1);
	RTHOOK(6);
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(6,1);
	tr2 = ((up2x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19189, "separator_color", loc3))(loc3)), (((up2x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up2x.it_r = RTBU(up2x))), (up2x.type = SK_POINTER), up2x.it_r);
	ur1 = RTCCL(tr2);
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(16530, "set_foreground_color", tr1))(tr1, ur1x);
	RTHOOK(7);
	tb1 = *(EIF_BOOLEAN *)(loc3 + RTVA(19187, "are_column_separators_enabled", loc3));
	if (tb1) {
		RTHOOK(8);
		ti4_1 = *(EIF_INTEGER_32 *)(arg1 + RTVA(18694, "index", arg1));
		if ((EIF_BOOLEAN) (ti4_1 > ((EIF_INTEGER_32) 1L))) {
			RTHOOK(9);
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			RTNHOOK(9,1);
			ui4_1 = ((EIF_INTEGER_32) 0L);
			ui4_2 = ((EIF_INTEGER_32) 0L);
			ui4_3 = ((EIF_INTEGER_32) 0L);
			ui4_4 = (EIF_INTEGER_32) (arg7 - ((EIF_INTEGER_32) 1L));
			(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
		}
		RTHOOK(10);
		ti4_1 = *(EIF_INTEGER_32 *)(arg1 + RTVA(18694, "index", arg1));
		ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19317, "column_count", loc3))(loc3)).it_i4);
		if ((EIF_BOOLEAN) (ti4_1 < ti4_2)) {
			RTHOOK(11);
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19355, "column_offsets", loc3))(loc3)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			RTNHOOK(11,1);
			ti4_1 = *(EIF_INTEGER_32 *)(arg1 + RTVA(18694, "index", arg1));
			ui4_1 = (EIF_INTEGER_32) (ti4_1 + ((EIF_INTEGER_32) 1L));
			ti4_1 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", tr1))(tr1, ui4_1x)).it_i4);
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19355, "column_offsets", loc3))(loc3)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			RTNHOOK(11,2);
			ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19317, "column_count", loc3))(loc3)).it_i4);
			ui4_1 = (EIF_INTEGER_32) (ti4_2 + ((EIF_INTEGER_32) 1L));
			ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE, EIF_TYPED_VALUE)) RTVF(8559, "i_th", tr1))(tr1, ui4_1x)).it_i4);
			if ((EIF_BOOLEAN)(ti4_1 == ti4_2)) {
				RTHOOK(12);
				RTDBGAL(Current, 2, 0x04000000, 1, 0); /* loc2 */
				loc2 = (EIF_BOOLEAN) (EIF_BOOLEAN) 1;
			}
		}
		RTHOOK(13);
		tb1 = '\01';
		ti4_1 = *(EIF_INTEGER_32 *)(arg1 + RTVA(18694, "index", arg1));
		ti4_2 = (((FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(19317, "column_count", loc3))(loc3)).it_i4);
		if (!(EIF_BOOLEAN)(ti4_1 == ti4_2)) {
			tb1 = loc2;
		}
		if (tb1) {
			RTHOOK(14);
			tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
			RTNHOOK(14,1);
			ui4_1 = (EIF_INTEGER_32) (arg6 - ((EIF_INTEGER_32) 1L));
			ui4_2 = ((EIF_INTEGER_32) 0L);
			ui4_3 = (EIF_INTEGER_32) (arg6 - ((EIF_INTEGER_32) 1L));
			ui4_4 = (EIF_INTEGER_32) (arg7 - ((EIF_INTEGER_32) 1L));
			(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
		}
	}
	RTHOOK(15);
	tb1 = *(EIF_BOOLEAN *)(loc3 + RTVA(19188, "are_row_separators_enabled", loc3));
	if (tb1) {
		RTHOOK(16);
		tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10861, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
		RTNHOOK(16,1);
		ui4_1 = ((EIF_INTEGER_32) 0L);
		ui4_2 = (EIF_INTEGER_32) (arg7 - ((EIF_INTEGER_32) 1L));
		ui4_3 = arg6;
		ui4_4 = (EIF_INTEGER_32) (arg7 - ((EIF_INTEGER_32) 1L));
		(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(16713, "draw_segment", tr1))(tr1, ui4_1x, ui4_2x, ui4_3x, ui4_4x);
	}
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(17);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(12);
	RTEE;
#undef up1
#undef up2
#undef ur1
#undef ui4_1
#undef ui4_2
#undef ui4_3
#undef ui4_4
#undef arg7
#undef arg6
#undef arg5
#undef arg4
#undef arg3
#undef arg2
#undef arg1
}

/* {EV_GRID_DRAWER_I}.white */
RTOID (F658_12120)
EIF_TYPED_VALUE F658_12120 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "white";
	RTEX;
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_REFERENCE tr1 = NULL;
	EIF_REFERENCE tr2 = NULL;
	RTSN;
	RTDA;
	RTLD;
	
#define Result RTOTRR
	RTOTDR(F658_12120);

	RTLI(3);
	RTLR(0,tr1);
	RTLR(1,tr2);
	RTLR(2,Current);
	RTLIU(3);
	RTLU (SK_REF, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 0, 16031);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 0);
	RTGC;
	RTDBGEAA(657, Current, 16031);
	RTIV(Current, RTAL);
	RTOTP;
	RTHOOK(1);
	RTDBGAL(Current, 0, 0xF80003C2, 0,0); /* Result */
	tr1 = RTLN(eif_new_type(128, 0x01).id);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTWC(32, Dtype(tr1)))(tr1);
	RTNHOOK(1,1);
	tr2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(3572, "white", tr1))(tr1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	Result = (EIF_REFERENCE) RTCCL(tr2);
	RTVI(Current, RTAL);
	RTRS;
	RTOTE;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef Result
}

/* {EV_GRID_DRAWER_I}.black */
RTOID (F658_12121)
EIF_TYPED_VALUE F658_12121 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "black";
	RTEX;
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_REFERENCE tr1 = NULL;
	EIF_REFERENCE tr2 = NULL;
	RTSN;
	RTDA;
	RTLD;
	
#define Result RTOTRR
	RTOTDR(F658_12121);

	RTLI(3);
	RTLR(0,tr1);
	RTLR(1,tr2);
	RTLR(2,Current);
	RTLIU(3);
	RTLU (SK_REF, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 0, 16032);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 0);
	RTGC;
	RTDBGEAA(657, Current, 16032);
	RTIV(Current, RTAL);
	RTOTP;
	RTHOOK(1);
	RTDBGAL(Current, 0, 0xF80003C2, 0,0); /* Result */
	tr1 = RTLN(eif_new_type(128, 0x01).id);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTWC(32, Dtype(tr1)))(tr1);
	RTNHOOK(1,1);
	tr2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(3573, "black", tr1))(tr1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	Result = (EIF_REFERENCE) RTCCL(tr2);
	RTVI(Current, RTAL);
	RTRS;
	RTOTE;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef Result
}

/* {EV_GRID_DRAWER_I}.gray */
RTOID (F658_12122)
EIF_TYPED_VALUE F658_12122 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "gray";
	RTEX;
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_REFERENCE tr1 = NULL;
	EIF_REFERENCE tr2 = NULL;
	RTSN;
	RTDA;
	RTLD;
	
#define Result RTOTRR
	RTOTDR(F658_12122);

	RTLI(3);
	RTLR(0,tr1);
	RTLR(1,tr2);
	RTLR(2,Current);
	RTLIU(3);
	RTLU (SK_REF, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 0, 16033);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 0);
	RTGC;
	RTDBGEAA(657, Current, 16033);
	RTIV(Current, RTAL);
	RTOTP;
	RTHOOK(1);
	RTDBGAL(Current, 0, 0xF80003C2, 0,0); /* Result */
	tr1 = RTLN(eif_new_type(128, 0x01).id);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTWC(32, Dtype(tr1)))(tr1);
	RTNHOOK(1,1);
	tr2 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTVF(3575, "gray", tr1))(tr1)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	Result = (EIF_REFERENCE) RTCCL(tr2);
	RTVI(Current, RTAL);
	RTRS;
	RTOTE;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef up1
#undef Result
}

/* {EV_GRID_DRAWER_I}.horizontal_border_width */
EIF_TYPED_VALUE F658_12123 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {EV_GRID_DRAWER_I}.pre_search_iteration_size */
EIF_TYPED_VALUE F658_12124 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1000L);
	return r;
}

/* {EV_GRID_DRAWER_I}.item_buffer_pixmap */
RTOID (F658_12125)
EIF_TYPED_VALUE F658_12125 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "item_buffer_pixmap";
	RTEX;
	EIF_TYPED_VALUE ui4_1x = {{0}, SK_INT32};
#define ui4_1 ui4_1x.it_i4
	EIF_TYPED_VALUE ui4_2x = {{0}, SK_INT32};
#define ui4_2 ui4_2x.it_i4
	EIF_REFERENCE tr1 = NULL;
	RTSN;
	RTDA;
	RTLD;
	
#define Result RTOTRR
	RTOTDR(F658_12125);

	RTLI(2);
	RTLR(0,tr1);
	RTLR(1,Current);
	RTLIU(2);
	RTLU (SK_REF, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 0, 16036);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 0);
	RTGC;
	RTDBGEAA(657, Current, 16036);
	RTIV(Current, RTAL);
	RTOTP;
	RTHOOK(1);
	RTDBGAL(Current, 0, 0xF800041D, 0,0); /* Result */
	tr1 = RTLN(eif_new_type(1053, 0x01).id);
	(FUNCTION_CAST(void, (EIF_REFERENCE)) RTWC(32, Dtype(tr1)))(tr1);
	RTNHOOK(1,1);
	Result = (EIF_REFERENCE) RTCCL(tr1);
	RTHOOK(2);
	ui4_1 = ((EIF_INTEGER_32) 100L);
	ui4_2 = ((EIF_INTEGER_32) 16L);
	(FUNCTION_CAST(void, (EIF_REFERENCE, EIF_TYPED_VALUE, EIF_TYPED_VALUE)) RTVF(17674, "set_size", Result))(Result, ui4_1x, ui4_2x);
	RTVI(Current, RTAL);
	RTRS;
	RTOTE;
	RTHOOK(3);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_REF; r.it_r = Result; return r; }
#undef ui4_1
#undef ui4_2
#undef Result
}

/* {EV_GRID_DRAWER_I}.temp_rectangle */
EIF_TYPED_VALUE F658_12126 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(10862,Dtype(Current)));
	return r;
}


/* {EV_GRID_DRAWER_I}.drawable_x_to_virtual_x */
EIF_TYPED_VALUE F658_12127 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x)
{
	GTCX
	char *l_feature_name = "drawable_x_to_virtual_x";
	RTEX;
#define arg1 arg1x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(2);
	RTLR(0,Current);
	RTLR(1,tr1);
	RTLIU(2);
	RTLU (SK_INT32, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 1, 16038);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16038);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 0, 0x10000000, 1,0); /* Result */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,1);
	ti4_1 = *(EIF_INTEGER_32 *)(tr1 + RTVA(19360, "internal_client_x", tr1));
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,2);
	ti4_2 = *(EIF_INTEGER_32 *)(tr1 + RTVA(19362, "viewport_x_offset", tr1));
	Result = (EIF_INTEGER_32) (EIF_INTEGER_32) ((EIF_INTEGER_32) (ti4_1 + arg1) - ti4_2);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(3);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
#undef up1
#undef arg1
}

/* {EV_GRID_DRAWER_I}.drawable_y_to_virtual_y */
EIF_TYPED_VALUE F658_12128 (EIF_REFERENCE Current, EIF_TYPED_VALUE arg1x)
{
	GTCX
	char *l_feature_name = "drawable_y_to_virtual_y";
	RTEX;
#define arg1 arg1x.it_i4
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_REFERENCE tr1 = NULL;
	EIF_INTEGER_32 ti4_1;
	EIF_INTEGER_32 ti4_2;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTCDT;
	RTSN;
	RTDA;
	RTLD;
	
	if ((arg1x.type & SK_HEAD) == SK_REF) arg1x.it_i4 = * (EIF_INTEGER_32 *) arg1x.it_r;
	
	RTLI(2);
	RTLR(0,Current);
	RTLR(1,tr1);
	RTLIU(2);
	RTLU (SK_INT32, &Result);
	RTLU(SK_INT32,&arg1);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 657, Current, 0, 1, 16039);
	RTSA(dtype);
	RTSC;
	RTME(dtype, 0);
	RTGC;
	RTDBGEAA(657, Current, 16039);
	RTIV(Current, RTAL);
	RTHOOK(1);
	RTDBGAL(Current, 0, 0x10000000, 1,0); /* Result */
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,1);
	ti4_1 = *(EIF_INTEGER_32 *)(tr1 + RTVA(19361, "internal_client_y", tr1));
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	RTNHOOK(1,2);
	ti4_2 = *(EIF_INTEGER_32 *)(tr1 + RTVA(19363, "viewport_y_offset", tr1));
	Result = (EIF_INTEGER_32) (EIF_INTEGER_32) ((EIF_INTEGER_32) (ti4_1 + arg1) - ti4_2);
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(2);
	RTDBGLE;
	RTMD(0);
	RTLE;
	RTLO(3);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
#undef up1
#undef arg1
}

/* {EV_GRID_DRAWER_I}._invariant */
void F658_29561 (EIF_REFERENCE Current, int where)
{
	GTCX
	char *l_feature_name = "_invariant";
	RTEX;
	EIF_TYPED_VALUE up1x = {{0}, SK_POINTER};
#define up1 up1x.it_p
	EIF_REFERENCE tr1 = NULL;
	RTCDT;
	RTLD;
	RTDA;
	
	RTLI(2);
	RTLR(0,Current);
	RTLR(1,tr1);
	RTLIU(2);
	RTLU (SK_VOID, NULL);
	RTLU (SK_REF, &Current);
	RTEAINV(l_feature_name, 657, Current, 0, 29560);
	RTSA(dtype);
	RTME(dtype, 0);
	RTIT("grid_not_void", Current);
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10835, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	if ((EIF_BOOLEAN)(tr1 != NULL)) {
		RTCK;
	} else {
		RTCF;
	}
	RTIT("temp_rectangle_not_void", Current);
	tr1 = ((up1x = (FUNCTION_CAST(EIF_TYPED_VALUE, (EIF_REFERENCE)) RTWF(10862, dtype))(Current)), (((up1x.type & SK_HEAD) == SK_REF)? (EIF_REFERENCE) 0: (up1x.it_r = RTBU(up1x))), (up1x.type = SK_POINTER), up1x.it_r);
	if ((EIF_BOOLEAN)(tr1 != NULL)) {
		RTCK;
	} else {
		RTCF;
	}
	RTLO(2);
	RTMD(0);
	RTLE;
	RTEE;
#undef up1
}

void EIF_Minit658 (void)
{
	GTCX
	RTOTS (12116,F658_12116)
	RTOTS (12120,F658_12120)
	RTOTS (12121,F658_12121)
	RTOTS (12122,F658_12122)
	RTOTS (12125,F658_12125)
}


#ifdef __cplusplus
}
#endif
