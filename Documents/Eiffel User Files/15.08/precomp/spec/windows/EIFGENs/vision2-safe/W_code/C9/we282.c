/*
 * Code for class WEL_ESB_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F282_5253(EIF_REFERENCE);
extern EIF_TYPED_VALUE F282_5254(EIF_REFERENCE);
extern EIF_TYPED_VALUE F282_5255(EIF_REFERENCE);
extern EIF_TYPED_VALUE F282_5256(EIF_REFERENCE);
extern EIF_TYPED_VALUE F282_5257(EIF_REFERENCE);
extern EIF_TYPED_VALUE F282_5258(EIF_REFERENCE);
extern EIF_TYPED_VALUE F282_5259(EIF_REFERENCE);
extern void EIF_Minit282(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_ESB_CONSTANTS}.esb_disable_both */
EIF_TYPED_VALUE F282_5253 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_ESB_CONSTANTS}.esb_disable_down */
EIF_TYPED_VALUE F282_5254 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_ESB_CONSTANTS}.esb_disable_ltup */
EIF_TYPED_VALUE F282_5255 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_ESB_CONSTANTS}.esb_disable_left */
EIF_TYPED_VALUE F282_5256 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_ESB_CONSTANTS}.esb_disable_rtdn */
EIF_TYPED_VALUE F282_5257 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_ESB_CONSTANTS}.esb_disable_up */
EIF_TYPED_VALUE F282_5258 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_ESB_CONSTANTS}.esb_enable_both */
EIF_TYPED_VALUE F282_5259 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

void EIF_Minit282 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
