/*
 * Code for class WEL_PFM_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F417_7344(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7345(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7346(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7347(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7348(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7349(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7350(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7351(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7352(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7353(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7354(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7355(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7356(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7357(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7358(EIF_REFERENCE);
extern EIF_TYPED_VALUE F417_7359(EIF_REFERENCE);
extern void EIF_Minit417(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_PFM_CONSTANTS}.pfm_startindent */
EIF_TYPED_VALUE F417_7344 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_rightindent */
EIF_TYPED_VALUE F417_7345 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_offset */
EIF_TYPED_VALUE F417_7346 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_alignment */
EIF_TYPED_VALUE F417_7347 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_tabstops */
EIF_TYPED_VALUE F417_7348 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_numbering */
EIF_TYPED_VALUE F417_7349 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_spacebefore */
EIF_TYPED_VALUE F417_7350 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_spaceafter */
EIF_TYPED_VALUE F417_7351 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_linespacing */
EIF_TYPED_VALUE F417_7352 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_style */
EIF_TYPED_VALUE F417_7353 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_border */
EIF_TYPED_VALUE F417_7354 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2048L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_shading */
EIF_TYPED_VALUE F417_7355 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4096L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_numberingstyle */
EIF_TYPED_VALUE F417_7356 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8092L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_numberingtab */
EIF_TYPED_VALUE F417_7357 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16384L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_numberingstart */
EIF_TYPED_VALUE F417_7358 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32768L);
	return r;
}

/* {WEL_PFM_CONSTANTS}.pfm_offsetindent */
EIF_TYPED_VALUE F417_7359 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0x80000000L);
	return r;
}

void EIF_Minit417 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
