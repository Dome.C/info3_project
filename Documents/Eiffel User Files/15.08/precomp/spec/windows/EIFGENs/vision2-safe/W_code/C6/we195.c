/*
 * Code for class WEL_PBM_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F195_4506(EIF_REFERENCE);
extern EIF_TYPED_VALUE F195_4507(EIF_REFERENCE);
extern EIF_TYPED_VALUE F195_4508(EIF_REFERENCE);
extern EIF_TYPED_VALUE F195_4509(EIF_REFERENCE);
extern EIF_TYPED_VALUE F195_4510(EIF_REFERENCE);
extern EIF_TYPED_VALUE F195_4511(EIF_REFERENCE);
extern EIF_TYPED_VALUE F195_4512(EIF_REFERENCE);
extern EIF_TYPED_VALUE F195_4513(EIF_REFERENCE);
extern void EIF_Minit195(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_PBM_CONSTANTS}.pbm_getpos */
EIF_TYPED_VALUE F195_4506 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pbm_getpos";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 194, Current, 0, 0, 4611);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(194, Current, 4611);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PBM_GETPOS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_PBM_CONSTANTS}.pbm_getrange */
EIF_TYPED_VALUE F195_4507 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pbm_getrange";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 194, Current, 0, 0, 4612);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(194, Current, 4612);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PBM_GETRANGE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_PBM_CONSTANTS}.pbm_setrange */
EIF_TYPED_VALUE F195_4508 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pbm_setrange";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 194, Current, 0, 0, 4613);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(194, Current, 4613);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PBM_SETRANGE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_PBM_CONSTANTS}.pbm_setrange32 */
EIF_TYPED_VALUE F195_4509 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pbm_setrange32";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 194, Current, 0, 0, 4614);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(194, Current, 4614);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PBM_SETRANGE32;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_PBM_CONSTANTS}.pbm_setpos */
EIF_TYPED_VALUE F195_4510 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pbm_setpos";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 194, Current, 0, 0, 4615);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(194, Current, 4615);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PBM_SETPOS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_PBM_CONSTANTS}.pbm_deltapos */
EIF_TYPED_VALUE F195_4511 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pbm_deltapos";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 194, Current, 0, 0, 4616);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(194, Current, 4616);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PBM_DELTAPOS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_PBM_CONSTANTS}.pbm_setstep */
EIF_TYPED_VALUE F195_4512 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pbm_setstep";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 194, Current, 0, 0, 4617);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(194, Current, 4617);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PBM_SETSTEP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_PBM_CONSTANTS}.pbm_stepit */
EIF_TYPED_VALUE F195_4513 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pbm_stepit";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 194, Current, 0, 0, 4618);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(194, Current, 4618);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PBM_STEPIT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit195 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
