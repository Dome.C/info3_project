/*
 * Class WEL_GDIP_COLOR_MATRIX_FLAGS
 */

#include "eif_macros.h"


#ifdef __cplusplus
extern "C" {
#endif

static const EIF_TYPE_INDEX egt_0_110 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_1_110 [] = {0xFF01,1551,109,0xFFFF};
static const EIF_TYPE_INDEX egt_2_110 [] = {0xFF01,109,0xFFFF};
static const EIF_TYPE_INDEX egt_3_110 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_4_110 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_5_110 [] = {0xFF01,109,0xFFFF};
static const EIF_TYPE_INDEX egt_6_110 [] = {0xFF01,109,0xFFFF};
static const EIF_TYPE_INDEX egt_7_110 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_8_110 [] = {0xFF01,126,0xFFFF};
static const EIF_TYPE_INDEX egt_9_110 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_10_110 [] = {0xFF01,914,0xFFFF};
static const EIF_TYPE_INDEX egt_11_110 [] = {0xFF01,29,0xFFFF};
static const EIF_TYPE_INDEX egt_12_110 [] = {0xFF01,109,0xFFFF};


static const struct desc_info desc_110[] = {
	{EIF_GENERIC(NULL), 0xFFFFFFFF, 0xFFFFFFFF},
	{EIF_GENERIC(egt_0_110), 0, 0xFFFFFFFF},
	{EIF_GENERIC(egt_1_110), 1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 2, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 3, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 4, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 5, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 6, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 7, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 8, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 9, 0xFFFFFFFF},
	{EIF_GENERIC(egt_2_110), 10, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 11, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 12, 0xFFFFFFFF},
	{EIF_GENERIC(egt_3_110), 13, 0xFFFFFFFF},
	{EIF_GENERIC(egt_4_110), 14, 0xFFFFFFFF},
	{EIF_GENERIC(egt_5_110), 15, 0xFFFFFFFF},
	{EIF_GENERIC(egt_6_110), 16, 0xFFFFFFFF},
	{EIF_GENERIC(egt_7_110), 17, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 18, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 19, 0xFFFFFFFF},
	{EIF_GENERIC(egt_8_110), 20, 0xFFFFFFFF},
	{EIF_GENERIC(egt_9_110), 21, 0xFFFFFFFF},
	{EIF_GENERIC(egt_10_110), 22, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 23, 0xFFFFFFFF},
	{EIF_GENERIC(egt_11_110), 24, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 25, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 26, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 27, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0xDB /*109*/), 28, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06E3 /*881*/), 29, 0xFFFFFFFF},
	{EIF_GENERIC(egt_12_110), 30, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 1483, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 1484, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06CB /*869*/), 1485, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x06C5 /*866*/), 1486, 0xFFFFFFFF},
};
void Init110(void)
{
	IDSC(desc_110, 0, 109);
	IDSC(desc_110 + 1, 1, 109);
	IDSC(desc_110 + 32, 606, 109);
}


#ifdef __cplusplus
}
#endif
