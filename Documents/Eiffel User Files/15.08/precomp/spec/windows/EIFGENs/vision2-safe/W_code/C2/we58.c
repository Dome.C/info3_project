/*
 * Code for class WEL_RT_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F58_760(EIF_REFERENCE);
extern EIF_TYPED_VALUE F58_761(EIF_REFERENCE);
extern EIF_TYPED_VALUE F58_762(EIF_REFERENCE);
extern EIF_TYPED_VALUE F58_763(EIF_REFERENCE);
extern EIF_TYPED_VALUE F58_764(EIF_REFERENCE);
extern EIF_TYPED_VALUE F58_765(EIF_REFERENCE);
extern EIF_TYPED_VALUE F58_766(EIF_REFERENCE);
extern EIF_TYPED_VALUE F58_767(EIF_REFERENCE);
extern EIF_TYPED_VALUE F58_768(EIF_REFERENCE);
extern EIF_TYPED_VALUE F58_769(EIF_REFERENCE);
extern EIF_TYPED_VALUE F58_770(EIF_REFERENCE);
extern EIF_TYPED_VALUE F58_771(EIF_REFERENCE);
extern void EIF_Minit58(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_RT_CONSTANTS}.rt_cursor */
EIF_TYPED_VALUE F58_760 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rt_cursor";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 57, Current, 0, 0, 778);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(57, Current, 778);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) RT_CURSOR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_RT_CONSTANTS}.rt_bitmap */
EIF_TYPED_VALUE F58_761 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rt_bitmap";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 57, Current, 0, 0, 779);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(57, Current, 779);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) RT_BITMAP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_RT_CONSTANTS}.rt_icon */
EIF_TYPED_VALUE F58_762 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rt_icon";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 57, Current, 0, 0, 780);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(57, Current, 780);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) RT_ICON;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_RT_CONSTANTS}.rt_menu */
EIF_TYPED_VALUE F58_763 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rt_menu";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 57, Current, 0, 0, 781);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(57, Current, 781);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) RT_MENU;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_RT_CONSTANTS}.rt_dialog */
EIF_TYPED_VALUE F58_764 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rt_dialog";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 57, Current, 0, 0, 782);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(57, Current, 782);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) RT_DIALOG;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_RT_CONSTANTS}.rt_string */
EIF_TYPED_VALUE F58_765 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rt_string";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 57, Current, 0, 0, 783);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(57, Current, 783);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) RT_STRING;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_RT_CONSTANTS}.rt_fontdir */
EIF_TYPED_VALUE F58_766 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rt_fontdir";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 57, Current, 0, 0, 784);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(57, Current, 784);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) RT_FONTDIR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_RT_CONSTANTS}.rt_font */
EIF_TYPED_VALUE F58_767 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rt_font";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 57, Current, 0, 0, 785);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(57, Current, 785);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) RT_FONT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_RT_CONSTANTS}.rt_accelerator */
EIF_TYPED_VALUE F58_768 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rt_accelerator";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 57, Current, 0, 0, 786);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(57, Current, 786);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) RT_ACCELERATOR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_RT_CONSTANTS}.rt_rcdata */
EIF_TYPED_VALUE F58_769 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rt_rcdata";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 57, Current, 0, 0, 787);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(57, Current, 787);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) RT_RCDATA;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_RT_CONSTANTS}.rt_group_cursor */
EIF_TYPED_VALUE F58_770 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rt_group_cursor";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 57, Current, 0, 0, 788);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(57, Current, 788);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) RT_GROUP_CURSOR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_RT_CONSTANTS}.rt_group_icon */
EIF_TYPED_VALUE F58_771 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rt_group_icon";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 57, Current, 0, 0, 789);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(57, Current, 789);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) RT_GROUP_ICON;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

void EIF_Minit58 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
