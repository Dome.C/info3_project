/*
 * Code for class WEL_NIIF_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F29_249(EIF_REFERENCE);
extern EIF_TYPED_VALUE F29_250(EIF_REFERENCE);
extern EIF_TYPED_VALUE F29_251(EIF_REFERENCE);
extern EIF_TYPED_VALUE F29_252(EIF_REFERENCE);
extern EIF_TYPED_VALUE F29_253(EIF_REFERENCE);
extern EIF_TYPED_VALUE F29_254(EIF_REFERENCE);
extern EIF_TYPED_VALUE F29_255(EIF_REFERENCE);
extern void EIF_Minit29(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_NIIF_CONSTANTS}.niif_none */
EIF_TYPED_VALUE F29_249 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_NIIF_CONSTANTS}.niif_info */
EIF_TYPED_VALUE F29_250 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_NIIF_CONSTANTS}.niif_warning */
EIF_TYPED_VALUE F29_251 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_NIIF_CONSTANTS}.niif_error */
EIF_TYPED_VALUE F29_252 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_NIIF_CONSTANTS}.niif_user */
EIF_TYPED_VALUE F29_253 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_NIIF_CONSTANTS}.niif_icon_mask */
EIF_TYPED_VALUE F29_254 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 15L);
	return r;
}

/* {WEL_NIIF_CONSTANTS}.niif_nosound */
EIF_TYPED_VALUE F29_255 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

void EIF_Minit29 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
