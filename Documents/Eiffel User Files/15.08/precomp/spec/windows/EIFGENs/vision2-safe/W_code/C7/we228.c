/*
 * Code for class WEL_RBS_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F228_4794(EIF_REFERENCE);
extern EIF_TYPED_VALUE F228_4795(EIF_REFERENCE);
extern EIF_TYPED_VALUE F228_4796(EIF_REFERENCE);
extern EIF_TYPED_VALUE F228_4797(EIF_REFERENCE);
extern void EIF_Minit228(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_RBS_CONSTANTS}.rbs_tooltips */
EIF_TYPED_VALUE F228_4794 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbs_tooltips";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 227, Current, 0, 0, 4897);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(227, Current, 4897);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBS_TOOLTIPS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBS_CONSTANTS}.rbs_varheight */
EIF_TYPED_VALUE F228_4795 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbs_varheight";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 227, Current, 0, 0, 4898);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(227, Current, 4898);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBS_VARHEIGHT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBS_CONSTANTS}.rbs_bandborders */
EIF_TYPED_VALUE F228_4796 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbs_bandborders";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 227, Current, 0, 0, 4899);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(227, Current, 4899);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBS_BANDBORDERS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_RBS_CONSTANTS}.rbs_fixedorder */
EIF_TYPED_VALUE F228_4797 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "rbs_fixedorder";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 227, Current, 0, 0, 4900);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(227, Current, 4900);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) RBS_FIXEDORDER;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit228 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
