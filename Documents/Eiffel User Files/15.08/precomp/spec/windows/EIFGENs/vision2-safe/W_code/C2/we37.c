/*
 * Code for class WEL_IME_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F37_473(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_474(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_475(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_476(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_477(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_478(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_479(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_480(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_481(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_482(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_483(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_484(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_485(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_486(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_487(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_488(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_489(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_490(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_491(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_492(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_493(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_494(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_495(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_496(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_497(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_498(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_499(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_500(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_501(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_502(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_503(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_504(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_505(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_506(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_507(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_508(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_509(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_510(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_511(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_512(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_513(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_514(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_515(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_516(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_517(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_518(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_519(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_520(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_521(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_522(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_523(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_524(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_525(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_526(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_527(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_528(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_529(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_530(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_531(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_532(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_533(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_534(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_535(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_536(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_537(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_538(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_539(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_540(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_541(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_542(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_543(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_544(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_545(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_546(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_547(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_548(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_549(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_550(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_551(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_552(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_553(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_554(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_555(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_556(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_557(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_558(EIF_REFERENCE);
extern EIF_TYPED_VALUE F37_559(EIF_REFERENCE);
extern void EIF_Minit37(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_IME_CONSTANTS}.ime_config_general */
EIF_TYPED_VALUE F37_473 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_config_register_word */
EIF_TYPED_VALUE F37_474 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_config_selectdictionary */
EIF_TYPED_VALUE F37_475 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_chotkey_ime_nonime_toggle */
EIF_TYPED_VALUE F37_476 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 10L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_chotkey_shape_toggle */
EIF_TYPED_VALUE F37_477 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 11L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_chotkey_symbol_toggle */
EIF_TYPED_VALUE F37_478 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 12L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_jhotkey_close_open */
EIF_TYPED_VALUE F37_479 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 48L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_khotkey_shape_toggle */
EIF_TYPED_VALUE F37_480 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 80L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_khotkey_hanjaconvert */
EIF_TYPED_VALUE F37_481 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 81L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_khotkey_english */
EIF_TYPED_VALUE F37_482 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 82L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_thotkey_ime_nonime_toggle */
EIF_TYPED_VALUE F37_483 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 112L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_thotkey_shape_toggle */
EIF_TYPED_VALUE F37_484 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 113L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_thotkey_symbol_toggle */
EIF_TYPED_VALUE F37_485 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 114L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_hotkey_dswitch_first */
EIF_TYPED_VALUE F37_486 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_hotkey_dswitch_last */
EIF_TYPED_VALUE F37_487 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 287L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_hotkey_private_first */
EIF_TYPED_VALUE F37_488 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_ithotkey_resend_resultstr */
EIF_TYPED_VALUE F37_489 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_ithotkey_previous_composition */
EIF_TYPED_VALUE F37_490 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 513L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_ithotkey_uistyle_toggle */
EIF_TYPED_VALUE F37_491 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 514L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_ithotkey_reconvertstring */
EIF_TYPED_VALUE F37_492 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 515L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_hotkey_private_last */
EIF_TYPED_VALUE F37_493 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 543L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_alphanumeric */
EIF_TYPED_VALUE F37_494 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_native */
EIF_TYPED_VALUE F37_495 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_chinese */
EIF_TYPED_VALUE F37_496 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_hangeul */
EIF_TYPED_VALUE F37_497 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_hangul */
EIF_TYPED_VALUE F37_498 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_japanese */
EIF_TYPED_VALUE F37_499 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_katakana */
EIF_TYPED_VALUE F37_500 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_language */
EIF_TYPED_VALUE F37_501 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_fullshape */
EIF_TYPED_VALUE F37_502 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_roman */
EIF_TYPED_VALUE F37_503 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_charcode */
EIF_TYPED_VALUE F37_504 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_hanjaconvert */
EIF_TYPED_VALUE F37_505 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_softkbd */
EIF_TYPED_VALUE F37_506 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_noconversion */
EIF_TYPED_VALUE F37_507 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_eudc */
EIF_TYPED_VALUE F37_508 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_symbol */
EIF_TYPED_VALUE F37_509 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1024L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_cmode_fixed */
EIF_TYPED_VALUE F37_510 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2048L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_smode_none */
EIF_TYPED_VALUE F37_511 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_smode_pluralclause */
EIF_TYPED_VALUE F37_512 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_smode_singleconvert */
EIF_TYPED_VALUE F37_513 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_smode_automatic */
EIF_TYPED_VALUE F37_514 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_smode_phrasepredict */
EIF_TYPED_VALUE F37_515 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_smode_conversation */
EIF_TYPED_VALUE F37_516 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 10L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_default */
EIF_TYPED_VALUE F37_517 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_japanese_xjis */
EIF_TYPED_VALUE F37_518 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_japanese_unicode */
EIF_TYPED_VALUE F37_519 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_chinese_big5 */
EIF_TYPED_VALUE F37_520 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_chinese_prcp */
EIF_TYPED_VALUE F37_521 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_chinese_unicode */
EIF_TYPED_VALUE F37_522 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_chinese_prc */
EIF_TYPED_VALUE F37_523 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_chinese_bopomofo */
EIF_TYPED_VALUE F37_524 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_korean_ksc */
EIF_TYPED_VALUE F37_525 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_korean_unicode */
EIF_TYPED_VALUE F37_526 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_german_phone_book */
EIF_TYPED_VALUE F37_527 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_hungarian_default */
EIF_TYPED_VALUE F37_528 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_hungarian_technical */
EIF_TYPED_VALUE F37_529 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_georgian_traditional */
EIF_TYPED_VALUE F37_530 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_IME_CONSTANTS}.sort_georgian_modern */
EIF_TYPED_VALUE F37_531 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.igp_getimeversion */
EIF_TYPED_VALUE F37_532 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) -4L);
	return r;
}

/* {WEL_IME_CONSTANTS}.igp_property */
EIF_TYPED_VALUE F37_533 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_IME_CONSTANTS}.igp_conversion */
EIF_TYPED_VALUE F37_534 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_IME_CONSTANTS}.igp_sentence */
EIF_TYPED_VALUE F37_535 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 12L);
	return r;
}

/* {WEL_IME_CONSTANTS}.igp_ui */
EIF_TYPED_VALUE F37_536 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_IME_CONSTANTS}.igp_setcompstr */
EIF_TYPED_VALUE F37_537 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 20L);
	return r;
}

/* {WEL_IME_CONSTANTS}.igp_select */
EIF_TYPED_VALUE F37_538 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 24L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_prop_at_caret */
EIF_TYPED_VALUE F37_539 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 65536L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_prop_special_ui */
EIF_TYPED_VALUE F37_540 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 131072L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_prop_candlist_start_from_1 */
EIF_TYPED_VALUE F37_541 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 264144L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_prop_unicode */
EIF_TYPED_VALUE F37_542 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 524288L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ime_prop_complete_on_unselect */
EIF_TYPED_VALUE F37_543 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1048576L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ui_cap_2700 */
EIF_TYPED_VALUE F37_544 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ui_cap_rot90 */
EIF_TYPED_VALUE F37_545 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_IME_CONSTANTS}.ui_cap_rotany */
EIF_TYPED_VALUE F37_546 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_IME_CONSTANTS}.scs_cap_compstr */
EIF_TYPED_VALUE F37_547 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.scs_cap_makeread */
EIF_TYPED_VALUE F37_548 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_IME_CONSTANTS}.scs_cap_setreconvertstring */
EIF_TYPED_VALUE F37_549 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_IME_CONSTANTS}.select_cap_conversion */
EIF_TYPED_VALUE F37_550 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.select_cap_sentence */
EIF_TYPED_VALUE F37_551 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_IME_CONSTANTS}.imever_0310 */
EIF_TYPED_VALUE F37_552 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 200000L);
	return r;
}

/* {WEL_IME_CONSTANTS}.imever_0400 */
EIF_TYPED_VALUE F37_553 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 262144L);
	return r;
}

/* {WEL_IME_CONSTANTS}.cfs_default */
EIF_TYPED_VALUE F37_554 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_IME_CONSTANTS}.cfs_rect */
EIF_TYPED_VALUE F37_555 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_IME_CONSTANTS}.cfs_point */
EIF_TYPED_VALUE F37_556 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_IME_CONSTANTS}.cfs_force_position */
EIF_TYPED_VALUE F37_557 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_IME_CONSTANTS}.cfs_candidatepos */
EIF_TYPED_VALUE F37_558 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_IME_CONSTANTS}.cfs_exclude */
EIF_TYPED_VALUE F37_559 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

void EIF_Minit37 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
