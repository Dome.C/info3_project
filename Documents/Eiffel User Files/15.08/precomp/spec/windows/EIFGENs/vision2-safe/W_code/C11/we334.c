/*
 * Code for class WEL_ICC_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F334_6350(EIF_REFERENCE);
extern EIF_TYPED_VALUE F334_6351(EIF_REFERENCE);
extern EIF_TYPED_VALUE F334_6352(EIF_REFERENCE);
extern EIF_TYPED_VALUE F334_6353(EIF_REFERENCE);
extern EIF_TYPED_VALUE F334_6354(EIF_REFERENCE);
extern EIF_TYPED_VALUE F334_6355(EIF_REFERENCE);
extern EIF_TYPED_VALUE F334_6356(EIF_REFERENCE);
extern EIF_TYPED_VALUE F334_6357(EIF_REFERENCE);
extern EIF_TYPED_VALUE F334_6358(EIF_REFERENCE);
extern EIF_TYPED_VALUE F334_6359(EIF_REFERENCE);
extern EIF_TYPED_VALUE F334_6360(EIF_REFERENCE);
extern EIF_TYPED_VALUE F334_6361(EIF_REFERENCE);
extern void EIF_Minit334(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_ICC_CONSTANTS}.icc_animate_class */
EIF_TYPED_VALUE F334_6350 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "icc_animate_class";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 333, Current, 0, 0, 6301);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(333, Current, 6301);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ICC_ANIMATE_CLASS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ICC_CONSTANTS}.icc_bar_classes */
EIF_TYPED_VALUE F334_6351 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "icc_bar_classes";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 333, Current, 0, 0, 6302);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(333, Current, 6302);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ICC_BAR_CLASSES;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ICC_CONSTANTS}.icc_cool_classes */
EIF_TYPED_VALUE F334_6352 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "icc_cool_classes";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 333, Current, 0, 0, 6303);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(333, Current, 6303);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ICC_COOL_CLASSES;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ICC_CONSTANTS}.icc_date_classes */
EIF_TYPED_VALUE F334_6353 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "icc_date_classes";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 333, Current, 0, 0, 6304);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(333, Current, 6304);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ICC_DATE_CLASSES;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ICC_CONSTANTS}.icc_hotkey_class */
EIF_TYPED_VALUE F334_6354 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "icc_hotkey_class";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 333, Current, 0, 0, 6305);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(333, Current, 6305);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ICC_HOTKEY_CLASS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ICC_CONSTANTS}.icc_listview_classes */
EIF_TYPED_VALUE F334_6355 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "icc_listview_classes";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 333, Current, 0, 0, 6294);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(333, Current, 6294);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ICC_LISTVIEW_CLASSES;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ICC_CONSTANTS}.icc_progress_class */
EIF_TYPED_VALUE F334_6356 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "icc_progress_class";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 333, Current, 0, 0, 6295);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(333, Current, 6295);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ICC_PROGRESS_CLASS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ICC_CONSTANTS}.icc_tab_classes */
EIF_TYPED_VALUE F334_6357 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "icc_tab_classes";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 333, Current, 0, 0, 6296);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(333, Current, 6296);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ICC_TAB_CLASSES;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ICC_CONSTANTS}.icc_treeview_classes */
EIF_TYPED_VALUE F334_6358 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "icc_treeview_classes";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 333, Current, 0, 0, 6297);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(333, Current, 6297);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ICC_TREEVIEW_CLASSES;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ICC_CONSTANTS}.icc_updown_class */
EIF_TYPED_VALUE F334_6359 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "icc_updown_class";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 333, Current, 0, 0, 6298);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(333, Current, 6298);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ICC_UPDOWN_CLASS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ICC_CONSTANTS}.icc_userex_classes */
EIF_TYPED_VALUE F334_6360 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "icc_userex_classes";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 333, Current, 0, 0, 6299);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(333, Current, 6299);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ICC_USEREX_CLASSES;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_ICC_CONSTANTS}.icc_win95_classes */
EIF_TYPED_VALUE F334_6361 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "icc_win95_classes";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 333, Current, 0, 0, 6300);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(333, Current, 6300);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) ICC_WIN95_CLASSES;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit334 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
