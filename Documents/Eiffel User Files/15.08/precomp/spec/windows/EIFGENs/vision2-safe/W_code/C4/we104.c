/*
 * Code for class WEL_HELP_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F104_1318(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1319(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1320(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1321(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1322(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1323(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1324(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1325(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1326(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1327(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1328(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1329(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1330(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1331(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1332(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1333(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1334(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1335(EIF_REFERENCE);
extern EIF_TYPED_VALUE F104_1336(EIF_REFERENCE);
extern void EIF_Minit104(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_HELP_CONSTANTS}.help_context */
EIF_TYPED_VALUE F104_1318 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_quit */
EIF_TYPED_VALUE F104_1319 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_index */
EIF_TYPED_VALUE F104_1320 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_contents */
EIF_TYPED_VALUE F104_1321 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 3L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_helponhelp */
EIF_TYPED_VALUE F104_1322 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_setindex */
EIF_TYPED_VALUE F104_1323 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_setcontents */
EIF_TYPED_VALUE F104_1324 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_contextpopup */
EIF_TYPED_VALUE F104_1325 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_forcefile */
EIF_TYPED_VALUE F104_1326 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 9L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_key */
EIF_TYPED_VALUE F104_1327 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 257L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_command */
EIF_TYPED_VALUE F104_1328 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 258L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_contextmenu */
EIF_TYPED_VALUE F104_1329 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 10L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_partialkey */
EIF_TYPED_VALUE F104_1330 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 261L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_multikey */
EIF_TYPED_VALUE F104_1331 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 513L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_setwinpos */
EIF_TYPED_VALUE F104_1332 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 515L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_finder */
EIF_TYPED_VALUE F104_1333 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 11L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_setpopup_pos */
EIF_TYPED_VALUE F104_1334 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 13L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_tcard */
EIF_TYPED_VALUE F104_1335 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32768L);
	return r;
}

/* {WEL_HELP_CONSTANTS}.help_wm_help */
EIF_TYPED_VALUE F104_1336 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 12L);
	return r;
}

void EIF_Minit104 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
