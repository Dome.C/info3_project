/*
 * Code for class WEL_WPF_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F95_1239(EIF_REFERENCE);
extern EIF_TYPED_VALUE F95_1240(EIF_REFERENCE);
extern void EIF_Minit95(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_WPF_CONSTANTS}.wpf_setminposition */
EIF_TYPED_VALUE F95_1239 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "wpf_setminposition";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 94, Current, 0, 0, 1261);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(94, Current, 1261);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) WPF_SETMINPOSITION;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_WPF_CONSTANTS}.wpf_restoretomaximized */
EIF_TYPED_VALUE F95_1240 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "wpf_restoretomaximized";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 94, Current, 0, 0, 1260);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(94, Current, 1260);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) WPF_RESTORETOMAXIMIZED;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit95 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
