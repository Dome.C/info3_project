/*
 * Code for class WEL_KEYBOARD_LAYOUT_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F87_1167(EIF_REFERENCE);
extern EIF_TYPED_VALUE F87_1168(EIF_REFERENCE);
extern EIF_TYPED_VALUE F87_1169(EIF_REFERENCE);
extern EIF_TYPED_VALUE F87_1170(EIF_REFERENCE);
extern EIF_TYPED_VALUE F87_1171(EIF_REFERENCE);
extern EIF_TYPED_VALUE F87_1172(EIF_REFERENCE);
extern EIF_TYPED_VALUE F87_1173(EIF_REFERENCE);
extern EIF_TYPED_VALUE F87_1174(EIF_REFERENCE);
extern EIF_TYPED_VALUE F87_1175(EIF_REFERENCE);
extern EIF_TYPED_VALUE F87_1176(EIF_REFERENCE);
extern void EIF_Minit87(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_KEYBOARD_LAYOUT_CONSTANTS}.hkl_prev */
EIF_TYPED_VALUE F87_1167 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 0L);
	return r;
}

/* {WEL_KEYBOARD_LAYOUT_CONSTANTS}.hkl_next */
EIF_TYPED_VALUE F87_1168 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_KEYBOARD_LAYOUT_CONSTANTS}.klf_activate */
EIF_TYPED_VALUE F87_1169 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_KEYBOARD_LAYOUT_CONSTANTS}.klf_substitute_ok */
EIF_TYPED_VALUE F87_1170 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_KEYBOARD_LAYOUT_CONSTANTS}.klf_unload_previous */
EIF_TYPED_VALUE F87_1171 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_KEYBOARD_LAYOUT_CONSTANTS}.klf_reorder */
EIF_TYPED_VALUE F87_1172 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_KEYBOARD_LAYOUT_CONSTANTS}.kl_namelength */
EIF_TYPED_VALUE F87_1173 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 9L);
	return r;
}

/* {WEL_KEYBOARD_LAYOUT_CONSTANTS}.klf_replace_lang */
EIF_TYPED_VALUE F87_1174 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_KEYBOARD_LAYOUT_CONSTANTS}.klf_notellshell */
EIF_TYPED_VALUE F87_1175 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_KEYBOARD_LAYOUT_CONSTANTS}.klf_set_for_process */
EIF_TYPED_VALUE F87_1176 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

void EIF_Minit87 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
