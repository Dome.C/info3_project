/*
 * Code for class WEL_TTDT_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F148_3782(EIF_REFERENCE);
extern EIF_TYPED_VALUE F148_3783(EIF_REFERENCE);
extern EIF_TYPED_VALUE F148_3784(EIF_REFERENCE);
extern EIF_TYPED_VALUE F148_3785(EIF_REFERENCE);
extern void EIF_Minit148(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_TTDT_CONSTANTS}.ttdt_automatic */
EIF_TYPED_VALUE F148_3782 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttdt_automatic";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 147, Current, 0, 0, 3819);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(147, Current, 3819);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTDT_AUTOMATIC;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTDT_CONSTANTS}.ttdt_autopop */
EIF_TYPED_VALUE F148_3783 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttdt_autopop";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 147, Current, 0, 0, 3820);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(147, Current, 3820);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTDT_AUTOPOP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTDT_CONSTANTS}.ttdt_initial */
EIF_TYPED_VALUE F148_3784 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttdt_initial";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 147, Current, 0, 0, 3821);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(147, Current, 3821);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTDT_INITIAL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_TTDT_CONSTANTS}.ttdt_reshow */
EIF_TYPED_VALUE F148_3785 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "ttdt_reshow";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 147, Current, 0, 0, 3822);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(147, Current, 3822);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) TTDT_RESHOW;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit148 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
