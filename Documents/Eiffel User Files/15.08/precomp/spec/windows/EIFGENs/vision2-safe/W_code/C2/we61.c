/*
 * Code for class WEL_DS_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F61_809(EIF_REFERENCE);
extern EIF_TYPED_VALUE F61_810(EIF_REFERENCE);
extern EIF_TYPED_VALUE F61_811(EIF_REFERENCE);
extern EIF_TYPED_VALUE F61_812(EIF_REFERENCE);
extern EIF_TYPED_VALUE F61_813(EIF_REFERENCE);
extern EIF_TYPED_VALUE F61_814(EIF_REFERENCE);
extern EIF_TYPED_VALUE F61_815(EIF_REFERENCE);
extern void EIF_Minit61(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_DS_CONSTANTS}.ds_absalign */
EIF_TYPED_VALUE F61_809 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_DS_CONSTANTS}.ds_centermouse */
EIF_TYPED_VALUE F61_810 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4096L);
	return r;
}

/* {WEL_DS_CONSTANTS}.ds_setfont */
EIF_TYPED_VALUE F61_811 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 64L);
	return r;
}

/* {WEL_DS_CONSTANTS}.ds_modalframe */
EIF_TYPED_VALUE F61_812 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 128L);
	return r;
}

/* {WEL_DS_CONSTANTS}.ds_control */
EIF_TYPED_VALUE F61_813 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1024L);
	return r;
}

/* {WEL_DS_CONSTANTS}.ds_noidlemsg */
EIF_TYPED_VALUE F61_814 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_DS_CONSTANTS}.ds_setforeground */
EIF_TYPED_VALUE F61_815 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 512L);
	return r;
}

void EIF_Minit61 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
