/*
 * Code for class WEL_VERSION
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F55_734(EIF_REFERENCE);
extern EIF_TYPED_VALUE F55_735(EIF_REFERENCE);
extern EIF_TYPED_VALUE F55_736(EIF_REFERENCE);
extern void EIF_Minit55(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_VERSION}.major_version */
EIF_TYPED_VALUE F55_734 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 5L);
	return r;
}

/* {WEL_VERSION}.minor_version */
EIF_TYPED_VALUE F55_735 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 7L);
	return r;
}

/* {WEL_VERSION}.description */
RTOID (F55_736)


EIF_TYPED_VALUE F55_736 (EIF_REFERENCE Current)
{
	GTCX
	RTOTC (F55_736,755,RTMS_EX_H("Windows Eiffel Library version 5.7",34,1639342903));
}

void EIF_Minit55 (void)
{
	GTCX
	RTOTS (736,F55_736)
}


#ifdef __cplusplus
}
#endif
