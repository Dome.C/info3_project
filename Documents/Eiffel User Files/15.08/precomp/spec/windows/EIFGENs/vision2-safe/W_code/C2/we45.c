/*
 * Code for class WEL_TTF_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F45_670(EIF_REFERENCE);
extern EIF_TYPED_VALUE F45_671(EIF_REFERENCE);
extern EIF_TYPED_VALUE F45_672(EIF_REFERENCE);
extern EIF_TYPED_VALUE F45_673(EIF_REFERENCE);
extern void EIF_Minit45(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_TTF_CONSTANTS}.ttf_idishwnd */
EIF_TYPED_VALUE F45_670 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_TTF_CONSTANTS}.ttf_centertip */
EIF_TYPED_VALUE F45_671 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_TTF_CONSTANTS}.ttf_rtlreading */
EIF_TYPED_VALUE F45_672 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_TTF_CONSTANTS}.ttf_subclass */
EIF_TYPED_VALUE F45_673 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

void EIF_Minit45 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
