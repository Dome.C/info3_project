/*
 * Code for class WEL_DRAWING_ROUTINES_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F291_5504(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5505(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5506(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5507(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5508(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5509(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5510(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5511(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5512(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5513(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5514(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5515(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5516(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5517(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5518(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5519(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5520(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5521(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5522(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5523(EIF_REFERENCE);
extern EIF_TYPED_VALUE F291_5524(EIF_REFERENCE);
extern void EIF_Minit291(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.edge_bump */
EIF_TYPED_VALUE F291_5504 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "edge_bump";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5608);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5608);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) EDGE_BUMP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.edge_etched */
EIF_TYPED_VALUE F291_5505 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "edge_etched";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5609);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5609);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) EDGE_ETCHED;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.edge_raised */
EIF_TYPED_VALUE F291_5506 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "edge_raised";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5610);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5610);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) EDGE_RAISED;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.edge_sunken */
EIF_TYPED_VALUE F291_5507 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "edge_sunken";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5611);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5611);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) EDGE_SUNKEN;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.bdr_raisedouter */
EIF_TYPED_VALUE F291_5508 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.bdr_sunkenouter */
EIF_TYPED_VALUE F291_5509 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.bdr_raisedinner */
EIF_TYPED_VALUE F291_5510 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.bdr_sunkeninner */
EIF_TYPED_VALUE F291_5511 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.bf_rect */
EIF_TYPED_VALUE F291_5512 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "bf_rect";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5616);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5616);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) BF_RECT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.bf_adjust */
EIF_TYPED_VALUE F291_5513 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "bf_adjust";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5617);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5617);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) BF_ADJUST;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.bf_flat */
EIF_TYPED_VALUE F291_5514 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "bf_flat";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5618);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5618);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) BF_FLAT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.bf_soft */
EIF_TYPED_VALUE F291_5515 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "bf_soft";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5619);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5619);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) BF_SOFT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.dst_bitmap */
EIF_TYPED_VALUE F291_5516 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dst_bitmap";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5620);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5620);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DST_BITMAP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.dst_complex */
EIF_TYPED_VALUE F291_5517 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dst_complex";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5621);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5621);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DST_COMPLEX;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.dst_icon */
EIF_TYPED_VALUE F291_5518 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dst_icon";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5601);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5601);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DST_ICON;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.dst_prefixtext */
EIF_TYPED_VALUE F291_5519 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dst_prefixtext";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5602);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5602);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DST_PREFIXTEXT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.dst_text */
EIF_TYPED_VALUE F291_5520 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dst_text";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5603);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5603);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DST_TEXT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.dss_normal */
EIF_TYPED_VALUE F291_5521 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dss_normal";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5604);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5604);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DSS_NORMAL;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.dss_union */
EIF_TYPED_VALUE F291_5522 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dss_union";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5605);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5605);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DSS_UNION;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.dss_disabled */
EIF_TYPED_VALUE F291_5523 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dss_disabled";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5606);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5606);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DSS_DISABLED;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_DRAWING_ROUTINES_CONSTANTS}.dss_mono */
EIF_TYPED_VALUE F291_5524 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "dss_mono";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 290, Current, 0, 0, 5607);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(290, Current, 5607);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) DSS_MONO;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit291 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
