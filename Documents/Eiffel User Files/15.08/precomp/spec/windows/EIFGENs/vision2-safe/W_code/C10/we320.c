/*
 * Code for class WEL_TVS_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F320_6244(EIF_REFERENCE);
extern EIF_TYPED_VALUE F320_6245(EIF_REFERENCE);
extern EIF_TYPED_VALUE F320_6246(EIF_REFERENCE);
extern EIF_TYPED_VALUE F320_6247(EIF_REFERENCE);
extern EIF_TYPED_VALUE F320_6248(EIF_REFERENCE);
extern EIF_TYPED_VALUE F320_6249(EIF_REFERENCE);
extern EIF_TYPED_VALUE F320_6250(EIF_REFERENCE);
extern EIF_TYPED_VALUE F320_6251(EIF_REFERENCE);
extern void EIF_Minit320(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_TVS_CONSTANTS}.tvs_hasbuttons */
EIF_TYPED_VALUE F320_6244 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 1L);
	return r;
}

/* {WEL_TVS_CONSTANTS}.tvs_haslines */
EIF_TYPED_VALUE F320_6245 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2L);
	return r;
}

/* {WEL_TVS_CONSTANTS}.tvs_linesatroot */
EIF_TYPED_VALUE F320_6246 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 4L);
	return r;
}

/* {WEL_TVS_CONSTANTS}.tvs_editlabels */
EIF_TYPED_VALUE F320_6247 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 8L);
	return r;
}

/* {WEL_TVS_CONSTANTS}.tvs_disabledragdrop */
EIF_TYPED_VALUE F320_6248 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 16L);
	return r;
}

/* {WEL_TVS_CONSTANTS}.tvs_showselalways */
EIF_TYPED_VALUE F320_6249 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 32L);
	return r;
}

/* {WEL_TVS_CONSTANTS}.tvs_checkboxes */
EIF_TYPED_VALUE F320_6250 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 256L);
	return r;
}

/* {WEL_TVS_CONSTANTS}.tvs_infotip */
EIF_TYPED_VALUE F320_6251 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_INT32;
	r.it_i4 = (EIF_INTEGER_32) ((EIF_INTEGER_32) 2048L);
	return r;
}

void EIF_Minit320 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
