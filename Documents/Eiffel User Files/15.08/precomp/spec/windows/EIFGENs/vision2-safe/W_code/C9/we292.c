/*
 * Code for class WEL_IDI_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F292_5525(EIF_REFERENCE);
extern EIF_TYPED_VALUE F292_5526(EIF_REFERENCE);
extern EIF_TYPED_VALUE F292_5527(EIF_REFERENCE);
extern EIF_TYPED_VALUE F292_5528(EIF_REFERENCE);
extern EIF_TYPED_VALUE F292_5529(EIF_REFERENCE);
extern EIF_TYPED_VALUE F292_5530(EIF_REFERENCE);
extern EIF_TYPED_VALUE F292_5531(EIF_REFERENCE);
extern EIF_TYPED_VALUE F292_5532(EIF_REFERENCE);
extern void EIF_Minit292(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_IDI_CONSTANTS}.idi_application */
EIF_TYPED_VALUE F292_5525 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idi_application";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 291, Current, 0, 0, 5622);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(291, Current, 5622);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDI_APPLICATION;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDI_CONSTANTS}.idi_hand */
EIF_TYPED_VALUE F292_5526 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idi_hand";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 291, Current, 0, 0, 5623);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(291, Current, 5623);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDI_HAND;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDI_CONSTANTS}.idi_error */
EIF_TYPED_VALUE F292_5527 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idi_error";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 291, Current, 0, 0, 5624);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(291, Current, 5624);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDI_HAND;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDI_CONSTANTS}.idi_question */
EIF_TYPED_VALUE F292_5528 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idi_question";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 291, Current, 0, 0, 5625);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(291, Current, 5625);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDI_QUESTION;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDI_CONSTANTS}.idi_exclamation */
EIF_TYPED_VALUE F292_5529 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idi_exclamation";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 291, Current, 0, 0, 5626);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(291, Current, 5626);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDI_EXCLAMATION;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDI_CONSTANTS}.idi_warning */
EIF_TYPED_VALUE F292_5530 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idi_warning";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 291, Current, 0, 0, 5627);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(291, Current, 5627);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDI_EXCLAMATION;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDI_CONSTANTS}.idi_asterisk */
EIF_TYPED_VALUE F292_5531 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idi_asterisk";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 291, Current, 0, 0, 5628);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(291, Current, 5628);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDI_ASTERISK;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

/* {WEL_IDI_CONSTANTS}.idi_information */
EIF_TYPED_VALUE F292_5532 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "idi_information";
	RTEX;
	EIF_POINTER Result = ((EIF_POINTER) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_POINTER, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 291, Current, 0, 0, 5629);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(291, Current, 5629);
	RTIV(Current, RTAL);
	Result = (EIF_POINTER) IDI_ASTERISK;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_POINTER; r.it_p = Result; return r; }
}

void EIF_Minit292 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
