/*
 * Code for class WEL_POLYGONAL_CAPABILITIES_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F34_271(EIF_REFERENCE);
extern EIF_TYPED_VALUE F34_272(EIF_REFERENCE);
extern EIF_TYPED_VALUE F34_273(EIF_REFERENCE);
extern EIF_TYPED_VALUE F34_274(EIF_REFERENCE);
extern EIF_TYPED_VALUE F34_275(EIF_REFERENCE);
extern EIF_TYPED_VALUE F34_276(EIF_REFERENCE);
extern EIF_TYPED_VALUE F34_277(EIF_REFERENCE);
extern EIF_TYPED_VALUE F34_278(EIF_REFERENCE);
extern EIF_TYPED_VALUE F34_279(EIF_REFERENCE);
extern void EIF_Minit34(void);

#ifdef __cplusplus
}
#endif

#include "wel.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_POLYGONAL_CAPABILITIES_CONSTANTS}.pc_none */
EIF_TYPED_VALUE F34_271 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pc_none";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 33, Current, 0, 0, 271);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(33, Current, 271);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PC_NONE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_POLYGONAL_CAPABILITIES_CONSTANTS}.pc_polygon */
EIF_TYPED_VALUE F34_272 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pc_polygon";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 33, Current, 0, 0, 272);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(33, Current, 272);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PC_POLYGON;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_POLYGONAL_CAPABILITIES_CONSTANTS}.pc_rectangle */
EIF_TYPED_VALUE F34_273 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pc_rectangle";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 33, Current, 0, 0, 273);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(33, Current, 273);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PC_RECTANGLE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_POLYGONAL_CAPABILITIES_CONSTANTS}.pc_windpolygon */
EIF_TYPED_VALUE F34_274 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pc_windpolygon";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 33, Current, 0, 0, 274);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(33, Current, 274);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PC_WINDPOLYGON;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_POLYGONAL_CAPABILITIES_CONSTANTS}.pc_scanline */
EIF_TYPED_VALUE F34_275 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pc_scanline";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 33, Current, 0, 0, 275);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(33, Current, 275);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PC_SCANLINE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_POLYGONAL_CAPABILITIES_CONSTANTS}.pc_wide */
EIF_TYPED_VALUE F34_276 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pc_wide";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 33, Current, 0, 0, 276);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(33, Current, 276);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PC_WIDE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_POLYGONAL_CAPABILITIES_CONSTANTS}.pc_styled */
EIF_TYPED_VALUE F34_277 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pc_styled";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 33, Current, 0, 0, 277);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(33, Current, 277);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PC_STYLED;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_POLYGONAL_CAPABILITIES_CONSTANTS}.pc_widestyled */
EIF_TYPED_VALUE F34_278 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pc_widestyled";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 33, Current, 0, 0, 278);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(33, Current, 278);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PC_WIDESTYLED;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_POLYGONAL_CAPABILITIES_CONSTANTS}.pc_interiors */
EIF_TYPED_VALUE F34_279 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "pc_interiors";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 33, Current, 0, 0, 270);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(33, Current, 270);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) PC_INTERIORS;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit34 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
