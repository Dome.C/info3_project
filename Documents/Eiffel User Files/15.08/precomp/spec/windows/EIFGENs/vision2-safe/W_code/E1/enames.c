

#ifdef __cplusplus
extern "C" {
#endif

char *names8 [] =
{
"implementation",
};

char *names11 [] =
{
"internal_item",
};

char *names19 [] =
{
"name",
"class_id",
"last_change",
};

char *names20 [] =
{
"guid",
};

char *names25 [] =
{
"scaled_pixmaps",
"orginal_pixmaps",
};

char *names31 [] =
{
"parameters",
};

char *names33 [] =
{
"message",
};

char *names40 [] =
{
"s1",
"s1_2",
"s1_3",
"s1_4",
"s2",
"s3",
"s4",
"s5",
"s6",
"s7",
"s8",
"s8_2",
"s8_3",
"s8_4",
};

char *names41 [] =
{
"pixmap",
"id",
};

char *names44 [] =
{
"internal_item",
};

char *names52 [] =
{
"internal_pixel_buffer",
"red_shift",
"green_shift",
"blue_shift",
"alpha_shift",
"current_column_value",
"current_row_value",
"internal_color_value",
};

char *names54 [] =
{
"scaled_fonts",
"orginal_fonts",
};

char *names59 [] =
{
"item",
};

char *names60 [] =
{
"printer_name",
"file_path",
"output_to_file",
"portrait",
"from_page",
"to_page",
"copies",
"selection_type",
"horizontal_resolution",
"vertical_resolution",
"printer_context",
};

char *names63 [] =
{
"item",
};

char *names66 [] =
{
"message_box_result",
};

char *names68 [] =
{
"is_striked_out",
"is_underlined",
"vertical_offset",
};

char *names73 [] =
{
"item",
"value_cache",
};

char *names78 [] =
{
"is_user_min_width_set",
"is_user_min_height_set",
"is_positioned",
"is_size_specified",
"x",
"y",
"width",
"height",
"minimum_width",
"minimum_height",
};

char *names82 [] =
{
"application",
"first_recorded_exception",
};

char *names84 [] =
{
"internal_item",
};

char *names90 [] =
{
"table",
"widget",
"top_attachment",
"left_attachment",
"bottom_attachment",
"right_attachment",
};

char *names91 [] =
{
"version",
};

char *names92 [] =
{
"old_version",
"new_version",
"mismatches_by_name",
"mismatches_by_stored_position",
"has_version_mismatch",
"has_new_attribute",
"has_new_attached_attribute",
"type_id",
"old_count",
"new_count",
};

char *names97 [] =
{
"command",
"argument",
};

char *names103 [] =
{
"guid",
};

char *names117 [] =
{
"internal_item",
};

char *names118 [] =
{
"flatten_when_closing",
"keep_calls_records",
"recording_values",
"maximum_record_count",
};

char *names122 [] =
{
"class_file",
"class_name",
};

char *names127 [] =
{
"default_output",
};

char *names133 [] =
{
"internal_item",
};

char *names134 [] =
{
"target",
"pixmap",
"internal_name",
};

char *names135 [] =
{
"menu_items",
};

char *names137 [] =
{
"internal_item",
};

char *names138 [] =
{
"last_query_success",
"last_free_space_in_bytes",
"last_total_space_in_bytes",
};

char *names139 [] =
{
"internal_item",
};

char *names145 [] =
{
"font",
"id",
};

char *names149 [] =
{
"has_text_pixmap_overlapping",
"pixmap_x",
"pixmap_y",
"checkbox_x",
"checkbox_y",
"text_x",
"text_y",
"available_text_width",
};

char *names168 [] =
{
"implementation",
};

char *names171 [] =
{
"implementation",
};

char *names175 [] =
{
"implementation",
};

char *names180 [] =
{
"implementation",
};

char *names181 [] =
{
"last_string",
"clipboard_open",
};

char *names183 [] =
{
"implementation",
};

char *names186 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"expand_actions_internal",
"collapse_actions_internal",
};

char *names187 [] =
{
"implementation",
};

char *names190 [] =
{
"implementation",
};

char *names200 [] =
{
"action",
};

char *names203 [] =
{
"implementation",
};

char *names204 [] =
{
"implementation",
};

char *names206 [] =
{
"docked_actions_internal",
};

char *names208 [] =
{
"implementation",
};

char *names210 [] =
{
"implementation",
};

char *names211 [] =
{
"implementation",
};

char *names212 [] =
{
"implementation",
};

char *names216 [] =
{
"sign_string",
"fill_character",
"separator",
"trailing_sign",
"bracketted_negative",
"width",
"justification",
"sign_format",
};

char *names217 [] =
{
"implementation",
};

char *names218 [] =
{
"implementation",
};

char *names223 [] =
{
"implementation",
};

char *names225 [] =
{
"implementation",
};

char *names229 [] =
{
"implementation",
};

char *names237 [] =
{
"implementation",
};

char *names240 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"level_count",
};

char *names257 [] =
{
"vertical_scroll_actions_internal",
"horizontal_scroll_actions_internal",
};

char *names258 [] =
{
"vertical_scroll_actions_internal",
"horizontal_scroll_actions_internal",
};

char *names259 [] =
{
"implementation",
};

char *names262 [] =
{
"implementation",
};

char *names269 [] =
{
"implementation",
};

char *names270 [] =
{
"implementation",
};

char *names274 [] =
{
"select_actions_internal",
"deselect_actions_internal",
};

char *names278 [] =
{
"implementation",
};

char *names279 [] =
{
"implementation",
};

char *names288 [] =
{
"last_index",
};

char *names293 [] =
{
"implementation",
};

char *names295 [] =
{
"implementation",
};

char *names297 [] =
{
"message_information",
};

char *names300 [] =
{
"retrieved_errors",
};

char *names304 [] =
{
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"select_actions_internal",
"deselect_actions_internal",
"drop_actions_internal",
"activate_actions_internal",
"deactivate_actions_internal",
};

char *names306 [] =
{
"implementation",
};

char *names307 [] =
{
"implementation",
};

char *names308 [] =
{
"is_pointer_value_stored",
};

char *names309 [] =
{
"buffer",
"medium",
"is_pointer_value_stored",
"is_for_reading",
"is_little_endian_storable",
"buffer_size",
"buffer_position",
"stored_pointer_bytes",
};

char *names310 [] =
{
"buffer",
"medium",
"is_pointer_value_stored",
"is_for_reading",
"is_little_endian_storable",
"buffer_size",
"count",
"stored_pointer_bytes",
};

char *names312 [] =
{
"implementation",
};

char *names313 [] =
{
"drawable_position",
"drawable_cell",
};

char *names314 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
};

char *names316 [] =
{
"alignment_code",
};

char *names317 [] =
{
"alignment_code",
"vertical_alignment_code",
};

char *names323 [] =
{
"implementation",
};

char *names331 [] =
{
"launch_mutex",
"terminated",
"thread_id",
};

char *names332 [] =
{
"launch_mutex",
"thread_procedure",
"terminated",
"thread_id",
};

char *names336 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"expand_actions_internal",
"collapse_actions_internal",
};

char *names337 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"expand_actions_internal",
"collapse_actions_internal",
};

char *names338 [] =
{
"drop_down_actions_internal",
"list_hidden_actions_internal",
};

char *names339 [] =
{
"drop_down_actions_internal",
"list_hidden_actions_internal",
};

char *names345 [] =
{
"internal_character_format_out",
"internal_paragraph_format_out",
"highlight_set",
"color_set",
"is_bold",
"is_italic",
"is_striked_out",
"is_underlined",
"highlight_color",
"text_color",
"vertical_offset",
"font_height",
"character_format",
"alignment",
"bottom_spacing",
"top_spacing",
"right_margin",
"left_margin",
};

char *names349 [] =
{
"root_object",
"on_processing_object_action",
"on_processing_reference_action",
"object_action",
"visited_objects",
"visited_types",
"has_failed",
"has_reference_with_copy_semantics",
"is_skip_transient",
"is_skip_copy_semantics_reference",
"is_exception_on_copy_suppressed",
"is_exception_propagated",
};

char *names350 [] =
{
"root_object",
"on_processing_object_action",
"on_processing_reference_action",
"object_action",
"visited_objects",
"visited_types",
"has_failed",
"has_reference_with_copy_semantics",
"is_skip_transient",
"is_skip_copy_semantics_reference",
"is_exception_on_copy_suppressed",
"is_exception_propagated",
};

char *names351 [] =
{
"root_object",
"on_processing_object_action",
"on_processing_reference_action",
"object_action",
"visited_objects",
"visited_types",
"has_failed",
"has_reference_with_copy_semantics",
"is_skip_transient",
"is_skip_copy_semantics_reference",
"is_exception_on_copy_suppressed",
"is_exception_propagated",
};

char *names354 [] =
{
"deltas",
"deltas_array",
};

char *names355 [] =
{
"deltas",
"deltas_array",
};

char *names356 [] =
{
"deltas",
"deltas_array",
};

char *names357 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"column_title_click_actions_internal",
"column_resized_actions_internal",
};

char *names358 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"column_title_click_actions_internal",
"column_resized_actions_internal",
};

char *names359 [] =
{
"expose_actions_internal",
};

char *names360 [] =
{
"expose_actions_internal",
};

char *names361 [] =
{
"select_actions_internal",
"deselect_actions_internal",
};

char *names362 [] =
{
"select_actions_internal",
"deselect_actions_internal",
};

char *names363 [] =
{
"check_actions_internal",
"uncheck_actions_internal",
};

char *names364 [] =
{
"check_actions_internal",
"uncheck_actions_internal",
};

char *names374 [] =
{
"expose_actions_internal",
};

char *names375 [] =
{
"expose_actions_internal",
};

char *names376 [] =
{
"selection_actions_internal",
};

char *names377 [] =
{
"selection_actions_internal",
};

char *names378 [] =
{
"caret_move_actions_internal",
"selection_change_actions_internal",
"file_access_actions_internal",
};

char *names379 [] =
{
"caret_move_actions_internal",
"selection_change_actions_internal",
"file_access_actions_internal",
};

char *names381 [] =
{
"change_actions_internal",
};

char *names382 [] =
{
"change_actions_internal",
};

char *names384 [] =
{
"deserializer",
"last_decoded_object",
"errors",
"reflector",
"reflected_object",
"object_references",
"dynamic_type_table",
"has_reference_with_copy_semantics",
"version",
};

char *names385 [] =
{
"deserializer",
"last_decoded_object",
"errors",
"reflector",
"reflected_object",
"object_references",
"dynamic_type_table",
"has_reference_with_copy_semantics",
"version",
};

char *names386 [] =
{
"deserializer",
"last_decoded_object",
"errors",
"reflector",
"reflected_object",
"object_references",
"dynamic_type_table",
"attributes_mapping",
"has_reference_with_copy_semantics",
"version",
};

char *names387 [] =
{
"serializer",
"reflector",
"reflected_object",
"traversable",
"object_indexes",
"is_root_object_set",
"has_reference_with_copy_semantics",
"version",
};

char *names388 [] =
{
"serializer",
"reflector",
"reflected_object",
"traversable",
"object_indexes",
"is_root_object_set",
"has_reference_with_copy_semantics",
"version",
};

char *names389 [] =
{
"serializer",
"reflector",
"reflected_object",
"traversable",
"object_indexes",
"is_root_object_set",
"has_reference_with_copy_semantics",
"version",
};

char *names390 [] =
{
"serializer",
"reflector",
"reflected_object",
"traversable",
"object_indexes",
"is_root_object_set",
"has_reference_with_copy_semantics",
"version",
};

char *names392 [] =
{
"select_actions_internal",
"drop_down_actions_internal",
};

char *names393 [] =
{
"select_actions_internal",
"drop_down_actions_internal",
};

char *names394 [] =
{
"change_actions_internal",
};

char *names395 [] =
{
"change_actions_internal",
};

char *names399 [] =
{
"sign_string",
"fill_character",
"separator",
"decimal",
"trailing_sign",
"bracketted_negative",
"after_decimal_separate",
"zero_not_shown",
"trailing_zeros_shown",
"width",
"justification",
"sign_format",
"decimals",
};

char *names404 [] =
{
"select_actions_internal",
};

char *names405 [] =
{
"select_actions_internal",
};

char *names407 [] =
{
"select_actions_internal",
"deselect_actions_internal",
};

char *names408 [] =
{
"select_actions_internal",
"deselect_actions_internal",
};

char *names410 [] =
{
"post_launch_actions_internal",
"kamikaze_actions",
"idle_actions_internal",
"pick_actions_internal",
"drop_actions_internal",
"cancel_actions_internal",
"pnd_motion_actions_internal",
"file_drop_actions_internal",
"uncaught_exception_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"mouse_wheel_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"theme_changed_actions_internal",
"system_color_change_actions_internal",
"destroy_actions_internal",
};

char *names411 [] =
{
"post_launch_actions_internal",
"kamikaze_actions",
"idle_actions_internal",
"pick_actions_internal",
"drop_actions_internal",
"cancel_actions_internal",
"pnd_motion_actions_internal",
"file_drop_actions_internal",
"uncaught_exception_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"mouse_wheel_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"theme_changed_actions_internal",
"system_color_change_actions_internal",
"destroy_actions_internal",
};

char *names413 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
};

char *names414 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
};

char *names415 [] =
{
"current_wel_cursor",
"cursor_pixmap",
"awaiting_movement",
"original_x",
"original_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names418 [] =
{
"return_actions_internal",
};

char *names419 [] =
{
"return_actions_internal",
};

char *names420 [] =
{
"item_select_actions_internal",
};

char *names421 [] =
{
"item_select_actions_internal",
};

char *names424 [] =
{
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
};

char *names425 [] =
{
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
};

char *names426 [] =
{
"widget",
"window",
"fixed",
"drawing_area",
"viewport",
"grid",
"offset",
"locked_index",
"internal_client_y",
"internal_client_x",
"viewport_y_offset",
"viewport_x_offset",
"last_horizontal_scroll_bar_value",
"last_vertical_scroll_bar_value",
};

char *names427 [] =
{
"widget",
"window",
"fixed",
"drawing_area",
"viewport",
"grid",
"row_i",
"offset",
"locked_index",
"internal_client_y",
"internal_client_x",
"viewport_y_offset",
"viewport_x_offset",
"last_horizontal_scroll_bar_value",
"last_vertical_scroll_bar_value",
};

char *names428 [] =
{
"widget",
"window",
"fixed",
"drawing_area",
"viewport",
"grid",
"column_i",
"offset",
"locked_index",
"internal_client_y",
"internal_client_x",
"viewport_y_offset",
"viewport_x_offset",
"last_horizontal_scroll_bar_value",
"last_vertical_scroll_bar_value",
};

char *names429 [] =
{
"new_item_actions_internal",
};

char *names430 [] =
{
"new_item_actions_internal",
};

char *names438 [] =
{
"item_resize_actions_internal",
"item_resize_start_actions_internal",
"item_resize_end_actions_internal",
"item_pointer_button_press_actions_internal",
"item_pointer_double_press_actions_internal",
};

char *names439 [] =
{
"item_resize_actions_internal",
"item_resize_start_actions_internal",
"item_resize_end_actions_internal",
"item_pointer_button_press_actions_internal",
"item_pointer_double_press_actions_internal",
};

char *names442 [] =
{
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"rubber_band_is_drawn",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
};

char *names443 [] =
{
"scale_width",
"scale_height",
"color_mode",
};

char *names444 [] =
{
"scale_width",
"scale_height",
"color_mode",
};

char *names445 [] =
{
"scale_width",
"scale_height",
"color_mode",
};

char *names446 [] =
{
"select_actions_internal",
"deselect_actions_internal",
};

char *names447 [] =
{
"select_actions_internal",
"deselect_actions_internal",
};

char *names448 [] =
{
"select_actions_internal",
};

char *names449 [] =
{
"select_actions_internal",
};

char *names450 [] =
{
"max_natural_type",
"max_integer_type",
};

char *names451 [] =
{
"integer_overflow_state1",
"integer_overflow_state2",
"natural_overflow_state1",
"natural_overflow_state2",
"max_natural_type",
"max_integer_type",
};

char *names452 [] =
{
"leading_separators",
"trailing_separators",
"trailing_separators_acceptable",
"leading_separators_acceptable",
"conversion_type",
"last_state",
"sign",
"max_natural_type",
"max_integer_type",
};

char *names453 [] =
{
"leading_separators",
"trailing_separators",
"trailing_separators_acceptable",
"leading_separators_acceptable",
"is_negative",
"has_negative_exponent",
"has_fractional_part",
"needs_digit",
"conversion_type",
"last_state",
"sign",
"exponent",
"max_natural_type",
"max_integer_type",
"natural_part",
"fractional_part",
"fractional_divider",
};

char *names454 [] =
{
"leading_separators",
"trailing_separators",
"trailing_separators_acceptable",
"leading_separators_acceptable",
"internal_overflowed",
"conversion_type",
"last_state",
"sign",
"max_natural_type",
"part1",
"part2",
"max_integer_type",
};

char *names455 [] =
{
"leading_separators",
"trailing_separators",
"internal_lookahead",
"trailing_separators_acceptable",
"leading_separators_acceptable",
"internal_overflowed",
"conversion_type",
"last_state",
"sign",
"max_natural_type",
"part1",
"part2",
"max_integer_type",
};

char *names457 [] =
{
"check_actions_internal",
"uncheck_actions_internal",
};

char *names458 [] =
{
"check_actions_internal",
"uncheck_actions_internal",
};

char *names459 [] =
{
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
};

char *names460 [] =
{
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
};

char *names461 [] =
{
"expose_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
};

char *names462 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
};

char *names463 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
};

char *names465 [] =
{
"maximize_actions_internal",
"minimize_actions_internal",
"restore_actions_internal",
};

char *names466 [] =
{
"maximize_actions_internal",
"minimize_actions_internal",
"restore_actions_internal",
};

char *names468 [] =
{
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
};

char *names469 [] =
{
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
};

char *names471 [] =
{
"id",
};

char *names490 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names491 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names492 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names493 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names494 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names495 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names496 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
"error_code",
};

char *names497 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
"signal_code",
};

char *names498 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"exception_information",
"internal_is_ignorable",
"line_number",
"hresult",
"hresult_code",
};

char *names499 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names500 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names501 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names502 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names503 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names504 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
"internal_code",
};

char *names505 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names506 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names507 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names508 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names509 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
"internal_code",
};

char *names510 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names511 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
"error_code",
"internal_code",
};

char *names512 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names513 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names514 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names515 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"routine_name",
"class_name",
"internal_is_ignorable",
"line_number",
};

char *names516 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names517 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names518 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names519 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names520 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names521 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names522 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names523 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names524 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names525 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names526 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names527 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names528 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"is_entry",
"line_number",
};

char *names529 [] =
{
"managed_pointer",
"shared",
"internal_item",
};

char *names530 [] =
{
"managed_pointer",
"shared",
"type",
"internal_item",
};

char *names531 [] =
{
"managed_pointer",
"shared",
"type",
"internal_item",
};

char *names533 [] =
{
"select_actions_internal",
"deselect_actions_internal",
};

char *names534 [] =
{
"select_actions_internal",
"deselect_actions_internal",
};

char *names542 [] =
{
"world",
};

char *names543 [] =
{
"internal_item",
"pixel_buffer",
"object_comparison",
"max_column_value",
"max_row_value",
"column_value",
"row_value",
};

char *names544 [] =
{
"object_comparison",
"index",
"seed",
"last_item",
"last_result",
};

char *names545 [] =
{
"object_comparison",
"index",
};

char *names546 [] =
{
"object_comparison",
"index",
};

char *names547 [] =
{
"world",
};

char *names557 [] =
{
"buffer",
"medium",
"is_pointer_value_stored",
"is_for_reading",
"is_little_endian_storable",
"is_last_chunk",
"buffer_size",
"buffer_position",
"stored_pointer_bytes",
};

char *names558 [] =
{
"buffer",
"medium",
"is_pointer_value_stored",
"is_for_reading",
"is_little_endian_storable",
"buffer_size",
"buffer_position",
"stored_pointer_bytes",
};

char *names559 [] =
{
"data",
"type",
};

char *names564 [] =
{
"dynamic_type",
};

char *names565 [] =
{
"referring_object",
"dynamic_type",
"physical_offset",
"referring_physical_offset",
};

char *names566 [] =
{
"enclosing_object",
"dynamic_type",
"physical_offset",
};

char *names579 [] =
{
"target",
"target_index",
"start_index",
"end_index",
};

char *names581 [] =
{
"object_comparison",
"lower_defined",
"upper_defined",
"upper_internal",
"lower_internal",
};

char *names582 [] =
{
"opo_change_actions",
"object_comparison",
"lower_defined",
"upper_defined",
"upper_internal",
"lower_internal",
};

char *names583 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names584 [] =
{
"area",
"originating_pixmap",
"object_comparison",
"lower",
"upper",
"height",
"width",
};

char *names585 [] =
{
"message_information",
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names590 [] =
{
"last_string",
"security_attributes",
"exists",
"input_closed",
"output_closed",
"last_write_successful",
"last_read_successful",
"last_written_bytes",
"last_read_bytes",
"output_handle",
"input_handle",
};

char *names591 [] =
{
"last_call_successful",
};

char *names592 [] =
{
"managed_data",
"count",
};

char *names594 [] =
{
"application_i",
};

char *names595 [] =
{
"tool_info",
"internal_tooltip_string",
};

char *names598 [] =
{
"reference_tracked",
"references_count",
"internal_object_id",
"internal_number_id",
};

char *names600 [] =
{
"allocated_objects",
"allocated_objects_number",
"found_object_index",
"cache_time",
"successful_cache",
"index_lightest_object",
};

char *names601 [] =
{
"allocated_objects",
"allocated_objects_number",
"found_object_index",
"cache_time",
"successful_cache",
"index_lightest_object",
};

char *names603 [] =
{
"cursor",
"internal_exhausted",
"starter",
};

char *names604 [] =
{
"index",
};

char *names605 [] =
{
"position",
};

char *names609 [] =
{
"deserializer",
"last_decoded_object",
"errors",
"reflector",
"reflected_object",
"object_references",
"dynamic_type_table",
"attributes_mapping",
"class_type_translator",
"attribute_name_translator",
"mismatches",
"mismatched_object",
"has_reference_with_copy_semantics",
"is_conforming_mismatch_allowed",
"is_attribute_removal_allowed",
"is_stopping_on_data_retrieval_error",
"is_checking_data_consistency",
"version",
};

char *names610 [] =
{
"found_item",
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_item",
"ht_deleted_key",
"object_comparison",
"hash_table_version_64",
"has_default",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"count",
};

char *names611 [] =
{
"found_item",
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_item",
"ht_deleted_key",
"stored_version",
"current_version",
"object_comparison",
"hash_table_version_64",
"has_default",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"count",
};

char *names612 [] =
{
"found_item",
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_item",
"object_comparison",
"hash_table_version_64",
"has_default",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"ht_deleted_key",
"count",
};

char *names613 [] =
{
"content",
"keys",
"indexes_map",
"deleted_marks",
"object_comparison",
"hash_table_version_64",
"has_default",
"last_index",
"found_item",
"ht_deleted_item",
"table_capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"capacity",
"ht_deleted_key",
};

char *names614 [] =
{
"area_v2",
"add_actions",
"remove_actions",
"internal_add_actions",
"internal_remove_actions",
"object_comparison",
"in_operation",
"index",
};

char *names617 [] =
{
"return_code",
};

char *names619 [] =
{
"managed_data",
"unit_count",
};

char *names620 [] =
{
"managed_data",
"count",
};

char *names621 [] =
{
"buffered_file_info",
"internal_file_name",
"internal_name_pointer",
"exists",
"is_following_symlinks",
};

char *names622 [] =
{
"buffered_file_info",
"internal_file_name",
"internal_name_pointer",
"exists",
"is_following_symlinks",
};

char *names624 [] =
{
"x_precise",
"y_precise",
};

char *names625 [] =
{
"x",
"y",
"width",
"height",
};

char *names631 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"veto_pebble_function",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names632 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names633 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names634 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names635 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names636 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names637 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names638 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names639 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names640 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names641 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names642 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names643 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names644 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names645 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names646 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names647 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names648 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names649 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names650 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names651 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names652 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names653 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names654 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names655 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names656 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names658 [] =
{
"grid",
"temp_rectangle",
};

char *names659 [] =
{
"implementation",
};

char *names660 [] =
{
"implementation",
};

char *names661 [] =
{
"item_activate_actions_internal",
"item_drop_actions_internal",
"item_deactivate_actions_internal",
"item_select_actions_internal",
"item_deselect_actions_internal",
"row_select_actions_internal",
"column_select_actions_internal",
"row_deselect_actions_internal",
"column_deselect_actions_internal",
"pointer_motion_item_actions_internal",
"pointer_double_press_item_actions_internal",
"pointer_leave_item_actions_internal",
"pointer_enter_item_actions_internal",
"pointer_button_press_item_actions_internal",
"pointer_button_release_item_actions_internal",
"row_expand_actions_internal",
"row_collapse_actions_internal",
"virtual_position_changed_actions_internal",
"virtual_size_changed_actions_internal",
"pre_draw_overlay_actions_internal",
"post_draw_overlay_actions_internal",
"fill_background_actions_internal",
};

char *names663 [] =
{
"item",
};

char *names664 [] =
{
"cond_pointer",
};

char *names665 [] =
{
"lastentry",
"internal_name",
"internal_detachable_name_pointer",
"mode",
"directory_pointer",
"last_entry_pointer",
};

char *names666 [] =
{
"sem_pointer",
};

char *names667 [] =
{
"item",
};

char *names668 [] =
{
"shlwapi_dll",
"item",
};

char *names669 [] =
{
"internal_area",
"is_resizable",
};

char *names670 [] =
{
"is_shared",
"count",
"item",
"counter",
};

char *names671 [] =
{
"owner",
"mutex_pointer",
};

char *names674 [] =
{
"text_metrics",
"log_fonts",
"internal_font_faces",
};

char *names675 [] =
{
"last_string",
"last_character",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"last_real",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names676 [] =
{
"last_string",
"last_character",
"is_closed",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"buffer_size",
"object_stored_size",
"last_real",
"internal_buffer_access",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names677 [] =
{
"last_string",
"internal_name",
"internal_detachable_name_pointer",
"last_character",
"separator",
"object_comparison",
"descriptor_available",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"mode",
"last_real",
"file_pointer",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names678 [] =
{
"last_string",
"internal_name",
"internal_detachable_name_pointer",
"internal_integer_buffer",
"last_character",
"separator",
"object_comparison",
"descriptor_available",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"mode",
"last_real",
"file_pointer",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names679 [] =
{
"last_string",
"internal_name",
"internal_detachable_name_pointer",
"last_character",
"separator",
"object_comparison",
"descriptor_available",
"is_sequence_an_expected_numeric",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"mode",
"last_real",
"file_pointer",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names680 [] =
{
"internal_id",
};

char *names681 [] =
{
"origin",
"positioner",
"notify_list_ids",
"being_positioned",
"position_changed",
"controlled_by_positioner",
"internal_id",
"x",
"y",
"last_x_abs",
"last_y_abs",
"angle",
"scale_x",
"scale_y",
"last_angle_abs",
"last_scale_x_abs",
"last_scale_y_abs",
};

char *names682 [] =
{
"target_name",
"target_data_function",
"internal_id",
};

char *names683 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"is_show_requested",
"valid",
"internal_is_sensitive",
"internal_id",
"draw_id",
};

char *names684 [] =
{
"area_v2",
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"object_comparison",
"is_show_requested",
"valid",
"internal_is_sensitive",
"index",
"internal_id",
"draw_id",
};

char *names685 [] =
{
"area_v2",
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"real_position_agent",
"start_actions",
"end_actions",
"move_actions",
"show_agent",
"hide_agent",
"object_comparison",
"is_show_requested",
"valid",
"internal_is_sensitive",
"is_always_shown",
"is_snapping",
"index",
"internal_id",
"draw_id",
"minimum_x",
"maximum_x",
"minimum_y",
"maximum_y",
"rel_x",
"rel_y",
};

char *names686 [] =
{
"area_v2",
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"background_color",
"capture_figure",
"object_comparison",
"is_show_requested",
"valid",
"internal_is_sensitive",
"points_visible",
"grid_enabled",
"grid_visible",
"is_redraw_needed",
"index",
"internal_id",
"draw_id",
"grid_x",
"grid_y",
};

char *names687 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"line_width",
};

char *names688 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"pixmap",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"is_default_pixmap_used",
"internal_id",
"draw_id",
"line_width",
};

char *names689 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"text",
"font",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"is_default_font_used",
"internal_id",
"draw_id",
"line_width",
"width",
"height",
};

char *names690 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"line_width",
"line_count",
};

char *names691 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"line_width",
};

char *names692 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"line_width",
"start_angle",
"aperture",
};

char *names693 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"line_width",
};

char *names694 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"line_width",
};

char *names695 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"line_width",
"radius",
};

char *names696 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"line_width",
};

char *names697 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"line_width",
};

char *names698 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"line_width",
"side_count",
};

char *names699 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"line_width",
"start_angle",
"aperture",
};

char *names700 [] =
{
"shared",
"item",
};

char *names701 [] =
{
"shared",
"item",
};

char *names702 [] =
{
"shared",
"item",
};

char *names703 [] =
{
"shared",
"accessible",
"item",
"handle",
};

char *names704 [] =
{
"last_string",
"internal_string",
"shared",
"accessible",
"item",
"handle",
};

char *names705 [] =
{
"shared",
"use_masked_bitmap",
"last_position",
"bitmaps_width",
"bitmaps_height",
"item",
};

char *names706 [] =
{
"filenames_index",
"bitmap_ids_index",
"image_id_to_bitmap_id_index",
"image_list_info",
"reference_tracked",
"shared",
"use_masked_bitmap",
"references_count",
"internal_object_id",
"internal_number_id",
"last_position",
"bitmaps_width",
"bitmaps_height",
"item",
};

char *names707 [] =
{
"shared",
"is_loaded_at_all_time",
"item",
};

char *names708 [] =
{
"shared",
"is_loaded_at_all_time",
"item",
};

char *names709 [] =
{
"shared",
"is_loaded_at_all_time",
"item",
};

char *names710 [] =
{
"shared",
"item",
};

char *names711 [] =
{
"shared",
"item",
};

char *names712 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names713 [] =
{
"reference_tracked",
"shared",
"is_made_by_dib",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"ppv_bits",
};

char *names714 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names715 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names716 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names717 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names718 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names719 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names720 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names721 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names722 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names723 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names724 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names725 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names726 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names727 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names728 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names729 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names730 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names731 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names732 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names733 [] =
{
"shared",
"item",
"gdi_plus_handle",
};

char *names734 [] =
{
"shared",
"item",
"gdi_plus_handle",
};

char *names735 [] =
{
"shared",
"item",
"gdi_plus_handle",
};

char *names736 [] =
{
"shared",
"item",
"gdi_plus_handle",
};

char *names737 [] =
{
"shared",
"item",
"gdi_plus_handle",
};

char *names738 [] =
{
"shared",
"item",
"gdi_plus_handle",
};

char *names739 [] =
{
"shared",
"item",
"gdi_plus_handle",
};

char *names740 [] =
{
"shared",
"item",
"gdi_plus_handle",
};

char *names741 [] =
{
"shared",
"item",
"gdi_plus_handle",
};

char *names742 [] =
{
"shared",
"item",
"gdi_plus_handle",
};

char *names743 [] =
{
"shared",
"item",
"gdi_plus_handle",
};

char *names744 [] =
{
"shared",
"item",
"gdi_plus_handle",
};

char *names745 [] =
{
"raw_format_recorded",
"shared",
"item",
"gdi_plus_handle",
};

char *names746 [] =
{
"shared",
"item",
};

char *names747 [] =
{
"shared",
"item",
};

char *names748 [] =
{
"shared",
"item",
};

char *names749 [] =
{
"str_text",
"shared",
"item",
};

char *names750 [] =
{
"shared",
"item",
};

char *names751 [] =
{
"shared",
"count",
"item",
};

char *names752 [] =
{
"shared",
"item",
};

char *names753 [] =
{
"shared",
"item",
};

char *names754 [] =
{
"shared",
"allocated_not_reliable",
"item",
};

char *names755 [] =
{
"user_tree_view_item",
"shared",
"item",
};

char *names756 [] =
{
"shared",
"item",
};

char *names757 [] =
{
"shared",
"item",
};

char *names758 [] =
{
"shared",
"item",
};

char *names759 [] =
{
"shared",
"item",
};

char *names760 [] =
{
"shared",
"item",
};

char *names761 [] =
{
"shared",
"item",
};

char *names762 [] =
{
"shared",
"item",
};

char *names763 [] =
{
"shared",
"item",
};

char *names764 [] =
{
"shared",
"item",
};

char *names765 [] =
{
"internal_mask_bitmap",
"internal_color_bitmap",
"shared",
"is_initialized",
"internal_mask_bitmap_object_id",
"internal_color_bitmap_object_id",
"item",
};

char *names766 [] =
{
"shared",
"item",
};

char *names767 [] =
{
"shared",
"item",
};

char *names768 [] =
{
"shared",
"item",
};

char *names769 [] =
{
"shared",
"item",
};

char *names770 [] =
{
"shared",
"item",
};

char *names771 [] =
{
"shared",
"item",
};

char *names772 [] =
{
"shared",
"item",
};

char *names773 [] =
{
"range",
"text",
"shared",
"item",
};

char *names774 [] =
{
"shared",
"item",
};

char *names775 [] =
{
"shared",
"item",
};

char *names776 [] =
{
"shared",
"item",
};

char *names777 [] =
{
"shared",
"item",
};

char *names778 [] =
{
"internal_bitmap",
"shared",
"predefined_id",
"internal_bitmap_object_id",
"item",
"internal_bitmap_id",
};

char *names779 [] =
{
"shared",
"item",
};

char *names780 [] =
{
"shared",
"rgb_quad_count",
"item",
};

char *names781 [] =
{
"point",
"rect",
"shared",
"item",
};

char *names782 [] =
{
"shared",
"item",
};

char *names783 [] =
{
"shared",
"item",
};

char *names784 [] =
{
"shared",
"item",
};

char *names785 [] =
{
"shared",
"item",
};

char *names786 [] =
{
"shared",
"item",
};

char *names787 [] =
{
"shared",
"item",
};

char *names788 [] =
{
"shared",
"item",
};

char *names789 [] =
{
"string_to_find",
"shared",
"item",
};

char *names790 [] =
{
"shared",
"item",
};

char *names791 [] =
{
"shared",
"item",
};

char *names792 [] =
{
"internal_local_name",
"internal_remote_name",
"internal_comment",
"internal_provider",
"shared",
"item",
};

char *names793 [] =
{
"str_class_name",
"str_menu_name",
"shared",
"atom",
"item",
};

char *names794 [] =
{
"shared",
"item",
};

char *names795 [] =
{
"shared",
"memory_allocated_with_global_alloc",
"item",
};

char *names796 [] =
{
"shared",
"item",
};

char *names797 [] =
{
"shared",
"item",
};

char *names798 [] =
{
"shared",
"item",
};

char *names799 [] =
{
"shared",
"item",
};

char *names800 [] =
{
"shared",
"item",
};

char *names801 [] =
{
"shared",
"item",
};

char *names802 [] =
{
"shared",
"count",
"item",
};

char *names803 [] =
{
"str_class_name",
"str_title",
"shared",
"item",
};

char *names804 [] =
{
"shared",
"item",
};

char *names805 [] =
{
"shared",
"item",
};

char *names806 [] =
{
"shared",
"item",
};

char *names807 [] =
{
"shared",
"item",
};

char *names808 [] =
{
"shared",
"item",
};

char *names809 [] =
{
"str_text",
"shared",
"item",
};

char *names810 [] =
{
"shared",
"item",
};

char *names811 [] =
{
"shared",
"item",
};

char *names812 [] =
{
"shared",
"item",
};

char *names813 [] =
{
"shared",
"item",
};

char *names814 [] =
{
"shared",
"item",
};

char *names815 [] =
{
"shared",
"item",
};

char *names816 [] =
{
"shared",
"item",
};

char *names817 [] =
{
"shared",
"item",
};

char *names818 [] =
{
"shared",
"private_num_entries",
"item",
};

char *names819 [] =
{
"shared",
"item",
};

char *names820 [] =
{
"shared",
"item",
};

char *names821 [] =
{
"shared",
"item",
};

char *names822 [] =
{
"shared",
"item",
};

char *names823 [] =
{
"str_document_name",
"str_output",
"shared",
"item",
};

char *names824 [] =
{
"shared",
"item",
};

char *names825 [] =
{
"str_text",
"shared",
"item",
};

char *names826 [] =
{
"shared",
"item",
};

char *names827 [] =
{
"window",
"icon",
"shared",
"item",
};

char *names828 [] =
{
"shared",
"last_boolean_result",
"dispatch_result",
"item",
};

char *names829 [] =
{
"palette",
"info_header",
"shared",
"structure_size",
"item",
};

char *names830 [] =
{
"palette",
"info_header",
"shared",
"structure_size",
"item",
};

char *names831 [] =
{
"shared",
"is_unicode_data",
"stream_result",
"item",
};

char *names832 [] =
{
"buffer",
"shared",
"is_unicode_data",
"stream_result",
"item",
};

char *names833 [] =
{
"buffer",
"file",
"shared",
"is_unicode_data",
"stream_result",
"item",
};

char *names834 [] =
{
"buffer",
"string",
"shared",
"is_unicode_data",
"stream_result",
"last_position",
"item",
};

char *names835 [] =
{
"buffer",
"shared",
"is_unicode_data",
"stream_result",
"item",
};

char *names836 [] =
{
"buffer",
"file",
"shared",
"is_unicode_data",
"stream_result",
"item",
};

char *names837 [] =
{
"buffer",
"data",
"shared",
"is_unicode_data",
"stream_result",
"item",
};

char *names838 [] =
{
"shared",
"selected",
"item",
};

char *names840 [] =
{
"storage",
"internal_name",
"is_normalized",
};

char *names842 [] =
{
"item",
};

char *names843 [] =
{
"item",
};

char *names844 [] =
{
"item",
};

char *names845 [] =
{
"item",
};

char *names846 [] =
{
"item",
};

char *names847 [] =
{
"item",
};

char *names848 [] =
{
"item",
};

char *names849 [] =
{
"item",
};

char *names850 [] =
{
"item",
};

char *names851 [] =
{
"item",
};

char *names852 [] =
{
"item",
};

char *names853 [] =
{
"item",
};

char *names854 [] =
{
"item",
};

char *names855 [] =
{
"item",
};

char *names856 [] =
{
"item",
};

char *names857 [] =
{
"item",
};

char *names858 [] =
{
"item",
};

char *names859 [] =
{
"item",
};

char *names860 [] =
{
"item",
};

char *names861 [] =
{
"item",
};

char *names862 [] =
{
"item",
};

char *names863 [] =
{
"item",
};

char *names864 [] =
{
"item",
};

char *names865 [] =
{
"item",
};

char *names866 [] =
{
"item",
};

char *names867 [] =
{
"item",
};

char *names868 [] =
{
"item",
};

char *names869 [] =
{
"item",
};

char *names870 [] =
{
"item",
};

char *names871 [] =
{
"item",
};

char *names872 [] =
{
"item",
};

char *names873 [] =
{
"item",
};

char *names874 [] =
{
"item",
};

char *names875 [] =
{
"item",
};

char *names876 [] =
{
"item",
};

char *names877 [] =
{
"item",
};

char *names878 [] =
{
"item",
};

char *names879 [] =
{
"item",
};

char *names880 [] =
{
"item",
};

char *names881 [] =
{
"item",
};

char *names882 [] =
{
"item",
};

char *names883 [] =
{
"item",
};

char *names884 [] =
{
"item",
"computed_hash_code",
"weight",
"age",
};

char *names885 [] =
{
"item",
"computed_hash_code",
"weight",
"age",
"style",
"line_width",
"color_red",
"color_blue",
"color_green",
};

char *names886 [] =
{
"item",
"pattern",
"computed_hash_code",
"weight",
"age",
"color_red",
"color_blue",
"color_green",
};

char *names887 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"internal_id",
"draw_id",
"internal_hash_id",
};

char *names888 [] =
{
"area_v2",
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"lookup_table",
"object_comparison",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"is_grouped",
"index",
"internal_id",
"draw_id",
"internal_hash_id",
"current_angle",
};

char *names889 [] =
{
"area_v2",
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"lookup_table",
"background_color",
"capture_figure",
"object_comparison",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"is_grouped",
"points_visible",
"grid_enabled",
"grid_visible",
"is_redraw_needed",
"index",
"internal_id",
"draw_id",
"internal_hash_id",
"grid_x",
"grid_y",
"current_angle",
};

char *names890 [] =
{
"area_v2",
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"lookup_table",
"real_position_agent",
"start_actions",
"end_actions",
"move_actions",
"rotate_actions",
"scale_x_actions",
"scale_y_actions",
"show_agent",
"hide_agent",
"object_comparison",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"is_grouped",
"is_always_shown",
"is_snapping",
"is_moving",
"is_rotating",
"is_scaling",
"is_scale",
"is_move",
"is_rotate",
"index",
"internal_id",
"draw_id",
"internal_hash_id",
"minimum_x",
"maximum_x",
"minimum_y",
"maximum_y",
"move_button",
"scale_button",
"rotate_button",
"center_x",
"center_y",
"delta_center_x",
"delta_center_y",
"current_angle",
};

char *names891 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"internal_id",
"draw_id",
"internal_hash_id",
};

char *names892 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
};

char *names893 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
"line_count",
};

char *names894 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"scaled_pixmap",
"id_pixmap",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"is_default_pixmap_used",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
};

char *names895 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"text",
"scaled_font",
"id_font",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"is_default_font_used",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
"left_offset",
};

char *names896 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
};

char *names897 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
};

char *names898 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
"start_angle",
"aperture",
};

char *names899 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
};

char *names900 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
"start_angle",
"aperture",
};

char *names901 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
};

char *names902 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
};

char *names903 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
"start_angle",
"aperture",
};

char *names904 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
"start_angle",
"aperture",
};

char *names905 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
};

char *names906 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
};

char *names907 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
"side_count",
};

char *names908 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
};

char *names909 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
"radius",
};

char *names910 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
};

char *names911 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"background_color",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
"radius",
};

char *names912 [] =
{
"internal_hash_code",
"internal_case_insensitive_hash_code",
};

char *names913 [] =
{
"area",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names914 [] =
{
"internal_hash_code",
"internal_case_insensitive_hash_code",
};

char *names915 [] =
{
"area",
"object_comparison",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names916 [] =
{
"area",
"object_comparison",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names917 [] =
{
"area",
"object_comparison",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
"index",
};

char *names918 [] =
{
"area",
"object_comparison",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names919 [] =
{
"area",
"object_comparison",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names920 [] =
{
"area",
"object_comparison",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names921 [] =
{
"internal_hash_code",
"internal_case_insensitive_hash_code",
};

char *names922 [] =
{
"area",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
"area_lower",
};

char *names923 [] =
{
"area",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names924 [] =
{
"area",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
"area_lower",
};

char *names925 [] =
{
"area",
"object_comparison",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names928 [] =
{
"shared",
"item",
};

char *names929 [] =
{
"shared",
"item",
};

char *names930 [] =
{
"str_text",
"shared",
"item",
};

char *names931 [] =
{
"internal_title",
"shared",
"item",
};

char *names932 [] =
{
"message_box_result",
"language",
"sublanguage",
};

char *names933 [] =
{
"str_target",
"shared",
"item",
};

char *names934 [] =
{
"shared",
"item",
};

char *names935 [] =
{
"custom_colors",
"shared",
"selected",
"item",
};

char *names936 [] =
{
"str_text",
"shared",
"item",
};

char *names937 [] =
{
"str_folder_name",
"str_starting_folder",
"str_title",
"shared",
"selected",
"item",
};

char *names938 [] =
{
"shared",
"item",
};

char *names939 [] =
{
"shared",
"item",
};

char *names940 [] =
{
"shared",
"item",
};

char *names941 [] =
{
"log_font",
"shared",
"selected",
"item",
};

char *names942 [] =
{
"private_dc",
"shared",
"selected",
"item",
};

char *names943 [] =
{
"internal_text",
"shared",
"item",
};

char *names944 [] =
{
"str_file_name",
"str_file_title",
"str_filter",
"str_title",
"str_initial_path",
"str_default_extension",
"shared",
"selected",
"item",
};

char *names945 [] =
{
"str_file_name",
"str_file_title",
"str_filter",
"str_title",
"str_initial_path",
"str_default_extension",
"shared",
"selected",
"item",
};

char *names946 [] =
{
"str_file_name",
"str_file_title",
"str_filter",
"str_title",
"str_initial_path",
"str_default_extension",
"shared",
"selected",
"item",
};

char *names947 [] =
{
"pen",
"brush",
"palette",
"region",
"font",
"bitmap",
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"old_hpen",
"old_hbrush",
"old_hregion",
"old_hpalette",
"old_hfont",
"old_hbitmap",
};

char *names948 [] =
{
"pen",
"brush",
"palette",
"region",
"font",
"bitmap",
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"old_hpen",
"old_hbrush",
"old_hregion",
"old_hpalette",
"old_hfont",
"old_hbitmap",
};

char *names949 [] =
{
"pen",
"brush",
"palette",
"region",
"font",
"bitmap",
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"old_hpen",
"old_hbrush",
"old_hregion",
"old_hpalette",
"old_hfont",
"old_hbitmap",
};

char *names950 [] =
{
"pen",
"brush",
"palette",
"region",
"font",
"bitmap",
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"old_hpen",
"old_hbrush",
"old_hregion",
"old_hpalette",
"old_hfont",
"old_hbitmap",
};

char *names951 [] =
{
"pen",
"brush",
"palette",
"region",
"font",
"bitmap",
"device",
"driver",
"output",
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"old_hpen",
"old_hbrush",
"old_hregion",
"old_hpalette",
"old_hfont",
"old_hbitmap",
};

char *names952 [] =
{
"pen",
"brush",
"palette",
"region",
"font",
"bitmap",
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"old_hpen",
"old_hbrush",
"old_hregion",
"old_hpalette",
"old_hfont",
"old_hbitmap",
};

char *names953 [] =
{
"pen",
"brush",
"palette",
"region",
"font",
"bitmap",
"window",
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"old_hpen",
"old_hbrush",
"old_hregion",
"old_hpalette",
"old_hfont",
"old_hbitmap",
"hwindow",
};

char *names954 [] =
{
"pen",
"brush",
"palette",
"region",
"font",
"bitmap",
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"old_hpen",
"old_hbrush",
"old_hregion",
"old_hpalette",
"old_hfont",
"old_hbitmap",
};

char *names955 [] =
{
"pen",
"brush",
"palette",
"region",
"font",
"bitmap",
"window",
"paint_struct",
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"old_hpen",
"old_hbrush",
"old_hregion",
"old_hpalette",
"old_hfont",
"old_hbitmap",
"hwindow",
};

char *names956 [] =
{
"pen",
"brush",
"palette",
"region",
"font",
"bitmap",
"window",
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"old_hpen",
"old_hbrush",
"old_hregion",
"old_hpalette",
"old_hfont",
"old_hbitmap",
"hwindow",
};

char *names957 [] =
{
"pen",
"brush",
"palette",
"region",
"font",
"bitmap",
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"old_hpen",
"old_hbrush",
"old_hregion",
"old_hpalette",
"old_hfont",
"old_hbitmap",
"hwindow",
};

char *names958 [] =
{
"data",
"implementation",
};

char *names959 [] =
{
"data",
"implementation",
};

char *names960 [] =
{
"data",
"implementation",
"actions",
};

char *names961 [] =
{
"data",
"implementation",
"internal_out",
"changed",
};

char *names962 [] =
{
"data",
"implementation",
};

char *names963 [] =
{
"data",
"implementation",
};

char *names964 [] =
{
"data",
"implementation",
};

char *names965 [] =
{
"data",
"implementation",
};

char *names966 [] =
{
"data",
"implementation",
};

char *names967 [] =
{
"world",
"data",
"implementation",
"context",
};

char *names968 [] =
{
"world",
"data",
"implementation",
"context",
};

char *names969 [] =
{
"data",
"implementation",
};

char *names970 [] =
{
"data",
"implementation",
"actual_implementation",
};

char *names971 [] =
{
"data",
"implementation",
};

char *names972 [] =
{
"data",
"implementation",
"object_comparison",
};

char *names973 [] =
{
"data",
"implementation",
};

char *names974 [] =
{
"data",
"implementation",
};

char *names975 [] =
{
"data",
"implementation",
};

char *names976 [] =
{
"data",
"implementation",
"application_handler",
"is_launched",
};

char *names977 [] =
{
"data",
"implementation",
"application_handler",
"is_launched",
};

char *names978 [] =
{
"data",
"implementation",
"internal_name",
};

char *names979 [] =
{
"data",
"implementation",
};

char *names980 [] =
{
"data",
"implementation",
};

char *names981 [] =
{
"data",
"implementation",
"internal_id",
};

char *names982 [] =
{
"data",
"implementation",
};

char *names983 [] =
{
"data",
"implementation",
};

char *names984 [] =
{
"data",
"implementation",
};

char *names985 [] =
{
"data",
"implementation",
"internal_pixmap_path",
};

char *names986 [] =
{
"data",
"implementation",
};

char *names987 [] =
{
"data",
"implementation",
};

char *names988 [] =
{
"data",
"implementation",
};

char *names989 [] =
{
"data",
"implementation",
};

char *names990 [] =
{
"implementation",
"data",
};

char *names991 [] =
{
"data",
"implementation",
};

char *names992 [] =
{
"data",
"implementation",
};

char *names993 [] =
{
"data",
"implementation",
};

char *names994 [] =
{
"data",
"implementation",
};

char *names995 [] =
{
"data",
"implementation",
};

char *names996 [] =
{
"data",
"implementation",
};

char *names997 [] =
{
"data",
"implementation",
};

char *names998 [] =
{
"data",
"implementation",
};

char *names999 [] =
{
"data",
"implementation",
};

char *names1000 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_id",
};

char *names1001 [] =
{
"data",
"implementation",
"internal_name",
};

char *names1002 [] =
{
"data",
"implementation",
"internal_name",
};

char *names1003 [] =
{
"data",
"implementation",
"internal_name",
};

char *names1004 [] =
{
"data",
"implementation",
"internal_name",
"text",
"font",
"pixmap",
"layout_procedure",
"boolean_flags",
"right_border",
"top_border",
"bottom_border",
"spacing",
"internal_left_border",
};

char *names1005 [] =
{
"data",
"implementation",
"internal_name",
"text",
"font",
"pixmap",
"layout_procedure",
"boolean_flags",
"right_border",
"top_border",
"bottom_border",
"spacing",
"internal_left_border",
};

char *names1006 [] =
{
"data",
"implementation",
"internal_name",
"text",
"font",
"pixmap",
"layout_procedure",
"item_strings",
"choice_list",
"initial_position",
"has_user_selected_item",
"boolean_flags",
"right_border",
"top_border",
"bottom_border",
"spacing",
"internal_left_border",
};

char *names1007 [] =
{
"data",
"implementation",
"internal_name",
"text",
"font",
"pixmap",
"layout_procedure",
"is_toggle_on_label_allowed",
"is_key_handled",
"boolean_flags",
"right_border",
"top_border",
"bottom_border",
"spacing",
"internal_left_border",
};

char *names1008 [] =
{
"data",
"implementation",
"internal_name",
"text",
"font",
"pixmap",
"layout_procedure",
"combo_box",
"item_strings",
"user_cancelled_activation",
"boolean_flags",
"right_border",
"top_border",
"bottom_border",
"spacing",
"internal_left_border",
};

char *names1009 [] =
{
"data",
"implementation",
"internal_name",
"text",
"font",
"pixmap",
"layout_procedure",
"validation_agent",
"text_field",
"user_cancelled_activation",
"boolean_flags",
"right_border",
"top_border",
"bottom_border",
"spacing",
"internal_left_border",
};

char *names1010 [] =
{
"data",
"implementation",
"internal_name",
};

char *names1011 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1012 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1013 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1014 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
"proportion",
};

char *names1015 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
"proportion",
};

char *names1016 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
"proportion",
};

char *names1017 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
"proportion",
};

char *names1018 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"linear_representation",
"external_representation",
"all_split_areas",
"all_holders",
"maximize_pixmap",
"minimize_pixmap",
"close_pixmap",
"restore_pixmap",
"stored_splitter_widths",
"minimized_states",
"docked_out_actions_internal",
"docked_in_actions_internal",
"close_actions_internal",
"restore_actions_internal",
"maximize_actions_internal",
"minimize_actions_internal",
"pre_insertion_heights",
"pre_insertion_holders",
"maximized_tool",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"disabled_minimize_button_shown",
"disabled_close_button_shown",
"top_widget_resizing",
"is_blocked",
"rebuilding_locked",
"internal_id",
"holder_tool_height",
"proportion",
};

char *names1019 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1020 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1021 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1022 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1023 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1024 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"field",
"default_start_path",
"browse_button",
"internal_parent_window",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1025 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"list",
"text_field",
"add_actions",
"remove_actions",
"modify_actions",
"is_entry_valid",
"display_error_message",
"add_button",
"apply_button",
"remove_button",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1026 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"list",
"text_field",
"add_actions",
"remove_actions",
"modify_actions",
"is_entry_valid",
"display_error_message",
"add_button",
"apply_button",
"remove_button",
"parent_window",
"path_field",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1027 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1028 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"main_box",
"command_tool_bar",
"tool",
"parent_area",
"customizeable_area",
"label_box",
"maximize_button",
"minimize_button",
"close_button",
"display_name",
"label",
"tool_bar",
"minimum_size_cell",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"is_minimized",
"is_maximized",
"internal_id",
"position_docked_from",
"restore_height",
"original_height",
"original_width",
};

char *names1029 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1030 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"include_list",
"exclude_list",
"include_label",
"exclude_label",
"include_button",
"exclude_button",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1031 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1032 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1033 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"world",
"projector",
"drawing_area",
"vertical_scrollbar",
"horizontal_scrollbar",
"autoscroll",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"is_autoscroll_enabled",
"is_resize_enabled",
"is_scrollbar_enabled",
"is_scroll",
"is_hand",
"internal_id",
"autoscroll_border",
"world_border",
"start_x",
"start_y",
"start_horizontal_value",
"start_vertical_value",
"scroll_speed",
};

char *names1034 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1035 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1036 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1037 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"cell",
"previous_size",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
"scroll_step",
"scroll_percentage",
};

char *names1038 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1039 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"has_shadow",
"internal_id",
};

char *names1040 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"has_shadow",
"internal_id",
};

char *names1041 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1042 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1043 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1044 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"original_parent",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"expansion_was_disabled",
"internal_id",
"original_parent_index",
};

char *names1045 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"original_parent",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"expansion_was_disabled",
"internal_id",
"original_parent_index",
};

char *names1046 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"selected_button",
"button_box",
"scrollable_area",
"label",
"pixmap_box",
"buttons",
"background_color",
"foreground_color",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
"maximum_label_width",
"maximum_label_height",
};

char *names1047 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"selected_button",
"button_box",
"scrollable_area",
"label",
"pixmap_box",
"buttons",
"background_color",
"foreground_color",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
"maximum_label_width",
"maximum_label_height",
};

char *names1048 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"selected_button",
"button_box",
"scrollable_area",
"label",
"pixmap_box",
"buttons",
"background_color",
"foreground_color",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
"maximum_label_width",
"maximum_label_height",
};

char *names1049 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"selected_button",
"button_box",
"scrollable_area",
"label",
"pixmap_box",
"buttons",
"background_color",
"foreground_color",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
"maximum_label_width",
"maximum_label_height",
};

char *names1050 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"selected_button",
"button_box",
"scrollable_area",
"label",
"pixmap_box",
"buttons",
"background_color",
"foreground_color",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
"maximum_label_width",
"maximum_label_height",
};

char *names1051 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"selected_button",
"button_box",
"scrollable_area",
"label",
"pixmap_box",
"buttons",
"background_color",
"foreground_color",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
"maximum_label_width",
"maximum_label_height",
};

char *names1052 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1053 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1054 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"pixmap_path",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"pixmap_exists",
"internal_id",
};

char *names1055 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"pixmap_path",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"pixmap_exists",
"internal_id",
"x_hotspot",
"y_hotspot",
};

char *names1056 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1057 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"start_drag_actions",
"end_drag_actions",
"drag_actions",
"all_polygons",
"rich_text",
"figure_world",
"projector",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
"last_dashed_line_position",
"original_x",
"initial_tab_width",
};

char *names1058 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1059 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1060 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1061 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1062 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1063 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1064 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1065 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1066 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1067 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1068 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"menu",
"selected_item",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1069 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1070 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1071 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1072 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1073 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1074 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1075 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1076 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1077 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1078 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1079 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1080 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1081 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1082 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"internal_id",
};

char *names1083 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"internal_id",
};

char *names1084 [] =
{
"area_v2",
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"in_operation",
"index",
"internal_id",
};

char *names1085 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"internal_id",
};

char *names1086 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"column",
"internal_id",
};

char *names1087 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"internal_id",
};

char *names1088 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"internal_id",
};

char *names1089 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"internal_id",
};

char *names1090 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"internal_id",
};

char *names1091 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"internal_id",
};

char *names1092 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"internal_id",
};

char *names1093 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"internal_id",
};

char *names1094 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"internal_id",
};

char *names1095 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"internal_id",
};

char *names1096 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"internal_id",
};

char *names1097 [] =
{
"data",
"implementation",
};

char *names1098 [] =
{
"data",
"implementation",
};

char *names1099 [] =
{
"data",
"implementation",
};

char *names1100 [] =
{
"data",
"implementation",
};

char *names1101 [] =
{
"data",
"implementation",
};

char *names1102 [] =
{
"data",
"implementation",
};

char *names1103 [] =
{
"data",
"implementation",
};

char *names1104 [] =
{
"data",
"implementation",
};

char *names1105 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1106 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1107 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"end_edit_actions",
"hide_timer",
"widget",
"saved_text",
"relative_window",
"editable_columns",
"editable_rows",
"change_widgets",
"error_dialog",
"internal_dialog",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"all_columns_editable",
"all_rows_editable",
"unique_column_values",
"empty_column_values",
"internal_id",
"x_offset",
"y_offset",
"widget_column",
"widget_row",
};

char *names1108 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1109 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"grid",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1110 [] =
{
"data",
"implementation",
"internal_name",
"object_comparison",
};

char *names1111 [] =
{
"data",
"implementation",
"internal_name",
"object_comparison",
};

char *names1112 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"internal_id",
};

char *names1113 [] =
{
"data",
"implementation",
"internal_name",
"object_comparison",
};

char *names1114 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1115 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1116 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"internal_id",
};

char *names1117 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"object_comparison",
"internal_id",
};

char *names1118 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"internal_pixmap_path",
"subtree_function",
"object_comparison",
"internal_id",
"subtree_function_timeout",
"last_subtree_function_call_time",
};

char *names1119 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1120 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1121 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1122 [] =
{
"target_name",
"target_data_function",
"data",
"implementation",
"internal_name",
"object_comparison",
"minimum_width_set_by_user",
"minimum_height_set_by_user",
"internal_id",
};

char *names1123 [] =
{
"data",
"implementation",
"internal_pixmap_path",
};

char *names1124 [] =
{
"interface",
"state_flags",
};

char *names1125 [] =
{
"interface",
"state_flags",
};

char *names1126 [] =
{
"interface",
"beep_routines",
"state_flags",
};

char *names1127 [] =
{
"interface",
"state_flags",
};

char *names1128 [] =
{
"interface",
"wel_cursor",
"wel_bitmap",
"wel_mask_bitmap",
"state_flags",
};

char *names1129 [] =
{
"interface",
"internal_help_context",
"state_flags",
};

char *names1130 [] =
{
"interface",
"actions_internal",
"parented",
"state_flags",
};

char *names1131 [] =
{
"interface",
"actions_internal",
"key",
"parented",
"control_required",
"alt_required",
"shift_required",
"state_flags",
};

char *names1132 [] =
{
"post_launch_actions_internal",
"kamikaze_actions",
"idle_actions_internal",
"pick_actions_internal",
"drop_actions_internal",
"cancel_actions_internal",
"pnd_motion_actions_internal",
"file_drop_actions_internal",
"uncaught_exception_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"mouse_wheel_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"theme_changed_actions_internal",
"system_color_change_actions_internal",
"destroy_actions_internal",
"interface",
"idle_actions_snapshot",
"kamikaze_idle_actions_snapshot",
"dockable_targets",
"pnd_targets",
"locked_window",
"captured_widget",
"help_accelerator",
"contextual_help_accelerator",
"help_engine",
"idle_action_mutex",
"kamikaze_action_mutex",
"exception_dialog",
"clipboard_internal",
"old_pointer_style",
"old_pointer_button_press_actions",
"help_handler_procedure",
"contextual_help_handler_procedure",
"idle_actions_executing",
"events_processed_from_underlying_toolkit",
"user_events_processed_from_underlying_toolkit",
"invoke_garbage_collection_when_inactive",
"rubber_band_is_drawn",
"uncaught_exception_actions_called",
"stop_processing_requested",
"tab_navigation_state",
"state_flags",
"idle_iteration_count",
"action_sequence_call_counter",
"x_origin",
"y_origin",
"pnd_pointer_x",
"pnd_pointer_y",
};

char *names1133 [] =
{
"interface",
"preferred_families",
"state_flags",
};

char *names1134 [] =
{
"interface",
"state_flags",
};

char *names1135 [] =
{
"interface",
"is_timeout_executing",
"state_flags",
"count",
};

char *names1136 [] =
{
"interface",
"is_timeout_executing",
"state_flags",
"internal_id",
"count",
"interval",
};

char *names1137 [] =
{
"interface",
"state_flags",
};

char *names1138 [] =
{
"interface",
"state_flags",
};

char *names1139 [] =
{
"last_string",
"interface",
"clipboard_open",
"state_flags",
};

char *names1140 [] =
{
"interface",
"state_flags",
};

char *names1141 [] =
{
"interface",
"state_flags",
};

char *names1142 [] =
{
"interface",
"state_flags",
};

char *names1143 [] =
{
"interface",
"state_flags",
};

char *names1144 [] =
{
"interface",
"reference_tracked",
"shared",
"state_flags",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names1145 [] =
{
"interface",
"state_flags",
};

char *names1146 [] =
{
"interface",
"pixel_iterator_internal",
"is_locked",
"state_flags",
};

char *names1147 [] =
{
"interface",
"pixel_iterator_internal",
"pixmap",
"gdip_bitmap",
"data",
"is_locked",
"state_flags",
"initial_width",
"initial_height",
};

char *names1148 [] =
{
"interface",
"state_flags",
};

char *names1149 [] =
{
"interface",
"state_flags",
};

char *names1150 [] =
{
"interface",
"child_cell",
"state_flags",
};

char *names1151 [] =
{
"interface",
"child_cell",
"state_flags",
};

char *names1152 [] =
{
"interface",
"child_cell",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"state_flags",
};

char *names1153 [] =
{
"interface",
"state_flags",
};

char *names1154 [] =
{
"interface",
"state_flags",
};

char *names1155 [] =
{
"interface",
"state_flags",
"index",
};

char *names1156 [] =
{
"item_select_actions_internal",
"interface",
"state_flags",
"index",
};

char *names1157 [] =
{
"item_select_actions_internal",
"interface",
"state_flags",
"index",
};

char *names1158 [] =
{
"item_select_actions_internal",
"interface",
"new_item_actions_internal",
"remove_item_actions_internal",
"radio_group",
"ev_children",
"shared",
"state_flags",
"index",
"wel_item",
};

char *names1159 [] =
{
"item_select_actions_internal",
"interface",
"new_item_actions_internal",
"remove_item_actions_internal",
"radio_group",
"ev_children",
"parent_imp",
"shared",
"state_flags",
"index",
"wel_item",
};

char *names1160 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"interface",
"real_source",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"state_flags",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"original_parent_position",
};

char *names1161 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"interface",
"real_source",
"actual_source",
"orig_cursor",
"awaiting_movement",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"state_flags",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"original_x",
"original_y",
"original_parent_position",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1162 [] =
{
"interface",
"internal_non_sensitive",
"state_flags",
};

char *names1163 [] =
{
"docked_actions_internal",
"interface",
"veto_dock_function",
"is_docking_enabled",
"state_flags",
};

char *names1164 [] =
{
"docked_actions_internal",
"interface",
"veto_dock_function",
"is_docking_enabled",
"state_flags",
};

char *names1165 [] =
{
"interface",
"state_flags",
};

char *names1166 [] =
{
"interface",
"radio_group",
"state_flags",
};

char *names1167 [] =
{
"interface",
"state_flags",
};

char *names1168 [] =
{
"interface",
"state_flags",
};

char *names1169 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"interface",
"background_color",
"foreground_color",
"pixmap",
"locked_column",
"parent_i",
"header_item",
"is_show_requested",
"is_locked",
"internal_is_selected",
"state_flags",
"index",
"physical_index",
};

char *names1170 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"expand_actions_internal",
"collapse_actions_internal",
"interface",
"locked_row",
"background_color",
"foreground_color",
"subrows",
"parent_i",
"parent_row_i",
"index_of_first_item_dirty",
"is_expanded",
"internal_is_selected",
"is_locked",
"is_ensured_expandable",
"is_show_requested",
"state_flags",
"index_of_first_item_internal",
"internal_height",
"subrow_count_recursive",
"index",
"subrow_index",
"depth_in_tree",
"indent_depth_in_tree",
"expanded_subrow_count_recursive",
"hash_code",
};

char *names1171 [] =
{
"interface",
"state_flags",
"pixmaps_width",
"pixmaps_height",
};

char *names1172 [] =
{
"interface",
"state_flags",
};

char *names1173 [] =
{
"interface",
"private_pixmap",
"state_flags",
};

char *names1174 [] =
{
"interface",
"state_flags",
};

char *names1175 [] =
{
"tool_info",
"internal_tooltip_string",
"interface",
"state_flags",
};

char *names1176 [] =
{
"interface",
"state_flags",
};

char *names1177 [] =
{
"interface",
"state_flags",
};

char *names1178 [] =
{
"interface",
"state_flags",
};

char *names1179 [] =
{
"interface",
"state_flags",
"text_alignment",
};

char *names1180 [] =
{
"interface",
"state_flags",
};

char *names1181 [] =
{
"interface",
"state_flags",
};

char *names1182 [] =
{
"interface",
"private_font",
"private_wel_font",
"state_flags",
};

char *names1183 [] =
{
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"select_actions_internal",
"deselect_actions_internal",
"drop_actions_internal",
"activate_actions_internal",
"deactivate_actions_internal",
"interface",
"foreground_color",
"background_color",
"tooltip",
"parent_i",
"column_i",
"row_i",
"is_tab_navigatable",
"internal_is_selected",
"state_flags",
"hash_code",
};

char *names1184 [] =
{
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"select_actions_internal",
"deselect_actions_internal",
"drop_actions_internal",
"activate_actions_internal",
"deactivate_actions_internal",
"interface",
"foreground_color",
"background_color",
"tooltip",
"parent_i",
"column_i",
"row_i",
"expose_actions_internal",
"is_tab_navigatable",
"internal_is_selected",
"state_flags",
"hash_code",
"internal_required_width",
};

char *names1185 [] =
{
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"select_actions_internal",
"deselect_actions_internal",
"drop_actions_internal",
"activate_actions_internal",
"deactivate_actions_internal",
"interface",
"foreground_color",
"background_color",
"tooltip",
"parent_i",
"column_i",
"row_i",
"is_tab_navigatable",
"internal_is_selected",
"must_recompute_text_dimensions",
"state_flags",
"hash_code",
"internal_text_width",
"internal_text_height",
};

char *names1186 [] =
{
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"select_actions_internal",
"deselect_actions_internal",
"drop_actions_internal",
"activate_actions_internal",
"deactivate_actions_internal",
"interface",
"foreground_color",
"background_color",
"tooltip",
"parent_i",
"column_i",
"row_i",
"pixmaps_on_right",
"is_tab_navigatable",
"internal_is_selected",
"must_recompute_text_dimensions",
"state_flags",
"hash_code",
"internal_text_width",
"internal_text_height",
};

char *names1187 [] =
{
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"select_actions_internal",
"deselect_actions_internal",
"drop_actions_internal",
"activate_actions_internal",
"deactivate_actions_internal",
"interface",
"foreground_color",
"background_color",
"tooltip",
"parent_i",
"column_i",
"row_i",
"checked_changed_actions",
"is_tab_navigatable",
"internal_is_selected",
"must_recompute_text_dimensions",
"is_sensitive",
"is_checked",
"state_flags",
"hash_code",
"internal_text_width",
"internal_text_height",
};

char *names1188 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
};

char *names1189 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1190 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1191 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"internal_item_list",
"internal_array",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
"rows",
"columns",
"index",
};

char *names1192 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"internal_item_list",
"internal_array",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_row_layout",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
"rows",
"columns",
"index",
"finite_dimension",
"row_index",
"column_index",
};

char *names1193 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"first",
"second",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"item_refers_to_second",
"first_expandable",
"second_expandable",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1194 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"first",
"second",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"item_refers_to_second",
"first_expandable",
"second_expandable",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1195 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"first",
"second",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"item_refers_to_second",
"first_expandable",
"second_expandable",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1196 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
};

char *names1197 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
};

char *names1198 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"selection_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
};

char *names1199 [] =
{
"docked_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
};

char *names1200 [] =
{
"docked_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
};

char *names1201 [] =
{
"docked_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
};

char *names1202 [] =
{
"docked_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1203 [] =
{
"docked_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
"border_width",
};

char *names1204 [] =
{
"docked_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"item_activate_actions_internal",
"item_drop_actions_internal",
"item_deactivate_actions_internal",
"item_select_actions_internal",
"item_deselect_actions_internal",
"row_select_actions_internal",
"column_select_actions_internal",
"row_deselect_actions_internal",
"column_deselect_actions_internal",
"pointer_motion_item_actions_internal",
"pointer_double_press_item_actions_internal",
"pointer_leave_item_actions_internal",
"pointer_enter_item_actions_internal",
"pointer_button_press_item_actions_internal",
"pointer_button_release_item_actions_internal",
"row_expand_actions_internal",
"row_collapse_actions_internal",
"virtual_position_changed_actions_internal",
"virtual_size_changed_actions_internal",
"pre_draw_overlay_actions_internal",
"post_draw_overlay_actions_internal",
"fill_background_actions_internal",
"interface",
"internal_help_context",
"real_source",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"dynamic_content_function",
"locked_indexes",
"item_veto_pebble_function",
"item_accept_cursor_function",
"item_deny_cursor_function",
"separator_color",
"item_pebble_function",
"activate_window",
"currently_active_item",
"tree_node_connector_color",
"focused_selection_color",
"non_focused_selection_color",
"focused_selection_text_color",
"non_focused_selection_text_color",
"internal_row_data",
"rows",
"columns",
"column_offsets",
"row_offsets",
"row_indexes_to_visible_indexes",
"visible_indexes_to_row_indexes",
"drawable",
"viewport",
"header",
"expand_node_pixmap",
"collapse_node_pixmap",
"static_fixed_viewport",
"static_fixed",
"header_viewport",
"scroll_bar_spacer",
"fixed",
"drawer_internal",
"last_pointed_item",
"shift_key_start_item",
"physical_column_indexes_internal",
"internal_tooltip",
"internal_vertical_scroll_bar",
"internal_horizontal_scroll_bar",
"internal_selected_rows",
"internal_selected_items",
"last_selected_row",
"last_selected_item",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_header_displayed",
"is_resizing_divider_enabled",
"is_resizing_divider_solid",
"is_horizontal_scrolling_per_item",
"is_vertical_scrolling_per_item",
"is_vertical_overscroll_enabled",
"is_horizontal_overscroll_enabled",
"is_content_partially_dynamic",
"is_row_height_fixed",
"is_column_resize_immediate",
"are_tree_node_connectors_shown",
"are_column_separators_enabled",
"are_row_separators_enabled",
"is_always_selected",
"is_item_tab_navigation_enabled",
"has_horizontal_scrolling_per_item_just_changed",
"has_vertical_scrolling_per_item_just_changed",
"is_item_height_changing",
"is_vertical_scroll_bar_show_requested",
"is_horizontal_scroll_bar_show_requested",
"is_selection_on_click_enabled",
"is_selection_on_single_button_click_enabled",
"is_selection_keyboard_handling_enabled",
"is_tree_enabled",
"is_single_row_selection_enabled",
"is_multiple_row_selection_enabled",
"is_single_item_selection_enabled",
"is_multiple_item_selection_enabled",
"are_columns_drawn_above_rows",
"drawables_have_focus",
"vertical_computation_required",
"horizontal_computation_required",
"horizontal_computation_added_to_once_idle_actions",
"vertical_computation_added_to_once_idle_actions",
"is_full_redraw_on_virtual_position_change_enabled",
"is_locked",
"is_horizontal_offset_set_to_zero_when_items_smaller_than_viewable_width",
"horizontal_redraw_triggered_by_viewport_resize",
"vertical_redraw_triggered_by_viewport_resize",
"is_header_item_resizing",
"pointer_enter_called",
"physical_column_indexes_dirty",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
"row_height",
"subrow_indent",
"viewable_width",
"viewable_height",
"displayed_column_count",
"physical_column_count",
"invalid_row_index",
"invalid_column_index",
"redraw_object_counter",
"internal_client_x",
"internal_client_y",
"viewport_x_offset",
"viewport_y_offset",
"computed_visible_row_count",
"node_pixmap_width",
"total_tree_node_width",
"first_tree_node_indent",
"tree_subrow_indent",
"last_width_of_header_during_resize_internal",
"last_dashed_line_position",
"last_horizontal_scroll_bar_value",
"last_vertical_scroll_bar_value",
"item_counter",
"row_counter",
"non_displayed_row_count",
};

char *names1205 [] =
{
"docked_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1206 [] =
{
"docked_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1207 [] =
{
"docked_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1208 [] =
{
"docked_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"is_disconnected_from_window_manager",
"has_shadow",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1209 [] =
{
"docked_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"maximize_actions_internal",
"minimize_actions_internal",
"restore_actions_internal",
"interface",
"internal_help_context",
"real_source",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"help_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1210 [] =
{
"docked_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"maximize_actions_internal",
"minimize_actions_internal",
"restore_actions_internal",
"interface",
"internal_help_context",
"real_source",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"internal_default_push_button",
"internal_default_cancel_button",
"internal_current_push_button",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"help_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1211 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1212 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"item_resize_actions_internal",
"item_resize_start_actions_internal",
"item_resize_end_actions_internal",
"item_pointer_button_press_actions_internal",
"item_pointer_double_press_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
};

char *names1213 [] =
{
"docked_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
};

char *names1214 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"deselect_actions_internal",
"column_title_click_actions_internal",
"column_resized_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"ev_children",
"column_titles",
"column_widths",
"column_alignments",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
};

char *names1215 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1216 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"select_actions_internal",
"deselect_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
};

char *names1217 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"check_actions_internal",
"uncheck_actions_internal",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"select_actions_internal",
"deselect_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
};

char *names1218 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1219 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1220 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"caret_move_actions_internal",
"selection_change_actions_internal",
"file_access_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"tab_positions",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"buffer_locked_in_format_mode",
"buffer_locked_in_append_mode",
"last_load_successful",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1221 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"return_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1222 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"return_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1223 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1224 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1225 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1226 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1227 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1228 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1229 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1230 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"deselect_actions_internal",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
};

char *names1231 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"drop_down_actions_internal",
"list_hidden_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"return_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"deselect_actions_internal",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
"list_height_hint",
"list_width_hint",
};

char *names1232 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"deselect_actions_internal",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"default_key_processing_disabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
};

char *names1233 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"deselect_actions_internal",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"new_item_actions_internal",
"remove_item_actions_internal",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"image_list",
"ev_children",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
};

char *names1234 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"check_actions_internal",
"uncheck_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"deselect_actions_internal",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"default_key_processing_disabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
};

char *names1235 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1236 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"text_change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"return_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1237 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1238 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1239 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1240 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1241 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1242 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1243 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1244 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1245 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1246 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
};

char *names1247 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
};

char *names1248 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
};

char *names1249 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
};

char *names1250 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
};

char *names1251 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"expand_actions_internal",
"collapse_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
};

char *names1252 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"expand_actions_internal",
"collapse_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
};

char *names1253 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
};

char *names1254 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"item_select_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"index",
};

char *names1255 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
};

char *names1256 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
};

char *names1257 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
};

char *names1258 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"internal_pixmap",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
};

char *names1259 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"drop_down_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"enabled_before",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1260 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"drop_down_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"enabled_before",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1261 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"drop_down_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"enabled_before",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1262 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"drop_down_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"enabled_before",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1263 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"interface",
"state_flags",
};

char *names1264 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"interface",
"state_flags",
"maximum_to_page",
"minimum_from_page",
};

char *names1265 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"interface",
"state_flags",
};

char *names1266 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"interface",
"state_flags",
};

char *names1267 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"interface",
"state_flags",
};

char *names1268 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"interface",
"filters",
"state_flags",
};

char *names1269 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"interface",
"filters",
"state_flags",
};

char *names1270 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"interface",
"filters",
"state_flags",
};

char *names1271 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"interface",
"selected_button",
"blocking_window",
"state_flags",
};

char *names1272 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"custom_colors",
"interface",
"selected_button",
"blocking_window",
"stored_color",
"internal_title",
"shared",
"selected",
"state_flags",
"item",
};

char *names1273 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"log_font",
"interface",
"selected_button",
"blocking_window",
"internal_title",
"shared",
"selected",
"state_flags",
"item",
};

char *names1274 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"private_dc",
"interface",
"selected_button",
"blocking_window",
"internal_title",
"shared",
"selected",
"state_flags",
"maximum_to_page",
"minimum_from_page",
"item",
};

char *names1275 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"str_folder_name",
"str_starting_folder",
"str_title",
"interface",
"selected_button",
"blocking_window",
"shared",
"selected",
"state_flags",
"item",
};

char *names1276 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"interface",
"filters",
"selected_button",
"blocking_window",
"start_path",
"filter",
"state_flags",
};

char *names1277 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"str_file_name",
"str_file_title",
"str_filter",
"str_title",
"str_initial_path",
"str_default_extension",
"interface",
"filters",
"selected_button",
"blocking_window",
"start_path",
"filter",
"shared",
"selected",
"state_flags",
"item",
};

char *names1278 [] =
{
"ok_actions_internal",
"cancel_actions_internal",
"str_file_name",
"str_file_title",
"str_filter",
"str_title",
"str_initial_path",
"str_default_extension",
"interface",
"filters",
"selected_button",
"blocking_window",
"start_path",
"filter",
"shared",
"selected",
"state_flags",
"item",
};

char *names1279 [] =
{
"interface",
"state_flags",
};

char *names1280 [] =
{
"interface",
"postscript_result",
"font",
"background_color_internal",
"foreground_color_internal",
"tile",
"clip_area",
"landscape",
"dashed_line_style",
"state_flags",
"left_margin",
"bottom_margin",
"real_page_width",
"real_page_height",
"drawing_mode",
"line_width",
"height",
"width",
};

char *names1281 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"expose_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"focus_on_press_disabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1282 [] =
{
"interface",
"state_flags",
};

char *names1283 [] =
{
"interface",
"state_flags",
};

char *names1284 [] =
{
"interface",
"state_flags",
};

char *names1285 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"expose_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
};

char *names1286 [] =
{
"interface",
"notebook",
"widget",
"state_flags",
};

char *names1287 [] =
{
"interface",
"private_pixmap",
"notebook",
"widget",
"state_flags",
};

char *names1288 [] =
{
"last_string",
"internal_name",
"internal_detachable_name_pointer",
"last_character",
"separator",
"object_comparison",
"descriptor_available",
"is_sequence_an_expected_numeric",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"mode",
"last_real",
"file_pointer",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names1289 [] =
{
"item",
"strings",
"count",
};

char *names1290 [] =
{
"access_name",
};

char *names1291 [] =
{
"message_information",
};

char *names1292 [] =
{
"code",
};

char *names1293 [] =
{
"area",
};

char *names1294 [] =
{
"area",
};

char *names1295 [] =
{
"retrieved_errors",
"deserialized_object",
"error_message",
"successful",
"last_file_position",
};

char *names1296 [] =
{
"font_family",
"font_weight",
"font_shape",
"font_height",
"color",
"background_color",
"effects_striked_out",
"effects_underlined",
"effects_vertical_offset",
};

char *names1297 [] =
{
"managed_pointer",
"shared",
"internal_item",
};

char *names1298 [] =
{
"alignment",
"left_margin",
"right_margin",
"top_spacing",
"bottom_spacing",
};

char *names1301 [] =
{
"item",
};

char *names1302 [] =
{
"interface",
"name",
"state_flags",
"item",
};

char *names1303 [] =
{
"start_arrow",
"end_arrow",
};

char *names1304 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"start_arrow",
"end_arrow",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"is_closed",
"internal_id",
"draw_id",
"line_width",
};

char *names1305 [] =
{
"target_name",
"target_data_function",
"group",
"internal_invalid_rectangle",
"points",
"internal_pointer_style",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_proximity_in_actions",
"internal_proximity_out_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"start_arrow",
"end_arrow",
"is_show_requested",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"internal_id",
"draw_id",
"line_width",
};

char *names1307 [] =
{
"drawable",
};

char *names1308 [] =
{
"draw_routines",
"is_projecting",
};

char *names1309 [] =
{
"world",
"pen",
"brush",
"palette",
"region",
"font",
"bitmap",
"interface",
"drawable",
"draw_routines",
"print_dc",
"a_printer",
"reference_tracked",
"shared",
"is_projecting",
"state_flags",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"old_hpen",
"old_hbrush",
"old_hregion",
"old_hpalette",
"old_hfont",
"old_hbitmap",
};

char *names1310 [] =
{
"world",
"drawable",
"draw_routines",
"filename",
"is_projecting",
"offset_y",
"offset_x",
};

char *names1311 [] =
{
"world",
"drawable",
"draw_routines",
"project_agent",
"widget",
"area",
"current_figure",
"is_projecting",
"is_figure_selected",
"has_mouse",
"area_x",
"area_y",
"last_pointer_x",
"last_pointer_y",
};

char *names1312 [] =
{
"world",
"drawable",
"draw_routines",
"project_agent",
"pixmap",
"area",
"current_figure",
"is_projecting",
"is_figure_selected",
"has_mouse",
"area_x",
"area_y",
"last_pointer_x",
"last_pointer_y",
};

char *names1313 [] =
{
"drawable_position",
"drawable_cell",
"world",
"drawable",
"draw_routines",
"project_agent",
"widget",
"area",
"current_figure",
"recenter_agent",
"is_projecting",
"is_figure_selected",
"has_mouse",
"is_world_too_large",
"area_centered",
"area_x",
"area_y",
"last_pointer_x",
"last_pointer_y",
};

char *names1314 [] =
{
"world",
"drawable",
"draw_routines",
"project_agent",
"widget",
"area",
"current_figure",
"is_projecting",
"is_figure_selected",
"has_mouse",
"area_x",
"area_y",
"last_pointer_x",
"last_pointer_y",
};

char *names1315 [] =
{
"internal_start_arrow",
"internal_end_arrow",
"is_start_arrow",
"is_end_arrow",
"arrow_size",
"internal_arrow_size",
"internal_start_angle",
"internal_start_point_x",
"internal_start_point_y",
"internal_end_angle",
"internal_end_point_x",
"internal_end_point_y",
};

char *names1316 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"internal_start_arrow",
"internal_end_arrow",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"is_start_arrow",
"is_end_arrow",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
"arrow_size",
"internal_arrow_size",
"internal_start_angle",
"internal_start_point_x",
"internal_start_point_y",
"internal_end_angle",
"internal_end_point_x",
"internal_end_point_y",
};

char *names1317 [] =
{
"target_name",
"target_data_function",
"group",
"point_array",
"internal_invalid_rectangle",
"last_update_rectangle",
"internal_pointer_motion_actions",
"internal_pointer_button_press_actions",
"internal_pointer_double_press_actions",
"internal_pointer_button_release_actions",
"internal_pointer_enter_actions",
"internal_pointer_leave_actions",
"internal_pick_actions",
"internal_conforming_pick_actions",
"internal_drop_actions",
"internal_projection_matrix",
"internal_pointer_style",
"center",
"internal_bounding_box",
"deny_cursor",
"accept_cursor",
"pebble_function",
"pebble",
"foreground_color",
"internal_start_arrow",
"internal_end_arrow",
"is_show_requested",
"is_center_valid",
"are_events_sent_to_group",
"valid",
"internal_is_sensitive",
"dashed_line_style",
"is_start_arrow",
"is_end_arrow",
"is_closed",
"internal_id",
"draw_id",
"internal_hash_id",
"line_width",
"arrow_size",
"internal_arrow_size",
"internal_start_angle",
"internal_start_point_x",
"internal_start_point_y",
"internal_end_angle",
"internal_end_point_x",
"internal_end_point_y",
};

char *names1320 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"internal_previous_cursor",
};

char *names1321 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"internal_previous_cursor",
};

char *names1323 [] =
{
"recorder",
"object",
"breakable_info",
"parent",
"steps",
"call_records",
"value_records",
"last_position",
"rt_information_available",
"is_expanded",
"is_flat",
"is_closed",
"class_type_id",
"feature_rout_id",
"depth",
};

char *names1324 [] =
{
"top_callstack_record",
"bottom_callstack_record",
"replayed_call",
"replay_stack",
"flatten_when_closing",
"keep_calls_records",
"recording_values",
"is_replaying",
"last_replay_operation_failed",
"record_count",
"maximum_record_count",
};

char *names1325 [] =
{
"breakable_info",
"position",
"type",
};

char *names1328 [] =
{
"postscript_result",
"point_width",
"point_height",
"left_margin",
"bottom_margin",
};

char *names1329 [] =
{
"drawable",
};

char *names1330 [] =
{
"draw_routines",
"is_projecting",
};

char *names1331 [] =
{
"world",
"postscript_result",
"draw_routines",
"filename",
"file",
"is_projecting",
"point_width",
"point_height",
"left_margin",
"bottom_margin",
};

char *names1332 [] =
{
"world",
"pen",
"brush",
"palette",
"region",
"font",
"bitmap",
"interface",
"drawable",
"draw_routines",
"dc",
"a_printer",
"reference_tracked",
"shared",
"is_projecting",
"state_flags",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
"old_hpen",
"old_hbrush",
"old_hregion",
"old_hpalette",
"old_hfont",
"old_hbitmap",
};

char *names1333 [] =
{
"world",
"drawable",
"draw_routines",
"widget",
"area",
"current_figure",
"is_projecting",
"has_mouse",
"area_x",
"area_y",
"last_pointer_x",
"last_pointer_y",
};

char *names1334 [] =
{
"world",
"drawable",
"draw_routines",
"widget",
"area",
"current_figure",
"is_projecting",
"has_mouse",
"area_x",
"area_y",
"last_pointer_x",
"last_pointer_y",
};

char *names1335 [] =
{
"world",
"drawable",
"draw_routines",
"widget",
"area",
"current_figure",
"is_projecting",
"has_mouse",
"area_x",
"area_y",
"last_pointer_x",
"last_pointer_y",
};

char *names1337 [] =
{
"window",
"message",
"w_param",
"l_param",
};

char *names1338 [] =
{
"window",
"message",
"w_param",
"l_param",
};

char *names1339 [] =
{
"window",
"message",
"w_param",
"l_param",
};

char *names1340 [] =
{
"window",
"message",
"w_param",
"l_param",
};

char *names1341 [] =
{
"window",
"message",
"w_param",
"l_param",
};

char *names1342 [] =
{
"window",
"message",
"w_param",
"l_param",
};

char *names1343 [] =
{
"window",
"message",
"w_param",
"l_param",
};

char *names1344 [] =
{
"window",
"message",
"w_param",
"l_param",
};

char *names1345 [] =
{
"window",
"message",
"w_param",
"l_param",
};

char *names1346 [] =
{
"window",
"message",
"w_param",
"l_param",
};

char *names1347 [] =
{
"window",
"message",
"w_param",
"l_param",
};

char *names1348 [] =
{
"window",
"scroll_info_struct",
"horizontal_line",
"vertical_line",
"window_item",
};

char *names1349 [] =
{
"rich_text",
"internal_text",
"last_fontname",
"all_fonts",
"all_colors",
"all_formats",
"all_paragraph_formats",
"all_paragraph_format_keys",
"all_paragraph_indexes",
"current_format",
"format_stack",
"plain_text",
"paragraph_start_indexes",
"paragraph_formats",
"hashed_formats",
"format_offsets",
"buffered_text",
"formats",
"heights",
"formats_index",
"start_formats",
"end_formats",
"hashed_colors",
"color_offset",
"back_color_offset",
"color_text",
"hashed_fonts",
"font_offset",
"font_text",
"last_load_successful",
"first_color_is_auto",
"is_current_format_underlined",
"is_current_format_striked_through",
"is_current_format_bold",
"is_current_format_italic",
"highest_read_char",
"last_colorindex",
"last_fontindex",
"last_fontcharset",
"last_fontfamily",
"last_colorred",
"last_colorgreen",
"last_colorblue",
"number_of_characters_opened",
"current_depth",
"main_iterator",
"temp_iterator",
"lowest_buffered_value",
"highest_buffered_value",
"color_count",
"font_count",
"current_vertical_offset",
};

char *names1351 [] =
{
"last_pixmap_loading_had_error",
};

char *names1352 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"expose_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"private_bitmap",
"private_mask_bitmap",
"private_icon",
"private_cursor",
"private_palette",
"pixmap_filename",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"last_pixmap_loading_had_error",
"update_needed",
"internal_tabable_info",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
"private_width",
"private_height",
"private_bitmap_id",
};

char *names1354 [] =
{
"interface",
"state_flags",
};

char *names1356 [] =
{
"interface",
"preferred_families",
"wel_font",
"internal_face_name",
"internal_is_proportional",
"state_flags",
"wel_screen_font_family",
"wel_screen_font_pitch",
"shape",
"weight",
"family",
};

char *names1357 [] =
{
"interface",
"shared",
"state_flags",
"item",
};

char *names1358 [] =
{
"shared",
"item",
};

char *names1359 [] =
{
"shared",
"item",
};

char *names1360 [] =
{
"interface",
"shared",
"state_flags",
"wel_screen_font_family",
"item",
};

char *names1361 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names1362 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names1363 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names1364 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names1365 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names1366 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names1367 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names1368 [] =
{
"reference_tracked",
"shared",
"references_count",
"internal_object_id",
"internal_number_id",
"item",
};

char *names1369 [] =
{
"environment_variables",
"input_pipe",
"output_pipe",
"process_info",
"last_launch_successful",
"hidden",
"last_process_result",
"internal_block_size",
};

char *names1373 [] =
{
"str_text",
"shared",
"item",
};

char *names1374 [] =
{
"shared",
"item",
};

char *names1375 [] =
{
"shared",
"item",
};

char *names1376 [] =
{
"window",
"message",
"w_param",
"l_param",
};

char *names1377 [] =
{
"shared",
"item",
};

char *names1378 [] =
{
"internal_children",
"internal_hwnds",
};

char *names1379 [] =
{
"str_text",
"shared",
"item",
};

char *names1380 [] =
{
"str_text",
"shared",
"item",
};

char *names1381 [] =
{
"shared",
"item",
};

char *names1382 [] =
{
"dc",
"shared",
"item",
};

char *names1383 [] =
{
"shared",
"item",
};

char *names1384 [] =
{
"exception_callback",
};

char *names1385 [] =
{
"exception_callback",
};

char *names1386 [] =
{
"idle_action_enabled",
};

char *names1387 [] =
{
"idle_action_enabled",
};

char *names1388 [] =
{
"post_launch_actions_internal",
"kamikaze_actions",
"idle_actions_internal",
"pick_actions_internal",
"drop_actions_internal",
"cancel_actions_internal",
"pnd_motion_actions_internal",
"file_drop_actions_internal",
"uncaught_exception_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"mouse_wheel_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"theme_changed_actions_internal",
"system_color_change_actions_internal",
"destroy_actions_internal",
"interface",
"idle_actions_snapshot",
"kamikaze_idle_actions_snapshot",
"dockable_targets",
"pnd_targets",
"locked_window",
"captured_widget",
"help_accelerator",
"contextual_help_accelerator",
"help_engine",
"idle_action_mutex",
"kamikaze_action_mutex",
"exception_dialog",
"clipboard_internal",
"old_pointer_style",
"old_pointer_button_press_actions",
"help_handler_procedure",
"contextual_help_handler_procedure",
"window_with_focus",
"theme_drawer",
"dockable_source",
"controls_dll",
"rich_edit_dll",
"blocking_windows_stack",
"reusable_message",
"pick_and_drop_source",
"idle_actions_executing",
"events_processed_from_underlying_toolkit",
"user_events_processed_from_underlying_toolkit",
"invoke_garbage_collection_when_inactive",
"rubber_band_is_drawn",
"uncaught_exception_actions_called",
"stop_processing_requested",
"idle_action_enabled",
"drop_actions_executing",
"awaiting_movement",
"transport_just_ended",
"override_from_mouse_activate",
"tab_navigation_state",
"state_flags",
"idle_iteration_count",
"action_sequence_call_counter",
"x_origin",
"y_origin",
"pnd_pointer_x",
"pnd_pointer_y",
"tooltip_delay",
"process_handle",
"main_application_window_handle",
};

char *names1389 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1390 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1391 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1392 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"resource_name",
"dialog_children",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"result_id",
"resource_id",
"item",
"default_window_procedure",
};

char *names1393 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"resource_name",
"dialog_children",
"shared",
"internal_wm_size_called",
"is_result_id_set",
"level_count",
"internal_object_id",
"result_id",
"resource_id",
"item",
"default_window_procedure",
};

char *names1394 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"resource_name",
"dialog_children",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"result_id",
"resource_id",
"item",
"default_window_procedure",
};

char *names1395 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"resource_name",
"dialog_children",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"result_id",
"resource_id",
"item",
"default_window_procedure",
};

char *names1396 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"resource_name",
"dialog_children",
"internal_eiffel_file_edit",
"internal_h_file_edit",
"internal_class_name_edit",
"internal_translate_button",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"result_id",
"resource_id",
"item",
"default_window_procedure",
};

char *names1397 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"wnd_class",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1398 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"wnd_class",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1399 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"wnd_class",
"internal_client_window",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1400 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"wnd_class",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1401 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"wnd_class",
"timeouts",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1402 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"wnd_class",
"notify_icon_data",
"notify_message_name",
"internal_notify_icon_actions",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"notify_message_id",
"notify_uid",
"item",
"default_window_procedure",
};

char *names1403 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"wnd_class",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1404 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"wnd_class",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1405 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"wnd_class",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1406 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"wnd_class",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1407 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"wnd_class",
"window",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1408 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"wnd_class",
"toolbar",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"item",
"default_window_procedure",
};

char *names1409 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"wel_item",
"default_window_procedure",
};

char *names1410 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"shared",
"internal_wm_size_called",
"is_theme_background_requested",
"ev_resizing_flags",
"level_count",
"internal_object_id",
"wel_item",
"default_window_procedure",
};

char *names1411 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1412 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1413 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1414 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1415 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1416 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1417 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
"last_item",
};

char *names1418 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"has_bitmap",
"level_count",
"internal_object_id",
"id",
"last_bitmap_index",
"last_string_index",
"item",
"default_window_procedure",
};

char *names1419 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"default_image_list",
"disabled_image_list",
"hot_image_list",
"shared",
"internal_wm_size_called",
"has_bitmap",
"use_image_list",
"level_count",
"internal_object_id",
"id",
"last_bitmap_index",
"last_string_index",
"bitmaps_width",
"bitmaps_height",
"last_disabled_bitmap_index",
"last_hot_bitmap_index",
"item",
"default_window_procedure",
};

char *names1420 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"has_bitmap",
"level_count",
"internal_object_id",
"id",
"last_bitmap_index",
"last_string_index",
"item",
"default_window_procedure",
};

char *names1421 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"column_count",
"item",
"default_window_procedure",
};

char *names1422 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1423 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1424 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1425 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1426 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"bitmap",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1427 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"internal_text",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1428 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"scroll_caret_at_selection",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1429 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"scroll_caret_at_selection",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1430 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"scroll_caret_at_selection",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1431 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"scroll_caret_at_selection",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1432 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"tool_info",
"internal_tooltip_string",
"wel_parent",
"commands",
"shared",
"internal_wm_size_called",
"scroll_caret_at_selection",
"level_count",
"internal_object_id",
"id",
"text_alignment",
"item",
"default_window_procedure",
};

char *names1433 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1434 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1435 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"tool_info",
"internal_tooltip_string",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1436 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroll_info_struct",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"line",
"item",
"default_window_procedure",
};

char *names1437 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1438 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1439 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1440 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1441 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"tool_info",
"internal_tooltip_string",
"wel_parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1442 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1443 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1444 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1445 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1446 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1447 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1448 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1449 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1450 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"internal_bitmap",
"internal_icon",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"current_pixmap",
"item",
"default_window_procedure",
};

char *names1451 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1452 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1453 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1454 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1455 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"id",
"item",
"default_window_procedure",
};

char *names1457 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"parent",
"commands",
"scroller",
"wnd_class",
"menu_item_list",
"shared",
"internal_wm_size_called",
"level_count",
"internal_object_id",
"default_style",
"item",
"default_window_procedure",
};

char *names1458 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1459 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"awaiting_movement",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1460 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"awaiting_movement",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1461 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"internal_text",
"interface",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"user_can_resize",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"text_alignment",
"maximum_width",
"minimum_width",
"item",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1462 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"awaiting_movement",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1463 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"internal_text",
"lv_item",
"cb_item",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"has_pixmap",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"image_index",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1464 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"internal_pixmap",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"internal_tooltip_string",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1465 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"expand_actions_internal",
"collapse_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"tool_info",
"internal_tooltip_string",
"str_text",
"interface",
"new_item_actions_internal",
"remove_item_actions_internal",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"real_text",
"internal_children",
"ev_children",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"has_pixmap",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"index",
"image_index",
"wel_item",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1466 [] =
{
"select_actions_internal",
"deselect_actions_internal",
"expand_actions_internal",
"collapse_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"tool_info",
"internal_tooltip_string",
"str_text",
"interface",
"new_item_actions_internal",
"remove_item_actions_internal",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"real_text",
"internal_children",
"ev_children",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"has_pixmap",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"index",
"image_index",
"wel_item",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1467 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"awaiting_movement",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"id",
"image_index",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1468 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"id",
"image_index",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1469 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"drop_down_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"real_source",
"actual_source",
"orig_cursor",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"real_text",
"private_gray_pixmap",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"enabled_before",
"is_pnd_in_transport",
"is_dnd_in_transport",
"has_gray_pixmap",
"has_pixmap",
"is_sensitive",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"id",
"original_parent_position",
"image_index",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1470 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"drop_down_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"real_source",
"actual_source",
"orig_cursor",
"radio_group",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"real_text",
"private_gray_pixmap",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"enabled_before",
"is_pnd_in_transport",
"is_dnd_in_transport",
"has_gray_pixmap",
"has_pixmap",
"is_sensitive",
"is_selected",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"id",
"original_parent_position",
"image_index",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1471 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"drop_down_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"real_source",
"actual_source",
"orig_cursor",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"real_text",
"private_gray_pixmap",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"enabled_before",
"is_pnd_in_transport",
"is_dnd_in_transport",
"has_gray_pixmap",
"has_pixmap",
"is_sensitive",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"id",
"original_parent_position",
"image_index",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1472 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"drop_down_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"real_source",
"actual_source",
"orig_cursor",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"real_text",
"private_gray_pixmap",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"enabled_before",
"is_pnd_in_transport",
"is_dnd_in_transport",
"has_gray_pixmap",
"has_pixmap",
"is_sensitive",
"is_selected",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"id",
"original_parent_position",
"image_index",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1473 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"interface",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_item_source",
"awaiting_movement",
"rubber_band_is_drawn",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_actions_called",
"item_is_pnd_source_at_entry",
"call_press_event",
"parent_is_pnd_source",
"item_is_pnd_source",
"item_is_dockable_source",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1474 [] =
{
"interface",
"private_font",
"private_wel_font",
"internal_background_brush",
"internal_brush",
"internal_pen",
"tile",
"clip_area",
"foreground_color_internal",
"background_color_internal",
"internal_initialized_pen",
"internal_initialized_text_color",
"internal_initialized_font",
"internal_initialized_background_brush",
"internal_initialized_brush",
"dashed_line_style",
"state_flags",
"wel_drawing_mode",
"line_width",
};

char *names1475 [] =
{
"interface",
"private_font",
"private_wel_font",
"internal_background_brush",
"internal_brush",
"internal_pen",
"tile",
"clip_area",
"foreground_color_internal",
"background_color_internal",
"dc",
"internal_initialized_pen",
"internal_initialized_text_color",
"internal_initialized_font",
"internal_initialized_background_brush",
"internal_initialized_brush",
"dashed_line_style",
"state_flags",
"wel_drawing_mode",
"line_width",
};

char *names1476 [] =
{
"interface",
"private_font",
"private_wel_font",
"internal_background_brush",
"internal_brush",
"internal_pen",
"tile",
"clip_area",
"foreground_color_internal",
"background_color_internal",
"drawable",
"dc",
"internal_initialized_pen",
"internal_initialized_text_color",
"internal_initialized_font",
"internal_initialized_background_brush",
"internal_initialized_brush",
"dashed_line_style",
"state_flags",
"wel_drawing_mode",
"line_width",
"height",
"width",
};

char *names1477 [] =
{
"interface",
"private_font",
"private_wel_font",
"internal_background_brush",
"internal_brush",
"internal_pen",
"tile",
"clip_area",
"foreground_color_internal",
"background_color_internal",
"dc",
"internal_initialized_pen",
"internal_initialized_text_color",
"internal_initialized_font",
"internal_initialized_background_brush",
"internal_initialized_brush",
"dashed_line_style",
"state_flags",
"wel_drawing_mode",
"line_width",
};

char *names1478 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"expose_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"real_source",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_background_brush",
"internal_brush",
"internal_pen",
"tile",
"clip_area",
"foreground_color_internal",
"background_color_internal",
"mask_dc",
"transparent_color",
"internal_bitmap",
"internal_mask_bitmap",
"palette",
"dc",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_initialized_pen",
"internal_initialized_text_color",
"internal_initialized_font",
"internal_initialized_background_brush",
"internal_initialized_brush",
"dashed_line_style",
"internal_tabable_info",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_parent_position",
"wel_drawing_mode",
"line_width",
"height",
"width",
};

char *names1479 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"original_parent_position",
"last_x",
"last_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1480 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"awaiting_movement",
"rubber_band_is_drawn",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"original_parent_position",
"last_x",
"last_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1481 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"internal_item_list",
"internal_array",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"ev_children",
"columns_minimum",
"rows_minimum",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_theme_background_requested",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_homogeneous",
"ev_resizing_flags",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"rows",
"columns",
"index",
"internal_object_id",
"last_x",
"last_y",
"columns_sum",
"rows_sum",
"row_spacing",
"column_spacing",
"border_width",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1482 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"first",
"second",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"splitter_brush",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"item_refers_to_second",
"first_expandable",
"second_expandable",
"internal_wm_size_called",
"is_theme_background_requested",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_maximum_split_position_computed",
"ev_resizing_flags",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"internal_split_position",
"last_dimension",
"click_relative_position",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1483 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"first",
"second",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"splitter_brush",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"item_refers_to_second",
"first_expandable",
"second_expandable",
"internal_wm_size_called",
"is_theme_background_requested",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_maximum_split_position_computed",
"is_computing_max_position",
"ev_resizing_flags",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"internal_split_position",
"last_dimension",
"click_relative_position",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1484 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"first",
"second",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"splitter_brush",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"item_refers_to_second",
"first_expandable",
"second_expandable",
"internal_wm_size_called",
"is_theme_background_requested",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_maximum_split_position_computed",
"ev_resizing_flags",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"internal_split_position",
"last_dimension",
"click_relative_position",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1485 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"awaiting_movement",
"rubber_band_is_drawn",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"index",
"original_parent_position",
"last_x",
"last_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1486 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"ev_children",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_theme_background_requested",
"is_pnd_in_transport",
"is_dnd_in_transport",
"ev_resizing_flags",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"index",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1487 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"selection_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"wel_parent",
"commands",
"scroller",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"image_list",
"ev_children",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"check_notebook_assertions",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
"internal_object_id",
"id",
"last_x",
"last_y",
"tab_pos",
"wel_item",
"default_window_procedure",
"open_theme",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1488 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"non_expandable_children",
"reversed_sizing_agents",
"ev_children",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_theme_background_requested",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_homogeneous",
"ev_resizing_flags",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"index",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"childvisible_nb",
"childexpand_nb",
"border_width",
"padding",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1489 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"non_expandable_children",
"reversed_sizing_agents",
"ev_children",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_theme_background_requested",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_homogeneous",
"ev_resizing_flags",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"index",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"childvisible_nb",
"childexpand_nb",
"border_width",
"padding",
"children_width",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1490 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"non_expandable_children",
"reversed_sizing_agents",
"ev_children",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_theme_background_requested",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_homogeneous",
"ev_resizing_flags",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"index",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"childvisible_nb",
"childexpand_nb",
"border_width",
"padding",
"children_height",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1491 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"awaiting_movement",
"rubber_band_is_drawn",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"original_parent_position",
"last_x",
"last_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1492 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_theme_background_requested",
"is_pnd_in_transport",
"is_dnd_in_transport",
"ev_resizing_flags",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"text_alignment",
"border_width",
"internal_object_id",
"last_x",
"last_y",
"text_height",
"text_width",
"frame_style",
"wel_item",
"default_window_procedure",
"open_theme",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1493 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"menu_bar",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"call_show_actions",
"override_movement",
"is_parented_window",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"internal_height",
"internal_width",
"maximum_width",
"maximum_height",
"wel_item",
"default_window_procedure",
"last_focused_widget",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1494 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"menu_bar",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"is_disconnected_from_window_manager",
"has_shadow",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"call_show_actions",
"override_movement",
"is_parented_window",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"internal_height",
"internal_width",
"maximum_width",
"maximum_height",
"wel_item",
"default_window_procedure",
"last_focused_widget",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1495 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"maximize_actions_internal",
"minimize_actions_internal",
"restore_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"menu_bar",
"internal_title",
"current_icon_pixmap",
"internal_class_name",
"internal_icon_name",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"help_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"call_show_actions",
"override_movement",
"is_parented_window",
"fire_restore_actions",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"id",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"internal_height",
"internal_width",
"maximum_width",
"maximum_height",
"wel_item",
"default_window_procedure",
"last_focused_widget",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1496 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"maximize_actions_internal",
"minimize_actions_internal",
"restore_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"internal_default_push_button",
"internal_default_cancel_button",
"internal_current_push_button",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"menu_bar",
"internal_title",
"current_icon_pixmap",
"internal_class_name",
"internal_icon_name",
"parent_window",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"help_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"call_show_actions",
"override_movement",
"is_parented_window",
"fire_restore_actions",
"apply_center_dialog",
"is_closeable",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"id",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"internal_height",
"internal_width",
"maximum_width",
"maximum_height",
"wel_item",
"default_window_procedure",
"last_focused_widget",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1497 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"maximize_actions_internal",
"minimize_actions_internal",
"restore_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"internal_default_push_button",
"internal_default_cancel_button",
"internal_current_push_button",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"menu_bar",
"internal_title",
"current_icon_pixmap",
"internal_class_name",
"internal_icon_name",
"parent_window",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"help_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"call_show_actions",
"override_movement",
"is_parented_window",
"fire_restore_actions",
"apply_center_dialog",
"is_closeable",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"id",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"internal_height",
"internal_width",
"maximum_width",
"maximum_height",
"wel_item",
"default_window_procedure",
"last_focused_widget",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1498 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"maximize_actions_internal",
"minimize_actions_internal",
"restore_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"internal_default_push_button",
"internal_default_cancel_button",
"internal_current_push_button",
"wel_parent",
"commands",
"scroller",
"resource_name",
"dialog_children",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"menu_bar",
"internal_title",
"current_icon_pixmap",
"internal_class_name",
"internal_icon_name",
"parent_window",
"other_imp",
"post_creation_update_actions",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"help_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"call_show_actions",
"override_movement",
"is_parented_window",
"fire_restore_actions",
"apply_center_dialog",
"is_closeable",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"id",
"original_parent_position",
"internal_object_id",
"result_id",
"resource_id",
"last_x",
"last_y",
"internal_height",
"internal_width",
"maximum_width",
"maximum_height",
"wel_item",
"default_window_procedure",
"last_focused_widget",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1499 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"maximize_actions_internal",
"minimize_actions_internal",
"restore_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"internal_default_push_button",
"internal_default_cancel_button",
"internal_current_push_button",
"wel_parent",
"commands",
"scroller",
"resource_name",
"dialog_children",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"menu_bar",
"internal_title",
"current_icon_pixmap",
"internal_class_name",
"internal_icon_name",
"parent_window",
"other_imp",
"post_creation_update_actions",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"help_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"call_show_actions",
"override_movement",
"is_parented_window",
"fire_restore_actions",
"apply_center_dialog",
"is_closeable",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"id",
"original_parent_position",
"internal_object_id",
"result_id",
"resource_id",
"last_x",
"last_y",
"internal_height",
"internal_width",
"maximum_width",
"maximum_height",
"wel_item",
"default_window_procedure",
"last_focused_widget",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1500 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"maximize_actions_internal",
"minimize_actions_internal",
"restore_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"internal_default_push_button",
"internal_default_cancel_button",
"internal_current_push_button",
"wel_parent",
"commands",
"scroller",
"resource_name",
"dialog_children",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"menu_bar",
"internal_title",
"current_icon_pixmap",
"internal_class_name",
"internal_icon_name",
"parent_window",
"other_imp",
"post_creation_update_actions",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"help_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"call_show_actions",
"override_movement",
"is_parented_window",
"fire_restore_actions",
"apply_center_dialog",
"is_closeable",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"id",
"original_parent_position",
"internal_object_id",
"result_id",
"resource_id",
"last_x",
"last_y",
"internal_height",
"internal_width",
"maximum_width",
"maximum_height",
"wel_item",
"default_window_procedure",
"last_focused_widget",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1501 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"maximize_actions_internal",
"minimize_actions_internal",
"restore_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"internal_default_push_button",
"internal_default_cancel_button",
"internal_current_push_button",
"wel_parent",
"commands",
"scroller",
"resource_name",
"dialog_children",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"menu_bar",
"internal_title",
"current_icon_pixmap",
"internal_class_name",
"internal_icon_name",
"parent_window",
"other_imp",
"post_creation_update_actions",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"help_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"call_show_actions",
"override_movement",
"is_parented_window",
"fire_restore_actions",
"apply_center_dialog",
"is_closeable",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"id",
"original_parent_position",
"internal_object_id",
"result_id",
"resource_id",
"last_x",
"last_y",
"internal_height",
"internal_width",
"maximum_width",
"maximum_height",
"wel_item",
"default_window_procedure",
"last_focused_widget",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1502 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"maximize_actions_internal",
"minimize_actions_internal",
"restore_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"internal_default_push_button",
"internal_default_cancel_button",
"internal_current_push_button",
"wel_parent",
"commands",
"scroller",
"resource_name",
"dialog_children",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"menu_bar",
"internal_title",
"current_icon_pixmap",
"internal_class_name",
"internal_icon_name",
"parent_window",
"other_imp",
"post_creation_update_actions",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"help_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"call_show_actions",
"override_movement",
"is_parented_window",
"fire_restore_actions",
"apply_center_dialog",
"is_closeable",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"id",
"original_parent_position",
"internal_object_id",
"result_id",
"resource_id",
"last_x",
"last_y",
"internal_height",
"internal_width",
"maximum_width",
"maximum_height",
"wel_item",
"default_window_procedure",
"last_focused_widget",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1503 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"close_request_actions_internal",
"move_actions_internal",
"show_actions_internal",
"hide_actions_internal",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"maximize_actions_internal",
"minimize_actions_internal",
"restore_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"upper_bar",
"lower_bar",
"accel_list",
"accelerators_internal",
"internal_default_push_button",
"internal_default_cancel_button",
"internal_current_push_button",
"wel_parent",
"commands",
"scroller",
"resource_name",
"dialog_children",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"menu_bar",
"internal_title",
"current_icon_pixmap",
"internal_class_name",
"internal_icon_name",
"parent_window",
"other_imp",
"post_creation_update_actions",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"user_can_resize",
"internal_is_border_enabled",
"help_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"call_show_actions",
"override_movement",
"is_parented_window",
"fire_restore_actions",
"apply_center_dialog",
"is_closeable",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"id",
"original_parent_position",
"internal_object_id",
"result_id",
"resource_id",
"last_x",
"last_y",
"internal_height",
"internal_width",
"maximum_width",
"maximum_height",
"wel_item",
"default_window_procedure",
"last_focused_widget",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1504 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_theme_background_requested",
"is_pnd_in_transport",
"is_dnd_in_transport",
"ev_resizing_flags",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1505 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"item_activate_actions_internal",
"item_drop_actions_internal",
"item_deactivate_actions_internal",
"item_select_actions_internal",
"item_deselect_actions_internal",
"row_select_actions_internal",
"column_select_actions_internal",
"row_deselect_actions_internal",
"column_deselect_actions_internal",
"pointer_motion_item_actions_internal",
"pointer_double_press_item_actions_internal",
"pointer_leave_item_actions_internal",
"pointer_enter_item_actions_internal",
"pointer_button_press_item_actions_internal",
"pointer_button_release_item_actions_internal",
"row_expand_actions_internal",
"row_collapse_actions_internal",
"virtual_position_changed_actions_internal",
"virtual_size_changed_actions_internal",
"pre_draw_overlay_actions_internal",
"post_draw_overlay_actions_internal",
"fill_background_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"dynamic_content_function",
"locked_indexes",
"item_veto_pebble_function",
"item_accept_cursor_function",
"item_deny_cursor_function",
"separator_color",
"item_pebble_function",
"activate_window",
"currently_active_item",
"tree_node_connector_color",
"focused_selection_color",
"non_focused_selection_color",
"focused_selection_text_color",
"non_focused_selection_text_color",
"internal_row_data",
"rows",
"columns",
"column_offsets",
"row_offsets",
"row_indexes_to_visible_indexes",
"visible_indexes_to_row_indexes",
"drawable",
"viewport",
"header",
"expand_node_pixmap",
"collapse_node_pixmap",
"static_fixed_viewport",
"static_fixed",
"header_viewport",
"scroll_bar_spacer",
"fixed",
"drawer_internal",
"last_pointed_item",
"shift_key_start_item",
"physical_column_indexes_internal",
"internal_tooltip",
"internal_vertical_scroll_bar",
"internal_horizontal_scroll_bar",
"internal_selected_rows",
"internal_selected_items",
"last_selected_row",
"last_selected_item",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"cell_item",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_header_displayed",
"is_resizing_divider_enabled",
"is_resizing_divider_solid",
"is_horizontal_scrolling_per_item",
"is_vertical_scrolling_per_item",
"is_vertical_overscroll_enabled",
"is_horizontal_overscroll_enabled",
"is_content_partially_dynamic",
"is_row_height_fixed",
"is_column_resize_immediate",
"are_tree_node_connectors_shown",
"are_column_separators_enabled",
"are_row_separators_enabled",
"is_always_selected",
"is_item_tab_navigation_enabled",
"has_horizontal_scrolling_per_item_just_changed",
"has_vertical_scrolling_per_item_just_changed",
"is_item_height_changing",
"is_vertical_scroll_bar_show_requested",
"is_horizontal_scroll_bar_show_requested",
"is_selection_on_click_enabled",
"is_selection_on_single_button_click_enabled",
"is_selection_keyboard_handling_enabled",
"is_tree_enabled",
"is_single_row_selection_enabled",
"is_multiple_row_selection_enabled",
"is_single_item_selection_enabled",
"is_multiple_item_selection_enabled",
"are_columns_drawn_above_rows",
"drawables_have_focus",
"vertical_computation_required",
"horizontal_computation_required",
"horizontal_computation_added_to_once_idle_actions",
"vertical_computation_added_to_once_idle_actions",
"is_full_redraw_on_virtual_position_change_enabled",
"is_locked",
"is_horizontal_offset_set_to_zero_when_items_smaller_than_viewable_width",
"horizontal_redraw_triggered_by_viewport_resize",
"vertical_redraw_triggered_by_viewport_resize",
"is_header_item_resizing",
"pointer_enter_called",
"physical_column_indexes_dirty",
"internal_wm_size_called",
"is_theme_background_requested",
"is_pnd_in_transport",
"is_dnd_in_transport",
"ev_resizing_flags",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"row_height",
"subrow_indent",
"viewable_width",
"viewable_height",
"displayed_column_count",
"physical_column_count",
"invalid_row_index",
"invalid_column_index",
"redraw_object_counter",
"internal_client_x",
"internal_client_y",
"viewport_x_offset",
"viewport_y_offset",
"computed_visible_row_count",
"node_pixmap_width",
"total_tree_node_width",
"first_tree_node_indent",
"tree_subrow_indent",
"last_width_of_header_during_resize_internal",
"last_dashed_line_position",
"last_horizontal_scroll_bar_value",
"last_vertical_scroll_bar_value",
"item_counter",
"row_counter",
"non_displayed_row_count",
"internal_object_id",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1506 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_theme_background_requested",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_horizontal_scroll_bar_visible",
"is_vertical_scroll_bar_visible",
"ev_resizing_flags",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1507 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"vertical_scroll_actions_internal",
"horizontal_scroll_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"new_item_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"internal_merged_radio_button_groups",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"top_level_window_imp",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"background_pixmap_imp",
"last_background_pixmap_imp",
"background_brush_gdip_cached",
"radio_group",
"remove_item_actions",
"item",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_theme_background_requested",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_horizontal_scroll_bar_visible",
"is_vertical_scroll_bar_visible",
"is_in_size_call",
"ev_resizing_flags",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1508 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"original_parent_position",
"last_x",
"last_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1509 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"deselect_actions_internal",
"column_title_click_actions_internal",
"column_resized_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"new_item_actions_internal",
"remove_item_actions_internal",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"ev_children",
"column_titles",
"column_widths",
"column_alignments",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_item_source",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"internal_selected_items",
"image_list",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_actions_called",
"item_is_pnd_source_at_entry",
"call_press_event",
"parent_is_pnd_source",
"item_is_pnd_source",
"item_is_dockable_source",
"not_is_tabable_from",
"internal_selected_items_uptodate",
"smart_resize",
"multiple_selection_enabled",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
"internal_object_id",
"id",
"wel_column_count",
"last_x",
"last_y",
"default_row_height",
"row_height",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1510 [] =
{
"docked_actions_internal",
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"new_item_actions_internal",
"remove_item_actions_internal",
"real_source",
"actual_source",
"orig_cursor",
"veto_dock_function",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_item_source",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"default_imagelist",
"hot_imagelist",
"radio_group",
"ev_children",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_minwidth_recomputation_needed",
"is_minheight_recomputation_needed",
"is_notify_originator",
"is_in_min_height",
"is_in_min_width",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_docking_enabled",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"has_bitmap",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_actions_called",
"item_is_pnd_source_at_entry",
"call_press_event",
"parent_is_pnd_source",
"item_is_pnd_source",
"item_is_dockable_source",
"not_is_tabable_from",
"is_in_reset_button",
"has_false_image_list",
"is_vertical",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"index",
"original_parent_position",
"internal_object_id",
"id",
"last_bitmap_index",
"last_string_index",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1511 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"internal_text",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"text_alignment",
"internal_object_id",
"id",
"last_x",
"last_y",
"vertical_text_alignment",
"text_height",
"text_width",
"angle",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1512 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"expose_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"internal_background_brush",
"internal_brush",
"internal_pen",
"tile",
"clip_area",
"foreground_color_internal",
"background_color_internal",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"internal_paint_dc",
"screen_dc",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"focus_on_press_disabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"internal_initialized_pen",
"internal_initialized_text_color",
"internal_initialized_font",
"internal_initialized_background_brush",
"internal_initialized_brush",
"dashed_line_style",
"not_is_tabable_from",
"in_paint",
"to_be_cleared",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"wel_drawing_mode",
"line_width",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1513 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"item_resize_actions_internal",
"item_resize_start_actions_internal",
"item_resize_end_actions_internal",
"item_pointer_button_press_actions_internal",
"item_pointer_double_press_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"new_item_actions_internal",
"remove_item_actions_internal",
"real_source",
"actual_source",
"orig_cursor",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_item_source",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"image_list",
"ev_children",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_actions_called",
"item_is_pnd_source_at_entry",
"call_press_event",
"parent_is_pnd_source",
"item_is_pnd_source",
"item_is_dockable_source",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
"internal_object_id",
"id",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1514 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"expose_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"internal_background_brush",
"internal_brush",
"internal_pen",
"tile",
"clip_area",
"foreground_color_internal",
"background_color_internal",
"mask_dc",
"transparent_color",
"internal_bitmap",
"internal_mask_bitmap",
"palette",
"dc",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"internal_initialized_pen",
"internal_initialized_text_color",
"internal_initialized_font",
"internal_initialized_background_brush",
"internal_initialized_brush",
"dashed_line_style",
"not_is_tabable_from",
"parented",
"press_action",
"release_action",
"motion_action",
"internal_tabable_info",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"wel_drawing_mode",
"line_width",
"height",
"width",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1515 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"vertical_scroll_actions_internal",
"horizontal_scroll_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"select_actions_internal",
"deselect_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"new_item_actions_internal",
"remove_item_actions_internal",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_item_source",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"all_ev_children",
"image_list",
"ev_children",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_actions_called",
"item_is_pnd_source_at_entry",
"call_press_event",
"parent_is_pnd_source",
"item_is_pnd_source",
"item_is_dockable_source",
"not_is_tabable_from",
"removing_item",
"expand_called_manually",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
"internal_object_id",
"id",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"last_item",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1516 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"vertical_scroll_actions_internal",
"horizontal_scroll_actions_internal",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"check_actions_internal",
"uncheck_actions_internal",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"select_actions_internal",
"deselect_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"new_item_actions_internal",
"remove_item_actions_internal",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_item_source",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"all_ev_children",
"image_list",
"ev_children",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_actions_called",
"item_is_pnd_source_at_entry",
"call_press_event",
"parent_is_pnd_source",
"item_is_pnd_source",
"item_is_dockable_source",
"not_is_tabable_from",
"removing_item",
"expand_called_manually",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
"internal_object_id",
"id",
"last_x",
"last_y",
"click_original_x_pos",
"click_original_y_pos",
"wel_item",
"default_window_procedure",
"last_item",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1517 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"deselect_actions_internal",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"new_item_actions_internal",
"remove_item_actions_internal",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"image_list",
"ev_children",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_item_source",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"selected_items_at_disable_sensitive",
"internal_selected_items",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"default_key_processing_disabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_actions_called",
"item_is_pnd_source_at_entry",
"call_press_event",
"parent_is_pnd_source",
"item_is_pnd_source",
"item_is_dockable_source",
"not_is_tabable_from",
"internal_selected_items_uptodate",
"multiple_selection_enabled",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
"internal_object_id",
"id",
"column_count",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1518 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"check_actions_internal",
"uncheck_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"deselect_actions_internal",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"new_item_actions_internal",
"remove_item_actions_internal",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"image_list",
"ev_children",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_item_source",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"selected_items_at_disable_sensitive",
"internal_selected_items",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"default_key_processing_disabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_actions_called",
"item_is_pnd_source_at_entry",
"call_press_event",
"parent_is_pnd_source",
"item_is_pnd_source",
"item_is_dockable_source",
"not_is_tabable_from",
"internal_selected_items_uptodate",
"multiple_selection_enabled",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
"internal_object_id",
"id",
"column_count",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1519 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"private_pixmap",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"internal_bitmap",
"internal_icon",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"mouse_on_button",
"is_default_push_button",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"text_alignment",
"internal_object_id",
"id",
"current_pixmap",
"last_x",
"last_y",
"extra_width",
"wel_item",
"default_window_procedure",
"open_theme",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1520 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"radio_group",
"private_pixmap",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"internal_bitmap",
"internal_icon",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"mouse_on_button",
"is_default_push_button",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"text_alignment",
"internal_object_id",
"id",
"current_pixmap",
"last_x",
"last_y",
"extra_width",
"wel_item",
"default_window_procedure",
"open_theme",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1521 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"private_pixmap",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"internal_bitmap",
"internal_icon",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"mouse_on_button",
"is_default_push_button",
"is_selected",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"text_alignment",
"internal_object_id",
"id",
"current_pixmap",
"last_x",
"last_y",
"extra_width",
"wel_item",
"default_window_procedure",
"open_theme",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1522 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"select_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"private_pixmap",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"internal_bitmap",
"internal_icon",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"mouse_on_button",
"is_default_push_button",
"is_selected",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"text_alignment",
"internal_object_id",
"id",
"current_pixmap",
"last_x",
"last_y",
"extra_width",
"wel_item",
"default_window_procedure",
"open_theme",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1523 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1524 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1525 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1526 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"override_context_menu",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"original_parent_position",
"last_x",
"last_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1527 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"drop_down_actions_internal",
"list_hidden_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"return_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"deselect_actions_internal",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"new_item_actions_internal",
"remove_item_actions_internal",
"real_source",
"actual_source",
"orig_cursor",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"image_list",
"ev_children",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_item_source",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"text_field",
"combo",
"old_selected_item",
"last_edit_change",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"press_actions_called",
"item_is_pnd_source_at_entry",
"call_press_event",
"parent_is_pnd_source",
"item_is_pnd_source",
"item_is_dockable_source",
"not_is_tabable_from",
"override_context_menu",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"index",
"original_parent_position",
"pixmaps_width",
"pixmaps_height",
"list_height_hint",
"list_width_hint",
"internal_object_id",
"id",
"last_x",
"last_y",
"internal_list_minimum_width",
"text_alignment",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1528 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"return_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"scroll_caret_at_selection",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"override_context_menu",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"id",
"last_x",
"last_y",
"text_alignment",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1529 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"return_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"scroll_caret_at_selection",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"override_context_menu",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"id",
"last_x",
"last_y",
"text_alignment",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1530 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"scroll_caret_at_selection",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"override_context_menu",
"text_up_to_date",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"id",
"last_x",
"last_y",
"internal_text_length",
"internal_wel_text_length",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1531 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"caret_move_actions_internal",
"selection_change_actions_internal",
"file_access_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"tab_positions",
"rich_text",
"internal_text",
"last_fontname",
"all_fonts",
"all_colors",
"all_formats",
"all_paragraph_formats",
"all_paragraph_format_keys",
"all_paragraph_indexes",
"current_format",
"format_stack",
"plain_text",
"paragraph_start_indexes",
"paragraph_formats",
"hashed_formats",
"format_offsets",
"buffered_text",
"formats",
"heights",
"formats_index",
"start_formats",
"end_formats",
"hashed_colors",
"color_offset",
"back_color_offset",
"color_text",
"hashed_fonts",
"font_offset",
"font_text",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"buffer_locked_in_format_mode",
"buffer_locked_in_append_mode",
"implementation_last_load_successful",
"last_load_successful",
"first_color_is_auto",
"is_current_format_underlined",
"is_current_format_striked_through",
"is_current_format_bold",
"is_current_format_italic",
"internal_wm_size_called",
"scroll_caret_at_selection",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"override_context_menu",
"text_up_to_date",
"internal_actions_blocked",
"must_restore_selection",
"must_fire_final_selection",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"highest_read_char",
"last_colorindex",
"last_fontindex",
"last_fontcharset",
"last_fontfamily",
"last_colorred",
"last_colorgreen",
"last_colorblue",
"number_of_characters_opened",
"current_depth",
"main_iterator",
"temp_iterator",
"lowest_buffered_value",
"highest_buffered_value",
"color_count",
"font_count",
"current_vertical_offset",
"internal_object_id",
"id",
"last_x",
"last_y",
"internal_text_length",
"internal_wel_text_length",
"original_caret_position",
"original_selection_start",
"original_selection_end",
"last_known_caret_position",
"tab_width",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1532 [] =
{
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"original_parent_position",
"last_x",
"last_y",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1533 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"text_change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"return_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"private_font",
"private_wel_font",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"wel_parent",
"commands",
"scroller",
"wnd_class",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"internal_arrows_control",
"internal_text_field",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"last_x",
"last_y",
"last_change_value",
"step",
"leap",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1534 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"id",
"last_x",
"last_y",
"leap",
"step",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1535 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"id",
"last_x",
"last_y",
"leap",
"step",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1536 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"id",
"last_x",
"last_y",
"leap",
"step",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1537 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"id",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1538 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"id",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1539 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"wel_parent",
"commands",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"id",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1540 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"wel_parent",
"commands",
"scroll_info_struct",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"id",
"step",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1541 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"wel_parent",
"commands",
"scroll_info_struct",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"id",
"step",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1542 [] =
{
"has_return_value_area",
"message_return_value_area",
"default_processing_area",
"dock_started_actions_internal",
"dock_ended_actions_internal",
"change_actions_internal",
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"file_drop_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"pointer_button_release_actions_internal",
"pointer_enter_actions_internal",
"mouse_wheel_actions_internal",
"pointer_leave_actions_internal",
"key_press_actions_internal",
"key_press_string_actions_internal",
"key_release_actions_internal",
"focus_in_actions_internal",
"focus_out_actions_internal",
"resize_actions_internal",
"tool_info",
"internal_tooltip_string",
"interface",
"internal_help_context",
"child_cell",
"real_source",
"actual_source",
"orig_cursor",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"actual_drop_target_agent",
"real_target",
"default_key_processing_handler",
"value_range",
"wel_parent",
"commands",
"scroll_info_struct",
"pnd_stored_cursor",
"original_top_level_window_imp",
"background_color_imp",
"foreground_color_imp",
"top_level_window_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"is_dockable",
"not_external_docking_enabled",
"not_is_external_docking_relative",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"internal_wm_size_called",
"is_pnd_in_transport",
"is_dnd_in_transport",
"not_is_tabable_from",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"level_count",
"original_x",
"original_y",
"original_parent_position",
"internal_object_id",
"id",
"step",
"last_x",
"last_y",
"wel_item",
"default_window_procedure",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1543 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"real_text",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_sensitive",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"id",
"internal_object_id",
"accelerator_text_position",
"plain_text_position",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1544 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"real_text",
"parent_imp",
"radio_group",
"awaiting_movement",
"rubber_band_is_drawn",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_sensitive",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"id",
"internal_object_id",
"accelerator_text_position",
"plain_text_position",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1545 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"radio_group",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"real_text",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_sensitive",
"is_selected",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"id",
"internal_object_id",
"accelerator_text_position",
"plain_text_position",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1546 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"item_select_actions_internal",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"new_item_actions_internal",
"remove_item_actions_internal",
"radio_group",
"ev_children",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"real_text",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"shared",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_sensitive",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"id",
"index",
"internal_object_id",
"accelerator_text_position",
"plain_text_position",
"wel_item",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1547 [] =
{
"pick_actions_internal",
"pick_ended_actions_internal",
"conforming_pick_actions_internal",
"drop_actions_internal",
"current_wel_cursor",
"cursor_pixmap",
"source_being_docked",
"originating_source",
"dockable_dialog_target",
"select_actions_internal",
"pointer_motion_actions_internal",
"pointer_button_press_actions_internal",
"pointer_double_press_actions_internal",
"interface",
"private_pixmap",
"pebble",
"pebble_function",
"configurable_target_menu_handler",
"accept_cursor",
"deny_cursor",
"pnd_stored_cursor",
"original_top_level_window_imp",
"pnd_original_parent",
"real_text",
"parent_imp",
"awaiting_movement",
"rubber_band_is_drawn",
"internal_non_sensitive",
"is_transport_enabled",
"internal_pebble_positioning_enabled",
"is_pnd_in_transport",
"is_dnd_in_transport",
"is_sensitive",
"is_selected",
"press_action",
"release_action",
"motion_action",
"state_flags",
"user_interface_mode",
"original_x_offset",
"original_y_offset",
"pointer_x",
"pointer_y",
"pick_x",
"pick_y",
"original_x",
"original_y",
"id",
"internal_object_id",
"accelerator_text_position",
"plain_text_position",
"original_x_tilt",
"original_y_tilt",
"original_pressure",
};

char *names1549 [] =
{
"operands",
"closed_operands",
"open_map",
"open_types",
"is_target_closed",
"is_basic",
"open_count",
"routine_id",
"written_type_id_inline_agent",
"rout_disp",
"calc_rout_addr",
"encaps_rout_disp",
};

char *names1550 [] =
{
"to_pointer",
};

char *names1551 [] =
{
"to_pointer",
};

char *names1552 [] =
{
"internal_name",
};

char *names1553 [] =
{
"internal_name",
};

char *names1554 [] =
{
"operands",
"closed_operands",
"open_map",
"open_types",
"is_target_closed",
"is_basic",
"open_count",
"routine_id",
"written_type_id_inline_agent",
"rout_disp",
"calc_rout_addr",
"encaps_rout_disp",
};

char *names1555 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names1559 [] =
{
"operands",
"closed_operands",
"open_map",
"open_types",
"is_target_closed",
"is_basic",
"last_result",
"open_count",
"routine_id",
"written_type_id_inline_agent",
"rout_disp",
"calc_rout_addr",
"encaps_rout_disp",
};

char *names1560 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names1561 [] =
{
"object_comparison",
};

char *names1562 [] =
{
"object_comparison",
};

char *names1563 [] =
{
"object_comparison",
};

char *names1564 [] =
{
"object_comparison",
};

char *names1565 [] =
{
"object_comparison",
};

char *names1566 [] =
{
"object_comparison",
};

char *names1567 [] =
{
"object_comparison",
};

char *names1568 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1569 [] =
{
"object_comparison",
};

char *names1570 [] =
{
"object_comparison",
};

char *names1571 [] =
{
"object_comparison",
};

char *names1572 [] =
{
"object_comparison",
};

char *names1573 [] =
{
"object_comparison",
};

char *names1574 [] =
{
"object_comparison",
};

char *names1575 [] =
{
"object_comparison",
};

char *names1576 [] =
{
"object_comparison",
};

char *names1577 [] =
{
"object_comparison",
};

char *names1578 [] =
{
"object_comparison",
};

char *names1579 [] =
{
"object_comparison",
};

char *names1580 [] =
{
"area",
};

char *names1581 [] =
{
"object_comparison",
};

char *names1582 [] =
{
"object_comparison",
};

char *names1584 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names1585 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names1586 [] =
{
"area_v2",
"object_comparison",
"in_operation",
"index",
};

char *names1587 [] =
{
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names1588 [] =
{
"object_comparison",
};

char *names1589 [] =
{
"object_comparison",
};

char *names1590 [] =
{
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names1591 [] =
{
"active",
"after",
"before",
};

char *names1592 [] =
{
"item",
"right",
};

char *names1593 [] =
{
"item",
};

char *names1594 [] =
{
"target",
"active",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names1595 [] =
{
"operands",
"closed_operands",
"open_map",
"open_types",
"last_result",
"is_target_closed",
"is_basic",
"open_count",
"routine_id",
"written_type_id_inline_agent",
"rout_disp",
"calc_rout_addr",
"encaps_rout_disp",
};

char *names1596 [] =
{
"operands",
"closed_operands",
"open_map",
"open_types",
"is_target_closed",
"is_basic",
"last_result",
"open_count",
"routine_id",
"written_type_id_inline_agent",
"rout_disp",
"calc_rout_addr",
"encaps_rout_disp",
};

char *names1597 [] =
{
"area",
"object_comparison",
"out_index",
"count",
};

char *names1598 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names1599 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1600 [] =
{
"found_item",
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_item",
"ht_deleted_key",
"object_comparison",
"hash_table_version_64",
"has_default",
"is_case_insensitive",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"count",
};

char *names1601 [] =
{
"found_item",
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_item",
"ht_deleted_key",
"object_comparison",
"hash_table_version_64",
"has_default",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"count",
};

char *names1602 [] =
{
"target",
"is_reversed",
"version",
"iteration_position",
"first_index",
"last_index",
"step",
};

char *names1604 [] =
{
"object_comparison",
};

char *names1606 [] =
{
"interface",
"state_flags",
"index",
};

char *names1607 [] =
{
"interface",
"state_flags",
"index",
};

char *names1608 [] =
{
"item",
"after",
"before",
};

char *names1609 [] =
{
"data",
"implementation",
"internal_name",
"object_comparison",
};

char *names1610 [] =
{
"data",
"implementation",
"internal_name",
"object_comparison",
};

char *names1611 [] =
{
"interface",
"new_item_actions_internal",
"remove_item_actions_internal",
"state_flags",
"index",
};

char *names1612 [] =
{
"interface",
"state_flags",
"index",
};

char *names1614 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names1615 [] =
{
"object_comparison",
};

char *names1616 [] =
{
"object_comparison",
};

char *names1617 [] =
{
"object_comparison",
};

char *names1618 [] =
{
"object_comparison",
};

char *names1619 [] =
{
"object_comparison",
};

char *names1620 [] =
{
"object_comparison",
};

char *names1621 [] =
{
"object_comparison",
};

char *names1622 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names1626 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1627 [] =
{
"object_comparison",
};

char *names1628 [] =
{
"object_comparison",
};

char *names1629 [] =
{
"object_comparison",
};

char *names1630 [] =
{
"object_comparison",
};

char *names1631 [] =
{
"object_comparison",
};

char *names1632 [] =
{
"object_comparison",
};

char *names1633 [] =
{
"object_comparison",
};

char *names1634 [] =
{
"object_comparison",
};

char *names1635 [] =
{
"object_comparison",
};

char *names1636 [] =
{
"object_comparison",
};

char *names1637 [] =
{
"object_comparison",
};

char *names1638 [] =
{
"area",
};

char *names1640 [] =
{
"internal_name",
};

char *names1641 [] =
{
"object_comparison",
};

char *names1642 [] =
{
"object_comparison",
};

char *names1643 [] =
{
"to_pointer",
};

char *names1644 [] =
{
"to_pointer",
};

char *names1645 [] =
{
"internal_name",
};

char *names1646 [] =
{
"internal_name",
};

char *names1647 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1648 [] =
{
"object_comparison",
};

char *names1649 [] =
{
"area_v2",
"add_actions",
"remove_actions",
"internal_add_actions",
"internal_remove_actions",
"object_comparison",
"in_operation",
"index",
};

char *names1650 [] =
{
"area_v2",
"add_actions",
"remove_actions",
"object_comparison",
"in_operation",
"index",
};

char *names1651 [] =
{
"object_comparison",
};

char *names1652 [] =
{
"object_comparison",
};

char *names1653 [] =
{
"object_comparison",
};

char *names1654 [] =
{
"object_comparison",
};

char *names1655 [] =
{
"object_comparison",
};

char *names1656 [] =
{
"object_comparison",
};

char *names1657 [] =
{
"object_comparison",
};

char *names1658 [] =
{
"object_comparison",
};

char *names1659 [] =
{
"object_comparison",
};

char *names1663 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names1665 [] =
{
"found_item",
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_item",
"object_comparison",
"hash_table_version_64",
"has_default",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"ht_deleted_key",
"count",
};

char *names1666 [] =
{
"target",
"is_reversed",
"version",
"iteration_position",
"first_index",
"last_index",
"step",
};

char *names1668 [] =
{
"internal_name",
};

char *names1670 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names1671 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1672 [] =
{
"object_comparison",
};

char *names1673 [] =
{
"object_comparison",
};

char *names1674 [] =
{
"object_comparison",
};

char *names1675 [] =
{
"object_comparison",
};

char *names1676 [] =
{
"object_comparison",
};

char *names1677 [] =
{
"object_comparison",
};

char *names1678 [] =
{
"object_comparison",
};

char *names1679 [] =
{
"object_comparison",
};

char *names1680 [] =
{
"object_comparison",
};

char *names1681 [] =
{
"area",
};

char *names1682 [] =
{
"object_comparison",
};

char *names1683 [] =
{
"object_comparison",
};

char *names1687 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names1691 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names1692 [] =
{
"object_comparison",
};

char *names1693 [] =
{
"object_comparison",
};

char *names1694 [] =
{
"object_comparison",
};

char *names1695 [] =
{
"object_comparison",
};

char *names1696 [] =
{
"object_comparison",
};

char *names1697 [] =
{
"object_comparison",
};

char *names1698 [] =
{
"object_comparison",
};

char *names1699 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1700 [] =
{
"object_comparison",
};

char *names1701 [] =
{
"object_comparison",
};

char *names1702 [] =
{
"object_comparison",
};

char *names1703 [] =
{
"object_comparison",
};

char *names1704 [] =
{
"object_comparison",
};

char *names1705 [] =
{
"object_comparison",
};

char *names1706 [] =
{
"object_comparison",
};

char *names1707 [] =
{
"object_comparison",
};

char *names1708 [] =
{
"object_comparison",
};

char *names1709 [] =
{
"object_comparison",
};

char *names1710 [] =
{
"object_comparison",
};

char *names1711 [] =
{
"area",
};

char *names1712 [] =
{
"object_comparison",
};

char *names1713 [] =
{
"object_comparison",
};

char *names1715 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names1717 [] =
{
"internal_name",
};

char *names1718 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names1722 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names1723 [] =
{
"object_comparison",
};

char *names1724 [] =
{
"object_comparison",
};

char *names1725 [] =
{
"object_comparison",
};

char *names1726 [] =
{
"object_comparison",
};

char *names1727 [] =
{
"object_comparison",
};

char *names1728 [] =
{
"object_comparison",
};

char *names1729 [] =
{
"object_comparison",
};

char *names1730 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1731 [] =
{
"object_comparison",
};

char *names1732 [] =
{
"object_comparison",
};

char *names1733 [] =
{
"object_comparison",
};

char *names1734 [] =
{
"object_comparison",
};

char *names1735 [] =
{
"object_comparison",
};

char *names1736 [] =
{
"object_comparison",
};

char *names1737 [] =
{
"object_comparison",
};

char *names1738 [] =
{
"object_comparison",
};

char *names1739 [] =
{
"object_comparison",
};

char *names1740 [] =
{
"object_comparison",
};

char *names1741 [] =
{
"object_comparison",
};

char *names1742 [] =
{
"area",
};

char *names1743 [] =
{
"object_comparison",
};

char *names1744 [] =
{
"object_comparison",
};

char *names1746 [] =
{
"breakable_info",
"object",
"index",
"type",
"value",
};

char *names1747 [] =
{
"allocated_objects",
"allocated_objects_number",
"found_object_index",
"cache_time",
"successful_cache",
"index_lightest_object",
};

char *names1748 [] =
{
"breakable_info",
"rt_type",
"position",
"type",
"callstack_depth",
"value",
};

char *names1749 [] =
{
"to_pointer",
};

char *names1750 [] =
{
"to_pointer",
};

char *names1751 [] =
{
"internal_name",
};

char *names1752 [] =
{
"internal_name",
};

char *names1754 [] =
{
"internal_name",
};

char *names1755 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names1758 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names1759 [] =
{
"object_comparison",
};

char *names1760 [] =
{
"object_comparison",
};

char *names1761 [] =
{
"object_comparison",
};

char *names1762 [] =
{
"object_comparison",
};

char *names1763 [] =
{
"object_comparison",
};

char *names1764 [] =
{
"object_comparison",
};

char *names1765 [] =
{
"object_comparison",
};

char *names1766 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1767 [] =
{
"object_comparison",
};

char *names1768 [] =
{
"object_comparison",
};

char *names1769 [] =
{
"object_comparison",
};

char *names1770 [] =
{
"object_comparison",
};

char *names1771 [] =
{
"object_comparison",
};

char *names1772 [] =
{
"object_comparison",
};

char *names1773 [] =
{
"object_comparison",
};

char *names1774 [] =
{
"object_comparison",
};

char *names1775 [] =
{
"object_comparison",
};

char *names1776 [] =
{
"object_comparison",
};

char *names1777 [] =
{
"object_comparison",
};

char *names1778 [] =
{
"area",
};

char *names1779 [] =
{
"object_comparison",
};

char *names1780 [] =
{
"object_comparison",
};

char *names1782 [] =
{
"internal_name",
};

char *names1783 [] =
{
"found_item",
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_item",
"object_comparison",
"hash_table_version_64",
"has_default",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"count",
"ht_deleted_key",
};

char *names1784 [] =
{
"target",
"is_reversed",
"version",
"iteration_position",
"first_index",
"last_index",
"step",
};

char *names1786 [] =
{
"internal_name",
};

char *names1788 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names1792 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names1793 [] =
{
"object_comparison",
};

char *names1794 [] =
{
"object_comparison",
};

char *names1795 [] =
{
"object_comparison",
};

char *names1796 [] =
{
"object_comparison",
};

char *names1797 [] =
{
"object_comparison",
};

char *names1798 [] =
{
"object_comparison",
};

char *names1799 [] =
{
"object_comparison",
};

char *names1800 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1801 [] =
{
"object_comparison",
};

char *names1802 [] =
{
"object_comparison",
};

char *names1803 [] =
{
"object_comparison",
};

char *names1804 [] =
{
"object_comparison",
};

char *names1805 [] =
{
"object_comparison",
};

char *names1806 [] =
{
"object_comparison",
};

char *names1807 [] =
{
"object_comparison",
};

char *names1808 [] =
{
"object_comparison",
};

char *names1809 [] =
{
"object_comparison",
};

char *names1810 [] =
{
"object_comparison",
};

char *names1811 [] =
{
"object_comparison",
};

char *names1812 [] =
{
"area",
};

char *names1813 [] =
{
"object_comparison",
};

char *names1814 [] =
{
"object_comparison",
};

char *names1816 [] =
{
"object_comparison",
};

char *names1819 [] =
{
"internal_name",
};

char *names1820 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names1824 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names1825 [] =
{
"object_comparison",
};

char *names1826 [] =
{
"object_comparison",
};

char *names1827 [] =
{
"object_comparison",
};

char *names1828 [] =
{
"object_comparison",
};

char *names1829 [] =
{
"object_comparison",
};

char *names1830 [] =
{
"object_comparison",
};

char *names1831 [] =
{
"object_comparison",
};

char *names1832 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1833 [] =
{
"object_comparison",
};

char *names1834 [] =
{
"object_comparison",
};

char *names1835 [] =
{
"object_comparison",
};

char *names1836 [] =
{
"object_comparison",
};

char *names1837 [] =
{
"object_comparison",
};

char *names1838 [] =
{
"object_comparison",
};

char *names1839 [] =
{
"object_comparison",
};

char *names1840 [] =
{
"object_comparison",
};

char *names1841 [] =
{
"object_comparison",
};

char *names1842 [] =
{
"object_comparison",
};

char *names1843 [] =
{
"object_comparison",
};

char *names1844 [] =
{
"area",
};

char *names1845 [] =
{
"object_comparison",
};

char *names1846 [] =
{
"object_comparison",
};

char *names1848 [] =
{
"breakable_info",
"object",
"index",
"type",
"value",
};

char *names1849 [] =
{
"breakable_info",
"rt_type",
"position",
"type",
"callstack_depth",
"value",
};

char *names1850 [] =
{
"to_pointer",
};

char *names1851 [] =
{
"to_pointer",
};

char *names1852 [] =
{
"internal_name",
};

char *names1854 [] =
{
"internal_name",
};

char *names1855 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names1859 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names1860 [] =
{
"object_comparison",
};

char *names1861 [] =
{
"object_comparison",
};

char *names1862 [] =
{
"object_comparison",
};

char *names1863 [] =
{
"object_comparison",
};

char *names1864 [] =
{
"object_comparison",
};

char *names1865 [] =
{
"object_comparison",
};

char *names1866 [] =
{
"object_comparison",
};

char *names1867 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1868 [] =
{
"object_comparison",
};

char *names1869 [] =
{
"object_comparison",
};

char *names1870 [] =
{
"object_comparison",
};

char *names1871 [] =
{
"object_comparison",
};

char *names1872 [] =
{
"object_comparison",
};

char *names1873 [] =
{
"object_comparison",
};

char *names1874 [] =
{
"object_comparison",
};

char *names1875 [] =
{
"object_comparison",
};

char *names1876 [] =
{
"object_comparison",
};

char *names1877 [] =
{
"object_comparison",
};

char *names1878 [] =
{
"object_comparison",
};

char *names1879 [] =
{
"area",
};

char *names1880 [] =
{
"object_comparison",
};

char *names1881 [] =
{
"object_comparison",
};

char *names1883 [] =
{
"to_pointer",
};

char *names1884 [] =
{
"to_pointer",
};

char *names1885 [] =
{
"internal_name",
};

char *names1886 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names1887 [] =
{
"to_pointer",
};

char *names1888 [] =
{
"to_pointer",
};

char *names1889 [] =
{
"internal_name",
};

char *names1890 [] =
{
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_key",
"object_comparison",
"hash_table_version_64",
"has_default",
"found_item",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"ht_deleted_item",
"count",
};

char *names1891 [] =
{
"target",
"is_reversed",
"version",
"iteration_position",
"first_index",
"last_index",
"step",
};

char *names1893 [] =
{
"object_comparison",
};

char *names1895 [] =
{
"internal_name",
};

char *names1896 [] =
{
"internal_name",
};

char *names1898 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names1902 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names1903 [] =
{
"object_comparison",
};

char *names1904 [] =
{
"object_comparison",
};

char *names1905 [] =
{
"object_comparison",
};

char *names1906 [] =
{
"object_comparison",
};

char *names1907 [] =
{
"object_comparison",
};

char *names1908 [] =
{
"object_comparison",
};

char *names1909 [] =
{
"object_comparison",
};

char *names1910 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1911 [] =
{
"object_comparison",
};

char *names1912 [] =
{
"object_comparison",
};

char *names1913 [] =
{
"object_comparison",
};

char *names1914 [] =
{
"object_comparison",
};

char *names1915 [] =
{
"object_comparison",
};

char *names1916 [] =
{
"object_comparison",
};

char *names1917 [] =
{
"object_comparison",
};

char *names1918 [] =
{
"object_comparison",
};

char *names1919 [] =
{
"object_comparison",
};

char *names1920 [] =
{
"object_comparison",
};

char *names1921 [] =
{
"object_comparison",
};

char *names1922 [] =
{
"area",
};

char *names1923 [] =
{
"object_comparison",
};

char *names1924 [] =
{
"object_comparison",
};

char *names1926 [] =
{
"to_pointer",
};

char *names1927 [] =
{
"to_pointer",
};

char *names1928 [] =
{
"internal_name",
};

char *names1929 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names1930 [] =
{
"breakable_info",
"object",
"index",
"type",
"value",
};

char *names1931 [] =
{
"item",
"less_than_comparator",
};

char *names1933 [] =
{
"internal_name",
};

char *names1934 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names1938 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names1939 [] =
{
"object_comparison",
};

char *names1940 [] =
{
"object_comparison",
};

char *names1941 [] =
{
"object_comparison",
};

char *names1942 [] =
{
"object_comparison",
};

char *names1943 [] =
{
"object_comparison",
};

char *names1944 [] =
{
"object_comparison",
};

char *names1945 [] =
{
"object_comparison",
};

char *names1946 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1947 [] =
{
"object_comparison",
};

char *names1948 [] =
{
"object_comparison",
};

char *names1949 [] =
{
"object_comparison",
};

char *names1950 [] =
{
"object_comparison",
};

char *names1951 [] =
{
"object_comparison",
};

char *names1952 [] =
{
"object_comparison",
};

char *names1953 [] =
{
"object_comparison",
};

char *names1954 [] =
{
"object_comparison",
};

char *names1955 [] =
{
"object_comparison",
};

char *names1956 [] =
{
"object_comparison",
};

char *names1957 [] =
{
"object_comparison",
};

char *names1958 [] =
{
"area",
};

char *names1959 [] =
{
"object_comparison",
};

char *names1960 [] =
{
"object_comparison",
};

char *names1962 [] =
{
"item",
};

char *names1963 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names1964 [] =
{
"internal_name",
};

char *names1965 [] =
{
"internal_name",
};

char *names1967 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names1971 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names1972 [] =
{
"object_comparison",
};

char *names1973 [] =
{
"object_comparison",
};

char *names1974 [] =
{
"object_comparison",
};

char *names1975 [] =
{
"object_comparison",
};

char *names1976 [] =
{
"object_comparison",
};

char *names1977 [] =
{
"object_comparison",
};

char *names1978 [] =
{
"object_comparison",
};

char *names1979 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names1980 [] =
{
"object_comparison",
};

char *names1981 [] =
{
"object_comparison",
};

char *names1982 [] =
{
"object_comparison",
};

char *names1983 [] =
{
"object_comparison",
};

char *names1984 [] =
{
"object_comparison",
};

char *names1985 [] =
{
"object_comparison",
};

char *names1986 [] =
{
"object_comparison",
};

char *names1987 [] =
{
"object_comparison",
};

char *names1988 [] =
{
"object_comparison",
};

char *names1989 [] =
{
"object_comparison",
};

char *names1990 [] =
{
"object_comparison",
};

char *names1991 [] =
{
"area",
};

char *names1992 [] =
{
"object_comparison",
};

char *names1993 [] =
{
"object_comparison",
};

char *names1995 [] =
{
"to_pointer",
};

char *names1996 [] =
{
"to_pointer",
};

char *names1997 [] =
{
"internal_name",
};

char *names1998 [] =
{
"content",
"keys",
"indexes_map",
"deleted_marks",
"object_comparison",
"hash_table_version_64",
"has_default",
"found_item",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"ht_deleted_item",
"ht_deleted_key",
"count",
};

char *names1999 [] =
{
"target",
"is_reversed",
"version",
"iteration_position",
"first_index",
"last_index",
"step",
};

char *names2002 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names2003 [] =
{
"shared",
"count",
"item_size",
"item",
};

char *names2004 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names2005 [] =
{
"parent",
"item",
"left_child",
"right_child",
"object_comparison",
"child_index",
};

char *names2006 [] =
{
"area",
"object_comparison",
};

char *names2007 [] =
{
"object_comparison",
};

char *names2008 [] =
{
"parent",
"object_comparison",
};

char *names2009 [] =
{
"parent",
"item",
"left_child",
"right_child",
"object_comparison",
"child_index",
};

char *names2010 [] =
{
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names2011 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names2013 [] =
{
"internal_name",
};

char *names2014 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names2018 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names2019 [] =
{
"object_comparison",
};

char *names2020 [] =
{
"object_comparison",
};

char *names2021 [] =
{
"object_comparison",
};

char *names2022 [] =
{
"object_comparison",
};

char *names2023 [] =
{
"object_comparison",
};

char *names2024 [] =
{
"object_comparison",
};

char *names2025 [] =
{
"object_comparison",
};

char *names2026 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names2027 [] =
{
"object_comparison",
};

char *names2028 [] =
{
"object_comparison",
};

char *names2029 [] =
{
"object_comparison",
};

char *names2030 [] =
{
"object_comparison",
};

char *names2031 [] =
{
"object_comparison",
};

char *names2032 [] =
{
"object_comparison",
};

char *names2033 [] =
{
"object_comparison",
};

char *names2034 [] =
{
"object_comparison",
};

char *names2035 [] =
{
"object_comparison",
};

char *names2036 [] =
{
"object_comparison",
};

char *names2037 [] =
{
"object_comparison",
};

char *names2038 [] =
{
"area",
};

char *names2039 [] =
{
"object_comparison",
};

char *names2040 [] =
{
"object_comparison",
};

char *names2042 [] =
{
"internal_name",
};

char *names2043 [] =
{
"area",
};

char *names2045 [] =
{
"internal_name",
};

char *names2046 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names2050 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names2051 [] =
{
"object_comparison",
};

char *names2052 [] =
{
"object_comparison",
};

char *names2053 [] =
{
"object_comparison",
};

char *names2054 [] =
{
"object_comparison",
};

char *names2055 [] =
{
"object_comparison",
};

char *names2056 [] =
{
"object_comparison",
};

char *names2057 [] =
{
"object_comparison",
};

char *names2058 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names2059 [] =
{
"object_comparison",
};

char *names2060 [] =
{
"object_comparison",
};

char *names2061 [] =
{
"object_comparison",
};

char *names2062 [] =
{
"object_comparison",
};

char *names2063 [] =
{
"object_comparison",
};

char *names2064 [] =
{
"object_comparison",
};

char *names2065 [] =
{
"object_comparison",
};

char *names2066 [] =
{
"object_comparison",
};

char *names2067 [] =
{
"object_comparison",
};

char *names2068 [] =
{
"object_comparison",
};

char *names2069 [] =
{
"object_comparison",
};

char *names2070 [] =
{
"object_comparison",
};

char *names2071 [] =
{
"object_comparison",
};

char *names2073 [] =
{
"internal_name",
};

char *names2074 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names2075 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names2076 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names2077 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names2078 [] =
{
"to_pointer",
};

char *names2079 [] =
{
"to_pointer",
};

char *names2080 [] =
{
"internal_name",
};

char *names2081 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names2082 [] =
{
"internal_name",
};

char *names2083 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names2084 [] =
{
"breakable_info",
"rt_type",
"position",
"type",
"callstack_depth",
"value",
};

char *names2085 [] =
{
"to_pointer",
};

char *names2086 [] =
{
"to_pointer",
};

char *names2087 [] =
{
"internal_name",
};

char *names2088 [] =
{
"to_pointer",
};

char *names2089 [] =
{
"to_pointer",
};

char *names2090 [] =
{
"internal_name",
};

char *names2091 [] =
{
"breakable_info",
"object",
"rt_type",
"offset",
"type",
"value",
};

char *names2092 [] =
{
"area_v2",
"add_actions",
"remove_actions",
"internal_add_actions",
"internal_remove_actions",
"object_comparison",
"in_operation",
"index",
};

char *names2093 [] =
{
"area_v2",
"add_actions",
"remove_actions",
"object_comparison",
"in_operation",
"index",
};

char *names2094 [] =
{
"area_v2",
"object_comparison",
"in_operation",
"index",
};

char *names2095 [] =
{
"item",
};

char *names2096 [] =
{
"breakable_info",
"object",
"index",
"type",
"value",
};

char *names2097 [] =
{
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_key",
"object_comparison",
"hash_table_version_64",
"has_default",
"is_case_insensitive",
"found_item",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"ht_deleted_item",
"count",
};

char *names2098 [] =
{
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names2099 [] =
{
"object_comparison",
};

char *names2100 [] =
{
"object_comparison",
};

char *names2101 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names2102 [] =
{
"active",
"after",
"before",
};

char *names2103 [] =
{
"right",
"item",
};

char *names2104 [] =
{
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names2105 [] =
{
"target",
"active",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names2106 [] =
{
"breakable_info",
"rt_type",
"position",
"type",
"value",
"callstack_depth",
};

char *names2107 [] =
{
"internal_name",
};

char *names2108 [] =
{
"internal_name",
};

char *names2109 [] =
{
"object_comparison",
"index",
};

char *names2110 [] =
{
"object_comparison",
};

char *names2111 [] =
{
"object_comparison",
};

char *names2112 [] =
{
"breakable_info",
"object",
"rt_type",
"offset",
"type",
"value",
};

char *names2113 [] =
{
"breakable_info",
"object",
"index",
"type",
"value",
};

char *names2114 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names2115 [] =
{
"to_pointer",
};

char *names2116 [] =
{
"to_pointer",
};

char *names2117 [] =
{
"internal_name",
};

char *names2118 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names2119 [] =
{
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names2120 [] =
{
"object_comparison",
};

char *names2121 [] =
{
"object_comparison",
};

char *names2122 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names2123 [] =
{
"active",
"after",
"before",
};

char *names2124 [] =
{
"right",
"item",
};

char *names2125 [] =
{
"item",
};

char *names2126 [] =
{
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names2127 [] =
{
"target",
"active",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names2128 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names2129 [] =
{
"internal_name",
};

char *names2130 [] =
{
"object_comparison",
};

char *names2131 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names2132 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names2133 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names2134 [] =
{
"internal_name",
};

char *names2135 [] =
{
"internal_name",
};

char *names2137 [] =
{
"target",
"is_reversed",
"version",
"target_index",
"first_index",
"last_index",
"step",
};

char *names2141 [] =
{
"area",
"object_comparison",
"lower",
"upper",
};

char *names2142 [] =
{
"object_comparison",
};

char *names2143 [] =
{
"object_comparison",
};

char *names2144 [] =
{
"object_comparison",
};

char *names2145 [] =
{
"object_comparison",
};

char *names2146 [] =
{
"object_comparison",
};

char *names2147 [] =
{
"object_comparison",
};

char *names2148 [] =
{
"object_comparison",
};

char *names2149 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names2150 [] =
{
"object_comparison",
};

char *names2151 [] =
{
"object_comparison",
};

char *names2152 [] =
{
"object_comparison",
};

char *names2153 [] =
{
"object_comparison",
};

char *names2154 [] =
{
"object_comparison",
};

char *names2155 [] =
{
"object_comparison",
};

char *names2156 [] =
{
"object_comparison",
};

char *names2157 [] =
{
"object_comparison",
};

char *names2158 [] =
{
"object_comparison",
};

char *names2159 [] =
{
"object_comparison",
};

char *names2160 [] =
{
"object_comparison",
};

char *names2161 [] =
{
"area",
};

char *names2162 [] =
{
"object_comparison",
};

char *names2163 [] =
{
"object_comparison",
};

char *names2165 [] =
{
"internal_name",
};

char *names2166 [] =
{
"to_pointer",
};

char *names2167 [] =
{
"to_pointer",
};

char *names2168 [] =
{
"internal_name",
};

char *names2169 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names2170 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names2171 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names2172 [] =
{
"internal_name",
};

char *names2173 [] =
{
"breakable_info",
"object",
"rt_type",
"offset",
"type",
"value",
};

char *names2174 [] =
{
"internal_name",
};

char *names2175 [] =
{
"target",
"is_reversed",
"version",
"iteration_position",
"first_index",
"last_index",
"step",
};

char *names2177 [] =
{
"content",
"keys",
"indexes_map",
"deleted_marks",
"object_comparison",
"hash_table_version_64",
"has_default",
"found_item",
"ht_deleted_item",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"count",
"ht_deleted_key",
};

char *names2178 [] =
{
"object_comparison",
};

char *names2180 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names2181 [] =
{
"breakable_info",
"object",
"rt_type",
"offset",
"type",
"value",
};

char *names2182 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names2183 [] =
{
"to_pointer",
};

char *names2184 [] =
{
"to_pointer",
};

char *names2185 [] =
{
"internal_name",
};

char *names2186 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names2187 [] =
{
"breakable_info",
"rt_type",
"position",
"type",
"callstack_depth",
"value",
};

char *names2188 [] =
{
"internal_name",
};

char *names2189 [] =
{
"breakable_info",
"object",
"rt_type",
"offset",
"type",
"value",
};

char *names2190 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names2191 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names2192 [] =
{
"breakable_info",
"rt_type",
"position",
"type",
"callstack_depth",
"value",
};

char *names2193 [] =
{
"to_pointer",
};

char *names2194 [] =
{
"to_pointer",
};

char *names2195 [] =
{
"internal_name",
};

char *names2196 [] =
{
"item",
};

char *names2197 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names2198 [] =
{
"breakable_info",
"object",
"rt_type",
"offset",
"type",
"value",
};

char *names2199 [] =
{
"breakable_info",
"object",
"index",
"type",
"value",
};

char *names2200 [] =
{
"internal_name",
};



#ifdef __cplusplus
}
#endif
