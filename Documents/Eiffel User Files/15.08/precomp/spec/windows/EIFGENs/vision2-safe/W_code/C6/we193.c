/*
 * Code for class WEL_CHOOSE_COLOR_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F193_4496(EIF_REFERENCE);
extern EIF_TYPED_VALUE F193_4497(EIF_REFERENCE);
extern EIF_TYPED_VALUE F193_4498(EIF_REFERENCE);
extern EIF_TYPED_VALUE F193_4499(EIF_REFERENCE);
extern EIF_TYPED_VALUE F193_4500(EIF_REFERENCE);
extern EIF_TYPED_VALUE F193_4501(EIF_REFERENCE);
extern EIF_TYPED_VALUE F193_4502(EIF_REFERENCE);
extern EIF_TYPED_VALUE F193_4503(EIF_REFERENCE);
extern EIF_TYPED_VALUE F193_4504(EIF_REFERENCE);
extern void EIF_Minit193(void);

#ifdef __cplusplus
}
#endif

#include "cdlg.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_CHOOSE_COLOR_CONSTANTS}.cc_rgbinit */
EIF_TYPED_VALUE F193_4496 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_rgbinit";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 192, Current, 0, 0, 4601);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(192, Current, 4601);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_RGBINIT;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CHOOSE_COLOR_CONSTANTS}.cc_fullopen */
EIF_TYPED_VALUE F193_4497 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_fullopen";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 192, Current, 0, 0, 4602);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(192, Current, 4602);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_FULLOPEN;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CHOOSE_COLOR_CONSTANTS}.cc_preventfullopen */
EIF_TYPED_VALUE F193_4498 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_preventfullopen";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 192, Current, 0, 0, 4603);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(192, Current, 4603);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_PREVENTFULLOPEN;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CHOOSE_COLOR_CONSTANTS}.cc_showhelp */
EIF_TYPED_VALUE F193_4499 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_showhelp";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 192, Current, 0, 0, 4604);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(192, Current, 4604);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_SHOWHELP;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CHOOSE_COLOR_CONSTANTS}.cc_enablehook */
EIF_TYPED_VALUE F193_4500 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_enablehook";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 192, Current, 0, 0, 4605);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(192, Current, 4605);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_ENABLEHOOK;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CHOOSE_COLOR_CONSTANTS}.cc_enabletemplate */
EIF_TYPED_VALUE F193_4501 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_enabletemplate";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 192, Current, 0, 0, 4606);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(192, Current, 4606);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_ENABLETEMPLATE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CHOOSE_COLOR_CONSTANTS}.cc_enabletemplatehandle */
EIF_TYPED_VALUE F193_4502 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_enabletemplatehandle";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 192, Current, 0, 0, 4607);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(192, Current, 4607);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_ENABLETEMPLATEHANDLE;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CHOOSE_COLOR_CONSTANTS}.cc_solidcolor */
EIF_TYPED_VALUE F193_4503 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_solidcolor";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 192, Current, 0, 0, 4608);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(192, Current, 4608);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_SOLIDCOLOR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_CHOOSE_COLOR_CONSTANTS}.cc_anycolor */
EIF_TYPED_VALUE F193_4504 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "cc_anycolor";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 192, Current, 0, 0, 4609);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(192, Current, 4609);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) CC_ANYCOLOR;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit193 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
