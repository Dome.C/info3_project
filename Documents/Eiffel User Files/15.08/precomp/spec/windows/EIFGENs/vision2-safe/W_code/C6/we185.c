/*
 * Code for class WEL_HDN_CONSTANTS
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F185_4432(EIF_REFERENCE);
extern EIF_TYPED_VALUE F185_4433(EIF_REFERENCE);
extern EIF_TYPED_VALUE F185_4434(EIF_REFERENCE);
extern EIF_TYPED_VALUE F185_4435(EIF_REFERENCE);
extern EIF_TYPED_VALUE F185_4436(EIF_REFERENCE);
extern EIF_TYPED_VALUE F185_4437(EIF_REFERENCE);
extern EIF_TYPED_VALUE F185_4438(EIF_REFERENCE);
extern EIF_TYPED_VALUE F185_4439(EIF_REFERENCE);
extern void EIF_Minit185(void);

#ifdef __cplusplus
}
#endif

#include "cctrl.h"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {WEL_HDN_CONSTANTS}.hdn_begin_track */
EIF_TYPED_VALUE F185_4432 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdn_begin_track";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 184, Current, 0, 0, 4545);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(184, Current, 4545);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDN_BEGINTRACK;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDN_CONSTANTS}.hdn_divider_dbl_click */
EIF_TYPED_VALUE F185_4433 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdn_divider_dbl_click";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 184, Current, 0, 0, 4538);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(184, Current, 4538);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDN_DIVIDERDBLCLICK;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDN_CONSTANTS}.hdn_end_track */
EIF_TYPED_VALUE F185_4434 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdn_end_track";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 184, Current, 0, 0, 4539);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(184, Current, 4539);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDN_ENDTRACK;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDN_CONSTANTS}.hdn_item_changed */
EIF_TYPED_VALUE F185_4435 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdn_item_changed";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 184, Current, 0, 0, 4540);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(184, Current, 4540);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDN_ITEMCHANGED;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDN_CONSTANTS}.hdn_item_changing */
EIF_TYPED_VALUE F185_4436 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdn_item_changing";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 184, Current, 0, 0, 4541);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(184, Current, 4541);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDN_ITEMCHANGING;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDN_CONSTANTS}.hdn_item_click */
EIF_TYPED_VALUE F185_4437 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdn_item_click";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 184, Current, 0, 0, 4542);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(184, Current, 4542);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDN_ITEMCLICK;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDN_CONSTANTS}.hdn_item_dbl_click */
EIF_TYPED_VALUE F185_4438 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdn_item_dbl_click";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 184, Current, 0, 0, 4543);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(184, Current, 4543);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDN_ITEMDBLCLICK;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

/* {WEL_HDN_CONSTANTS}.hdn_track */
EIF_TYPED_VALUE F185_4439 (EIF_REFERENCE Current)
{
	GTCX
	char *l_feature_name = "hdn_track";
	RTEX;
	EIF_INTEGER_32 Result = ((EIF_INTEGER_32) 0);
	
	RTSN;
	RTDA;
	RTLD;
	
	RTLI(1);
	RTLR(0,Current);
	RTLIU(1);
	RTLU (SK_INT32, &Result);
	RTLU (SK_REF, &Current);
	
	RTEAA(l_feature_name, 184, Current, 0, 0, 4544);
	RTSA(Dtype(Current));
	RTSC;
	RTME(Dtype(Current), 1);
	RTDBGEAA(184, Current, 4544);
	RTIV(Current, RTAL);
	Result = (EIF_INTEGER_32) HDN_TRACK;
	RTVI(Current, RTAL);
	RTRS;
	RTHOOK(1);
	RTDBGLE;
	RTMD(1);
	RTLE;
	RTLO(2);
	RTEE;
	{ EIF_TYPED_VALUE r; r.type = SK_INT32; r.it_i4 = Result; return r; }
}

void EIF_Minit185 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
