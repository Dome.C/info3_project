/*
 * Class SEQUENCE [INTEGER_32]
 */

#include "eif_macros.h"


#ifdef __cplusplus
extern "C" {
#endif

static const EIF_TYPE_INDEX egt_0_300 [] = {0xFF01,220,0xFFFF};
static const EIF_TYPE_INDEX egt_1_300 [] = {0xFF01,237,299,206,0xFFFF};
static const EIF_TYPE_INDEX egt_2_300 [] = {0xFF01,299,206,0xFFFF};
static const EIF_TYPE_INDEX egt_3_300 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_4_300 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_5_300 [] = {0xFF01,299,206,0xFFFF};
static const EIF_TYPE_INDEX egt_6_300 [] = {0xFF01,299,206,0xFFFF};
static const EIF_TYPE_INDEX egt_7_300 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_8_300 [] = {0xFF01,14,0xFFFF};
static const EIF_TYPE_INDEX egt_9_300 [] = {0xFF01,220,0xFFFF};
static const EIF_TYPE_INDEX egt_10_300 [] = {0xFF01,220,0xFFFF};
static const EIF_TYPE_INDEX egt_11_300 [] = {0xFF01,15,0xFFFF};
static const EIF_TYPE_INDEX egt_12_300 [] = {299,206,0xFFFF};
static const EIF_TYPE_INDEX egt_13_300 [] = {0xFF01,299,206,0xFFFF};
static const EIF_TYPE_INDEX egt_14_300 [] = {0xFF01,288,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_15_300 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_16_300 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_17_300 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_18_300 [] = {0xFFF8,1,0xFFFF};


static const struct desc_info desc_300[] = {
	{EIF_GENERIC(NULL), 0xFFFFFFFF, 0xFFFFFFFF},
	{EIF_GENERIC(egt_0_300), 0, 0xFFFFFFFF},
	{EIF_GENERIC(egt_1_300), 1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 3, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 4, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 5, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 6, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 7, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 8, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 9, 0xFFFFFFFF},
	{EIF_GENERIC(egt_2_300), 10, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 11, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 12, 0xFFFFFFFF},
	{EIF_GENERIC(egt_3_300), 13, 0xFFFFFFFF},
	{EIF_GENERIC(egt_4_300), 14, 0xFFFFFFFF},
	{EIF_GENERIC(egt_5_300), 15, 0xFFFFFFFF},
	{EIF_GENERIC(egt_6_300), 16, 0xFFFFFFFF},
	{EIF_GENERIC(egt_7_300), 17, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 18, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 19, 0xFFFFFFFF},
	{EIF_GENERIC(egt_8_300), 20, 0xFFFFFFFF},
	{EIF_GENERIC(egt_9_300), 21, 0xFFFFFFFF},
	{EIF_GENERIC(egt_10_300), 22, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 23, 0xFFFFFFFF},
	{EIF_GENERIC(egt_11_300), 24, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 25, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 26, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 27, 0xFFFFFFFF},
	{EIF_GENERIC(egt_12_300), 28, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01AF /*215*/), 29, 0xFFFFFFFF},
	{EIF_GENERIC(egt_13_300), 30, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2273, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2019, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 1850, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 1851, 0},
	{EIF_NON_GENERIC(0x017F /*191*/), 1852, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 1853, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 1854, 0xFFFFFFFF},
	{EIF_GENERIC(egt_14_300), 2284, 0xFFFFFFFF},
	{EIF_GENERIC(egt_15_300), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x019D /*206*/), 2274, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2459, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x019D /*206*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x019D /*206*/), 2276, 0xFFFFFFFF},
	{EIF_GENERIC(egt_16_300), 2277, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2278, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_17_300), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2458, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2280, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2281, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2282, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2283, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 1923, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2509, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 1924, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2510, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2511, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_18_300), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2512, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2513, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 1987, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x019D /*206*/), 2276, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2275, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2507, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2508, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x019D /*206*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), -1, 0xFFFFFFFF},
};
void Init300(void)
{
	IDSC(desc_300, 0, 299);
	IDSC(desc_300 + 1, 1, 299);
	IDSC(desc_300 + 32, 77, 299);
	IDSC(desc_300 + 41, 155, 299);
	IDSC(desc_300 + 50, 30, 299);
	IDSC(desc_300 + 57, 282, 299);
	IDSC(desc_300 + 66, 243, 299);
	IDSC(desc_300 + 72, 229, 299);
	IDSC(desc_300 + 73, 294, 299);
	IDSC(desc_300 + 76, 237, 299);
	IDSC(desc_300 + 78, 50, 299);
	IDSC(desc_300 + 79, 35, 299);
}


#ifdef __cplusplus
}
#endif
