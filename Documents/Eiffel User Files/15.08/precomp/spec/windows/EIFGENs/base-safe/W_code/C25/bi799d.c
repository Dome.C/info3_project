/*
 * Class BILINEAR [CHARACTER_32]
 */

#include "eif_macros.h"


#ifdef __cplusplus
extern "C" {
#endif

static const EIF_TYPE_INDEX egt_0_799 [] = {0xFF01,220,0xFFFF};
static const EIF_TYPE_INDEX egt_1_799 [] = {0xFF01,237,798,182,0xFFFF};
static const EIF_TYPE_INDEX egt_2_799 [] = {0xFF01,798,182,0xFFFF};
static const EIF_TYPE_INDEX egt_3_799 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_4_799 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_5_799 [] = {0xFF01,798,182,0xFFFF};
static const EIF_TYPE_INDEX egt_6_799 [] = {0xFF01,798,182,0xFFFF};
static const EIF_TYPE_INDEX egt_7_799 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_8_799 [] = {0xFF01,14,0xFFFF};
static const EIF_TYPE_INDEX egt_9_799 [] = {0xFF01,220,0xFFFF};
static const EIF_TYPE_INDEX egt_10_799 [] = {0xFF01,220,0xFFFF};
static const EIF_TYPE_INDEX egt_11_799 [] = {0xFF01,15,0xFFFF};
static const EIF_TYPE_INDEX egt_12_799 [] = {798,182,0xFFFF};
static const EIF_TYPE_INDEX egt_13_799 [] = {0xFF01,798,182,0xFFFF};
static const EIF_TYPE_INDEX egt_14_799 [] = {0xFF01,647,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_15_799 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_16_799 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_17_799 [] = {0xFFF8,1,0xFFFF};


static const struct desc_info desc_799[] = {
	{EIF_GENERIC(NULL), 2499, 0xFFFFFFFF},
	{EIF_GENERIC(egt_0_799), 0, 0xFFFFFFFF},
	{EIF_GENERIC(egt_1_799), 1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 3, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 4, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 5, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 6, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 7, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 8, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 9, 0xFFFFFFFF},
	{EIF_GENERIC(egt_2_799), 10, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 11, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 12, 0xFFFFFFFF},
	{EIF_GENERIC(egt_3_799), 13, 0xFFFFFFFF},
	{EIF_GENERIC(egt_4_799), 14, 0xFFFFFFFF},
	{EIF_GENERIC(egt_5_799), 15, 0xFFFFFFFF},
	{EIF_GENERIC(egt_6_799), 16, 0xFFFFFFFF},
	{EIF_GENERIC(egt_7_799), 17, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 18, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 19, 0xFFFFFFFF},
	{EIF_GENERIC(egt_8_799), 20, 0xFFFFFFFF},
	{EIF_GENERIC(egt_9_799), 21, 0xFFFFFFFF},
	{EIF_GENERIC(egt_10_799), 22, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 23, 0xFFFFFFFF},
	{EIF_GENERIC(egt_11_799), 24, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 25, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 26, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 27, 0xFFFFFFFF},
	{EIF_GENERIC(egt_12_799), 28, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01AF /*215*/), 29, 0xFFFFFFFF},
	{EIF_GENERIC(egt_13_799), 30, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2403, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 1900, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 1901, 0},
	{EIF_NON_GENERIC(0x017F /*191*/), 1902, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 1903, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 1904, 0xFFFFFFFF},
	{EIF_GENERIC(egt_14_799), 2414, 0xFFFFFFFF},
	{EIF_GENERIC(egt_15_799), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x019D /*206*/), 2404, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2498, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x019D /*206*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x019D /*206*/), 2406, 0xFFFFFFFF},
	{EIF_GENERIC(egt_16_799), 2407, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2408, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_17_799), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2497, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2410, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2411, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2412, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), 2413, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x017F /*191*/), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2405, 0xFFFFFFFF},
};
void Init799(void)
{
	IDSC(desc_799, 0, 798);
	IDSC(desc_799 + 1, 1, 798);
	IDSC(desc_799 + 32, 77, 798);
	IDSC(desc_799 + 41, 155, 798);
	IDSC(desc_799 + 50, 30, 798);
	IDSC(desc_799 + 57, 294, 798);
}


#ifdef __cplusplus
}
#endif
